Let SAPP_HOME refer to the directory where you unzipped the
contents of the SAPP installer zip. This is the SAPP
installation directory.

Trying out SAPP UI
-----------------------------
1. deploy SAPP_HOME/war/sample-webui*.war to a java app server
   currently only tested on tomcat
   Note: you must enable https
2. create a user on some oracle database
3. update SAPP_HOME/sapp-config.xml to reflect the database connection
   info including user name & password
4. update web.xml and edit the init-param sections to specify right
   values for your installation for:
   - ATTR_CONFIG_FILE: location of sapp-config.xml
   - ATTR_SAPP_HOME_DIR: this is the same as SAPP_HOME
   - ATTR_FILE_SAVE_LOCATION_ROOT: directory where application files
     will be stored
5. you may want to save a copy of sapp-config.xml and web.xml
   elsewhere so that they don't get over-written if you unzip a
   different version of SAPP later
6. re-start the webapp after modifying the config files
