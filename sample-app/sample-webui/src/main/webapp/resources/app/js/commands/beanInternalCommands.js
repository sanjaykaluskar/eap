'use strict';

define({
     fetchBeans: {
        name: "fetchBeans",
        execute: function(executeContext, beanFacade, fetchCriteria, fetchCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "rest/" + beanFacade,
                data: fetchCriteria ? fetchCriteria : {
                    fields: ["id"]
                },
                willHandleError: executeContext.willHandleError,
                callback: fetchCallback,
                isGetFullResponse: false
            });
        }
    },

    updateBeanInternal: {
        name: 'updateBeanInternal',
		execute: function(executeContext, beanFacade, beanId, criteria, bean, updateCallback) {
            var updateData = null;
            if (criteria) {
                updateData = criteria;
            } else {
                updateData = {
                    dataMap: {
                        id: beanId,
                        bean: bean
                    }
                }
            }
            return SUIService.executeRestCall({
                method: "PUT",
                URL: "rest/" + beanFacade,
                data: updateData,
                willHandleError: executeContext.willHandleError,
                callback: function(data, status) {
                    if (updateCallback) {
                        updateCallback(bean, data, status);
                    }
                },
                isGetFullResponse: false
            });
        }
    },

    deleteBeanInternal: {
        name: 'deleteBeanInternal',
        execute: function(executeContext, beanFacade, beanIdField, criteria, bean, deleteCallback) {
            var updateData = {};
            updateData.criteria = criteria;
            updateData.id = [];
            if (bean) {
                if (SUIService.isArray(bean)) {
                    for (var cc = 0; cc < bean.length; cc++) {
                        updateData.id.push(SUIService.getObjectProperty(bean[cc], beanIdField));
                    }
                } else {
                    updateData.id.push(SUIService.getObjectProperty(bean, beanIdField));
                }
            }
            return SUIService.executeRestCall({
                method: "DELETE",
                URL: "rest/" + beanFacade,
                data: updateData,
                willHandleError: executeContext.willHandleError,
                callback: function(data) {
                    if (deleteCallback) {
                        deleteCallback(bean, data);
                    }
                },
                isGetFullResponse: false
            });
        }
    }
});
