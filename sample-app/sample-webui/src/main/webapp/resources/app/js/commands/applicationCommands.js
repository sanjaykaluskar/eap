'use strict';

define({
    startLongRunningOp: {
        name: "startLongRunningOp",
        execute: function() {
            var $callerScope = arguments[0].currentScope;
            $callerScope.fetchStatus = "Loading " + $callerScope.waitTime + " ms long server call.";
            var qSvc = SUIService.getService("$q");

            var longRunDef = qSvc.defer();
            qSvc.when(SUIService.getService("SUIServiceHTTP").sendRequest(
                "GET",
                "rest/longRunningRequest/" + $callerScope.waitTime,
                null,
                null,
                null
            )).then(
                function(data, status) {
                    if (data.status === SUIService.HTTP_STATUS_CODE_OK && data.data.responseStatusCode === SUIService.STATUS_CODE_OK) {
                        longRunDef.resolve();
                        $callerScope.fetchStatus = "Successfully loaded " + $callerScope.waitTime + " ms long server call.";
                        return;
                    }
                    $callerScope.fetchStatus = "Error 1 loading " + $callerScope.waitTime + " ms long server call. >> " + data.data.responseCode;
                    longRunDef.reject(data);
                },
                function(data, status) {
                    $callerScope.fetchStatus = "Error 2 loading " + $callerScope.waitTime + " ms long server call. >> " + data.data.responseCode;
                    longRunDef.reject(data);
                }
            );
            return longRunDef.promise;
        },
        onSuccessCB: function() {}
    }
});
