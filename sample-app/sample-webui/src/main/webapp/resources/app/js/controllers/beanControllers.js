'use strict';

define({
    BeanWorkspaceCtrl: function($scope, SUIService) {

        /********************** references to common stuff ***********************/
        $scope.suiService = SUIService;

        $scope.suiUtil = SUIService.getService("ChartFactory");

        $scope.utilService = SUIService.getService("UtilService");

        $scope.formatPropertyName = function(property) {
            return $scope.utilService.formatPropertyName(property);
        }

        $scope.formatPropertyValue = function(property, bean, value) {
            return $scope.utilService.formatPropertyValue(property, bean, value);
        }

        $scope.userProfile = SUIService.getUserProfileData();

        /************************** set bean properties ***************************/
        $scope.beanProp = {
            beanName: "Bean",
            resourceType: "",
            facadeName: "beanfacade",
            descFieldName: "name",
            idFieldName: "pObjInfo.id",
            maxOpenBeans: 4,
            beanFetchFields: [],
            beanViewFields: [],
            beanViewSplitTop: false,
            beanViewTopLeft: null,
            beanViewExtras: [],
            beanEditForm: null,
            beanEditAllowed: function(bean) {
                return true;
            },
            beanDeleteAllowed: function(bean) {
                return true;
            },
            beanEditInitialize: null,
            beanEditCheck: null,
            beanViewCustomCommands: null,
            listName: "Beans",
            listViewCheck: function () {
                return SUIService.getUserProfileData().orgAdmin;
            },
            listViewActiveRefresh: function () {
                return false;
            },
            listViewNewBeanAllowed: true,
            listViewCustomCommands: null,
            listViewFields: [],
            listViewColumnDefs: [],
	    listViewPageSize: 10,
	    listViewPagination: false,
            customFilters: [],
            highlightUpdatedInList: false,
            extraTabs: null,
            postLoadAction: null
        }

        $.extend($scope.beanProp, $scope.beanProperties);

        /******************************* bean list ********************************/
        $scope.beanListViewId = $scope.beanProp.beanName + "ListView";
        $scope.beanListViewCommands = ["reloadBeanList"];
	if ($scope.beanProp.listViewPagination === true) {
            $scope.beanListViewCommands.unshift("nextPage");
            $scope.beanListViewCommands.unshift("prevPage");
	}
        if ($scope.beanProp.listViewNewBeanAllowed) {
            $scope.beanListViewCommands.unshift("newBean");
        };
        if (($scope.beanProp.listViewCustomCommands !== null) &&
            ($scope.beanProp.listViewCustomCommands.length > 0)) {
            $scope.beanListViewCommands = $scope.beanProp.listViewCustomCommands.concat($scope.beanListViewCommands);
        };

        $scope.beanListViewEnabled = $scope.beanProp.listViewCheck();
        $scope.filterOptions = {
            filterText: ''
        };

        $scope.BEAN_ROW_TEMPLATE =
            '<div style="height: 100%" ng-class="{\'highlight-row-updated\': isUpdated(row.getProperty(\'pObjInfo\')), \'highlight-row-new\': isNew(row.getProperty(\'pObjInfo\'))}">' +
              '<div ng-repeat="col in renderedColumns" ng-class="col\.colIndex()" class="ngCell ">' +
                '<div ng-cell></div>' +
              '</div>' +
            '</div>';
        $scope.beanList = [];
	$scope.beanListStartPos = 1;
        $scope.beanListGridOptions = {
            data: 'beanList',
            multiSelect: false,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: $scope.filterOptions,
            columnDefs: $scope.beanProp.listViewColumnDefs
        };

        if ($scope.beanProp.highlightUpdatedInList === true) {
            $scope.beanListGridOptions.rowTemplate = $scope.BEAN_ROW_TEMPLATE;
        }

        $scope.isUpdated = function(obj) {
            var lastViewedVsn = $scope.utilService.getLastViewedVsn(obj.id);
            return (obj.version > lastViewedVsn) && (lastViewedVsn >= 0);
        }

        $scope.isNew = function(obj) {
            var lastViewedVsn = $scope.utilService.getLastViewedVsn(obj.id);
            return (lastViewedVsn === -1);
        }

        $scope.beanListTabSelected = function() {
            $scope.beanListTabActive = true;
            $scope.reloadBeanList(false);
        };

        $scope.activateBeanListTab = function() {
            $scope.beanListTabActive = true;
        };

        $scope.onBeanListViewLoaded = function() {
            $scope.beanListViewLoaded = true;
            $scope.activateBeanListTab();
            $scope.reloadBeanList(false);
        };

        $scope.isLoadBeanListInProgress = false;
        $scope.setBeanList = function(beanData) {
            $scope.isLoadBeanListInProgress = false;
            $scope.beanList = beanData;
            $scope.$broadcast('beanListLoaded');
        };

        var activeRefresh = $scope.beanProp.listViewActiveRefresh();

        $scope.customFilterValues = {};

        if ($scope.beanProp.customFilters && ($scope.beanProp.customFilters !== null) && ($scope.beanProp.customFilters.length > 0)) {
            for (var i = 0; i < $scope.beanProp.customFilters.length; i++) {
                var cf = $scope.beanProp.customFilters[i];
                $scope.customFilterValues[cf.name] = cf.defaultValue;
            }
        };

        $scope.reloadBeanList = function(backgroundRefresh) {
            if ($scope.isLoadBeanListInProgress === true) {
                return;
            }
            if (!$scope.beanListViewLoaded || !$scope.beanListTabActive) {
                return;
            }
            $scope.isLoadBeanListInProgress = true;

            var executeContext = {
                scope: $scope,
                viewId: $scope.beanListViewId,
                dontShowBusy: activeRefresh && backgroundRefresh,
                prompt: "Loading " + $scope.beanProp.listName
            }

	    var fetchDetails = {
                fields: $scope.beanProp.listViewFields,
                filters: $scope.customFilterValues
	    }

	    if ($scope.beanProp.listViewPagination === true) {
		fetchDetails.startRow = $scope.beanListStartPos;
		fetchDetails.pageSize = $scope.beanProp.listViewPageSize;
	    }

            SUIService.executeCommand('fetchBeans', [$scope.beanProp.facadeName, fetchDetails, $scope.setBeanList], executeContext);
        };

	$scope.nextPage = function() {
	    $scope.beanListStartPos += $scope.beanProp.listViewPageSize;
	    $scope.reloadBeanList(false);
	}

	$scope.prevPage = function() {
	    $scope.beanListStartPos -= $scope.beanProp.listViewPageSize;
	    $scope.reloadBeanList(false);
	}

        var getIndexOfBean = function(beanList, beanId) {
            var idx = -1;
            for (var i = 0; i < beanList.length; i++) {
                if (SUIService.getObjectProperty(beanList[i], $scope.beanProp.idFieldName) === beanId) {
                    idx = i;
                    break;
                }
            }
            return idx;
        }

        /* subscribe to new beans */
        var newBeanListener = {
            onMessage: function(message) {
                var b = message.data;
                $scope.beanList.unshift(b);
                $scope.$apply();
            }
        };

        /* subscribe to changes in list */
        var beanChangeListener = {
            onMessage: function(message) {
                var b = message.data;
                var beanId = SUIService.getObjectProperty(b, $scope.beanProp.idFieldName);
                var beanListPos = getIndexOfBean($scope.beanList, beanId);

                /* do something if the bean was on our list */
                if (beanListPos !== -1) {
                    /* refresh the list */
                    angular.copy(b, $scope.beanList[beanListPos]);

                    /* reload the bean if it is open */
                    var openBeanPos = getIndexOfBean($scope.openBeans, beanId);
                    if (openBeanPos !== -1) {               
                        var wasActive = $scope.openBeans[openBeanPos].active;
                        angular.copy(b, $scope.openBeans[openBeanPos]);
                        $scope.openBeans[openBeanPos].active = wasActive;
                        /*
                         * if the bean tab was already active then update
                         * the last viewed version to be the latest, given
                         * that the user has already viewed the reloaded version.
                         */
                        if (wasActive === true) {
                            $scope.utilService.setLastViewedVsn(beanId, b.pObjInfo.version);
                        }

                        /* let any extra views reload */
                        $scope.$broadcast('BEAN_RELOADED', $scope.openBeans[openBeanPos]);
                    }
                    $scope.beanListTabActive = oldValue;
                }
            }
        };

        if (activeRefresh) {
            SUIService.getService("SUIServiceMessaging").subscribe(newBeanListener, "BO_CREATE");
            SUIService.getService("SUIServiceMessaging").subscribe(beanChangeListener, "BO_UPDATE");
        }

	$scope.$on('newBean', function(event, data) {
	    $scope.reloadBeanList(false);
	});

        /******************************* bean view ********************************/
        $scope.openBeans = [];
        $scope.openBeansMaxSize = $scope.beanProp.maxOpenBeans;

        $scope.beanTabSelected = function(bean) {
            $scope.utilService.setLastViewedVsn(SUIService.getObjectProperty(bean, $scope.beanProp.idFieldName), bean.pObjInfo.version);
            $scope.$broadcast('BEAN_TAB_SELECTED', bean);
        }

        $scope.loadBeanById = function(beanId, cbk) {
            var executeContext = {
                scope: $scope,
                viewId: $scope.beanListViewId,
                prompt: "Loading " + $scope.beanProp.beanName + " - " + beanId + "..."
            }
            SUIService.executeCommand('fetchBeans', [$scope.beanProp.facadeName, {
                    id: beanId,
                    fields: $scope.beanProp.beanFetchFields
                },
                function(beanData) {
                    cbk(beanData);
                }               
            ], executeContext);
        }

        $scope.openBean = function(bean) {

            var executeContext = {
                scope: $scope,
                viewId: $scope.beanListViewId,
                prompt: "Loading " + $scope.beanProp.beanName + " - "
                    + bean[$scope.beanProp.descFieldName] + "..."
            }
            SUIService.executeCommand('fetchBeans', [$scope.beanProp.facadeName, {
                    id: SUIService.getObjectProperty(bean, $scope.beanProp.idFieldName),
                    fields: $scope.beanProp.beanFetchFields
                },
                $scope.setBean
            ], executeContext);
        };

        $scope.openBeanById = function(beanId) {
            $scope.loadBeanById(beanId, function(beanData) {
                $scope.setBean(beanData);
            });
        };

        $scope.setBean = function(beanData) {
            if (beanData && beanData.length == 1 && beanData[0]) {
                var bean = beanData[0];

                /* remove the bean from the array if it exists */
                for (var clIndex = 0; clIndex < $scope.openBeans.length; clIndex++) {
                    if (SUIService.getObjectProperty($scope.openBeans[clIndex], $scope.beanProp.idFieldName) === SUIService.getObjectProperty(bean, $scope.beanProp.idFieldName)) {
                        $scope.openBeans.splice(clIndex, 1);
                        break;
                    }
                }

                /* add it to the end */
                bean.objectType = $scope.beanProp.resourceType;
                $scope.openBeans.push(angular.copy(bean));
                $scope.openBeans[$scope.openBeans.length - 1].active = true;
                $scope.bean = bean;

                /* remove least recently accessed bean if size exceeded */
                if ($scope.openBeans.length > $scope.openBeansMaxSize)
                    $scope.openBeans.shift();

                $scope.beanListTabActive = false;

                $scope.utilService.setLastViewedVsn(SUIService.getObjectProperty(bean, $scope.beanProp.idFieldName), bean.pObjInfo.version);
            } else {
                //TODO::
            }
        };

        $scope.closeBean = function(bean) {
            /* remove the bean from the list of open beans if it exists */
            var idx = getIndexOfBean($scope.openBeans, SUIService.getObjectProperty(bean, $scope.beanProp.idFieldName));
            if (idx !== -1) {
                $scope.openBeans.splice(idx, 1);
                $scope.$broadcast('BEAN_TAB_CLOSED', bean);
            }
        };

        $scope.beanViewLoaded = function(bean) {
            $scope.$broadcast('BEAN_TAB_LOADED', bean);
        };

        $scope.beanViewCommands = [];

        if ($scope.beanProp.beanEditAllowed && $scope.beanProp.beanEditAllowed()) {
            $scope.beanViewCommands.push("editBean");
        };

        if ($scope.beanProp.beanDeleteAllowed && $scope.beanProp.beanDeleteAllowed()) {
            $scope.beanViewCommands.push("deleteBean");
        };

        if (($scope.beanProp.beanViewCustomCommands !== null) &&
            ($scope.beanProp.beanViewCustomCommands.length > 0)) {
            $scope.beanViewCommands = $scope.beanProp.beanViewCustomCommands.concat($scope.beanViewCommands);
        };

        $scope.showEditBean = function(bean, isNew, viewId) {
            var modalInstance = SUIService.getService("$modal").open({
                templateUrl: 'resources/app/templates/html/dialogs/editBean.html',
                controller: EditBeanModalInstanceCtrl,
                scope: $scope,
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    bean: function() {
                        return bean;
                    }
                }
            });

            modalInstance.result.then(function() {
                if (isNew) {
                    if (viewId === $scope.beanListViewId) {
                        $scope.reloadBeanList(false);
                    } else {
                        $scope.openBean(bean);
                    }
                } else {
                    $scope.openBean(bean);
                }
            }, function() {
                // TODO: close handler
            });
        };

        $scope.deleteBean = function(bean) {
            SUIService.showDialogMessage({
                isModal: true,
                dialogTitle: "Delete Confirmation",
                dialogMessage: "Are you sure you want to delete '" +
                    bean[$scope.beanProp.descFieldName] + "' ?",
                dialogType: SUIService.DIALOG_TYPE_CONFIRM,
                callbackFn: function(dialogResult) {
                    if (dialogResult === SUIService.DIALOG_OK) {
                        var executeContext = {
                            scope: $scope,
                            viewId: 'editBeanView' + SUIService.getObjectProperty(bean, $scope.beanProp.idFieldName),
                            prompt: "Deleting " + $scope.beanProp.beanName + ": " +
                                bean[$scope.beanProp.descFieldName] +
                                "..."
                        }
                        SUIService.executeCommand('deleteBeanInternal', [
                            $scope.beanProp.facadeName, $scope.beanProp.idFieldName, null, bean,
                            function() {
                                $scope.closeBean(bean);
                            }
                        ], executeContext);
                    }
                }
            });
        };

        var EditBeanModalInstanceCtrl = function($scope,
            $modalInstance, bean) {

            $scope.fieldGroup = {
                open: true
            }

            $scope.masterBean = angular.copy(bean);
            $scope.bean = angular.copy($scope.masterBean);

            if ($scope.beanProp.beanEditInitialize)
                $scope.beanProp.beanEditInitialize($scope, $scope.bean);

            $scope.ok = function() {
                var errorDialog = null;
                var beanId = SUIService.getObjectProperty($scope.bean, $scope.beanProp.idFieldName);

                if ($scope.beanProp.beanEditCheck)
                {
                    errorDialog = $scope.beanProp.beanEditCheck($scope, $scope.bean,
                                                                $scope.masterBean);
                }

                if (errorDialog === null)
                {
                    var executeContext = {
                        scope: $scope,
                        viewId: 'editBeanView' + beanId,
                        prompt: "Saving " + $scope.beanProp.beanName + ": " +
			    SUIService.getObjectProperty($scope.bean, $scope.beanProp.descFieldName) +
                            "...",
                        willHandleError: true
                    }

                    SUIService.executeCommand('updateBeanInternal', [
			$scope.beanProp.facadeName, beanId, null, $scope.bean,
                        function(updatedBean, responseData, responseStatus) {
                            if (responseStatus === SUIService.STATUS_CODE_OK) {
                                $modalInstance.close();
                            }
                        }
                    ], executeContext);
                }
                else
                {
                    SUIService.showDialogMessage(errorDialog);
                }
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.reset = function() {
                $scope.bean = angular.copy($scope.masterBean);
                if ($scope.beanProp.beanEditInitialize)
                    $scope.beanProp.beanEditInitialize($scope, $scope.bean);
            };
        };

        if ($scope.beanProp.postLoadAction)
            $scope.beanProp.postLoadAction($scope);
    }
});
