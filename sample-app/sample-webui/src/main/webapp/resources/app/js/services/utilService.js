define({
    UtilService: function(SUIService) {
        console.log("Loading UtilService");

        function strip(html) {  
            var tmp = document.createElement("DIV"); 
            tmp.innerHTML = html; 
            var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;   
            return tmp.innerHTML .replace(urlRegex, function(url) {     
                return '\n' + url 
            });
        };

        // get the values of an enum and store it in a variable
        this.getEnum = function(varName, enumName)
        {
            var that = this;
            var executeContext = {
                prompt: "Populating " + enumName + " values..."
            }

            SUIService.executeCommand('listEnum', [
                enumName,
                function(responseData, responseStatus) {
                    if (responseStatus === SUIService.STATUS_CODE_OK) {
                        that[varName] = [];
                        for (var i = 0; i < responseData.length; i++) {
                            that[varName].push({name: responseData[i], value: responseData[i]});
                        }
                    }
                }
            ], executeContext);
        };

        this.getEnum("operationTypes", "com.dombigroup.eap.common.datamodels.OperationType");
        this.getEnum("prefixTypes", "com.dombigroup.eap.common.datamodels.PrefixType");
        this.getEnum("monthNames", "com.dombigroup.eap.common.datamodels.Month");
        this.getEnum("phoneNumberTypes", "com.dombigroup.eap.common.datamodels.PhoneType");
        this.getEnum("phoneLocationTypes", "com.dombigroup.eap.common.datamodels.PhoneLocation");

        this.formatPropertyName = function(property) {
            var parts = property.split(".");
            if (parts.length > 1) {
                return this.formatPropertyName(parts.pop());
            }

            switch (property) {
                case "id":
                    return "Id";
                case "pObjInfo.id":
                    return "Id";
                case "parentId":
                    return "Parent Id";
                case "name":
                    return "Name";
                case "firstName":
                    return "First Name";
                case "middleName":
                    return "Middle Name";
                case "lastName":
                    return "Last Name";
                case "userName":
                    return "User Name";
                case "password":
                    return "Password";
                case "address":
                    return "Address";
                case "line1":
                    return "Line1";
                case "line2":
                    return "Line2";
                case "line3":
                    return "Line3";
                case "city":
                    return "City";
                case "state":
                    return "State";
                case "country":
                    return "Country";
                case "zipcode":
                    return "Zipcode";
                case "prefix":
                    return "Prefix";
                case "email":
                    return "Email Address";
                case "type":
                    return "Type";
                case "dbType":
                    return "Database Type";
                case "desc":
                    return "Description";
                case "title":
                    return "Title";
                case "notes":
                    return "Notes";
                default:
                    return property;
            }
        };

        this.formatPropertyValue = function(property, bean, value) {
            switch (property) {
                case "address":
                    return this.formatAddress(value);
                case "lastRefreshTime":
                    return this.formatDayTime(value);
                default:
                    return value;
            }
        };

        this.formatAddress = function(address) {
            if (address) {
                return (address.line1 ? address.line1 + ", " : "") +
                    (address.line2 ? address.line2 + ", " : "") +
                    (address.line3 ? address.line3 + ", " : "") +
                    (address.city ? address.city + ", " : "") +
                    (address.state ? address.state + ", " : "") +
                    (address.country ? address.country + ", " : "") +
                    (address.zipcode ? address.zipcode : "");
            } else {
                return "";
            }
        };

        this.formatPhoneNumbers = function(phoneNumbers) {
            if (phoneNumbers && SUIService.isArray(phoneNumbers)) {
                var phoneNumbersString = "";
                for (var pn = 0; pn < phoneNumbers.length; pn++) {
                    if (pn > 0) {
                        phoneNumbersString += ", ";
                    }
                    phoneNumbersString += this.formatPhoneNumber(phoneNumbers[pn]);
                }
                return phoneNumbersString;
            } else {
                return "";
            }
        };

        this.formatPhoneNumber = function(phoneNumber) {
            if (phoneNumber) {
                var phoneNumberString = "";
                if (!SUIService.isNull(phoneNumber.countryCode)) {
                    phoneNumberString += phoneNumber.countryCode;
                }

                if (phoneNumber.areaCode) {
                    if (phoneNumberString.trim() !== "") {
                        phoneNumberString += "-";
                    }
                    phoneNumberString += phoneNumber.areaCode;
                }

                if (phoneNumber.number) {
                    if (phoneNumberString.trim() !== "") {
                        phoneNumberString += "-";
                    }
                    phoneNumberString += phoneNumber.number;
                }

                if (phoneNumber.extension) {
                    if (phoneNumberString.trim() !== "") {
                        phoneNumberString += " ";
                    }
                    phoneNumberString += "Ext. " + phoneNumber.extension;
                }

                if (!SUIService.isNull(phoneNumber.numberType) || !SUIService.isNull(phoneNumber.location)) {
                    phoneNumberString += " (";
                    var phoneTypeLocStr = "";
                    if (!SUIService.isNull(phoneNumber.numberType)) {
                        phoneTypeLocStr += phoneNumber.numberType;
                    }

                    if (!SUIService.isNull(phoneNumber.location)) {
                        if (phoneTypeLocStr.trim() !== "") {
                            phoneTypeLocStr += ", ";
                        }
                        phoneTypeLocStr += phoneNumber.location;
                    }
                    phoneNumberString += phoneTypeLocStr.trim();
                    phoneNumberString += ")";
                }
                return phoneNumberString;
            } else {
                return "";
            }
        };

        this.getMonthFromName = function(mon) {
            var d = Date.parse(mon + " 1, 2012");
            if (!isNaN(d)){
                return (new Date(d).getMonth()) + 1;
            }
            return -1;
        };

        this.convertDateTimeToLocal = function(date) {
            if (!date || (date === null))
                return null;

            var d = new Date();
            var offsetInMin = d.getTimezoneOffset();
            var month = this.getMonthFromName(date.month);
            var inputDate = new Date(date.year, month-1, date.day, date.hour, date.min);
            var outputDate = new Date(inputDate.getTime() - offsetInMin * 60000);

            return {
                year: outputDate.getFullYear(),
                month: this.monthNames[outputDate.getMonth()].name,
                day: outputDate.getDate(),
                hour: outputDate.getHours(),
                min: outputDate.getMinutes()
            };
        };

        this.formatDayTime = function(date) 
        {
            var localDate = this.convertDateTimeToLocal(date);

            if (localDate)
            {
                var hour = localDate.hour;
                if (hour < 10)
                {
                    hour = "0" + hour;
                }
                var min = localDate.min;
                if (min < 10)
                {
                    min = "0" + min;
                }
                return localDate.day + "-" + localDate.month.substring(0, Math.min(localDate.month.length, 3)) + "-" + localDate.year + " " + hour + ":" + min;
            }
            else
            {
                return "";
            }
        };

        this.dayTimeComparator = function(dt1, dt2) {
            if ((dt1 === null) && (dt2 === null)) {
                return 0;
            }
            
            if (dt1 === null) {
                return -1;
            }

            if (dt2 === null) {
                return 1;
            }

            var dt1month = this.getMonthFromName(dt1.month);
            var dt2month = this.getMonthFromName(dt2.month);

            if (dt1.year !== dt2.year) {
                return dt1.year - dt2.year;
            } else if (dt1month !== dt2month) {
                return dt1month - dt2month;
            } else if (dt1.day !== dt2.day) {
                return dt1.day - dt2.day;
            } else if (dt1.hour !== dt2.hour) {
                return dt1.hour - dt2.hour;
            } else if (dt1.min !== dt2.min) {
                return dt1.min - dt2.min;
            } else {
                return 0;
            }
        };

        this.renderHTML = function(text) {
            var rawText = strip(text);
            var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;   

            return rawText.replace(urlRegex, function(url) {   

                if ((url.indexOf(".jpg") > 0) || (url.indexOf(".png") > 0) || (url.indexOf(".gif") > 0)) {
                    return '<img src="' + url + '">' + '<br/>'
                } else {
                    return '<a href="' + url + '">' + url + '</a>' + '<br/>'
                }
            });
        };

        this.saveState = function(key, value) {
            window.localStorage.setItem(key, JSON.stringify(value));
        };

        this.loadState = function(key) {
            var value = window.localStorage.getItem(key);
            return value && JSON.parse(value);
        };

        this.setLastViewedVsn = function(id, vsn) {
            var viewTimes = this.loadState('ViewTimes');

            if (viewTimes === null) {
                viewTimes = [];
            }

            var vt = viewTimes[id];
            viewTimes[id] = vsn;

            this.saveState('ViewTimes', viewTimes);
            //console.log('setLastViewedVsn: id - ' + id + ', vsn - ' + vsn);
        };

        this.getLastViewedVsn = function(id) {
            var viewTimes = this.loadState('ViewTimes');
            var vsn = -1;

            if (viewTimes &&
                (viewTimes !== null) &&
                (viewTimes.length > 0) &&
                (viewTimes[id] !== undefined) &&
                (viewTimes[id] !== null)) {
                vsn = viewTimes[id];
            }
            //console.log('getLastViewedVsn: id - ' + id + ', vsn - ' + vsn);
            return vsn;
        };

	this.getBeanSelectionList = function($scope, facadeName, nameField) {
            var beans = [];

            var executeContext = {
                scope: $scope,
                prompt: "Getting bean list...",
                willHandleError: true
            }

            SUIService.executeCommand('fetchBeans', [
                facadeName, {fields: [nameField]},
                function(responseData, responseStatus) {
                    if (responseStatus === SUIService.STATUS_CODE_OK) {
                        for (var clIndex = 0; clIndex < responseData.length; clIndex++) {
                            var bean = {
                                name: responseData[clIndex][nameField],
                                value: responseData[clIndex].pObjInfo.id
                            };
                            beans.push(bean);
                        }
                    }
                }], executeContext);
            return beans;
	};
    }
});
