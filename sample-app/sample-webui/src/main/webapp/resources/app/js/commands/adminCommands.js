'use strict';

define({

    fetchGrantableRoles: {
        name: 'fetchGrantableRoles',
        execute: function(executeContext, userId, fetchCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/userRoles.getGrantableRoles",
                willHandleError: executeContext.willHandleError,
                callback: function(data, status) {
                    if (fetchCallback) {
                        fetchCallback(data, status);
                    }
                },
                isGetFullResponse: false
            });
        }
    },

    reloadPolicyList: {
        name: "reloadPolicyList",
        iconClass: "glyphicon glyphicon-refresh",
        title: "Refresh",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.reloadPolicyList(arguments);
        }
    },

    fetchPolicies: {
        name: "fetchPolicies",
        execute: function(executeContext, fetchCriteria, fetchCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/org.getPolicyList",
                data: fetchCriteria ? fetchCriteria : {
                    fields: ["name", "desc", "controllable", "enabled"]
                },
                willHandleError: executeContext.willHandleError,
                callback: fetchCallback,
                isGetFullResponse: false
            });
        }
    },

    setPolicyEnabled: {
        name: 'setPolicyEnabled',
        execute: function(executeContext, name, value, updateCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/org.setPolicyEnabled",
                data: {policyName: name, enabledValue: value},
                callback: function(data, status) {
                    if (updateCallback) {
                        updateCallback(status);
                    }
                }
            });
        }
    },

    /* Object Priv Commands */

    showEditPrivs: {
        name: "showEditPrivs",
        iconClass: "glyphicon glyphicon-pencil",
        title: "Privileges",
        execute: function(executeContext, obj) {
            var $callerScope = executeContext.currentScope;
            var adminSvc = SUIService.getService("AdminService");
            adminSvc.showEditPrivs(obj.pObjInfo, obj.name, obj.objectType, $callerScope);
        }
    },

    getObjectPrivList: {
        name: "getObjectPrivList",
        execute: function(executeContext, objectId, fetchCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/objectPriv.getPrivList",
                data: {
                    id: objectId,
                },
                willHandleError: executeContext.willHandleError,
                callback: fetchCallback,
                isGetFullResponse: false
            });
        }
    },

    getGranteeList: {
        name: "getGranteeList",
        execute: function(executeContext, objectId, fetchCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/objectPriv.getGranteeList",
                data: {
                },
                willHandleError: executeContext.willHandleError,
                callback: fetchCallback,
                isGetFullResponse: false
            });
        }
    },

    modifyObjectPrivs: {
        name: "modifyObjectPrivs",
        execute: function(executeContext, objectPrivs, callback) {

            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/objectPriv.modifyObjectPrivs",
                data: {
                    objectPrivs: objectPrivs,
                },
                willHandleError: executeContext.willHandleError,
                callback: callback,
                isGetFullResponse: false
            });
        }
    },

});
