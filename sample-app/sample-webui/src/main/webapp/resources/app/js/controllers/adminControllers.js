'use strict';

define({
    OrgWorkspaceCtrl: function($scope, SUIService) {
        $scope.beanProperties = {
            beanName: "Org",
            resourceType: "COMPANY",
            facadeName: "org",
            descFieldName: "name",
            maxOpenBeans: 4,
            beanFetchFields: ["name", "address", "phoneNumbers"],
            beanViewFields: ["name", "pObjInfo.id"],
            beanViewExtras: [{
		contextField: "address",
                template: "resources/app/templates/html/widgets/addressView.html",
		}, {
                template: "resources/app/templates/html/widgets/phoneNumbersView.html",
	    }],
            beanEditForm: "resources/app/templates/html/forms/orgForm.html",
            beanEditInitialize: null,
            beanEditCheck: null,
            listName: "Orgs",
            listViewFields: ["name"],
            listViewCheck: function () {
                var up = SUIService.getUserProfileData();
                return (up.siteAdmin);
            },
            listViewColumnDefs: [{
                field: 'pObjInfo.id',
                displayName: 'Org Id',
                width: '10%',
                cellTemplate: '<div class="ngCellText"><a ng-click="openBean(row.entity);">{{row.getProperty(col.field)}}</a></div>'
            }, {
                field: 'name',
                displayName: 'Namespace',
                width: '90%',
            }],
            extraTabs: [{
                tabHeading: "Users",
                tabEnabled: function() {
                    var up = SUIService.getUserProfileData();
                    return (up.orgAdmin);
                },
                tabViewTemplate: "resources/app/templates/html/views/userList.html"
	    },{
                tabHeading: "Security",
                tabEnabled: function() {
                    var up = SUIService.getUserProfileData();
                    return (up.orgAdmin);
                },
                tabViewTemplate: "resources/app/templates/html/forms/securityPolicyForm.html"
            }],
            postLoadAction: function ($scope) {
                if (SUIService.getUserProfileData().orgAdmin)
                    $scope.openBeanById(SUIService.getUserProfileData().userNameSpace.namespaceId);
            }
        };
    },

    PolicyWorkspaceCtrl: function($scope, SUIService) {
        /* Policy UI */
        $scope.policyListEnabled = (SUIService.getUserProfileData().orgAdmin === true);
        $scope.policyList = [];
        $scope.policyListViewId = "policyListView";
        $scope.policyListViewCommands = ["reloadPolicyList"];

        $scope.descCellTemplate = '<div class="ngCellText" title="{{row.getProperty(col.field)}}">{{row.getProperty(col.field)}}</div>';
        $scope.checkBoxCellViewTemplate = '<div class="ngCellText" style="text-align:center">{{row.getProperty(col.field) ? "&#10003;":"&#10007;"}}</div>';
        $scope.enabledCellEditTemplate =  '<div class="padding-2 text-center"><button class="btn btn-primary btn-xs regular-font-strict" ng-disabled="!canEnable(row.entity)" ng-click="setPolicyEnabled(row.entity, true)">Enable</button>&nbsp;<button class="btn btn-primary btn-xs regular-font-strict" ng-disabled="!canDisable(row.entity)" ng-click="setPolicyEnabled(row.entity, false)">Disable</button></div>';
        $scope.policyListGridOptions = {
            data: 'policyList',
            multiSelect: false,
            plugins: [new ngGridFlexibleHeightPlugin()],
            columnDefs: [{
                field: 'name',
                displayName: 'Name',
                width: '20%',
            }, {
                field: 'desc',
                displayName: 'Description',
                cellTemplate: $scope.descCellTemplate,
                width: '45%',
            }, {
                field: 'controllable',
                displayName: 'Org Level Control',
                width: '13%',
                cellTemplate: $scope.checkBoxCellViewTemplate
            }, {
                field: 'enabled',
                displayName: 'Enabled',
                width: '7%',
                cellTemplate: $scope.checkBoxCellViewTemplate
            }, {
                field: 'enabled',
                displayName: 'Change',
                width: '15%',
                cellTemplate: $scope.enabledCellEditTemplate,
            }]
        };

        $scope.activatePolicyListTab = function() {
            $scope.policyListTabActive = true;
        };

        $scope.policyListTabSelected = function() {
            $scope.policyListTabActive = true;
            $scope.reloadPolicyList();
        };

        $scope.reloadPolicyList = function() {
            if ($scope.isLoadPolicyListInProgress) {
                return;
            }
            if (!$scope.policyListViewLoaded || !$scope.policyListTabActive) {
                return;
            }
            $scope.isLoadPolicyListInProgress = true;
            var executeContext = {
                scope: $scope,
                viewId: $scope.policyListViewId,
                prompt: "Loading security policies..."
            }
            SUIService.executeCommand('fetchPolicies', [null, $scope.setPolicyList],
                executeContext);
        };

        $scope.setPolicyList = function(data) {
            $scope.isLoadPolicyListInProgress = false;
            $scope.policyList = data;
        };

        $scope.onPolicyListViewLoaded = function() {
            $scope.policyListViewLoaded = true;
            $scope.activatePolicyListTab();
            $scope.reloadPolicyList();
        };

        $scope.canEnable = function(policy)
        {
            if ((policy.controllable === true) && (policy.enabled === false))
                return true;

            return false;
        }

        $scope.canDisable = function(policy)
        {
            if ((policy.controllable === true) && (policy.enabled === true))
                return true;

            return false;
        }

        $scope.setPolicyEnabled = function(policy, enabled)
        {
            var executeContext = {
                scope: $scope,
                viewId: $scope.policyViewId,
                prompt: "Updating setting for - " + policy.name + "..."
            }

            SUIService.executeCommand('setPolicyEnabled', [policy.name, enabled,
                function(status) {
                    if (status === SUIService.STATUS_CODE_OK) {
                        policy.enabled = enabled;
                    }
                }
            ], executeContext);
        };
    },

    UserWorkspaceCtrl: function($scope, SUIService) {

        var getMissingRoles = function(newList, oldList) {
            var missingRoles = [];

            if (newList && (newList !== null) && (newList.length > 0)) {
            	for (var roleIdx = 0; roleIdx < newList.length; roleIdx++)
            	{
            	    var roleName = newList[roleIdx].roleName;
            	    var roleFound = false;
            	    if (oldList && (oldList !== null) && (oldList.length > 0)) {
                        roleFound = oldList.find(function(element, index, arr) {
				return element.roleName === roleName;
			    });
	    	    }

            	    if (!roleFound) {
            	        missingRoles.push(roleName);
	    	    }
	    	}
	    }
            return missingRoles;
	}

        $scope.beanProperties = {
            beanName: "User",
            resourceType: "USER",
            facadeName: "user",
            descFieldName: "user.userName",
            idFieldName: "user.pObjInfo.id",
            maxOpenBeans: 4,
            beanFetchFields: ["user", "grantedRoles", "grantedPrivs"],
            beanViewFields: ['user.userName', 'user.pObjInfo.id', 'user.prefix', 'user.firstName', 'user.middleName', 'user.lastName', 'user.email'],
            beanViewExtras: [{
		    contextField: "user",
		    template: "resources/app/templates/html/widgets/phoneNumbersView.html"
		},{
		    template: "resources/app/templates/html/widgets/userRolesView.html",
		}, {
		    template: "resources/app/templates/html/widgets/userGrantsView.html",
		}
            ],
            beanEditForm: "resources/app/templates/html/forms/userForm.html",
            beanEditInitialize: function ($scope, bean) {
	    },
            beanEditCheck: function ($scope, updatedBean, origBean) {
                /* update through $scope to add new fields */
                $scope.bean.addedRoles = getMissingRoles(updatedBean.grantedRoles, origBean.grantedRoles);
                $scope.bean.removedRoles = getMissingRoles(origBean.grantedRoles, updatedBean.grantedRoles);
		return null;
	    },
            listName: "Users",
            listViewFields: ["user"],
            listViewCheck: function () {
                var up = SUIService.getUserProfileData();
                return (up.orgAdmin);
            },
            listViewColumnDefs: [{
                field: 'user.pObjInfo.id',
                displayName: 'Id',
                width: '10%',
                cellTemplate: '<div class="ngCellText">{{row.getProperty(col.field)}}</div>'
            }, {
                field: 'user.userName',
                displayName: 'Username',
                width: '20%',
                cellTemplate: '<div class="ngCellText"><a ng-click="openBean(row.entity);">{{row.getProperty(col.field)}}</a></div>'
            }, {
                field: 'user.prefix',
                displayName: 'Prefix',
                width: '10%'
            }, {
                field: 'user.firstName',
                displayName: 'Firstname',
                width: '20%'
            }, {
                field: 'user.lastName',
                displayName: 'Lastname',
                width: '20%'
            }, {
                field: 'user.email',
                displayName: 'Email',
                width: '20%'
            }]
	};
    }
});
