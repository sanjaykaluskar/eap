'use strict';

define({
    ItemListCtrl: function($scope, SUIService) {

        $scope.utilService = SUIService.getService("UtilService");

        // default properties
        $scope.itemListProp = {
            listName: "Items",
            getItemList: null,
            createNewItem:  function($scope, bean) {
                return {};
            },
            open: false,
            allowAdd: true,
            allowRemove: true,
            columnDefs: null,
            compareItems: function(item1, item2) {
                return (item1 == item2);
            },
            updateItemList: function(updatedList, ctx) {
            },
        }

        $.extend(true, $scope.itemListProp, $scope.itemListProperties);

        $scope.itemList = $scope.itemListProp.getItemList($scope, $scope.contextObject);
        $scope.itemListSelections = [];
        $scope.itemListGridOptions = {
            data: 'itemList',
            multiSelect: true,
            enableCellSelection: true,
            enableCellEdit: true,
            showSelectionCheckbox: true,
            selectWithCheckboxOnly: true,
            enableCellEditOnFocus: true,
            selectedItems: $scope.itemListSelections,
            columnDefs: $scope.itemListProp.columnDefs
        };

        $scope.gridOptsReadOnly = angular.copy($scope.itemListGridOptions);
        $scope.gridOptsReadOnly.enableCellSelection = false;
        $scope.gridOptsReadOnly.enableCellEdit = false;
        $scope.gridOptsReadOnly.showSelectionCheckbox = false;
        $scope.gridOptsReadOnly.multiSelect = false;
        $scope.gridOptsReadOnly.enableCellEditOnFocus = false;

        $scope.getItemListGridOptions = function(isReadOnly) {
            if (isReadOnly) {
                return $scope.gridOptsReadOnly;
            } else {
                return $scope.itemListGridOptions;
            }
        };

        $scope.addNewItem = function() {
            if (!$scope.itemList || ($scope.itemList === null)) {
                $scope.itemList = [];
            }
            $scope.itemList.push($scope.itemListProp.createNewItem($scope, $scope.contextObject));
            $scope.itemListProp.updateItemList($scope.itemList, $scope.contextObject);
        };

        $scope.removeItem = function() {
            if (!$scope.itemList) {
                return;
            }

            angular.forEach($scope.itemListSelections, function(
                selItem, selIndex) {
                var clIndex = $scope.itemList.length;
                while (clIndex--) {
                    if ($scope.itemListProp.compareItems($scope.itemList[clIndex], selItem)) {
                        $scope.itemList.splice(clIndex, 1);
                    }
                }
            });
            $scope.itemListSelections.splice(0, $scope.itemListSelections.length);
            $scope.itemListProp.updateItemList($scope.itemList, $scope.contextObject);
        };
    }
});
