var SUIApplication = function(SUIService) {
    'use strict';

    this.getApplicationContext = function() {
        var that = this;
        return {
            appName: "SAPP_UI",
            title: "Contacts Management",
            loginView: 'resources/app/templates/html/login.html',
            logoutView: 'resources/app/templates/html/login.html',
            mainView: 'resources/app/templates/html/main.html',
            leftMainView: null,
            appLogo: 'resources/app/images/contacts-logo.png',
            showHeader: true,
            showSecondaryHeader: false,
            showLeftMain: false,
            showFooter: false,
            initialContext: {},
            workspaceSwitcherType: 'pills',
            applicationStartupServerCommand: 'userProfileData',
            applicationControllerResources: [
                'resources/app/js/controllers/beanControllers',
                'resources/app/js/controllers/widgetControllers',
                'resources/app/js/controllers/sappWidgetControllers',
                'resources/app/js/controllers/contactControllers',
                'resources/app/js/controllers/adminControllers',
                'resources/app/js/controllers/resourcesControllers',
                'resources/app/js/controllers/longRunningTaskController'
            ],
            applicationCommandResources: [
                'resources/app/js/commands/aboutCommands',
                'resources/app/js/commands/authCommands',
                'resources/app/js/commands/applicationCommands',
                'resources/app/js/commands/beanUserCommands',
                'resources/app/js/commands/beanInternalCommands',
                'resources/app/js/commands/adminCommands',
                'resources/app/js/commands/contactCommands',
                'resources/app/js/commands/longRunningTaskCommands'
            ],
            applicationServiceResources:[
                'resources/app/js/services/utilService',
                'resources/app/js/services/beanService',
                'resources/app/js/services/adminService',
            ],
            bootstrapApplication: function() {
                SUIService.addMenuItem(SUIService.USER_ACTIONS_MENU_ITEM, 'logout');

                SUIService.addMenuItem(SUIService.ABOUT_APPLICATION_MENU_ITEM, 'help');
                SUIService.addMenuItem(SUIService.ABOUT_APPLICATION_MENU_ITEM, SUIService.MENU_SEPARATOR);
                SUIService.addMenuItem(SUIService.ABOUT_APPLICATION_MENU_ITEM, 'feedback');
                SUIService.addMenuItem(SUIService.ABOUT_APPLICATION_MENU_ITEM, 'about');
            },
            getWorkspaces: function() {
                return getWorkspaces();
            },
            getBootstrapCommands: function() {
                return getBootstrapCommands();
            }
        }
    }

    var getWorkspaces = function() {
        var workspaces = [];

        var homeWS = {
            name: 'home',
            title: 'Home',
            iconClass: "glyphicon glyphicon-home",
            contentsURL: 'resources/app/templates/html/workspaces/HomeWS.html',
            isHome: true
        };

        var contactWS = {
            name: 'contacts',
            title: 'Contacts',
            iconClass: "glyphicon glyphicon-user",
            contentsURL: 'resources/app/templates/html/workspaces/ContactWS.html'
        };

        var longRunningTasksWS = {
            name: 'longRunningTasks',
            title: 'Background Tasks',
            iconClass: "glyphicon glyphicon-hourglass",
            contentsURL: 'resources/app/templates/html/workspaces/LongRunningTasksWS.html'
        };

        var adminWS = {
            name: 'admin',
            title: 'Admin',
            iconClass: "glyphicon glyphicon-cog",
            contentsURL: 'resources/app/templates/html/workspaces/AdminWS.html'
        };

        var resourceWS = {
            name: 'resource',
            title: 'Resources',
            iconClass: "glyphicon glyphicon-list-alt",
            contentsURL: 'resources/app/templates/html/workspaces/ResourcesWS.html'
        };

        workspaces.push(homeWS);
        workspaces.push(contactWS);
        workspaces.push(longRunningTasksWS);
        workspaces.push(adminWS);
        workspaces.push(resourceWS);

        return workspaces;
    }

    var getBootstrapCommands = function() {
        var bootstrapCommands = [];
        bootstrapCommands.push({
            name: "login",
            title: "Login",
            iconClass: "glyphicon glyphicon-log-in",
            execute: function() {
                var $callerScope = arguments[0].currentScope;

                return SUIService.executeRestCall({
                    method: "POST",
                    URL: "doLogin",
                    data: $callerScope ? $callerScope.userPrincipal : null,
                    willHandleError: true,
                    isGetFullResponse: false
                });
            },
            onSuccessCB: function() {
                window.location.reload();
            }
        },
        {
            name: "register",
            title: "Register",
            iconClass: "glyphicon glyphicon-log-in",
            execute: function(executionContext, regInfo, registerCallback) {
                console.log("Registering " + regInfo.namespace);
                return SUIService.executeRestCall({
                    method: "POST",
                    URL: "doRegister",
                    data: regInfo,
                    willHandleError: true,
                    isGetFullResponse: false,
                    callback: function(data, status) {
                        if (registerCallback) {
                            registerCallback(data, status);
                        }
                    }
                });
            }
        });
        return bootstrapCommands;
    }
}
