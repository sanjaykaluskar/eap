define({
    AdminService: function(SUIService, dateFilter) {
        /******************************* object privs ********************************/
        var EditObjectPrivModalInstanceCtrl = function($scope, $modalInstance, objectPrivFacade, objectName, objectType)
        {
            $scope.masterFacade = angular.copy(objectPrivFacade);
            $scope.objectPrivFacade = angular.copy($scope.masterFacade);
            $scope.objectName = objectName;
            $scope.objectType = objectType;
            $scope.ok = function() {

                /* populate addedPrivs */
                if (!$scope.objectPrivFacade.addedPrivs) {
                    $scope.objectPrivFacade.addedPrivs = [];
                }

                if ($scope.objectPrivFacade.grantedPrivs)
                {
                    for (var privIdx = 0; privIdx < $scope.objectPrivFacade.grantedPrivs.length; privIdx++)
                    {
                        var privFound = false;
                        var priv = $scope.objectPrivFacade.grantedPrivs[privIdx];
                        if ($scope.masterFacade.grantedPrivs)
                        {
                            for (var mIdx = 0; mIdx < $scope.masterFacade.grantedPrivs.length; mIdx++)
                            {
                                if (priv.pObjInfo.id === $scope.masterFacade.grantedPrivs[mIdx].pObjInfo.id)
                                {
                                    privFound = true;
                                }
                            }
                        }
                        if (!privFound)
                        {
                            $scope.objectPrivFacade.addedPrivs.push(priv);
                        }
                    }
                }

                /* populate removedPrivs */
                if (!$scope.objectPrivFacade.removedPrivs) {
                    $scope.objectPrivFacade.removedPrivs = [];
                }

                if ($scope.masterFacade.grantedPrivs)
                {
                    for (var mIdx = 0; mIdx < $scope.masterFacade.grantedPrivs.length; mIdx++)
                    {
                        var privFound = false;
                        var priv = $scope.masterFacade.grantedPrivs[mIdx];
                        for (var privIdx = 0; privIdx < $scope.objectPrivFacade.grantedPrivs.length; privIdx++)
                        {
                            if ($scope.objectPrivFacade.grantedPrivs[privIdx].pObjInfo.id === priv.pObjInfo.id)
                            {
                                privFound = true;
                            }
                        }
                        if (!privFound)
                        {
                            $scope.objectPrivFacade.removedPrivs.push(priv);
                        }
                    }
                }

                var executeContext = {
                    scope: $scope,
                    viewId: 'editObjPrivsView' + $scope.objectPrivFacade.id,
                    prompt: "Saving object privileges for " + objectName + "...",
                    willHandleError: true
                }

                SUIService.executeCommand('modifyObjectPrivs', [
                    $scope.objectPrivFacade,
                    function(responseData, responseStatus) {
                        if (responseStatus === SUIService.STATUS_CODE_OK) {
                            // the call below closes the dialog window and invokes the OK callback
                            $modalInstance.close();
                        }
                    }
                ], executeContext);
            };

            $scope.cancel = function() {
                // the call below closes the dialog window and invokes the Cancel callback
                $modalInstance.dismiss('cancel');
            };

            $scope.reset = function() {
                $scope.objectPrivFacade = angular.copy($scope.masterFacade);
            };
        };

        this.showEditPrivs = function(pobj, name, objType, callerScope) {

            var executeContext = {
                scope: callerScope,
                willHandleError: true
            }

            SUIService.executeCommand('getObjectPrivList', [
                pobj.id,
                function(responseData, responseStatus) {
                    if (responseStatus === SUIService.STATUS_CODE_OK) {
                        var objectPrivFacade = responseData;

                        var modalInstance = SUIService.getService("$modal").open({
                            templateUrl: 'resources/app/templates/html/dialogs/editObjectPriv.html',
                            controller: EditObjectPrivModalInstanceCtrl,
                            scope: callerScope,
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                objectPrivFacade: function() {
                                    return objectPrivFacade;
                                },
                                objectName: function() {
                                    return name;
                                },
                                objectType: function() {
                                    return objType;
                                }
                            }
                        });

                        modalInstance.result.then(function() {
                            // add anything that needs to be done after the OK completes
                        }, function() {
                            // add anything that needs to be done after the Cancel completes
                        });
                    }
                }
            ], executeContext);
        };
    }
});
