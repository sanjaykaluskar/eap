'use strict';

define({
    listEnum: {
        name: 'listEnum',
        execute: function(executeContext, enumClassName, successCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/sui.listEnum",
                data: {enumClass: enumClassName},
                callback: function(data, status) {
                    if (successCallback) {
                        successCallback(data, status);
                    }
                }
            });
        }
    },

});
