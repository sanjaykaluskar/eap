'use strict';

define({
    SAPPWidgetController: function($scope, SUIService) {
        $scope.utilService = SUIService.getService("UtilService");
    },

    SAPPAddressController: function($scope, SUIService) {
        $scope.addressGroup = {
            open: true
        }

        if ($scope.contextObject) {
            if (!$scope.contextObject.address) {
                $scope.contextObject.address = {};
            }
        }
    },

    SAPPPhoneNumberController: function($scope, SUIService) {

        $scope.utilService = SUIService.getService("UtilService");

        $scope.phoneNumberTypeCellEditTemplate =
            '<select ng-class="\'colt\' + col.index" style="position:absolute;top:4px;bottom:4px;" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="type.name as type.value for type in utilService.phoneNumberTypes"/>';
        $scope.phoneLocationTypeCellEditTemplate =
            '<select ng-class="\'colt\' + col.index" style="position:absolute;top:4px;bottom:4px;" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="type.name as type.value for type in utilService.phoneLocationTypes"/>';
        $scope.checkBoxCellViewTemplate =
            '<div class="ngCellText" style="text-align:center">{{row.getProperty(col.field) ? "&#10003;":"&#10007;"}}</div>';
        $scope.checkBoxCellEditTemplate =
            '<input type="checkbox" ng-class="\'colt\' + col.index" style="position:inline-block;vertical-align:bottom;" ng-input="COL_FIELD" ng-model="COL_FIELD"/>';

        $scope.phoneNumberColumnDefs = [{
                field: 'numberType',
                displayName: 'Type',
                editableCellTemplate: $scope.phoneNumberTypeCellEditTemplate,
                width: '15%'
            }, {
                field: 'location',
                displayName: 'Location',
                editableCellTemplate: $scope.phoneLocationTypeCellEditTemplate,
                width: '10%'
            }, {
                field: 'countryCode',
                displayName: 'Country code',
                width: '15%'
            }, {
                field: 'areaCode',
                displayName: 'Area code',
                width: '15%'
            }, {
                field: 'number',
                displayName: 'Number',
                width: '25%'
            }, {
                field: 'extension',
                displayName: 'Ext',
                width: '**'
        }];

        $scope.itemListProperties = {
            listName: "Phone numbers",
            open: true,
            getItemList: function($scope, ctx) {
                return ctx.phoneNumbers;
            },
            createNewItem: function($scope, ctx) {
                return {};
            },
            compareItems: function(i1, i2) {
                return i1.number === i2.number;
            },
            updateItemList: function(updatedList, ctx) {
                ctx.phoneNumbers = updatedList;
            },
            columnDefs: $scope.phoneNumberColumnDefs
        }
    },

    SAPPUserRolesController: function($scope, SUIService) {
        $scope.utilService = SUIService.getService("UtilService");

        /** role list **/
        $scope.masterUser = {};
        $scope.grantableRoleList = [];
        $scope.roleNameCellEditTemplate =
            '<select ng-class="\'colt\' + col.index" style="position:absolute;top:4px;bottom:4px;" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="value for value in grantableRoleList"/>';
        $scope.checkBoxCellViewTemplate =
            '<div class="ngCellText" style="text-align:center">{{row.getProperty(col.field) ? "&#10003;":"&#10007;"}}</div>';
        $scope.dayTimeCellViewTemplate =
            '<div class="ngCellText" style="text-align:center">{{utilService.formatDayTime(row.getProperty(col.field))}}</div>';

        $scope.roleListColumnDefs = [{
                field: 'roleName',
                displayName: 'Role',
                editableCellTemplate: $scope.roleNameCellEditTemplate,
                width: '20%'
            }, {
                field: 'roleDesc',
                displayName: 'Description',
                width: '40%'
            }, {
                field: 'grantDate',
                displayName: 'Grant Date',
                cellTemplate: $scope.dayTimeCellViewTemplate,
                width: '20%'
            }, {
                field: 'enabled',
                displayName: 'Enabled?',
                cellTemplate: $scope.checkBoxCellViewTemplate,
                width: '**'
            }];

        $scope.itemListProperties = {
            listName: "Roles",
            open: true,
            getItemList: function($scope, ctx) {
                var executeContext = {
                    scope: $scope,
                    willHandleError: true
                }
                var up = SUIService.getUserProfileData();
                SUIService.executeCommand('fetchGrantableRoles', [
                    0,
                    function(responseData, responseStatus) {
                      if (responseStatus === SUIService.STATUS_CODE_OK) {
                        $scope.grantableRoleList = responseData;
                      }
                    }
                ], executeContext);

                /* make a copy of the context object to detect modifications later */
                $scope.masterUser = angular.copy(ctx);
                return ctx.grantedRoles;
            },
            createNewItem: function($scope, ctx) {
                return {};
            },
            compareItems: function(i1, i2) {
                return i1.roleName === i2.roleName;
            },
            updateItemList: function(updatedList, ctx) {
                ctx.grantedRoles = updatedList;
            },
            columnDefs: $scope.roleListColumnDefs
        }
    },

    SAPPUserGrantsController: function($scope, SUIService) {

        // the grant group should not be hidden to start with. This is causing the grant list grid to act weird
        $scope.grantGroup = {
            open: true
        }

        $scope.utilService = SUIService.getService("UtilService");

        /** grant list **/
        $scope.dayTimeCellViewTemplate =
            '<div class="ngCellText" style="text-align:center">{{widgetService.formatDayTime(row.getProperty(col.field))}}</div>';

        $scope.grantListGridOptions = {
            data: 'contextObject.grantedPrivs',
            enableCellSelection: true,
            columnDefs: [{
                field: 'objectId',
                displayName: 'Object Id',
                width: '15%'
            }, {
                field: 'objectType',
                displayName: 'Object Type',
                width: '20%'
            }, {
                field: 'operation',
                displayName: 'Operation',
                width: '20%'
            }, {
                field: 'grantorName',
                displayName: 'Granted By',
                width: '20%'
            }, {
                field: 'grantDate',
                displayName: 'Grant Date',
                cellTemplate: $scope.dayTimeCellViewTemplate,
                width: '**'
            }]
        };

        $scope.getGrantListGridOptions = function() {
            return $scope.grantListGridOptions;
        }
    },

    SAPPObjectPrivController: function($scope, SUIService) {
        $scope.utilService = SUIService.getService("UtilService");

        /** priv list **/
        $scope.dayTimeCellViewTemplate =
            '<div class="ngCellText" style="text-align:center">{{utilService.formatDayTime(row.getProperty(col.field))}}</div>';

        $scope.operationCellEditTemplate = '<select ng-class="\'colt\' + col.index" style="position:absolute;top:4px;bottom:4px;" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="type.name as type.value for type in utilService.operationTypes"/>';

        $scope.granteeNameCellEditTemplate = '<select ng-class="\'colt\' + col.index" style="position:absolute;top:4px;bottom:4px;" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="type.value as type.name for type in granteeList"/>';

        $scope.granteeList = [];
        $scope.privListColumnDefs = [{
                field: 'operation',
                displayName: 'Operation',
                width: '20%',
                editableCellTemplate: $scope.operationCellEditTemplate,
            }, {
                field: 'grantorName',
                displayName: 'Granted By',
                width: '20%',
                enableCellEdit: false,
            }, {
                field: 'granteeName',
                displayName: 'Granted To',
                width: '20%',
                editableCellTemplate: $scope.granteeNameCellEditTemplate,
            }, {
                field: 'grantDate',
                displayName: 'Grant Date',
                cellTemplate: $scope.dayTimeCellViewTemplate,
                width: '**',
                enableCellEdit: false,
            }];

        $scope.setGranteeList = function(userList) {
            if ((userList !== null) && (userList.length > 0)) {
                for (var i = 0; i < userList.length; i++) {
                    var grantee = {
                        name: userList[i].userName,
                        value: userList[i].userName
                    };
                    $scope.granteeList.push(grantee);
                }
            }
        };

        $scope.itemListProperties = {
            listName: "Explicitly granted privileges",
            open: true,
            getItemList: function($scope, ctx) {
                var executeContext = {
                    scope: $scope,
                    prompt: "Loading grantees..."
                }
                SUIService.executeCommand('getGranteeList', [null, $scope.setGranteeList],
                    executeContext);
                return ctx.grantedPrivs;
            },
            createNewItem: function($scope, ctx) {
                var newPriv = {
                    pObjInfo: {id: -1},
                    granteeId: -1,
                    operation: "",
                    grantorName: SUIService.getUserProfileData().userName,
                    objectId: ctx.id,
                    objectType: $scope.objectType,
                };
                return newPriv;
            },
            compareItems: function(i1, i2) {
                return i1.pObjInfo.id === i2.pObjInfo.id;
            },
            updateItemList: function(updatedList, ctx) {
                ctx.grantedPrivs = updatedList;
            },
            columnDefs: $scope.privListColumnDefs
        };
    }
})
