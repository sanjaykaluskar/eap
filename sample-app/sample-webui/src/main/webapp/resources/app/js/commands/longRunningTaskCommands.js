'use strict';

define({
    reloadLongRunningTaskList: {
        name: "reloadLongRunningTaskList",
        iconClass: "glyphicon glyphicon-refresh",
        title: "Refresh",
        execute: function(executeContext) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/sui.listLongRunningTasks",
                data: null,
                willHandleError: executeContext.willHandleError,
                isGetFullResponse: false
            });
        },
        onSuccessCB: function(response, executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.setLongRunningTaskList(response.data);
        }
    },

    cancelLongRunningTask: {
        name: "cancelLongRunningTask",
        execute: function(executeContext, longRunningTaskId) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/sui.cancelLongRunningTask",
                data: {
                    "longRunningTaskId": longRunningTaskId
                },
                isGetFullResponse: false
            });
        },
        onSuccessCB: function(response, executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.reloadLongRunningTaskList();
        },
        onErrorCB: function(response, executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.reloadLongRunningTaskList();
        }
    }
});
