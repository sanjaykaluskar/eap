'user strict';

define({
    LongRunningTaskWorkspaceCtrl: function($scope, SUIService) {
        $scope.suiService = SUIService;
        $scope.longRunningTaskListViewId = "longRunningTaskListView";
        $scope.longRunningTaskList = [];

        $scope.longRunningTaskListViewCommands = ["reloadLongRunningTaskList"];

        $scope.canCancelTask = function(task) {
            return task && (task.asyncState === "CREATED" || task.asyncState === "STARTED");
        }

        $scope.messageCellTemplate = '<div class="ngCellText" title="{{row.getProperty(col.field)}}">{{row.getProperty(col.field)}}</div>';

        $scope.longRunningTaskListGridOptions = {
            data: 'longRunningTaskList',
            multiSelect: false,
            enableCellSelection: false,
            enableCellEdit: false,
            showSelectionCheckbox: false,
            sortInfo: {
                fields: ['lastModifiedTime'],
                directions: ['desc']
            },
            columnDefs: [{
                field: 'responseMessage',
                displayName: 'Name',
                width: '25%',
                cellTemplate: $scope.messageCellTemplate,
                sortable: true
            }, {
                field: 'startedByUserId',
                displayName: 'Started By',
                width: '15%',
                sortable: true
            }, {
                field: 'asyncState',
                displayName: 'State',
                cellTemplate: '<div class="ngCellText text-left">{{row.entity.asyncState}}&nbsp;<img ng-if="canCancelTask(row.entity)" class="busy-progress-generic-icon-small" src="resources/sui/images/busy_32.gif"></img></div>',
                width: '15%',
                sortable: true
            }, {
                field: 'percentCompleted',
                displayName: 'Progress %',
                width: '15%',
                sortable: true
            }, {
                field: 'lastModifiedTime',
                displayName: 'Last Modified',
                cellTemplate: '<div class="ngCellText text-left">{{suiService.Formatters.formatDate(row.entity.lastModifiedTime, true)}}</div>',
                width: '15%',
                sortable: true
            }, {
                field: 'action',
                displayName: 'Action',
                cellTemplate: '<div class="padding-2 text-center"><button class="btn btn-primary btn-xs regular-font-strict" ng-if="canCancelTask(row.entity)" ng-click="cancelLongRunningTask(row.entity.longRunningTaskId)">Cancel</button></div>',
                width: '**',
                sortable: true
            }]
        };

        $scope.reloadLongRunningTaskList = function() {
            var executeContext = {
                scope: $scope,
                viewId: $scope.longRunningTaskListViewId,
                prompt: "Loading background tasks..."
            }
            SUIService.executeCommand("reloadLongRunningTaskList", [], executeContext);
        }

        $scope.cancelLongRunningTask = function(taskId) {
            var executeContext = {
                scope: $scope,
                viewId: $scope.longRunningTaskListViewId,
                prompt: "Canceling background task..."
            }
            SUIService.executeCommand("cancelLongRunningTask", [taskId], executeContext);
        }


        $scope.setLongRunningTaskList = function(data) {
            $scope.longRunningTaskList.empty();
            if (data && data.length) {
                $scope.longRunningTaskList = $scope.longRunningTaskList.concat(data);
            }
        }

        $scope.onMessage = function(message) {
            if (message && message.messageData) {
                $scope.longRunningTaskList.addIfNotPresent(message.messageData,
                    function(o1, o2) {
                        return o1 && o2 && (o1.longRunningTaskId === o2.longRunningTaskId);
                    },
                    true);
            }
        }

        $scope.onLongRunningTaskListViewLoaded = function() {
            $scope.reloadLongRunningTaskList();
            SUIService.getService("SUIServiceMessaging").subscribe($scope, "LR_START");
            SUIService.getService("SUIServiceMessaging").subscribe($scope, "LR_UPDATE");
        }
    }
});
