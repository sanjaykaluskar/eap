'use strict';

define({
    helpCmd: {
        name: "help",
        title: "Help",
        iconClass: "glyphicon glyphicon-question-sign",
    },

    manageResourceFileCmd: {
        name: "manageResourceFile",
        title: "Manage Resource Files",
        iconClass: "glyphicon glyphicon-book",
        enabled: false
    },

    feedbackCmd: {
        name: "feedback",
        title: "Feedback",
        iconClass: "glyphicon glyphicon-thumbs-up",
        enabled: false
    },

    aboutCmd: {
        name: "about",
        title: "About " + SUIService.getApplicationContext().title,
        enabled: true
    }
});
