'use strict';

define({
    newBean: {
        name: "newBean",
        iconClass: "glyphicon glyphicon-file",
        title: "New",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            var currentViewId = executeContext.viewId;
            var bean = {
                pObjInfo: {

                }
            };
            bean.pObjInfo.id = -1;
            $callerScope.showEditBean(bean, true, currentViewId);
        }
    },

    editBean: {
        name: "editBean",
        iconClass: "glyphicon glyphicon-pencil",
        title: "Edit",
        execute: function(executeContext, bean) {
            var $callerScope = executeContext.currentScope;
            var currentViewId = executeContext.viewId;
            $callerScope.showEditBean(bean, false, currentViewId);
        }
    },

    deleteBean: {
        name: "deleteBean",
        iconClass: "glyphicon glyphicon-trash",
        title: "Delete",
        execute: function(executeContext, bean) {
            var $callerScope = executeContext.currentScope;
            $callerScope.deleteBean(bean);
        }
    },

    reloadBeanList: {
        name: "reloadBeanList",
        iconClass: "glyphicon glyphicon-refresh",
        title: "Refresh",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.reloadBeanList(false);
        }
    },

    nextPage: {
        name: "nextPage",
        iconClass: "glyphicon glyphicon-forward",
        title: "Next",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.nextPage();
        }
    },

    prevPage: {
        name: "prevPage",
        iconClass: "glyphicon glyphicon-backward",
        title: "Prev",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.prevPage();
        }
    },

});
