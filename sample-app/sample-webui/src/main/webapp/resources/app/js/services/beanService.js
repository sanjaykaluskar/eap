define({
    BeanService: function(SUIService) {

        this.addExtraTab = function(existingTabs, id, heading, ctxObjFn, viewTemplate, closeFn, maxTabs) {
            var newTab = {
                tabId: id,
                tabViewTemplate: viewTemplate,
                tabHeading: heading,
                getContextObject: ctxObjFn,
                tabCloseAllowed: closeFn && (closeFn !== null),
                tabClose: closeFn
            };

            /* remove the tab from the array if it exists */
            this.closeExtraTab(existingTabs, id);

            /* add it to the end */
            existingTabs.push(angular.copy(newTab));
            existingTabs[existingTabs.length - 1].active = true;

            /* remove least recently accessed tab if size exceeded */
            if (existingTabs.length > maxTabs)
                existingTabs.shift();
        };

        this.closeExtraTab = function(existingTabs, id) {
            for (var clIndex = 0; clIndex < existingTabs.length; clIndex++) {
                if (existingTabs[clIndex].tabId === id) {
                    existingTabs.splice(clIndex, 1);
                    break;
                }
            }
        };
    }
})
