'use strict';

define({
    PwhWorkspaceCtrl: function($scope, SUIService) {
        $scope.beanProperties = {
            beanName: "Contact",
            resourceType: "CONTACT",
            facadeName: "contact",
            descFieldName: "firstName",
            maxOpenBeans: 4,
            beanFetchFields: ["firstName",  "middleName",  "lastName",  "email", "notes", "address", "phoneNumbers"],
            beanViewFields: ["firstName",  "middleName",  "lastName",  "email", "notes"],
            beanViewExtras: [{
                contextField: "address",
                template: "resources/app/templates/html/widgets/addressView.html"
            }, {
                template: "resources/app/templates/html/widgets/phoneNumbersView.html"          
            }],
            beanEditForm: "resources/app/templates/html/forms/contactForm.html",
            beanEditInitialize: null,
            beanEditCheck: null,
            beanViewCustomCommands: ["showEditPrivs"],
            listName: "Contacts",
            listViewFields: ["firstName", "lastName", "email"],
            listViewCheck: function () {
                return true;
            },
            listViewColumnDefs: [{
                field: 'pObjInfo.id',
                displayName: 'Id',
                width: '10%'
            }, {
                field: 'firstName',
                displayName: 'First Name',
                width: '30%',
                cellTemplate: '<div class="ngCellText"><a ng-click="openBean(row.entity);">{{row.getProperty(col.field)}}</a></div>'
            }, {
                field: 'lastName',
                displayName: 'Last Name',
                width: '30%',
                cellTemplate: '<div class="ngCellText"><a ng-click="openBean(row.entity);">{{row.getProperty(col.field)}}</a></div>'
            }, {
                field: 'email',
                displayName: 'Email',
                width: '30%'
            }],
            extraTabs: []
        };
    },

});
