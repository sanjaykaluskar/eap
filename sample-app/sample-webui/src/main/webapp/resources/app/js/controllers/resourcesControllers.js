'use strict';

define({
    ResourcesWorkspaceCtrl: function($scope, SUIService) {
        $scope.resourcesListViewCommands = ["reloadResourcesList", "newResourceCmd", "setResourcePublic", "deleteResourceCmd"];
        $scope.resourcesListViewId = "resourceListMainView";
        $scope.resourcesListViewLoaded = false;

        $scope.$on('suiResourceListInitialized', function() {
            $scope.$broadcast('setResourceListConfiguration', {
                parentViewId: $scope.resourcesListViewId,
                isMultiSelect: true
            });
        })

        $scope.onResourcesListViewLoaded = function() {
            $scope.resourcesListViewLoaded = true;
        }
    }
});
