'use strict';

/* Controllers */
angular.module('SUIApplication.controllers').
    controller('RegisterCtrl', ["$scope", "SUIService", 
       function($scope, SUIService) {

        console.log("Register Init");

        $scope.regInfo = {};

        /** launch the registration dialog **/
        $scope.launchRegister = function() {
            var modalInstance = SUIService.getService("$modal").open({
                templateUrl: 'resources/app/templates/html/dialogs/register.html',
                controller: RegisterModalInstanceCtrl,
                scope: $scope,
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    regInfo: function() {
                        return $scope.regInfo;
                    }
                }
            });

            modalInstance.result.then(function() {
            }, function() {
                // TODO: close handler
            });
        };

        var RegisterModalInstanceCtrl = function($scope,
            $modalInstance, regInfo) {

            $scope.masterRegInfo = angular.copy(regInfo);
            $scope.regInfo = angular.copy($scope.masterRegInfo);

            $scope.ok = function() {
                if ($scope.regInfo.password === $scope.regInfo.reconfirm)
                {
                    var executeContext = {
                        scope: $scope,
                        viewId: 'registerView',
                        prompt: "Registering..." + $scope.regInfo.namespace,
                        willHandleError: true
                    }
                    console.log("Registering " + $scope.regInfo.namespace);                    
                    SUIService.executeCommand('register', [
                        $scope.regInfo,
                        function(responseData, responseStatus) {
                            if (responseStatus === SUIService.STATUS_CODE_OK) {
                                $modalInstance.close($modalInstance);
                                SUIService.showDialogMessage({
                                    dialogMessage: "Welcome to Contacts Management! " +
                                                   "You can now login using the new namespace '" +
                                                   responseData.namespace +
                                                   "' and user name '" + responseData.userName + "'.",
                                    dialogTitle: "Registration Successful",
                                    dialogType: SUIService.DIALOG_TYPE_SUCCESS,
                                    dialogSize: "md"
                                });
                            }
                        }
                    ], executeContext);
                }
                else
                {
                    SUIService.showDialogMessage({
                        dialogMessage: "Retype the same password in the reconfirm field.",
                        dialogTitle: "Password reconfirmation failed!",
                        dialogType: SUIService.DIALOG_TYPE_ERROR,
                        dialogSize: "sm"
                    });
                }
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.reset = function() {
                $scope.regInfo = angular.copy($scope.masterRegInfo);
            };
        };
    }]
);
