'use strict';

define({
	helpCmd : {
	    name: "help",
	    title: "Help",
	    iconClass: "glyphicon glyphicon-question-sign",
	    execute: function () {

	        var modalInstance = SUIService.getService("$modal").open({
		        templateUrl: 'resources/demo-app/templates/html/help.html',
				size: 'lg',
			});

/*
	    	var $callerScope = arguments[0].currentScope;
	    	if(SUIService.getCommand('about').enabled === true) {
	    		SUIService.getCommand('about').enabled = false;
	    	} else {
	    		SUIService.getCommand('about').enabled = true;
	    	}
*/
	    }
	},

	feedbackCmd: {
	    name: "feedback",
	    title: "Feedback",
	    iconClass: "glyphicon glyphicon-thumbs-up",
	    enabled: false
	},

	aboutCmd: {
	    name: "about",
	    title: "About " + SUIService.getApplicationContext().title,
	    enabled: true,
	    execute: function() {
	        var modalInstance = SUIService.getService("$modal").open({
		        templateUrl: 'resources/demo-app/templates/html/about.html',	        
				size: 'lg',
			});
	    }
	}
});