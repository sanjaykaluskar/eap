'use strict';

define({
    startLongRunningOp: {
        name: "startLongRunningOp",
        title: "Start Long Running Operation",
        iconClass: "glyphicon glyphicon-refresh",
        execute: function(executionContext) {
            var $callerScope = executionContext.currentScope;
            return SUIService.executeRestCall({
                method: "GET",
                URL: "command/employee.longRunningCmd",
                data: {
                    "count": 25
                },
                isGetFullResponse: false
            });
        }
    }
});
