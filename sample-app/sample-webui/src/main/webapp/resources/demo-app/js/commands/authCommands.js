'use strict';

define({
    logoutCmd: {
        name: "logout",
        title: "Logout",
        iconClass: "glyphicon glyphicon-log-out",
        execute: function(executionContext) {
            var $callerScope = executionContext.currentScope;
            return SUIService.executeRestCall({
                method: "POST",
                URL: "doLogout",
                data: $callerScope ? $callerScope.userPrincipal : null,
                isGetFullResponse: false
            });
        },
        onSuccessCB: function() {
            SUIService.getService('$window').location.reload();
        }
    }
});
