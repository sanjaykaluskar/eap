'use strict';

/* Services */


angular.module('SUIApplication')
  .value('version', '1.0')
  .service('EmployeeService', ['SUIService', 'dateFilter', function(SUIService, dateFilter) {

  	this.employeeTypes = [{
			name:"CEO", value:"CEO"
		}, {
			name:"MANAGER", value:"Manager"
		}, {
			name:"DEVELOPER", value:"Developer"
	}],

    this.formatPropertyName = function(property) {
      switch(property) {
        case "id" :
          return "Id";
        case "name" :
          return "Name";
        case "type" :
          return "Type";
        case "managerName" :
          return "Manager";
        case "state" :
          return "State";
        case "dob" :
          return "Date of Birth";          
        case "sex" :
          return "Sex";          

        default :
          return property;      
      }
    },

    this.formatPropertyValue = function(property, bean, value) {
    	if(!bean[property]) {
    		return "<NULL>";
    	} else {
    		switch(property) {
    			case "type" :
    				return this.getEmployeeTypeString(value);
    			case "state" : 
    				return this.getEmployeeStateString(value);
    			case "sex" : 
    				return this.getEmployeeSexString(value);    
				case "dob" : 
					return this.getEmployeeDOBString(+value);    								
    			default :
    				return value;		
    		}
    	}
    }, 	

    this.getEmployeeDOBString = function(dob) {

	    if(dob) {
			var dobDate = dob;
			if(!angular.isDate(dobDate) && dobDate > 0) {
				dobDate = new Date(dobDate);        
			}
			return dateFilter(dobDate, "longDate");
		} else {
			return "Not Set";
		}
	},        

  this.getEmployeeSexString = function(empSexEnum) {
    switch(empSexEnum) {
      case "MALE": 
        return "Male";
      case "FEMALE": 
        return "Female";
      default:
        return "<NA>";    
    }
	},       

  this.getEmployeeStateString = function(empStateEnum) {
    switch(empStateEnum) {
      case "PERMANENT": 
        return "Permanent";
      case "PROBATION": 
        return "Probation";
      default:
        return "<UNKNOWN>";    
    }
	},    

  this.getEmployeeTypeString = function(empTypeEnum) {
    switch(empTypeEnum) {
      case "CEO": 
        return "CEO";
      case "MANAGER": 
        return "Manager";
      case "DEVELOPER": 
        return "Developer";
      default:
        return "<UNKNOWN>";    
    }
	},

	this.getAllManagers = function(matchCriteria) {

    var executeContext = {
      dontShowBusy: true
    };

		var qSvc = SUIService.getService("$q");      	            	
		var longRunDef = qSvc.defer();        

    SUIService.executeCommand('fetchEmployees', [{
		    fields: ["id", "name", "type"]
      }, function(data) {
      	var retList = [];
      	var regEx = new RegExp(matchCriteria, "i");
      	angular.forEach(data, function(value, key) {
      		if(value.type === "MANAGER" || value.type === "CEO") {
      			if(regEx.test(value.name)) {
      				retList.push(value);
      			}
      		}
      	});
      	longRunDef.resolve(retList);
      }], executeContext);

      return longRunDef.promise;  
    },

  	this.getSubbordinates = function(managerId) {

        var executeContext = {
          dontShowBusy: true
        };

		var qSvc = SUIService.getService("$q");      	            	
		var longRunDef = qSvc.defer();        

        SUIService.executeCommand('fetchEmployees', [{
        	managerId : managerId,
			fields: ["id", "name", "type", "state"]
        }, function(data) {
        	longRunDef.resolve(data);
        }], executeContext);

        return longRunDef.promise;  
	 }
  }]);