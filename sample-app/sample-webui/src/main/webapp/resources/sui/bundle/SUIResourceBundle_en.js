var SUI_BUNDLE = {};

SUI_BUNDLE["MAIN"] = {
	NULL_LOGIN_CREDENTIALS : "Login Credentials Cannot be Null",
	LOGIN_FAILED : "Unable to login. {0}",
	NO_APPLICATION_REGISTERED : "No Application Registered.",
	INTERNAL_SERVER_ERROR_0: "Internal Server Error. Please contact your system administrator.",
	Internal_Server_Error_0: "Internal Server Error. Please contact your system administrator.",
	Authentication_Login_Error_3: "Invalid Username/Password/Namespace. Please try again."
};