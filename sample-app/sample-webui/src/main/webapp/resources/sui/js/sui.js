'use strict';

// Declare sui app level module which depends on filters, and services
angular.module('SUIApplication', [
        'ngRoute',
        'ngSanitize',
        'SUIApplication.filters',
        'SUIApplication.services',
        'SUIApplication.charting_service',
        'SUIApplication.directives',
        'SUIApplication.controllers',
        'ui.bootstrap',
        'ngGrid',
        'ngJsTree',
        'blueimp.fileupload'
    ])
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/:workspace', {
                    controller: 'SUIWorkspaceRouteController',
                    template: '<div/>'
                }).otherwise({
                    controller: 'SUIWorkspaceRouteController',
                    template: '<div/>'
                });
        }
    ])
    .run([
        'SUIService',
        'SUIServiceMessaging',
        '$rootScope',
        function(SUIService, SUIServiceMessaging, $rootScope) {

            SUIServiceMessaging.init();

            $rootScope.suiService = SUIService;

            window['SUIService'] = SUIService;

            SUIService.registerResourceBundle("SUI_BUNDLE", SUI_BUNDLE);

            var suiApplication = new SUIApplication(SUIService);
            var applicationContext = suiApplication.getApplicationContext();
            if (applicationContext) {
                SUIService.setApplicationContext(applicationContext);
                var commands = applicationContext.getBootstrapCommands();
                if (commands) {
                    angular.forEach(commands, function(cmd, idx) {
                        SUIService.registerCommandObject(cmd);
                    });
                }
            }
        }
    ]);
