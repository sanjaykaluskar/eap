'use strict'

/* Charting Service */

angular.module('SUIApplication.charting_service', [])
    .value('suiVersion', '1.0')
    .service('ChartFactory', ['$q', '$timeout', 'SUIUtils', 'SUIService',

        function($q, $timeout, SUIUtils, SUIService) {

            /**
              Chart Data Format :

              chartData is the data object that is passed to all the chart create APIs.
              The chartData needs to be mapped to the following tree DS for use by the chart implementations.

              Here is the mapped tree should look like

              mappedChartData = [
                {
                  name: <name for group1>,
                  title: <optional title for group1>,
                  color: <optional color for group1>,
                  data: [
                    {
                      name: <name of the group1.subgroup1>,
                      title: <optional title of group1.subgroup1>,
                      color: <optional color for group1.subgroup1>,
                      field1: <value>,
                      field2: <value>
                    },
                    {
                      name: <name of the group1.subgroup2>,
                      title: <optional title of group1.subgroup2>,
                      color: <optional color for group1.subgroup2>,
                      field1: <value>,
                      field2: <value>
                    }
                  ]
                }
              ]

              Now the data for a pie chart / line graph / bar chart can look like this
              ------------------------------------------------------------------------

              mappedPieChartData = [
                {
                  name:<name of pie1/bar1/line x point1>,
                  title: <optional title for pie1/bar1/line x point1>,
                  color: <optional color for this pie's arc / bars fill color>
                  data: [
                    {
                      name: <name of the data field or series name. This has to be unique for single dim charts>
                      color: <optional color for this pie's arc / bars fill color>
                      value: <data value that is used for rendering the pie's arc1 / bar's height1 / line y point1. Here the data value can be a function of multiple fields. This function can be specified in the chartConfig>,
                    }
                  ]
                },
                {
                  name:<name of pie2/bar2/line x point2>,
                  title: <optional title for pie2/bar2/line x point2>,
                  color: <optional color for this pie's arc / bars fill color>
                  data: [
                    {
                      name: <name of the data field or series name. This has to be unique for single dim charts>
                      color: <optional color for this pie's arc / bars fill color>
                      value: <data value that is used for rendering the pie's arc2 / bar's height2 / line y point2. Here the data value can be a function of multiple fields. This function can be specified in the chartConfig>,
                    }
                  ]
                }
              ]

              The data for a group bar chart 
              -------------------------------

              mappedGroupBarChartData = [
                {
                  name:<name of group1>,
                  title: <optional title for group1 / x/y axis display value>,
                  color: <NOT USED>
                  data: [
                    {
                      name: <name of the data field1 / series 1 in case of nvd3 data>
                      color: <optional color for this groups bar1>
                      value: <data value that is used for rendering the group's bar1. Here the data value can be a function of multiple fields. This function can be specified in the chartConfig>
                    }
                    {
                      name: <name of the data field2 / series 2 in case of nvd3 data>
                      color: <optional color for this groups bar2>
                      value: <data value that is used for rendering the group's bar2. Here the data value can be a function of multiple fields. This function can be specified in the chartConfig>
                    }
                  ]
                },
                {
                  name:<name of group2>,
                  title: <optional title for group2>,
                  color: <NOT USED>
                  data: [
                    {
                      name: <name of the data field1>
                      color: <optional color for this groups bar1>
                      value: <data value that is used for rendering the group's bar1. Here the data value can be a function of multiple fields. This function can be specified in the chartConfig>
                    }
                    {
                      name: <name of the data field2>
                      color: <optional color for this groups bar2>
                      value: <data value that is used for rendering the group's bar2. Here the data value can be a function of multiple fields. This function can be specified in the chartConfig>
                    }
                  ]
                }
              ]

            **/

            var DEFAULT_BAR_WIDTH = 30;

            var COLOR_LIST = [
                "#5DA5DA",
                "#FAA43A",
                "#60BD68",
                "#F17CB0",
                "#B2912F",
                "#B276B2",
                "#DECF3F",
                "#F15854"
            ];

            var idCtr = 0;

            var chartMetaData = {

            };

            var initalized = false;

            var nvUtilsOriginalcalcTicksY = nv.utils.calcTicksY;

            return {

                SCROLL_DIR_HORIZONTAL: "h",
                SCROLL_DIR_VERTICAL: "v",

                _debug: {
                    simLongXTicks: false,
                    longTickTemplate: "*WWWWWWWWWWWWWWWWWWWWWWWWWW-"
                },

                truncateLabel: function(label, maxNumOfChars) {
                    return SUIUtils.truncateLabel(label, maxNumOfChars);
                },

                getChartColor: function(index) {
                    if (index >= COLOR_LIST.length) {
                        for (var colIndex = 0; colIndex < (index + 1 - COLOR_LIST.length); colIndex++) {
                            var newColor = this.getRandomColor();
                            while ($.inArray(newColor, COLOR_LIST) != -1) {
                                newColor = this.getRandomColor();
                            }
                            COLOR_LIST.push(newColor);
                        }
                    }
                    return COLOR_LIST[index];
                },

                getForgroundColor: function(backgroundColor) {

                    var c = backgroundColor.substring(1); // strip #
                    var rgb = parseInt(c, 16); // convert rrggbb to decimal
                    var r = (rgb >> 16) & 0xff; // extract red
                    var g = (rgb >> 8) & 0xff; // extract green
                    var b = (rgb >> 0) & 0xff; // extract blue

                    var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

                    if (luma < 150) {
                        return "#FFFFFF";
                    } else {
                        return "#000000";
                    }
                },

                invertColor: function(hexTripletColor) {
                    var color = hexTripletColor;
                    color = color.substring(1); // remove #
                    color = parseInt(color, 16); // convert to integer
                    color = 0xFFFFFF ^ color; // invert three bytes
                    color = color.toString(16); // convert to hex
                    color = ("000000" + color).slice(-6); // pad with leading zeros
                    color = "#" + color; // prepend #
                    return color;
                },

                getRandomColor: function() {
                    var letters = '0123456789ABCDEF'.split('');
                    var color = '#';
                    for (var i = 0; i < 6; i++) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                },

                initialize: function() {
                    var that = this;
                    var resizePromise = null;
                    if (!initalized) {
                        var hoverDiv = $("<div/>").attr("id", "_sui_chart_hover").appendTo("body");
                        hoverDiv.addClass("sui-chart-tooltip");
                        nv.utils.windowResize(function() {
                            if (resizePromise) {
                                $timeout.cancel(resizePromise);
                            }
                            resizePromise = $timeout(function() {
                                that._redrawAllCharts();
                                resizePromise = null;
                            }, 300);
                        });
                    }
                    nv.utils.calcTicksY = function(numTicks, data) {
                        if (data && data.length && data[0].__isSpreadYTicksEvenly) {
                            return numTicks
                        } else {
                            return nvUtilsOriginalcalcTicksY.apply(this, arguments);
                        }
                    }
                    initalized = true;
                },

                createChart: function(type, element, chartDim, chartData, chartConfig) {

                    if (!initalized) {
                        throw new Error("Charting service not intialized.");
                    }

                    var that = this;

                    chartMetaData[chartConfig.chartUID] = {
                        type: type,
                        element: element,
                        chartDim: chartDim,
                        chartData: chartData,
                        chartConfig: chartConfig
                    }

                    this._redrawChart(chartMetaData[chartConfig.chartUID]);
                },

                _redrawChart: function(chartMetaData) {
                    var currTime = (new Date()).getTime();
                    if (chartMetaData.lastRedrawTime) {
                        if ((currTime - chartMetaData.lastRedrawTime) < 600) {
                            return;
                        }
                    }

                    chartMetaData.lastRedrawTime = currTime;

                    if (chartMetaData.element) {
                        d3.select(chartMetaData.element).selectAll("*").remove();
                    }
                    switch (chartMetaData.type) {
                        case "pie":
                            return this.createPieChart(chartMetaData.element, chartMetaData.chartDim, chartMetaData.chartData, chartMetaData.chartConfig);

                        case "group-bar":
                            chartMetaData.chartConfig.isStacked = false;
                            return this.createGroupBarChart(chartMetaData.element, chartMetaData.chartDim, chartMetaData.chartData, chartMetaData.chartConfig);

                        case "group-bar-stacked":
                            chartMetaData.chartConfig.isStacked = true;
                            return this.createGroupBarChart(chartMetaData.element, chartMetaData.chartDim, chartMetaData.chartData, chartMetaData.chartConfig);

                        case "group-bar-horizontal":
                            chartMetaData.chartConfig.isStacked = false;
                            return this.createGroupBarChartHorizontal(chartMetaData.element, chartMetaData.chartDim, chartMetaData.chartData, chartMetaData.chartConfig);

                        case "group-bar-horizontal-stacked":
                            chartMetaData.chartConfig.isStacked = true;
                            return this.createGroupBarChartHorizontal(chartMetaData.element, chartMetaData.chartDim, chartMetaData.chartData, chartMetaData.chartConfig);

                        case "line":
                            return this.createLineChart(chartMetaData.element, chartMetaData.chartDim, chartMetaData.chartData, chartMetaData.chartConfig);

                        case "geo":
                            return this.createGeoChart(chartMetaData.element, chartMetaData.chartDim, chartMetaData.chartData, chartMetaData.chartConfig);

                        default:
                            throw "Chart Type : '" + chartMetaData.type + "' not implemented.";
                    }
                },

                _redrawAllCharts: function() {
                    var that = this;
                    angular.forEach(chartMetaData, function(value, key) {
                        $timeout(function() {
                            that._redrawChart(value);
                        });
                    })
                },

                _getNextID: function(chartType) {
                    if (SUIService.getNextID) {
                        return SUIService.getNextID(chartType);
                    } else {
                        return chartType + (idCtr++);
                    }
                },

                _getDataUsingField: function(dataObject, fieldName, additionalProps) {
                    var splitFields = fieldName.split('.');
                    var retObj = dataObject;
                    for (var sf = 0; sf < splitFields.length; sf++) {
                        if (!retObj[splitFields[sf]]) {
                            break;
                        }
                        retObj = retObj[splitFields[sf]];
                    }
                    if (retObj && additionalProps) {
                        for (var additionalProp in additionalProps) {
                            if (!retObj[additionalProp]) {
                                retObj[additionalProp] = additionalProps[additionalProp];
                            }
                        }
                    }
                    return retObj;
                },

                _getChartData: function(chartData, chartConfig) {
                    var that = this;
                    var cd = null;
                    if (chartConfig.field) {
                        cd = SUIService.getObjectProperty(chartData, chartConfig.field)
                    } else if (chartConfig.fields) {
                        cd = [];
                        angular.forEach(chartConfig.fields, function(value, key) {
                            cd.push(SUIService.getObjectProperty(chartData, chartConfig.fields[key].name, {
                                name: chartConfig.fields[key].name,
                                title: chartConfig.fields[key].title ? chartConfig.fields[key].title : chartConfig.fields[key].name,
                                color: chartConfig.fields[key].color ? chartConfig.fields[key].color : that.getChartColor(key)
                            }));
                        });
                    } else {
                        cd = chartData;
                    }

                    return cd;
                },

                _mapChartData: function(chartData, chartConfig) {

                    if (chartData.mappedData) {
                        return chartData;
                    }

                    var numOfDuplicates = 0;

                    var subGroups = [];
                    var that = this;

                    var cd = this._getChartData(chartData, chartConfig);
                    if (chartConfig.getMappedChartData) {
                        cd = chartConfig.getMappedChartData(cd, chartConfig);
                    } else {
                        var groupFields = [];
                        var mappedData = [];
                        var tempDataMap = {};
                        var cdClone = angular.copy(cd);

                        //This default mapping function only works for single dimension / single series charts
                        angular.forEach(cdClone, function(v1, k1) {
                            var groupName = v1[chartConfig.groupTitleProperty];
                            if (groupName) {
                                groupFields.addIfNotPresent({
                                    name: groupName,
                                    title: groupName
                                }, function(o1, o2) {
                                    if (o1 && o2) {
                                        return o1.name === o2.name;
                                    } else {
                                        return false;
                                    }
                                });
                                if (!tempDataMap[groupName]) {
                                    tempDataMap[groupName] = [];
                                }
                                if (!v1.color) {
                                    v1.color = that.getChartColor(k1);
                                }
                                v1.name = chartConfig.seriesName || chartConfig.seriesTitle || "Series 1";
                                v1.title = chartConfig.seriesTitle || chartConfig.seriesName || "Series 1";

                                // v1.name = v1[chartConfig.groupTitleProperty];
                                // v1.title = v1[chartConfig.groupTitleProperty];
                                tempDataMap[groupName].push(v1);
                            }
                        });

                        angular.forEach(tempDataMap, function(value, key) {
                            mappedData.push({
                                name: key,
                                title: key,
                                //doing it the following way as this is always going to be single dimension / single series chart
                                color: value[0].color,
                                data: value
                            });
                        });
                        cd = mappedData;
                    }

                    var mapDataDef = $q.defer();

                    $q.when(cd).then(function(finalCD) {

                        if (numOfDuplicates > 0) { // FOR DEBUGGING
                            var duplicateVals = [];
                            for (var tempI = 0; tempI < numOfDuplicates; tempI++) {
                                finalCD.forEach(function(group) {
                                    var dupGroup = angular.copy(group);
                                    dupGroup.name = dupGroup.name + "_" + tempI;
                                    dupGroup.title = dupGroup.title + "_" + tempI;
                                    duplicateVals.push(dupGroup);
                                });
                            }
                            finalCD = finalCD.concat(duplicateVals);
                        }

                        finalCD.forEach(function(group) {
                            if (group.data) {
                                group.data.forEach(function(groupData, idx) {
                                    if (!groupData.color) {
                                        groupData.color = that.getChartColor(idx);
                                    }
                                    groupData.parentGroup = group;
                                    var subGroupName = chartConfig.getSubGroupTitle ? chartConfig.getSubGroupTitle(groupData, idx) : null;
                                    if (!subGroupName) {
                                        subGroupName = idx;
                                    }
                                    subGroups.addIfNotPresent({
                                        name: subGroupName,
                                        color: groupData.color
                                    }, function(o1, o2) {
                                        return o1 && o2 && o1.name === o2.name;
                                    });
                                    groupData["_chartInternalName"] = subGroupName;
                                    groupData["_chartInternalValue"] = +(chartConfig.getSubGroupDataValue ? chartConfig.getSubGroupDataValue(groupData, idx, chartConfig) : groupData[groupData.name]);
                                    groupData["_chartInternalMetadata"] = (chartConfig.getSubGroupDataMetadate ? chartConfig.getSubGroupDataMetadate(groupData, idx, chartConfig) : groupData.metadata);
                                })
                            }
                        });
                        finalCD.mappedData = true;
                        finalCD.subGroups = subGroups;

                        mapDataDef.resolve(finalCD);
                    });
                    return mapDataDef.promise;
                },

                _getNVD3Data: function(chartData, chartConfig) {
                    /*

                      This mapping function creates the following output in nvd3Data object
                      nvd3Data = [

                        {
                          key : <Series 1 name>
                          color: <Series 1 color>
                          values : [
                            {
                              x: < x value>,
                              y: < y value>,
                              label: < data point label>
                              color: <individual color, useful when you need put different color for a single bar chart>
                            }
                          ]
                        }

                      ]

                    */

                    var nvD3ReturnData = {
                        metadata: {
                            xTicks: {},
                            xTickCount: 0,
                            cumulativeMin: 0,
                            cumulativeMax: 0
                        },
                        data: null
                    }

                    var nvd3Data = [];

                    var series = {};

                    var totalNumOfValPoints = 0;

                    angular.forEach(chartData, function(val, key) {
                        var data = val.data;
                        totalNumOfValPoints++;

                        // looping for each series. 'dv' is data point for a series

                        angular.forEach(data, function(dv, dk) {
                            if (!series[dv.name]) {
                                series[dv.name] = {
                                    key: dv.title ? dv.title : dv.name,
                                    values: [],
                                    minVal: dv._chartInternalValue,
                                    maxVal: dv._chartInternalValue,
                                    __isSpreadYTicksEvenly: chartConfig.isSpreadYTicksEvenly
                                };
                                if (!SUIService.isNull(dv.color)) {
                                    series[dv.name].color = dv.color;
                                }
                                nvd3Data.push(series[dv.name]);
                            }

                            if (dv._chartInternalValue > series[dv.name].maxVal) {
                                series[dv.name].maxVal = dv._chartInternalValue;
                            }
                            if (dv._chartInternalValue < series[dv.name].minVal) {
                                series[dv.name].minVal = dv._chartInternalValue;
                            }

                            var xLabel = val.title ? val.title : val.name;

                            if (SUIService.isNull(nvD3ReturnData.metadata.xTicks[key])) {
                                nvD3ReturnData.metadata.xTicks[key] = xLabel;
                                nvD3ReturnData.metadata.xTickCount++;
                            }

                            series[dv.name].values.push({
                                seriesName: dv.name,
                                x: key,
                                y: dv._chartInternalValue,
                                label: xLabel,
                                _color: val.color,
                                _metadata: dv._chartInternalMetadata
                            });
                        });
                    });

                    //Add missing values in the series data

                    var seriesCounter = 0;

                    angular.forEach(series, function(s, key) {
                        if (seriesCounter === 0) {
                            nvD3ReturnData.metadata.cumulativeMin = s.minVal;
                            nvD3ReturnData.metadata.cumulativeMax = s.maxVal;
                        } else {
                            if (s.minVal < nvD3ReturnData.metadata.cumulativeMin) {
                                nvD3ReturnData.metadata.cumulativeMin = s.minVal;
                            }
                            if (s.maxVal > nvD3ReturnData.metadata.cumulativeMax) {
                                nvD3ReturnData.metadata.cumulativeMax = s.minMax;
                            }
                        }
                        if (s.values.length !== totalNumOfValPoints) {
                            var dummyVals = [];
                            var sv = s.values;
                            for (var sk = 0; sk < totalNumOfValPoints; sk++) {
                                if (sv.length < (sk + 1) || sv[sk].x !== sk) {
                                    sv.splice(sk, 0, {
                                        seriesName: key,
                                        x: sk,
                                        y: 0,
                                        label: nvD3ReturnData.metadata.xTicks[sk],
                                        _color: null,
                                        _metadata: null
                                    })
                                }
                            }
                        }
                    });

                    nvD3ReturnData.data = nvd3Data;

                    return nvD3ReturnData;
                },

                _renderSingleDimChart: function(element,
                    chartDim,
                    mappedChartData,
                    chartConfig,
                    originalChartMargin,
                    chartCreateFn,
                    onBeforeChartCreateFn,
                    onAfterChartCreateFn,
                    onAfterChartRenderFn,
                    onAfterFinalRedrawFn,
                    scrollDirection) {

                    var that = this;

                    nv.addGraph(function() {

                        var containerDiv = that._getChartContainer(element, chartDim, chartConfig);

                        var _nvd3Data = that._getNVD3Data(mappedChartData, chartConfig);
                        var nvd3Data = _nvd3Data.data;
                        var nvd3Metadata = _nvd3Data.metadata;

                        if (nvd3Data && nvd3Data.length > 1) {
                            containerDiv
                                .append("p")
                                .style("padding-left", "15px")
                                .style("padding-top", "15px")
                                .text("Cannot render chart. Chart not supported for multi dimensional data.")
                            return;
                        } else {
                            nvd3Data = nvd3Data[0].values;
                        }

                        var chartMargin = angular.copy(originalChartMargin);

                        var chartSVGId = that._getNextID("C");
                        var legendSVGId = that._getNextID("CL");
                        var legenfDivId = that._getNextID("CLD");

                        var topDivId = that._getNextID("CTD");
                        var chartDivId = that._getNextID("CD");

                        var topDiv = containerDiv.append("div")
                            .attr("style", "position:relative;height:100%; width:100%;")
                            .attr("id", topDivId);

                        var legendDIV = null;
                        var chartDivContainer = null;
                        var chartDIV = null;

                        chartDivContainer = topDiv.append("div")
                            .attr("style", "position:absolute; height: 100%; width: 100%; padding:0; overflow:hidden;");

                        chartDIV = chartDivContainer.append("div")
                            .attr("style", "margin-right:" + 2 * chartMargin.right + "px; position:absolute; height: 100%; width: 100%; padding:0;")
                            .attr("id", chartDivId)

                        legendDIV = topDiv.append("div")
                            .attr("style", "right:0px; position:absolute; height: 100%; width: " + 2 * chartMargin.right + "px; z-index:5; overflow: auto; padding:0;")

                        var legendSvg = legendDIV.append("svg")
                            .attr("style", "overflow:visible;bottom:0px; right:0px;")
                            .attr("id", legendSVGId)
                            .attr("x", 0)
                            .attr("y", 0);

                        var chartSvg = chartDIV.append("svg")
                            .attr("style", "overflow:visible;height: 100%; width:100%;min-height:" + chartDim.minH + ";min-width:" + chartDim.minW)
                            .attr("id", chartSVGId)
                            .attr("x", 0)
                            .attr("y", 0);

                        var topDivJQ = $("#" + topDivId);
                        var chartDivJQ = $("#" + chartDivId);

                        if (onBeforeChartCreateFn) {
                            onBeforeChartCreateFn();
                        }

                        var chart = chartCreateFn(chartMargin);

                        if (onAfterChartCreateFn) {
                            onAfterChartCreateFn(chart, nvd3Data, nvd3Metadata, chartSVGId);
                        }

                        d3.select("#" + chartSVGId)
                            .datum(nvd3Data)
                            .call(chart);

                        if (onAfterChartRenderFn) {
                            onAfterChartRenderFn(chart, nvd3Data, nvd3Metadata, chartSVGId);
                        }

                        var updatedMargin = chart.margin();

                        var chartSVGItem = $("#" + chartSVGId);
                        var legendSVGItem = $("#" + legendSVGId);

                        var mainChart = chartSVGItem.find("g.nvd3");
                        mainChart
                            .first()
                            .attr("transform", "translate(-" + chartMargin.right / 2 + "," + chartMargin.top + ")")

                        chartDivJQ.width(topDivJQ.width() - 2 * chartMargin.right);
                        chartDivJQ.height(topDivJQ.height());

                        chart.legend
                            .radioButtonMode(false)
                            .updateState(false)
                            .width(2 * chartMargin.right)
                            .height(topDivJQ.height())
                            .key(chart.x());
                        d3.select("#" + legendSVGId).datum(nvd3Data)
                            .call(chart.legend);

                        var legend = legendSVGItem.find("g.nvd3.nv-legend");

                        d3.select("#" + legendSVGId).select("g.nvd3.nv-legend").select("g")
                            .attr("transform", "translate(10, 0)");

                        $timeout(function() {
                            if (legend && legend.get(0)) {
                                var legendBBox = legend.get(0).getBBox();
                                legendSVGItem
                                    .height(legendBBox.height + 25)
                                    .width(legendBBox.width + 15)
                            }
                            try {
                                chart.update();
                                if (onAfterFinalRedrawFn) {
                                    onAfterFinalRedrawFn(chart, nvd3Data, nvd3Metadata, chartSVGId);
                                }
                            } catch (e) {

                            }
                        });
                    });
                },

                _renderChart: function(element,
                    chartDim,
                    mappedChartData,
                    chartConfig,
                    originalChartMargin,
                    chartCreateFn,
                    onBeforeChartCreateFn,
                    onAfterChartCreateFn,
                    onAfterChartRenderFn,
                    onAfterFinalRedrawFn,
                    scrollDirection) {

                    var that = this;

                    nv.addGraph(function() {

                        var enableSmartSizing = SUIService.isNull(chartConfig.enableSmartSizing) ? true : chartConfig.enableSmartSizing;
                        var barWidth = chartConfig.barWidth || DEFAULT_BAR_WIDTH;
                        var barCountPerGroup = 1;
                        var chartLabelSize = 30;
                        var groupSpacingRatio = 0.5;
                        var chartLabelVWidth = 0;
                        var chartLabelHHeight = 0;
                        var chartLegendHeight = 0;

                        var containerDiv = that._getChartContainer(element, chartDim, chartConfig);

                        var chartSVGId = that._getNextID("C");
                        var chartAnnotationSVGId = that._getNextID("C_ANOT");
                        var axisSVGId = that._getNextID("CA");
                        var axisDivId = that._getNextID("CAD");
                        var legendSVGId = that._getNextID("CL");
                        var legendDivId = that._getNextID("CLD");
                        var legendSVGGroupId = that._getNextID("CLG");

                        var axisSVGGroupId = that._getNextID("CAG");

                        var topDivId = that._getNextID("CTD");
                        var chartDivId = that._getNextID("CD");
                        var chartDivInnerId = that._getNextID("CDI");
                        var chartDIVInnerHLabelId = that._getNextID("CDIHL");
                        var chartDIVInnerVLabelId = that._getNextID("CDIVL");

                        var topDiv = containerDiv.append("div")
                            .attr("style", "position:relative;height:100%; width:100%;")
                            .attr("id", topDivId);

                        var topDivJQ = $("#" + topDivId);

                        var _nvd3Data = that._getNVD3Data(mappedChartData, chartConfig);
                        var nvd3Data = _nvd3Data.data;
                        var nvd3Metadata = _nvd3Data.metadata;

                        barCountPerGroup = chartConfig.isStacked ? 1 : nvd3Data.length;

                        var chartMargin = angular.copy(originalChartMargin);

                        var minSpaceRequired = (nvd3Metadata.xTickCount * (barCountPerGroup * (2 * barWidth))) + chartMargin.left + chartMargin.right + barWidth;

                        var totalHeight = topDivJQ.height();
                        var totalWidth = topDivJQ.width();

                        var isHeightOverFlow = (totalHeight < (minSpaceRequired));
                        var isWidthOverFlow = (totalWidth < (minSpaceRequired));

                        if (scrollDirection === that.SCROLL_DIR_VERTICAL) {
                            if (isHeightOverFlow) {
                                chartDim.minH = (minSpaceRequired) + "px";
                            }
                        } else {
                            if (isWidthOverFlow) {
                                chartDim.minW = (minSpaceRequired) + "px";
                            }
                        }

                        var axisDIV = null;
                        var legendDIV = null;
                        var chartDivContainer = null;
                        var chartDIV = null;
                        var chartDIVInner = null;
                        var chartDIVInnerHLabel = null;
                        var chartDIVInnerVLabel = null;

                        var chartDivOF = "overflow: hidden;";

                        if (!SUIService.isNull(chartDim.minW) && chartDim.minW !== "") {
                            chartDivOF += "overflow-x: auto;";
                        }

                        if (!SUIService.isNull(chartDim.minH) && chartDim.minH !== "") {
                            chartDivOF += "overflow-y: auto;";
                        }

                        chartDivContainer = topDiv.append("div")
                            .attr("style", "position:absolute; height: 100%; width: 100%; padding:0;z-index:5; overflow:hidden;");

                        chartDIV = chartDivContainer.append("div")
                            .attr("style", "position:absolute; height: 100%; width: 100%; padding:0;" + chartDivOF)
                            .attr("id", chartDivId)

                        chartDIVInner = chartDIV.append("div")
                            .attr("style", "position:absolute; height: 100%; width: 100%; padding:0;overflow:visible")
                            .attr("id", chartDivInnerId)

                        var horizontalAxisLabel = null;
                        var verticalAxisLabel = null;

                        if (scrollDirection === that.SCROLL_DIR_VERTICAL) {
                            if (chartConfig.yAxisLabel) {
                                horizontalAxisLabel = chartConfig.yAxisLabel;
                            }
                            if (chartConfig.xAxisLabel) {
                                verticalAxisLabel = chartConfig.xAxisLabel;
                            }
                        } else {
                            if (chartConfig.xAxisLabel) {
                                horizontalAxisLabel = chartConfig.xAxisLabel;
                            }
                            if (chartConfig.yAxisLabel) {
                                verticalAxisLabel = chartConfig.yAxisLabel;
                            }
                        }

                        if (horizontalAxisLabel) {
                            chartLabelHHeight = chartLabelSize;
                            chartDIVInnerHLabel = topDiv.append("div")
                                .attr("style", "bottom: " + (chartMargin.bottom) + "px; line-height: " + chartLabelSize + "px; height: " + chartLabelSize + "px;")
                                .attr("id", chartDIVInnerHLabelId)
                                .attr("class", "chart-horizontal-label")
                                .text(horizontalAxisLabel)
                        }
                        if (verticalAxisLabel) {
                            chartLabelVWidth = chartLabelSize;
                            chartDIVInnerVLabel = topDiv.append("div")
                                .attr("style", "left:" + -((totalHeight / 2) - chartLabelSize / 2) + "px; top:" + ((totalHeight / 2) - chartLabelSize / 2) + "px; line-height: " + chartLabelSize + "px; height: " + chartLabelSize + "px; width:" + totalHeight + "px;")
                                .attr("id", chartDIVInnerVLabelId)
                                .attr("class", "chart-vertical-label")
                                .text(verticalAxisLabel)
                        }

                        axisDIV = topDiv.append("div")
                            .attr("style", "position:absolute; height: 100%; width: 100%; overflow: hidden; padding:0;")
                            .attr("id", axisDivId)

                        var axisSvg = axisDIV.append("svg")
                            .attr("style", "overflow:visible;bottom:0px; right:0px;")
                            .attr("id", axisSVGId)
                            .attr("x", 0)
                            .attr("y", 0);

                        var axigSvgGroup = axisSvg
                            .append("g")
                            .attr("class", "nvd3 nv-wrap")
                            .append("g")
                            .attr("id", axisSVGGroupId)

                        var chartAnnotationSvg = chartDIVInner.append("svg")
                            .attr("style", "position:absolute;overflow:hidden;width:100%;height:100%;min-height:" + (chartDim.minH || "10px") + ";min-width:" + (chartDim.minW || "10px"))
                            .attr("id", chartAnnotationSVGId)
                            .attr("x", 0)
                            .attr("y", 0);

                        var chartSvg = chartDIVInner.append("svg")
                            .attr("style", "position:absolute;overflow:hidden;width:100%;height:100%;min-height:" + (chartDim.minH || "10px") + ";min-width:" + (chartDim.minW || "10px"))
                            .attr("id", chartSVGId)
                            .attr("x", 0)
                            .attr("y", 0);

                        var chartDivJQ = $("#" + chartDivId);
                        var axisDivJQ = $("#" + axisDivId);

                        if (onBeforeChartCreateFn) {
                            onBeforeChartCreateFn();
                        }

                        var chart = chartCreateFn(chartMargin);

                        if (nvd3Data && nvd3Data.length > 1) {
                            chart.showLegend(true)
                        }

                        chart.legend
                            .rightAlign(false);

                        chart.xAxis
                            .rotateLabels(25)
                            .showMaxMin(false);

                        chart.yAxis
                            .tickFormat(angular.isFunction(chartConfig.yAxisTickFormat) ? chartConfig.yAxisTickFormat : d3.format(chartConfig.yAxisTickFormat || ',.0f'));

                        if (onAfterChartCreateFn) {
                            onAfterChartCreateFn(chart, nvd3Data, nvd3Metadata, chartSVGId);
                        }

                        try {
                            d3.select("#" + chartSVGId)
                                .datum(nvd3Data)
                                .call(chart);
                        } catch (e) {}

                        try {
                            chart.legend.updateState(false);
                        } catch (e) {
                            //ignore...
                        }

                        if (onAfterChartRenderFn) {
                            onAfterChartRenderFn(chart, nvd3Data, nvd3Metadata, chartSVGId);
                        }

                        var chartInnerDivJQ = $("#" + chartDivInnerId);

                        var chartSVGItem = d3.select("#" + chartSVGId);
                        var chartAnnotationSVGItem = d3.select("#" + chartAnnotationSVGId);

                        var chartSVGItemJQ = $("#" + chartSVGId);
                        var chartAnnotationSVGItemJQ = $("#" + chartAnnotationSVGId);
                        var axisSVGItemJQ = $("#" + axisSVGId);
                        var mainChartSVGItemJQ = chartSVGItemJQ.children("g.nvd3");

                        var totalChartHeight = chartSVGItemJQ.height();
                        var totalChartWidth = chartSVGItemJQ.width();

                        // ****************** START ****************************//                        

                        var mainChartCanvasGroup = chartSVGItemJQ.children("g.nvd3");
                        var originalLegend = null;
                        var horizontalAxis = null;
                        var verticalAxis = null;
                        var horizontalAxisHeight = 0;
                        var verticalAxisWidth = 0;
                        var horizontalAxisBBox = null;
                        var verticalAxisBBox = null;
                        var mainChartSVGBBox = chartSVGItemJQ.get(0).getBBox();
                        var mainChartCanvasGroupBBox = mainChartCanvasGroup.get(0).getBBox();

                        if (scrollDirection === that.SCROLL_DIR_VERTICAL) {
                            horizontalAxis = chartSVGItemJQ.find("g.nv-y.nv-axis");
                            verticalAxis = chartSVGItemJQ.find("g.nv-x.nv-axis");

                        } else {
                            horizontalAxis = chartSVGItemJQ.find("g.nv-x.nv-axis");
                            verticalAxis = chartSVGItemJQ.find("g.nv-y.nv-axis");
                        }

                        var horizontalAxisBBox = horizontalAxis.get(0).getBBox();
                        var verticalAxisBBox = verticalAxis.get(0).getBBox();

                        var horizontalGap = Math.abs(horizontalAxisBBox.x);

                        var verticalAxisWidth = Math.abs(verticalAxisBBox.x);
                        var horizontalAxisHeight = horizontalAxisBBox.height + horizontalAxisBBox.y;

                        var mainChartOverflowWidth = Math.abs(mainChartCanvasGroupBBox.width - totalChartWidth - verticalAxisWidth);

                        var updatedMargin = chart.margin();

                        //Adding "frozen section for showing legends that doesnt scroll"
                        var legendSvg = null;
                        var legendSvgGroup = null;
                        if (nvd3Data && nvd3Data.length > 1) {
                            originalLegend = chartSVGItemJQ.find("g.nv-legendWrap");
                            chartLegendHeight = updatedMargin.top;
                            legendDIV = topDiv.append("div")
                                .attr("style", "position:absolute; z-index: 8; top: 0px; height: " + chartLegendHeight + "px; width: 100%; overflow: hidden; padding:0;")
                            legendSvg = legendDIV.append("svg")
                                .attr("style", "overflow:visible;")
                                .attr("id", legendSVGId)
                                .attr("x", 0)
                                .attr("y", 0);
                            legendSvgGroup = legendSvg
                                .append("g")
                                .attr("class", "nvd3 nv-wrap")
                                .append("g")
                                .attr("id", legendSVGGroupId)
                        }


                        var finalMargin = angular.copy(updatedMargin);

                        //updated margins will have legend height added as top margin. 
                        //If there is no legend, then it will have the original margin set during the chart initialization
                        //So no need to modify the top margin here.

                        //Update the left margin
                        finalMargin.left += Math.ceil(verticalAxisWidth + chartLabelVWidth);

                        //Update the bottom margin
                        finalMargin.bottom += Math.ceil(horizontalAxisHeight + chartLabelHHeight);

                        //Update the right margin
                        //TODO: Had to add 8px horizontal gap correction when horizontal gap is 0 for bar charts. Relook at this later!                        
                        var horizontalGapCorrection = 0;
                        if (horizontalGap < 1) {
                            horizontalGapCorrection = 9;
                        }
                        finalMargin.right += Math.ceil(mainChartOverflowWidth + horizontalGap + horizontalGapCorrection);

                        if (chart.groupSpacing) {
                            if (enableSmartSizing) {
                                if (scrollDirection === that.SCROLL_DIR_VERTICAL) {
                                    if (true) {
                                        //Adjust tick spacing.
                                        var availableHeightForChart = totalChartHeight - finalMargin.top - finalMargin.bottom;
                                        var heightNeededForBars = nvd3Metadata.xTickCount * barCountPerGroup * barWidth;
                                        var heightAvailableForBarGap = availableHeightForChart - heightNeededForBars;
                                        var barSpaceHeight = (heightAvailableForBarGap / (nvd3Metadata.xTickCount + 1));
                                        groupSpacingRatio = (barSpaceHeight / (barSpaceHeight + (barCountPerGroup * barWidth)));
                                    }
                                } else {
                                    if (true) {
                                        //Adjust tick spacing.
                                        var availableWidthForChart = totalChartWidth - finalMargin.left - finalMargin.right;
                                        var widthNeededForBars = nvd3Metadata.xTickCount * barCountPerGroup * barWidth;
                                        var widthAvailableForBarGap = availableWidthForChart - widthNeededForBars;
                                        var barSpaceWidth = (widthAvailableForBarGap / (nvd3Metadata.xTickCount + 1));
                                        if (barSpaceWidth < barWidth) {
                                            barSpaceWidth = barWidth;
                                        }
                                        groupSpacingRatio = (barSpaceWidth / (barSpaceWidth + (barCountPerGroup * barWidth)));
                                        var newTotalChartWidth = Math.ceil((nvd3Metadata.xTickCount * barCountPerGroup * barWidth) + (barSpaceWidth * (nvd3Metadata.xTickCount + 1)) + finalMargin.left + finalMargin.right);
                                        chartSVGItemJQ.css({
                                            'min-width': newTotalChartWidth + "px"
                                        });
                                    }
                                }
                            }
                            chart.groupSpacing(groupSpacingRatio);
                        }

                        chart.margin(finalMargin);
                        chart.update();

                        if (onAfterFinalRedrawFn) {
                            onAfterFinalRedrawFn(chart, nvd3Data, nvd3Metadata, chartSVGId);
                        }

                        totalChartHeight = chartSVGItemJQ.height();
                        totalChartWidth = chartSVGItemJQ.width();

                        var isHScrolling = chartInnerDivJQ.get(0).scrollWidth > chartInnerDivJQ.outerWidth();
                        var isVScrolling = chartInnerDivJQ.get(0).scrollHeight > chartInnerDivJQ.outerHeight();

                        if (scrollDirection === that.SCROLL_DIR_VERTICAL) {
                            if (isVScrolling > 0) {

                            }
                        } else {
                            if (isHScrolling) {
                                if (chartDIVInnerHLabel) {
                                    var scrollBarHeight = totalHeight - totalChartHeight;
                                    $("#" + chartDIVInnerHLabelId).css(
                                        "bottom", "+=" + scrollBarHeight
                                    );
                                }
                            }
                        }

                        //Drawing annotations
                        if (chartConfig.annotation) {
                            angular.forEach(chartConfig.annotation.yAxis, function(v, k) {
                                var pathData = null;

                                var startVal = v.startValue || 0;
                                var endVal = v.endValue;

                                if (v.isPercentile) {
                                    startVal = SUIService.isNull(v.startValue) ? 0 : ((v.startValue * nvd3Metadata.cumulativeMax) / 100);
                                    endVal = SUIService.isNull(v.endValue) ? v.endValue : ((v.endValue * nvd3Metadata.cumulativeMax) / 100);
                                }

                                if (scrollDirection === that.SCROLL_DIR_VERTICAL) {
                                    //pathData = "M" + ((chart.yAxis.scale()(startVal || 0) + finalMargin.left) + " " + (finalMargin.top) + " " + (chartSVGItemJQ.width() - (chartMargin.right)) + " " + (chart.yAxis.scale()(startVal || 0) + finalMargin.top) + " " + ((endVal ? chart.yAxis.scale()(endVal) : chartSVGItemJQ.width()) + finalMargin.left) + " " + ((endVal ? chart.yAxis.scale()(endVal) : 0) + finalMargin.top) + " " + (finalMargin.left) + " " + ((endVal ? chart.yAxis.scale()(endVal) : 0) + finalMargin.top) + " ";
                                    pathData = "M" + (chart.yAxis.scale()(startVal || 0) + finalMargin.left) + " " + (0) + " " + ((endVal ? chart.yAxis.scale()(endVal) : chartSVGItemJQ.width()) + finalMargin.left) + " " + (0) + " " + ((endVal ? chart.yAxis.scale()(endVal) : chartSVGItemJQ.width()) + finalMargin.left) + " " + (chartSVGItemJQ.height()) + " " + (chart.yAxis.scale()(startVal || 0) + finalMargin.left) + " " + (chartSVGItemJQ.height()) + " ";
                                } else {
                                    pathData = "M" + (-finalMargin.left) + " " + (chart.yAxis.scale()(startVal || 0) + finalMargin.top) + " " + (chartSVGItemJQ.width() - (chartMargin.right)) + " " + (chart.yAxis.scale()(startVal || 0) + finalMargin.top) + " " + (chartSVGItemJQ.width() - (chartMargin.right)) + " " + ((endVal ? chart.yAxis.scale()(endVal) : 0) + finalMargin.top) + " " + (-finalMargin.left) + " " + ((endVal ? chart.yAxis.scale()(endVal) : 0) + finalMargin.top) + " ";
                                }
                                chartAnnotationSVGItem.append("path")
                                    .attr("d", pathData)
                                    .attr("stroke-dasharray", v.strokeDashArray || "")
                                    .attr("stroke-width", v.strokeWidth || 2)
                                    .style("opacity", v.opacity || 0.5)
                                    .style("stroke", v.strokeColor || "#F00")
                                    .style("stroke-opacity", v.strokeOpacity || 0)
                                    .style("fill", v.fillColor || "#F00")
                                    .style("fill-opacity", v.fillOpacity || 0.1);
                            });
                        }

                        //Detaching fixed view elements and placing them in appropriate containers
                        //ONLY if there is an overflow

                        if (scrollDirection === that.SCROLL_DIR_VERTICAL) {
                            var chartSVGMinHeight;
                            try {
                                chartSVGMinHeight = chartSVGItemJQ.children("g.nvd3").get(0).getBoundingClientRect().height - horizontalAxisHeight;
                            } catch (e) {
                                chartSVGMinHeight = (chartSVGItemJQ.width() - finalMargin.top - barWidth);
                            }
                            chartSVGItemJQ.css({
                                'min-height': Math.ceil(chartSVGMinHeight) + "px"
                            });
                            chartAnnotationSVGItemJQ.css({
                                'min-height': Math.ceil(chartSVGMinHeight) + "px"
                            });
                            horizontalAxis.detach().appendTo("#" + axisSVGGroupId);
                            horizontalAxis.attr("transform", "translate(" + finalMargin.left + ", " + (chartDivJQ.height() - finalMargin.bottom - finalMargin.top) + ")");
                            mainChartCanvasGroup.attr("transform", "translate(" + finalMargin.left + ", " + -(0) + ")");
                            chartDivJQ.css({
                                'margin-top': finalMargin.top + "px"
                            });
                            axisDivJQ.css({
                                'margin-top': finalMargin.top + "px"
                            })
                            chartDivJQ.height(topDivJQ.height() - finalMargin.bottom - finalMargin.top);
                            chartDivJQ.width(topDivJQ.width());
                            if (originalLegend) {
                                originalLegend.attr("transform", "translate(" + finalMargin.left + ", " + 0 + ")");
                                originalLegend.detach().appendTo("#" + legendSVGGroupId);
                            }
                        } else {
                            if (true) {
                                var chartSVGMinWidth;
                                try {
                                    chartSVGMinWidth = chartSVGItemJQ.children("g.nvd3").get(0).getBoundingClientRect().width - verticalAxisWidth + horizontalGap + horizontalGapCorrection;
                                } catch (e) {
                                    chartSVGMinWidth = (chartSVGItemJQ.width() - finalMargin.left);
                                }
                                chartSVGItemJQ.css({
                                    'min-width': Math.ceil(chartSVGMinWidth) + "px"
                                });
                                chartAnnotationSVGItemJQ.css({
                                    'min-width': Math.ceil(chartSVGMinWidth) + "px"
                                });
                                verticalAxis.detach().appendTo("#" + axisSVGGroupId);
                                verticalAxis.attr("transform", "translate(" + (finalMargin.left) + ", " + updatedMargin.top + ")");
                                mainChartCanvasGroup.attr("transform", "translate(" + (horizontalGap) + ", " + updatedMargin.top + ")");
                                chartDivJQ.css({
                                    'margin-left': finalMargin.left + "px"
                                });
                                chartDivJQ.height(topDivJQ.height());
                                chartDivJQ.width(topDivJQ.width() - finalMargin.left);
                                if (originalLegend) {
                                    originalLegend.attr("transform", "translate(" + finalMargin.left + ", " + 0 + ")");
                                    originalLegend.detach().appendTo("#" + legendSVGGroupId);
                                }
                            }
                        }
                    });
                },

                createGeoChart: function(element, chartDim, chartData, chartConfig) {

                    var that = this;
                    var bC = {};
                    var chartMargin = chartConfig.chartMargin;
                    if (!chartMargin) {
                        chartMargin = {
                            top: 20,
                            right: 55,
                            bottom: 60,
                            left: 55
                        };
                    }

                    var colorSchemeSelect = chartConfig.choroplethScheme ? chartConfig.choroplethScheme : "Blues";
                    var colorScheme = colorbrewer[colorSchemeSelect];
                    var colorSchemeIndex = 9;

                    $q.when(this._mapChartData(chartData, chartConfig)).then(function(bD) {

                        if (bD.length === 0) {
                            return;
                        }

                        var containerDiv = that._getChartContainer(element, chartDim, chartConfig);

                        var _nvd3Data = that._getNVD3Data(bD, chartConfig);
                        var nvd3Data = _nvd3Data.data;
                        var nvd3Metadata = _nvd3Data.metadata;

                        var minVal = 0;
                        var maxVal = 0;

                        if (nvd3Data && nvd3Data.length > 1) {
                            containerDiv
                                .append("p")
                                .style("padding-left", "15px")
                                .style("padding-top", "15px")
                                .text("Cannot render chart. Chart not supported for multi dimensionl data.")
                            return;
                        } else {
                            minVal = nvd3Data[0].minVal;
                            maxVal = nvd3Data[0].maxVal;
                            nvd3Data = nvd3Data[0].values;
                        }

                        var colorFills = {};
                        var mapChartData = {};
                        var colorsList = colorScheme[colorSchemeIndex];

                        colorFills["defaultFill"] = "lightgrey";

                        angular.forEach(colorsList, function(c) {
                            colorFills[c] = c;
                        })

                        var color = d3.scale.quantile()
                            .domain([minVal, maxVal])
                            .range(colorsList);

                        angular.forEach(nvd3Data, function(value, key) {
                            var retObj = {};
                            var regionName = chartConfig.getGeoRegionName ? chartConfig.getGeoRegionName(value) : value._metadata.geoName;
                            mapChartData[regionName] = {
                                value: value,
                                count: value.y,
                                fillKey: color(value.y)
                            }
                        });

                        var chartSVGId = that._getNextID("C");
                        var topDivId = that._getNextID("CTD");

                        var topDiv = containerDiv.append("div")
                            .attr("style", "overflow:auto; position:relative;height:100%; width:100%;")
                            .attr("id", topDivId);
                        var topDivJQ = $("#" + topDivId);

                        var actualWidth = topDivJQ.width();
                        var width = 2 * topDivJQ.height();
                        var padding = (actualWidth - width) / 2;

                        if (padding > 0) {
                            topDivJQ.css({
                                "padding-left": padding
                            })
                        }

                        var map = new Datamap({
                            element: document.getElementById(topDivId),
                            scope: "usa",
                            width: Math.min(actualWidth, width),
                            height: topDivJQ.height(),
                            fills: colorFills,
                            data: mapChartData,
                            geographyConfig: {
                                popupOnHover: false,
                                highlightOnHover: false,
                                highlightBorderWidth: 2
                            },
                            done: function(datamap) {

                                var mouseClickCB = chartConfig.clickCallback;
                                if (!mouseClickCB) {
                                    mouseClickCB = function() {
                                        that._chartClick.apply(that, arguments);
                                    }
                                }

                                var mouseOutCB = chartConfig.mouseOutCallback;
                                if (!mouseOutCB) {
                                    mouseOutCB = function() {
                                        that._chartMouseOut.apply(that, arguments);
                                    }
                                }

                                var mouseMoveCB = chartConfig.mouseMoveCallback;
                                if (!mouseMoveCB) {
                                    mouseMoveCB = function() {
                                        that._chartMouseMove.apply(that, arguments);
                                    }
                                }

                                datamap.svg.selectAll('.datamaps-subunit').on('dblclick', function(geography) {})

                                if (chartConfig.clickCallback) {
                                    datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
                                        if (mouseOutCB) {
                                            mouseOutCB({
                                                label: geography.properties.name,
                                                value: mapChartData[geography.id] ? mapChartData[geography.id].count : "&lt;NA&gt;",
                                                seriesName: mapChartData[geography.id] ? mapChartData[geography.id].value.seriesName : null,
                                                color: mapChartData[geography.id] ? mapChartData[geography.id].fillKey : null,
                                                p: mapChartData[geography.id] ? mapChartData[geography.id].value : null,
                                                e: d3.event
                                            }, d3.event, chartConfig);
                                        }
                                        mouseClickCB({
                                            label: geography.properties.name,
                                            value: mapChartData[geography.id] ? mapChartData[geography.id].count : "&lt;NA&gt;",
                                            seriesName: mapChartData[geography.id] ? mapChartData[geography.id].value.seriesName : null,
                                            color: mapChartData[geography.id] ? mapChartData[geography.id].fillKey : null,
                                            p: mapChartData[geography.id] ? mapChartData[geography.id].value : null,
                                            e: d3.event
                                        }, d3.event, chartConfig);
                                    });
                                }
                                if (mouseMoveCB) {
                                    datamap.svg.selectAll('.datamaps-subunit').on('mousemove', function(geography) {
                                        mouseMoveCB({
                                            label: geography.properties.name,
                                            value: mapChartData[geography.id] ? mapChartData[geography.id].count : "&lt;NA&gt;",
                                            seriesName: mapChartData[geography.id] ? mapChartData[geography.id].value.seriesName : null,
                                            color: mapChartData[geography.id] ? mapChartData[geography.id].fillKey : null,
                                            p: mapChartData[geography.id] ? mapChartData[geography.id].value : null,
                                            e: d3.event
                                        }, d3.event, chartConfig);
                                    });
                                }
                                if (mouseOutCB) {
                                    datamap.svg.selectAll('.datamaps-subunit').on('mouseout', function(geography) {
                                        mouseOutCB({
                                            label: geography.properties.name,
                                            value: mapChartData[geography.id] ? mapChartData[geography.id].count : "&lt;NA&gt;",
                                            seriesName: mapChartData[geography.id] ? mapChartData[geography.id].value.seriesName : null,
                                            color: mapChartData[geography.id] ? mapChartData[geography.id].fillKey : null,
                                            p: mapChartData[geography.id] ? mapChartData[geography.id].value : null,
                                            e: d3.event
                                        }, d3.event, chartConfig);
                                    });
                                }
                            }
                        });
                        map.labels();
                    });
                },

                createGroupBarChart: function(element, chartDim, chartData, chartConfig) {
                    var that = this;
                    var bC = {};
                    var chartMargin = chartConfig.chartMargin;
                    if (!chartMargin) {
                        chartMargin = {
                            top: 8,
                            right: 0,
                            bottom: 0,
                            left: 0
                        };
                    }

                    var xAxisTickMap = {};

                    function createChart(updatedMargin) {
                        return nv.models.multiBarChart()
                            .x(function(d) {
                                if (that._debug.simLongXTicks) {
                                    d.label = that._debug.longTickTemplate + d.x;
                                }
                                xAxisTickMap[d.x] = that.truncateLabel(chartConfig.getAxisTickLabel ? chartConfig.getAxisTickLabel(d) : d.label, chartConfig.labelTruncateLength ? chartConfig.labelTruncateLength : 16);
                                return d.x;
                            })
                            .y(function(d) {
                                return d.y;
                            })
                            .margin(updatedMargin)
                            .reduceXTicks(false)
                            .tooltips(false)
                            .duration(0)
                            .showControls(false) //Allow user to switch between "Grouped" and "Stacked" mode.
                            .showLegend(false)
                            .stacked(chartConfig.isStacked)
                            .groupSpacing(0.5)
                    }

                    function onBeforeCreateChart() {

                    }

                    function onAfterCreateChart(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        if (nvd3Data && nvd3Data.length === 1) {
                            chart.barColor(function(d, i) {
                                if (chartConfig.getColor) {
                                    return chartConfig.getColor(d, i, d._color);
                                } else {
                                    return d._color;
                                }
                            })
                        }
                    }

                    function onAfterRenderChart(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        var tickValueArray = [];
                        angular.forEach(xAxisTickMap, function(val, key) {
                            tickValueArray.push(key);
                        });

                        chart.xAxis
                            .tickValues(tickValueArray);

                        chart.xAxis
                            .tickFormat(function(d) {
                                if (d < 0) {
                                    return "";
                                } else {
                                    return xAxisTickMap[d];
                                }
                            });

                        try {
                            chart.update();
                        } catch (e) {

                        }
                    }

                    function onAfterFinalRedrawFn(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        that._setupChartMouseHandlers(chartConfig, chart.multibar, "rect.nv-bar", chart, nvd3Data, nvd3Metadata, chartSVGId);
                    }

                    $q.when(this._mapChartData(chartData, chartConfig)).then(function(bD) {

                        if (bD.length === 0) {
                            return;
                        }

                        that._renderChart(
                            element, chartDim, bD, chartConfig,
                            chartMargin,
                            createChart,
                            null,
                            onAfterCreateChart,
                            onAfterRenderChart,
                            onAfterFinalRedrawFn
                        );
                    });
                },

                createGroupBarChartHorizontal: function(element, chartDim, chartData, chartConfig) {

                    var that = this;
                    var bC = {};
                    var chartMargin = chartConfig.chartMargin;
                    if (!chartMargin) {
                        chartMargin = {
                            top: 8,
                            right: 0,
                            bottom: 0,
                            left: 0
                        };
                    }

                    var barColor = null;

                    function createChart(updatedMargin) {
                        return nv.models.multiBarHorizontalChart()
                            .x(function(d) {
                                if (that._debug.simLongXTicks) {
                                    d.label = that._debug.longTickTemplate + d.x;
                                }
                                return that.truncateLabel(chartConfig.getAxisTickLabel ? chartConfig.getAxisTickLabel(d) : d.label, chartConfig.labelTruncateLength ? chartConfig.labelTruncateLength : 16);
                            })
                            .y(function(d) {
                                return d.y
                            })
                            .margin(updatedMargin)
                            .showValues(true) //Show bar value next to each bar.
                            .tooltips(false)
                            .duration(0)
                            .showControls(false) //Allow user to switch between "Grouped" and "Stacked" mode.
                            .showLegend(false)
                            .stacked(chartConfig.isStacked)
                    }

                    function onBeforeCreateChart() {}

                    function onAfterCreateChart(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        //TODO: Workaround for a bug in nvd3
                        chart.multibar.barColor = function(color) {
                            if (arguments.length === 0) {
                                return nv.utils.getColor(barColor);
                            } else {
                                barColor = color;
                            }
                        }

                        if (nvd3Data && nvd3Data.length === 1) {
                            chart.barColor(function(d, i) {
                                if (chartConfig.getColor) {
                                    return chartConfig.getColor(d, i, d._color);
                                } else {
                                    return d._color;
                                }
                            })
                        }
                    }

                    function onAfterRenderChart(chart, nvd3Data, nvd3Metadata, chartSVGId) {}

                    function onAfterFinalRedrawFn(chart, nvd3Data, nvd3Metadata, chartSVGId) {

                        var chartSVGItem = d3.select("#" + chartSVGId);
                        var xAxis = chartSVGItem.selectAll("g.nv-x.nv-axis");
                        var xTicks = xAxis.selectAll('g').select("text");
                        xTicks
                            .attr('transform', function(d, i, j) {
                                return 'rotate(' + -20 + ' 0,0) translate(0, 0)'
                            })
                        that._setupChartMouseHandlers(chartConfig, chart.multibar, "g.nv-bar rect", chart, nvd3Data, nvd3Metadata, chartSVGId);
                    }

                    $q.when(this._mapChartData(chartData, chartConfig)).then(function(bD) {

                        if (bD.length === 0) {
                            return;
                        }

                        that._renderChart(
                            element, chartDim, bD, chartConfig,
                            chartMargin,
                            createChart,
                            null,
                            onAfterCreateChart,
                            onAfterRenderChart,
                            onAfterFinalRedrawFn,
                            that.SCROLL_DIR_VERTICAL
                        );
                    });
                },

                createPieChart: function(element, chartDim, chartData, chartConfig) {
                    var that = this;
                    var bC = {};
                    var chartMargin = chartConfig.chartMargin;
                    if (!chartMargin) {
                        chartMargin = {
                            top: -45,
                            right: 75,
                            bottom: -45,
                            left: -45
                        };
                    }

                    function createChart(updatedMargin) {
                        return nv.models.pieChart()
                            .x(function(d) {
                                if (that._debug.simLongXTicks) {
                                    d.label = that._debug.longTickTemplate + d.x;
                                }
                                return that.truncateLabel(chartConfig.getAxisTickLabel ? chartConfig.getAxisTickLabel(d) : d.label, chartConfig.labelTruncateLength ? chartConfig.labelTruncateLength : 16);
                            })
                            .y(function(d) {
                                return d.y
                            })
                            .margin(updatedMargin)
                            .pieLabelsOutside(false)
                            .showLabels(true)
                            .showLegend(false)
                            .labelThreshold(.05) //Configure the minimum slice size for labels to show up
                            .labelType("percent")
                            .tooltips(false)
                            .growOnHover(false)
                    }

                    function onAfterCreateChart(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        chart.color(function(d) {
                            return getPieColor(d);
                        });
                    }

                    function onAfterRenderChart(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        chartConfig.runtime = {
                            setHoverBGColor: true
                        }
                    }

                    function onAfterFinalRedrawFn(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        that._setupChartMouseHandlers(chartConfig, chart.pie, "g.nv-slice", chart, nvd3Data, nvd3Metadata, chartSVGId);
                    }

                    function getPieColor(d) {
                        var tData = d.data ? d.data : d;
                        var defColor = tData._color ? tData._color : that.getChartColor(tData.x);
                        if (chartConfig.getColor) {
                            return chartConfig.getColor(tData, tData.x, defColor);
                        } else {
                            return defColor;
                        }
                    }

                    $q.when(this._mapChartData(chartData, chartConfig)).then(function(bD) {

                        if (bD.length === 0) {
                            return;
                        }

                        that._renderSingleDimChart(
                            element, chartDim, bD, chartConfig,
                            chartMargin,
                            createChart,
                            null,
                            onAfterCreateChart,
                            onAfterRenderChart,
                            onAfterFinalRedrawFn
                        );
                    });
                },

                createLineChart: function(element, chartDim, chartData, chartConfig) {

                    var that = this;
                    var bC = {};
                    var chartMargin = chartConfig.chartMargin;
                    if (!chartMargin) {
                        chartMargin = {
                            top: 8,
                            right: 0,
                            bottom: 0,
                            left: 0
                        };

                    }

                    var xAxisTickMap = {};

                    //chartConfig.barWidth = chartConfig.barWidth || (DEFAULT_BAR_WIDTH/2);

                    function createChart(updatedMargin) {
                        return nv.models.lineChart()
                            .x(function(d) {
                                if (that._debug.simLongXTicks) {
                                    d.label = that._debug.longTickTemplate + d.x;
                                }
                                xAxisTickMap[d.x] = that.truncateLabel(chartConfig.getAxisTickLabel ? chartConfig.getAxisTickLabel(d) : d.label, chartConfig.labelTruncateLength ? chartConfig.labelTruncateLength : 16);
                                return d.x
                            })
                            .y(function(d) {
                                return d.y
                            })
                            .margin(updatedMargin) //Adjust chart margins to give the x-axis some breathing room.
                            .interactive(true)
                            .useVoronoi(false)
                            .useInteractiveGuideline(false) //We want nice looking tooltips and a guideline!
                            .duration(0) //how fast do you want the lines to transition?
                            .showLegend(false) //Show the legend, allowing users to turn on/off line series.
                            .showYAxis(true) //Show the y-axis
                            .showXAxis(true) //Show the x-axis
                            .forceY(0)
                            .forceX(0)
                            .tooltips(false)
                            .interpolate("cardinal");
                    }

                    function onAfterCreateChart(chart, nvd3Data, nvd3Metadata) {

                    }

                    function onAfterRenderChart(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        var tickValueArray = [];
                        angular.forEach(xAxisTickMap, function(val, key) {
                            tickValueArray.push(key);
                        });

                        chart.xAxis
                            .tickValues(tickValueArray);

                        chart.xAxis
                            .tickFormat(function(d) {
                                if (d < 0) {
                                    return "";
                                } else {
                                    return xAxisTickMap[d];
                                }
                            });
                        try {
                            chart.update();
                        } catch (e) {

                        }

                        var chartSVGItem = d3.select("#" + chartSVGId);

                        chartSVGItem.selectAll("path.nv-point")
                            .each(function(d, i) {
                                var clr = chartConfig.getColor ? chartConfig.getColor(d, d.x, d._color) : d._color;
                                d3.select(this)
                                    .style("fill-opacity", 1)
                                    .style("stroke-opacity", 1)
                                    .style("fill", clr)
                                    .style("stroke", clr)
                                    .attr("r", 5)
                            });
                    }

                    function onAfterFinalRedrawFn(chart, nvd3Data, nvd3Metadata, chartSVGId) {
                        that._setupChartMouseHandlers(chartConfig, chart.lines, "path.nv-point", chart, nvd3Data, nvd3Metadata, chartSVGId);
                    }

                    $q.when(this._mapChartData(chartData, chartConfig)).then(function(bD) {

                        if (bD.length === 0) {
                            return;
                        }

                        that._renderChart(
                            element, chartDim, bD, chartConfig,
                            chartMargin,
                            createChart,
                            null,
                            onAfterCreateChart,
                            onAfterRenderChart,
                            onAfterFinalRedrawFn
                        );

                    });
                },

                _getChartContainer: function(element, chartDim, chartConfig, isShowLoadingPrompt) {

                    var existingContainerDiv = d3.select("div#" + chartConfig.chartUID);

                    if (!existingContainerDiv.empty()) {
                        existingContainerDiv.remove();
                    }

                    var containerDiv = d3.select(element).append("div")
                        .attr("id", chartConfig.chartUID)
                        .style('display', 'inline-block')
                        .style('height', '100%')
                        .style('width', '100%')
                        .style('padding', '4px')
                        .style('max-height', chartDim.maxH)
                        .style('max-width', chartDim.maxW)
                        .style('vertical-align', 'top')

                    if (isShowLoadingPrompt) {
                        containerDiv.style('background-position', "center");
                        containerDiv.style('background-repeat', "no-repeat");
                        containerDiv.style('background-image', 'url("resources/sui/images/busy_32.gif")');
                    }

                    return containerDiv;
                },

                _chartClick: function(chartData, event, chartConfig) {

                },

                _chartMouseMove: function(chartData, event, chartConfig) {
                    var chartHoverDiv = $("div#_sui_chart_hover");
                    var e = jQuery.event.fix(event);
                    chartHoverDiv.css({
                        'right': 'auto',
                        'left': (e.pageX + 10) + "px",
                        'top': (e.pageY + 15) + "px"
                    });
                    var bgColor = null;
                    if (chartConfig.runtime && chartConfig.runtime.setHoverBGColor) {
                        bgColor = chartData.color;
                    }
                    var fgColor = "#000";
                    if (bgColor) {
                        fgColor = this.getForgroundColor(bgColor);
                    } else {
                        bgColor = "#FFF";
                    }
                    var serName = chartData.seriesName;
                    var chartTooltipData = "";

                    if (serName && serName.trim().length > 0) {
                        chartTooltipData += '<h4 style="padding:8px; margin:0!important; border-bottom:1px solid #CCC; text-align:center; border-radius:0; color:' + fgColor + ';background-color:' + bgColor + ';">' + serName + '</h4>';
                    }
                    chartTooltipData += '<p style="padding:8px;margin:0!important">' + chartData.label + " = " + chartData.value + '</p>'
                    chartHoverDiv.html(chartTooltipData);

                    chartHoverDiv.show();

                    SUIService.fixPopupPosition(chartHoverDiv);
                },

                _chartMouseOut: function(chartData, event, chartConfig) {
                    $("div#_sui_chart_hover").hide();
                },

                _setupChartMouseHandlers: function(chartConfig, chartItem, itemSelector, chart, nvd3Data, nvd3Metadata, chartSVGId) {

                    var that = this;

                    var mouseClickCB = chartConfig.clickCallback;
                    if (!mouseClickCB) {
                        mouseClickCB = function() {
                            that._chartClick.apply(that, arguments);
                        }
                    }

                    var mouseOutCB = chartConfig.mouseOutCallback;
                    if (!mouseOutCB) {
                        mouseOutCB = function() {
                            that._chartMouseOut.apply(that, arguments);
                        }
                    }

                    var mouseMoveCB = chartConfig.mouseMoveCallback;
                    if (!mouseMoveCB) {
                        mouseMoveCB = function() {
                            that._chartMouseMove.apply(that, arguments);
                        }
                    }

                    if (mouseClickCB) {
                        chartItem.dispatch.on("elementClick", function(e) {
                            if (mouseOutCB) {
                                mouseOutCB({
                                    label: e.point.label,
                                    value: e.point.y,
                                    seriesName: e.point.seriesName,
                                    color: chartConfig.getColor ? chartConfig.getColor(e.point, e.point.x, e.point._color) : e.point._color,
                                    p: e.point,
                                    pointIndex: e.pointIndex,
                                    seriesIndex: e.seriesIndex,
                                    e: d3.event
                                }, d3.event, chartConfig);
                            }
                            mouseClickCB({
                                label: e.point.label,
                                value: e.point.y,
                                seriesName: e.point.seriesName,
                                color: chartConfig.getColor ? chartConfig.getColor(e.point, e.point.x, e.point._color) : e.point._color,
                                p: e.point,
                                pointIndex: e.pointIndex,
                                seriesIndex: e.seriesIndex,
                                e: d3.event
                            }, d3.event, chartConfig);
                        });
                    }
                    if (mouseMoveCB) {
                        chartItem.dispatch.on("elementMouseover", function(e) {
                            mouseMoveCB({
                                label: e.point.label,
                                value: e.point.y,
                                seriesName: e.point.seriesName,
                                color: chartConfig.getColor ? chartConfig.getColor(e.point, e.point.x, e.point._color) : e.point._color,
                                p: e.point,
                                pointIndex: e.pointIndex,
                                seriesIndex: e.seriesIndex,
                                e: d3.event
                            }, d3.event, chartConfig);
                        });
                    }
                    if (mouseOutCB) {
                        chartItem.dispatch.on("elementMouseout", function(e) {
                            mouseOutCB({
                                label: e.point.label,
                                value: e.point.y,
                                seriesName: e.point.seriesName,
                                color: chartConfig.getColor ? chartConfig.getColor(e.point, e.point.x, e.point._color) : e.point._color,
                                p: e.point,
                                pointIndex: e.pointIndex,
                                seriesIndex: e.seriesIndex,
                                e: d3.event
                            }, d3.event, chartConfig);
                        });
                    }


                    if (mouseMoveCB) {
                        d3.select("#" + chartSVGId)
                            .selectAll(itemSelector)
                            .on('mousemove', function(dd, i) {
                                var d = dd.data ? dd.data : dd;
                                mouseMoveCB({
                                    label: d.label,
                                    value: d.y,
                                    seriesName: d.seriesName,
                                    color: chartConfig.getColor ? chartConfig.getColor(d, d.x, d._color) : d._color,
                                    p: d,
                                    pointIndex: i,
                                    seriesIndex: d.series,
                                    e: d3.event
                                }, d3.event, chartConfig);
                            });
                    }
                }
            }
        }
    ]);
