'use strict';

/* Controllers */

angular.module('SUIApplication.controllers', [])
    .controller('SUIApplicationController', ['$log', '$injector', '$rootScope', '$scope', '$q', 'SUIService', 'SUIServiceHTTP', '$sce',
        function($log, $injector, $rootScope, $scope, $q, SUIService, SUIServiceHTTP, $sce) {

            $scope.appState = SUIService.getApplicationState();

            $scope.$watch('appState', function() {});

            $rootScope.$watch('waitMessage', function() {
                $scope.waitMessage = $rootScope.waitMessage;
            });

            $rootScope.$watch('showBusy', function() {
                $scope.showBusy = $rootScope.showBusy;
            });

            $rootScope.$watch('canCancelAction', function() {
                $scope.canCancelAction = $rootScope.canCancelAction;
            });

            $scope.initializeApplication = function(isForce) {
                $q.when(SUIService.initializeApplication(isForce)).then(
                    function(appState, data) {
                        $scope.appState = SUIService.getApplicationState();
                    },
                    function(appState, data) {
                        $scope.appState = SUIService.getApplicationState();
                        if (appState === SUIService.APPLICATION_INITIALIZING) {
                            //TODO: wait for the app to initialize
                        }
                        if (appState === SUIService.APPLICATION_ALREADY_INITIALIZED) {
                            //TODO: app already initialized.
                        }
                        if (appState == SUIService.APPLICATION_INIT_FAILED) {
                            //TODO: show error message to user
                        }
                    }
                );
            };

            $scope.initializeApplication();

            $scope.waitMessage = null;
            $scope.showBusy = false;
            $scope.canCancelAction = false;

            $scope.busyModalButtonClick = function(buttonId) {
                if (buttonId === 'cancel') {
                    $scope.showBusy = false;
                }
            };
        }
    ])
    .controller('SUIApplicationAuthenticationController', ['$scope', '$q', 'SUIService', '$sce',
        function($scope, $q, SUIService, $sce) {
            $scope.suiService = SUIService;

            $q.when(SUIService.loadSUITemplates()).then(function() {
                    $scope.setLoginView();
                },
                function() {
                    $scope.setLoginView();
                });
            $scope.setLoginView = function() {
                $scope.appLoginView = $sce.getTrustedResourceUrl(SUIService.getApplicationContext().loginView);
                $scope.$broadcast("reset-focus");
            }
        }
    ])
    .controller('SUIWorkspaceRouteController', ['$scope', 'SUIService', '$sce', '$routeParams',
        function($scope, SUIService, $sce, $routeParams) {
            var ws = SUIService.getWorkspace($routeParams.workspace);
            if (ws) {
                SUIService.onWorkspaceSelect($routeParams.workspace);
            } else {
                SUIService.onWorkspaceSelect();
            }
        }
    ])
    .controller('SUIFileUploadController', ['$scope',
        function($scope) {

            $scope.isUploadSuccess = function(file) {
                try {
                    return file.$response().textStatus === "success";
                } catch (e) {

                }
            }
        }
    ])
    .controller('SUIResourceController', ['$scope',
        function($scope) {

            $scope.$on('reloadResourcesList', function(event) {
                $scope.reloadResourcesList();
            });

            $scope.$on('showResourceUpload', function(event, currentViewId) {
                $scope.showResourceUpload(currentViewId);
            });

            $scope.$on('deleteResources', function(event) {
                $scope.deleteResources();
            });

            $scope.$on('setResourceVisibility', function(event, from, to) {
                $scope.setResourceVisibility(from, to);
            });

            $scope.$on('resetCommandState', function(event) {
                $scope.resetCommandState();
            });

            $scope.$on('setResourceListConfiguration', function(event, configuration) {
                $scope.resourcesListParentViewId = configuration.parentViewId;
                $scope.isMultiSelect = configuration.isMultiSelect;
                $scope.isResourceListCommandEnabled = configuration.isResourceListCommandEnabled;
                if(!SUIService.isNull($scope.isMultiSelect)) {
                    $scope.gridOptions.multiSelect = $scope.isMultiSelect;    
                }       
                $scope.selectionChangeCallback = configuration.selectionChangeCallback;
                $scope.forceEnableAllItems = configuration.forceEnableAllItems;         
            })

            $scope.resourcesList = [];
            $scope.resourcesListSelection = [];
            $scope.resourceGridOptions = {};
            $scope.resourceGridOptions.allSelected = false;
            $scope.gridOptions = {
                data: 'resourcesList',
                multiSelect: false,
                enableCellSelection: true,
                showSelectionCheckbox: true,
                selectWithCheckboxOnly: true,
                selectedItems: $scope.resourcesListSelection,
                checkboxHeaderTemplate: '<input ng-show="isMultiSelect" class="ngSelectionHeader" type="checkbox" ng-model="resourceGridOptions.allSelected" ng-change="toggleSelectAll(resourceGridOptions.allSelected)"/>',
                checkboxCellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox" ng-disabled="!forceEnableAllItems && row.entity.fileVisibilityValue === \'public\'" ng-checked="row.selected" /></div>',
                columnDefs: [{
                    field: 'fileName',
                    displayName: 'Name',
                    width: '28%',
                    cellTemplate: '<div class="ngCellText"><a ng-click="downloadResource(row.entity)">{{row.getProperty(col.field)}}</a></div>'
                }, {
                    field: 'fileType',
                    displayName: "Type",
                    width: '8%'
                }, {
                    field: 'fileSize',
                    displayName: "Size",
                    width: '8%',
                    cellTemplate: '<div class="ngCellText">{{+row.getProperty(col.field) | formatFileSize}}</div>'
                }, {
                    field: 'filePath',
                    displayName: "Path",
                    width: '36%'
                }, {
                    field: 'fileVisibilityValue',
                    displayName: "Visibility",
                    width: '**'
                }],
                afterSelectionChange: function(row, event) {
                    if (row.length) {
                        for (var rr = 0; rr < row.length; rr++) {
                            if (row[rr].entity.fileVisibilityValue === "public") {
                                $scope.gridOptions.selectItem(row[rr].rowIndex, false);
                            }
                        }
                    }

                    $scope.resetCommandState();

                    if($scope.selectionChangeCallback) {
                        $scope.selectionChangeCallback($scope.resourcesListSelection);
                    }
                }
            };

            $scope.downloadResource = function(resource) {
                var downloadURL = "rest/resource?fileName=" + resource.fileName + "&visibility=" + resource.fileVisibilityValue;                
                var $win = SUIService.getService("$window");
                $win.open(downloadURL, "fileDownloadTarget");
            }

            $scope.setResourceVisibility = function(from, to) {
                var executeContext = {
                    scope: $scope,
                    viewId: $scope.resourcesListParentViewId,
                    prompt: "Setting resources visibility from " + from + " to " + to
                }                
                SUIService.executeCommand('setResourceVisibility', [
                    $scope.resourcesListSelection,
                    from,
                    to
                ], executeContext);
            }            

            $scope.deleteResources = function() {
                SUIService.showDialogMessage({
                    isModal: true,
                    dialogTitle: "Delete Confirmation",
                    dialogMessage: "Are you sure you want to delete the selected resources ?",
                    dialogType: SUIService.DIALOG_TYPE_CONFIRM,
                    callbackFn: function(dialogResult) {
                        if (dialogResult === SUIService.DIALOG_OK) {
                            var executeContext = {
                                scope: $scope,
                                viewId: $scope.resourcesListParentViewId,
                                prompt: "Deleting resources..."
                            }
                            SUIService.executeCommand('deleteResourceInternal', [
                                $scope.resourcesListSelection,
                                function() {
                                    $scope.reloadResourcesList();
                                }
                            ], executeContext);
                        }
                    }
                });
            }

            $scope.reloadResourcesList = function() {
                var executeContext = {
                    scope: $scope,
                    viewId: $scope.resourcesListParentViewId,
                    prompt: "Loading resources..."
                }
                SUIService.executeCommand('fetchResources', [null, $scope.setResourcesList], executeContext);
            };

            $scope.setResourcesList = function(data) {
                $scope.resourcesListSelection.empty();
                $scope.resetCommandState();
                $scope.resourceGridOptions.allSelected = false;
                $scope.resourcesList = data;
            };

            $scope.isCommandEnabled = function(cmd, contextObject) {   
                var isEnabled = true;
                if($scope.isResourceListCommandEnabled) {
                    isEnabled = $scope.isResourceListCommandEnabled(cmd, contextObject);
                }
                if(!$scope.isResourceListCommandEnabled || SUIService.isNull(isEnabled)) {
                    var isSelection = $scope.resourcesListSelection.length > 0;
                    switch (cmd) {
                        case "deleteResourceCmd":
                            isEnabled = isSelection;
                            break;
                        case "setResourcePublic":
                            isEnabled = isSelection;
                            break;
                    }
                    console.log(cmd + " : " + isEnabled);
                    return isEnabled;
                }
            }

            $scope.resetCommandState = function() {
                if ($scope.resourcesListSelection.length > 0) {
                    SUIService.getCommand("deleteResourceCmd").enabled = true;
                    SUIService.getCommand("setResourcePublic").enabled = true;
                } else {
                    SUIService.getCommand("deleteResourceCmd").enabled = false;
                    SUIService.getCommand("setResourcePublic").enabled = false;
                }                
            }

            $scope.getSelectedResources = function() {
                return $scope.resourcesListSelection;
            }

            $scope.showResourceUpload = function() {
                var modalInstance = SUIService.getService("$modal").open({
                    templateUrl: 'resources/sui/templates/html/resourceUploadDialog.html',
                    controller: ResourceUploadModalInstanceCtrl,
                    size: 'lg',
                    backdrop: 'static',
                    scope: $scope
                });

                modalInstance.result.then(
                    function() {
                        $scope.reloadResourcesList();
                    },
                    function() {
                        $scope.reloadResourcesList();
                        // TODO: close handler
                    });
            };

            var ResourceUploadModalInstanceCtrl = function($scope, $modalInstance) {

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            };


            $scope.reloadResourcesList();

            $scope.$emit("suiResourceListInitialized");
        }
    ]);
