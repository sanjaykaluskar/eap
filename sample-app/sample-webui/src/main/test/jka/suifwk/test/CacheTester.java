package jka.suifwk.test;

import java.util.ArrayList;
import java.util.List;

import jka.suifwk.cache.ArrayListObjectCache;
import jka.suifwk.cache.GenericObjectCache;
import jka.suifwk.cache.GenericObjectCache.CacheDataProvider;

public class CacheTester {

	/**
	 * @param args
	 */

	private static List<CachedElement> mainDataSet;

	private static int pageSize = 100;

	public static void main(String[] args) {
		try {
			mainDataSet = new ArrayList<CachedElement>();

			for (int d = 0; d < 25000; d++) {
				mainDataSet.add(new CachedElement("K:" + d, "V:" + d));
			}
			new CacheTester().testCache();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testCache() {

		CacheDataProvider<CachedElement> cachedDataProvider = new CacheDataProvider<CacheTester.CachedElement>() {
			@Override
			public List<CachedElement> getData(int startIndex, int count) {
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return getElements(startIndex, count);
			}

			@Override
			public int getTotalCount() {
				return mainDataSet.size();
			}
		};
		String cacheId = "ElementCache_1";
		GenericObjectCache<CachedElement> objectCache = new ArrayListObjectCache<CachedElement>(
				cacheId, pageSize, cachedDataProvider);
		System.out.println(objectCache);

		// -------------------------------------------------------------------------//

		long start = System.currentTimeMillis();
		for (int i = 0; i < mainDataSet.size(); i += pageSize) {
			System.out.println(objectCache.getData(cacheId, i, pageSize));
			// objectCache.getData(cacheId, i, pageSize);
		}
		long end = System.currentTimeMillis();

		System.out.println("Total Time Taken : " + (end - start));

	}

	public List<CachedElement> getElements(int startIndex, int count) {
		if (startIndex < 0) {
			throw new IllegalArgumentException(
					"Start index cannot be less than 0");
		}
		if (count < 0) {
			throw new IllegalArgumentException("Count cannot be less than 0");
		}

		if (startIndex > mainDataSet.size()) {
			throw new RuntimeException("Start index : " + startIndex
					+ " greater than full data set length - "
					+ mainDataSet.size());
		} else {
			int toIndex = startIndex + count;
			if (toIndex >= mainDataSet.size()) {
				toIndex = mainDataSet.size();
			}
			return mainDataSet.subList(startIndex, toIndex);
		}
	}

	public static class CachedElement {
		public String key;
		public String value;

		public CachedElement(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String toString() {
			return "CE={key:" + key + ", value:" + value + "};";
		}
	}
}