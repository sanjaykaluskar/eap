package com.dombigroup.eap.app.webui.api.contact;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.CallContext;
import com.dombigroup.eap.app.webui.common.AbstractCommandHandler;
import com.dombigroup.eap.server.platform.context.ServiceContext;

/**
 * @author skaluska
 * 
 */
public class ContactCommandHandler extends AbstractCommandHandler
{
  /*
   * (non-Javadoc)
   * 
   * @see jka.suifwk.api.CommandHandler#getNamespace()
   */
  @Override
  public String getNamespace()
  {
    return "contact";
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.informatica.mlp.common.AbstractCommandHandler#internalCommand(jka.suifwk
   * .api.CallContext,
   * com.dombigroup.eap.server.platform.context.ServiceContext,
   * java.lang.String)
   */
  @Override
  protected APICallResponse internalCommand(CallContext context,
      ServiceContext svc, String command) throws Exception
  {
    APICallResponse ret = null;
    switch (command)
    {
    default:
      break;
    }

    return ret;
  }
}