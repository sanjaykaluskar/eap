/**
 * ObjectPrivFacade.java
 */
package com.dombigroup.eap.app.webui.api.admin;

import java.util.List;

import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;

/**
 * @author skaluska
 * 
 */
public class ObjectPrivFacade
{
  private int                     id;

  private List<GrantedObjectPriv> grantedPrivs;

  private List<GrantedObjectPriv> addedPrivs;

  private List<GrantedObjectPriv> removedPrivs;

  public ObjectPrivFacade()
  {
  }

  /**
   * Constructor for ObjectPrivFacade
   */
  public ObjectPrivFacade(int id, List<GrantedObjectPriv> grantedPrivs)
  {
    super();
    this.id = id;
    this.grantedPrivs = grantedPrivs;
  }

  /**
   * @return the id
   */
  public int getId()
  {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(int id)
  {
    this.id = id;
  }

  /**
   * @return the grantedPrivs
   */
  public List<GrantedObjectPriv> getGrantedPrivs()
  {
    return grantedPrivs;
  }

  /**
   * @param grantedPrivs the grantedPrivs to set
   */
  public void setGrantedPrivs(List<GrantedObjectPriv> grantedPrivs)
  {
    this.grantedPrivs = grantedPrivs;
  }

  /**
   * @return the addedPrivs
   */
  public List<GrantedObjectPriv> getAddedPrivs()
  {
    return addedPrivs;
  }

  /**
   * @param addedPrivs the addedPrivs to set
   */
  public void setAddedPrivs(List<GrantedObjectPriv> addedPrivs)
  {
    this.addedPrivs = addedPrivs;
  }

  /**
   * @return the removedPrivs
   */
  public List<GrantedObjectPriv> getRemovedPrivs()
  {
    return removedPrivs;
  }

  /**
   * @param removedPrivs the removedPrivs to set
   */
  public void setRemovedPrivs(List<GrantedObjectPriv> removedPrivs)
  {
    this.removedPrivs = removedPrivs;
  }
}
