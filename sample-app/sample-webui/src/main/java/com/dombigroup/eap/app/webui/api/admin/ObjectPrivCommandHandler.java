package com.dombigroup.eap.app.webui.api.admin;

import java.util.List;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.CallContext;

import com.dombigroup.eap.app.webui.common.AbstractCommandHandler;
import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.privmgmt.ObjectPrivMgmtService;
import com.dombigroup.eap.server.platform.privmgmt.ObjectPrivMgmtServiceFactory;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

/**
 * @author skaluska
 * 
 */
public class ObjectPrivCommandHandler extends AbstractCommandHandler
{
  private static final String GET_PRIV_LIST       = "getPrivList";
  private static final String GET_GRANTEE_LIST    = "getGranteeList";
  private static final String MODIFY_OBJECT_PRIVS = "modifyObjectPrivs";

  /*
   * (non-Javadoc)
   * 
   * @see jka.suifwk.api.CommandHandler#getNamespace()
   */
  @Override
  public String getNamespace()
  {
    return "objectPriv";
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.informatica.mlp.common.AbstractCommandHandler#internalCommand(jka.suifwk
   * .api.CallContext,
   * com.dombigroup.eap.server.platform.context.ServiceContext,
   * java.lang.String)
   */
  @Override
  protected APICallResponse internalCommand(CallContext context,
      ServiceContext svc, String command) throws Exception
  {
    APICallResponse ret = null;
    ObjectPrivMgmtService objectPrivSvc = ObjectPrivMgmtServiceFactory
        .getObjectPrivMgmtService();

    switch (command)
    {
    case GET_PRIV_LIST:
      int objectId = APIUtils.getPayloadDataAsInteger(context, "id", true, -1);
      svc.startCall();
      List<GrantedObjectPriv> privList = objectPrivSvc.getObjectPrivs(svc,
          objectId);
      svc.endCall(false);

      ObjectPrivFacade facade = new ObjectPrivFacade(objectId, privList);
      ret = new APICallResponse(facade);
      break;
    case GET_GRANTEE_LIST:
      svc.startCall();
      UserMgmt userSvc = UserMgmtServiceFactory.getUserMgmtService();
      List<User> userList = userSvc.getUserList(svc, svc.getSecContext().getOrgId());
      svc.endCall(false);

      ret = new APICallResponse(userList);
      break;
    case MODIFY_OBJECT_PRIVS:
      List<ObjectPrivFacade> facadeArray = APIUtils.getPayloadDataAsObject(context, "objectPrivs", ObjectPrivFacade.class);
      ObjectPrivFacade modifiedPrivs = facadeArray.get(0);
      svc.startTransaction();
      modifyPrivs(svc, objectPrivSvc, modifiedPrivs);
      svc.commitTransaction();

      ret = new APICallResponse(0);
      break;
    default:
      break;
    }

    return ret;
  }

  private void modifyPrivs(ServiceContext svc,
      ObjectPrivMgmtService objectPrivSvc, ObjectPrivFacade modifiedPrivs)
      throws MetadataException
  {
    /* grant added privs */
    if (modifiedPrivs.getAddedPrivs() != null)
    {
      for (GrantedObjectPriv priv : modifiedPrivs.getAddedPrivs())
      {
        /*
         * We will get the grantee id if the called only supplies the name
         */
        if (priv.getGranteeId() == Persisted.INVALID_ID)
        {
          UserMgmt userSvc = UserMgmtServiceFactory.getUserMgmtService();
          User grantee = userSvc.getUser(svc, svc.getSecContext().getOrgId(),
              priv.getGranteeName());
          priv.setGranteeId(grantee.getUserid());
        }
        objectPrivSvc.grantObjectPriv(svc, priv.getGranteeId(),
            priv.getObjectId(), priv.getObjectType(), priv.getOperation());
      }
    }

    /* revoke removed privs */
    if (modifiedPrivs.getRemovedPrivs() != null)
    {
      for (GrantedObjectPriv priv : modifiedPrivs.getRemovedPrivs())
      {
        objectPrivSvc.revokeObjectPriv(svc, priv.getGranteeId(), priv
            .getObjectId(), priv.getObjectType(), priv.getOperation());
      }
    }
  }
}