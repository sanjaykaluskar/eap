package com.dombigroup.eap.app.webui.web.startup;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;

import com.dombigroup.eap.app.webui.common.SAPPConstants;
import com.dombigroup.eap.app.webui.common.SAPPGlobals;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.server.mgmt.impl.ConnectionMgmt;
import com.dombigroup.eap.server.mgmt.impl.Server;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtService;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

//TODO: Revisit later. Security HOLE!!!!
public class StartupServlet extends HttpServlet
{

  /**
	 * 
	 */
  private static final long serialVersionUID = -4694271297264424812L;

  private static Logger     logger           = Logger
                                                 .getLogger(StartupServlet.class
                                                     .getName());

  /**
   * productInstalled - Returns whether product is installed.
   * 
   * @param svcContext
   *          - service context
   * 
   * @return - true if product is installed else false
   */
  private boolean productInstalled(ServiceContext svcContext)
  {
    boolean ret = true;

    try
    {
      svcContext.startCall();
      UserMgmtService um = UserMgmtServiceFactory.getUserMgmtService();
      User u = um.getUser(svcContext, UserMgmtService.SU_USERID);
      svcContext.endCall(false);

      ret = (u != null);
    }
    catch (Exception e)
    {
      ret = false;
    }

    return ret;
  }

  @Override
  public void init()
  {
    ServletConfig cfg = getServletConfig();
    String cfgFile = cfg.getInitParameter(SAPPConstants.ATTR_CONFIG_FILE);
    String suOrgIdStr = cfg.getInitParameter(SAPPConstants.ATTR_SU_ORG_ID);
    int suOrgId = 0;
    try
    {
      suOrgId = Integer.parseInt(suOrgIdStr);
    }
    catch (Exception e)
    {
      logger.log(Level.WARNING, "Unable to parse SU org Id", e);
    }

    String suUserName = cfg.getInitParameter(SAPPConstants.ATTR_SU_USERNAME);
    String suUserPass = cfg.getInitParameter(SAPPConstants.ATTR_SU_PASSWORD);
    ConnectionMgmt cmgr;
    try
    {
      cmgr = Server.getServer().getConnectionManager();
      ServiceContext svcContext = cmgr.createConnection(suOrgId, suUserName,
          suUserPass);
      Server.getServer().readConfig(svcContext, cfgFile);
      Server.getServer().startup(svcContext);

      /* Install the product if not already installed. */
      if (!productInstalled(svcContext))
        Server.getServer().install(svcContext);
      cmgr.endConnection(svcContext);
    }
    catch (Exception e)
    {
      logger.log(Level.SEVERE, "Unable to startup SIM", e);
    }

    String fileSsaveLocationRoot = cfg
        .getInitParameter(SAPPConstants.ATTR_FILE_SAVE_LOCATION_ROOT);
    SAPPGlobals.INSTANCE.put(SAPPConstants.ATTR_FILE_SAVE_LOCATION_ROOT,
        fileSsaveLocationRoot);
    String simHomeDir = cfg.getInitParameter(SAPPConstants.ATTR_SAPP_HOME_DIR);
    SAPPGlobals.INSTANCE.put(SAPPConstants.ATTR_SAPP_HOME_DIR, simHomeDir);
  }
}
