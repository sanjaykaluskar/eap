/**
 * SAPPAsyncUITask.java
 */
package com.dombigroup.eap.app.webui.common;

/**
 * @author skaluska
 *
 */
import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.AbstractAsyncTask;

import com.dombigroup.eap.server.platform.context.AsyncOp;
import com.dombigroup.eap.server.platform.context.IOpNotification;

public class SAPPAsyncUITask extends AbstractAsyncTask implements
    IOpNotification
{
  private static final long serialVersionUID = -8571048653026717595L;
  private AsyncOp           asyncOp;
  @SuppressWarnings("unused")
  private APICallResponse   response;

  public SAPPAsyncUITask(AsyncOp asyncOp)
  {
    this.asyncOp = asyncOp;
    this.asyncOp.addObserver(this);
    this.response = getAPICallResponse();
  }

  @Override
  public void onAsyncOpChange(AsyncOp arg0)
  {
    notifyChanges();
  }

  @Override
  public String getMessage()
  {
    /* get task description */
    String desc = asyncOp.getDesc();
    if (desc == null)
      desc = "";

    /* if there is an error show the error else status message */
    String msg = this.asyncOp.getError();
    if ((msg == null) || (msg.length() == 0))
      msg = asyncOp.getStatusMessage();

    String returnMessage = desc;
    if ((msg != null) && (msg.length() > 0))
    {
      returnMessage = returnMessage + ": " + msg.trim();
    }

    return returnMessage;
  }

  @Override
  public State getState()
  {
    return State.valueOf(this.asyncOp.getState().name());
  }

  @Override
  public float getPercentCompleted()
  {
    return 100 * this.asyncOp.getCompletedFraction();
  }

  @Override
  public void cancel()
  {
    this.asyncOp.interrupt();
  }

}
