package com.dombigroup.eap.app.webui.api.admin;

import java.util.List;
import com.dombigroup.eap.app.common.exceptions.SappException;
import com.dombigroup.eap.app.webui.common.AbstractBeanFacade;
import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.BOMgmt;
import com.dombigroup.eap.server.platform.interfaces.CompanyMgmt;
import com.dombigroup.eap.server.platform.orgmgmt.CompanyMgmtFactory;

public class OrgFacade extends AbstractBeanFacade<Company>
{
  private static final String FACADE_NAME  = "org";

  @Override
  public Company getBean(ServiceContext svcContext,
      int beanId) throws DgException
  {
    if (beanId != Persisted.INVALID_ID)
    {
      CompanyMgmt beanSvc = CompanyMgmtFactory.getCompanyMgmtService();

      svcContext.startCall();
      Company bean = beanSvc.getCompany(svcContext, beanId);
      svcContext.endCall(false);
      return bean;
    }
    else
    {
      return null;
    }
  }

  @Override
  protected List<Company> getBeanList(ServiceContext svcContext,
      int orgId, int startRow, int pageSize) throws DgException
  {
    if (orgId != Persisted.INVALID_ID)
    {
      CompanyMgmt beanSvc = CompanyMgmtFactory.getCompanyMgmtService();

      svcContext.startCall();
      List<Company> beanList = beanSvc.getCompanyList(svcContext);
      
      /* TODO: paginate */

      svcContext.endCall(false);
      return beanList;
    }
    else
    {
      return null;
    }
  }

  @Override
  protected Company createBean(ServiceContext svcContext, Company bean)
      throws DgException
  {
    CompanyMgmt beanSvc = CompanyMgmtFactory.getCompanyMgmtService();

    svcContext.startTransaction();
    int beanId = beanSvc.createCompany(svcContext, bean);
    svcContext.commitTransaction();
    bean.setId(beanId);
    return bean;
  }

  @Override
  protected Company updateBean(ServiceContext svcContext, Company bean)
      throws DgException
  {
    CompanyMgmt beanSvc = CompanyMgmtFactory.getCompanyMgmtService();

    svcContext.startTransaction();
    beanSvc.updateCompany(svcContext, bean);
    svcContext.commitTransaction();
    return bean;
  }

  @Override
  protected void deleteBean(ServiceContext svcContext, int beanId)
      throws DgException
  {
    CompanyMgmt beanSvc = CompanyMgmtFactory.getCompanyMgmtService();

    svcContext.startTransaction();
    beanSvc.deleteCompany(svcContext, beanId);
    svcContext.commitTransaction();
  }

  @Override
  public String getName()
  {
    return FACADE_NAME;
  }

  @Override
  protected Class<Company> getBeanClass()
  {
    return Company.class;
  }

  @Override
  protected BOMgmt<Company> getBeanMgmtService() throws SappException
  {
    return null;
  }

  @Override
  protected void initNewBean(Company bean)
  {
  }
}