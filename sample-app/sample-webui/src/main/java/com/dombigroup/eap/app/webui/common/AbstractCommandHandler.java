/**
 * AbstractCommandHandler.java
 */
package com.dombigroup.eap.app.webui.common;

import com.dombigroup.eap.server.platform.context.ServiceContext;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.CallContext;
import jka.suifwk.api.CommandHandler;

/**
 * @author skaluska
 * 
 */
public abstract class AbstractCommandHandler implements CommandHandler
{
  @Override
  public APICallResponse executeCommand(String commandName,
      CallContext context)
  {
    APICallResponse r = null;
    try
    {
      r = internalCommand(context, SuiUtils.getServiceContext(context), commandName);
    }
    catch (Exception e)
    {
      throw SuiUtils.mapToSuiException(e);
    }
    finally
    {
      SuiUtils.cleanup(context);
    }
    
    return r;
  }

  abstract protected APICallResponse internalCommand(CallContext context,
      ServiceContext svc, String command) throws Exception;
}
