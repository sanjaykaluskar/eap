package com.dombigroup.eap.app.webui.api.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.CallContext;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.exception.SUIUserException;

import com.dombigroup.eap.app.common.exceptions.SappException;
import com.dombigroup.eap.app.webui.common.AbstractBeanFacade;
import com.dombigroup.eap.app.webui.common.SuiUtils;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.BOMgmt;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

public class UserFacade extends AbstractBeanFacade<User>
{
  private static final String FACADE_NAME   = "user";

  private static final String USER_PROPERTY = "bean";

  @Override
  public APICallResponse readInternal(CallContext context) throws Exception
  {

    ServiceContext svcContext = SuiUtils.getServiceContext(context);

    int orgId = SuiUtils.getOrgId(context);
    List<String> propsToFetch = APIUtils.getPayloadDataAsList(context,
        "fields", String.class, false);
    int userId = APIUtils.getPayloadDataAsInteger(context, "id", false, -1);

    User user = null;
    UserBean userBean = null;
    List<User> userList = null;
    List<UserBean> userBeanList = null;

    List<Map<String, Object>> lwObjList = new ArrayList<Map<String, Object>>();

    UserMgmt mgmtService = UserMgmtServiceFactory.getUserMgmtService();

    svcContext.startCall();
    if (userId > 0)
    {
      user = mgmtService.getUser(svcContext, userId);
      userBean = new UserBean(user);
      userBean.setGrantedRoles(mgmtService.getRoles(svcContext, userId));
      userBean.setGrantedPrivs(mgmtService.getObjectPrivList(svcContext, userId));
    }
    else
    {
      userList = mgmtService.getUserList(svcContext, orgId);
      userBeanList = new ArrayList<UserBean>();
      for (User u : userList)
      {
        userBean = new UserBean(u);
        userBean.setGrantedRoles(mgmtService.getRoles(svcContext, u.getId()));
        userBean.setGrantedPrivs(mgmtService.getObjectPrivList(svcContext, u.getId()));
        userBeanList.add(userBean);
      }
    }
    svcContext.endCall(false);

    if (userList != null)
    {
      for (UserBean u : userBeanList)
      {
        lwObjList.add(getLightWeightObject(u, propsToFetch));
      }
    }
    else if (user != null)
    {
      lwObjList.add(getLightWeightObject(userBean, propsToFetch));
    }
    return new APICallResponse(lwObjList);
  }

  @Override
  public APICallResponse createInternal(CallContext context)
  {
    return update(context);
  }

  @Override
  public APICallResponse updateInternal(CallContext context) throws Exception
  {
    ServiceContext svcContext = SuiUtils.getServiceContext(context);
    int orgId = SuiUtils.getOrgId(context);
    List<Integer> updatedUserIdList = new ArrayList<Integer>();
    UserMgmt mgmtService = UserMgmtServiceFactory.getUserMgmtService();

    Object dataMapObject = APIUtils
        .getPayloadData(context, "dataMap", true);

    if (dataMapObject != null)
    {
      if (dataMapObject instanceof List<?>)
      {
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> propMapList = (List<Map<String, Object>>) dataMapObject;
        for (Map<String, Object> propMap : propMapList)
        {
          User updatedUser = updateUserProperties(svcContext,
              mgmtService, propMap, orgId);
          if (updatedUser != null)
          {
            updatedUserIdList.add(updatedUser.getId());
          }
        }
      }
      else if (dataMapObject instanceof Map<?, ?>)
      {
        @SuppressWarnings("unchecked")
        Map<String, Object> propMap = (Map<String, Object>) dataMapObject;
        User updatedUser = updateUserProperties(svcContext,
            mgmtService, propMap, orgId);
        if (updatedUser != null)
        {
          updatedUserIdList.add(updatedUser.getId());
        }
      }
    }
    return new APICallResponse(updatedUserIdList);
  }

  @Override
  public APICallResponse deleteInternal(CallContext context) throws Exception
  {
    ServiceContext svcContext = SuiUtils.getServiceContext(context);
    UserMgmt cm = UserMgmtServiceFactory.getUserMgmtService();

    List<Integer> deletedUserIds = new ArrayList<Integer>();
    List<Integer> idList = APIUtils.getPayloadDataAsList(context, "id",
        Integer.class, true);

    for (int userId : idList)
    {
      User userToDelete = getUser(svcContext, cm, userId);
      svcContext.startTransaction();
      cm.deleteUser(svcContext, userToDelete);
      svcContext.commitTransaction();
      deletedUserIds.add(userId);
    }
    return new APICallResponse(deletedUserIds);
  }

  protected User getUser(ServiceContext svcContext, UserMgmt svc, int userId)
      throws MetadataException, ServiceException
  {
    if (userId > 0)
    {
      svcContext.startCall();
      User user = svc.getUser(svcContext, userId);
      svcContext.endCall(false);
      return user;
    }
    else
    {
      return null;
    }
  }

  protected User updateUserBean(ServiceContext svcContext, UserMgmt cm,
      UserBean updatedUserBean, int orgId) throws DgException
  {
    boolean isCreate = false;
    if (updatedUserBean != null)
    {
      if (updatedUserBean.getUser().getId() <= 0)
      {
        updatedUserBean.getUser().setId(Persisted.INVALID_ID);
        updatedUserBean.getUser().setOrgId(orgId);
        isCreate = true;
      }
    }
    else
    {
      throw new SUIRuntimeException(
          "Unable to update user info. Null user info returned");
    }

    if (isCreate)
    {
      createUser(svcContext, cm, updatedUserBean.getUser(),
          updatedUserBean.getAddedRoles());
    }
    else
    {
      updateUser(svcContext, cm, updatedUserBean.getUser(),
          updatedUserBean.getAddedRoles(), updatedUserBean.getRemovedRoles());
    }

    return updatedUserBean.getUser();
  }

  protected User createUser(ServiceContext svcContext, UserMgmt svc,
      User newUser, List<String> rolesToGrant) throws DgException
  {
    svcContext.startTransaction();
    int userId = svc.createUser(svcContext, newUser);
    svcContext.commitTransaction();

    svcContext.startTransaction();
    if (rolesToGrant != null)
    {
      for (String role : rolesToGrant)
        svc.addRole(svcContext, userId, role);
    }
    svcContext.commitTransaction();
    newUser.setId(userId);
    return newUser;
  }

  protected User updateUser(ServiceContext svcContext, UserMgmt svc,
      User updatedUser, List<String> rolesToGrant, List<String> rolesToRevoke)
      throws DgException
  {
    svcContext.startTransaction();
    svc.updateUser(svcContext, updatedUser);
    if (rolesToGrant != null)
    {
      for (String role : rolesToGrant)
        svc.addRole(svcContext, updatedUser.getId(), role);
    }
    if (rolesToRevoke != null)
    {
      for (String role : rolesToRevoke)
        svc.removeRole(svcContext, updatedUser.getId(), role);
    }
    svcContext.commitTransaction();
    return updatedUser;
  }

  protected User updateUserProperties(ServiceContext svcContext,
      UserMgmt svc, Map<String, Object> dataMap, int orgId)
      throws DgException
  {
    int userId = (Integer) dataMap.get("id");

    User user = null;
    UserBean userBean = null;

    if (userId != User.INVALID_ID)
    {
      user = getUser(svcContext, svc, userId);
      if (user == null)
      {
        throw new SUIUserException("Unable to find the user to update.");
      }
      userBean = new UserBean(user);
    }

    Iterator<String> dataMapKeys = dataMap.keySet().iterator();

    while (dataMapKeys.hasNext())
    {
      String key = dataMapKeys.next();
      String dataValue = dataMap.get(key).toString();
      switch (key)
      {
      case USER_PROPERTY:
        userBean = APIUtils.mergeOrCreateObjectFromSerializedData(
            dataValue, userBean, UserBean.class);
        updateUserBean(svcContext, svc, userBean, orgId);
        break;
      }
    }
    return userBean.getUser();
  }

  @Override
  public String getName()
  {
    return FACADE_NAME;
  }

  @Override
  protected Class<User> getBeanClass()
  {
    return User.class;
  }

  @Override
  protected BOMgmt<User> getBeanMgmtService() throws SappException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  protected void initNewBean(User bean)
  {
  }
}
