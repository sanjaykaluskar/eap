package com.dombigroup.eap.app.webui.api.admin;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonNode;

import com.dombigroup.eap.app.webui.common.SuiUtils;
import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.CompanyMgmt;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.orgmgmt.CompanyMgmtFactory;
import com.dombigroup.eap.server.platform.privmgmt.PlatformRole;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

import jka.suifwk.common.SUIConstants;
import jka.suifwk.web.rest.WebResponse;
import jka.suifwk.web.utils.WebUtils;

public class RegisterServlet extends HttpServlet
{

  /**
	 * 
	 */
  private static final long   serialVersionUID                           = 2201911013280894851L;

  private static final String USER_NAMESPACE_FIELD                       = "namespace";
  private static final String USER_NAME_FIELD                            = "userName";
  private static final String USER_PASSWORD_FIELD                        = "password";
  private static final String USER_EMAIL_FIELD                           = "email";

  private static Logger       logger                                     = Logger
                                                                             .getLogger(RegisterServlet.class
                                                                                 .getName());

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException
  {

    WebResponse webResponse = null;

    try
    {
      String namespace = null;
      String userName = null;
      String password = null;
      String email = null;

      JsonNode inputJsonPayload = WebUtils.getJsonPayload(request);
      JsonNode inputJsonData = null;
      if (inputJsonPayload != null)
      {
        inputJsonData = inputJsonPayload
            .get(SUIConstants.JSON_NODE_PAYLOAD_DATA);
      }
      if (inputJsonData != null)
      {
        namespace = inputJsonData.get(USER_NAMESPACE_FIELD) != null ? inputJsonData
            .get(USER_NAMESPACE_FIELD).getTextValue()
            : null;
        userName = inputJsonData.get(USER_NAME_FIELD) != null ? inputJsonData
            .get(USER_NAME_FIELD).getTextValue()
            : null;
        password = inputJsonData.get(USER_PASSWORD_FIELD) != null ? inputJsonData
            .get(USER_PASSWORD_FIELD).getTextValue()
            : null;
        email = inputJsonData.get(USER_EMAIL_FIELD) != null ? inputJsonData
            .get(USER_EMAIL_FIELD).getTextValue() : null;

        /* initialize user */
        User user = new User();
        user.setUserName(userName);
        user.setPassword(password);
        user.setEmail(email);
        user.setAddress(new Address());

        /* initialize company */
        Company org = new Company();
        org.setName(namespace);
        org.setAddress(new Address());

        CompanyMgmt om = CompanyMgmtFactory.getCompanyMgmtService();
        UserMgmt um = UserMgmtServiceFactory.getUserMgmtService();
        ServiceContext svcContext = ServiceContext.bootstrapServiceContext(true);

        svcContext.startTransaction();
        /* create company */
        int orgId = om.createCompany(svcContext, org);
        org.setId(orgId);

        /* create user */
        user.setOrgId(orgId);
        int userId = um.createUser(svcContext, user);
        user.setUserid(userId);
        svcContext.commitTransaction();

        /* grant ORG_ADMIN role to the user */
        svcContext.startTransaction();
        um.addRole(svcContext, userId, PlatformRole.ORG_ADMIN.getName());
        svcContext.commitTransaction();

        webResponse = new WebResponse(SUIConstants.STATUS_CODE_OK, null,
            inputJsonData);
      }
    }
    catch (Exception e)
    {
      logger.log(Level.SEVERE, "Register Exception", e);
      webResponse = WebUtils.generateInternalErrorResponse(SuiUtils.mapToSuiException(e));
    }

    WebUtils.writeWebResponse(response, webResponse, true);
  }
}
