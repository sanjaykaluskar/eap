package com.dombigroup.eap.app.webui.api.resource;

import java.io.File;

import jka.suifwk.api.CallContext;
import jka.suifwk.api.resource.AbstractResourceBeanFacade;

import com.dombigroup.eap.app.webui.common.SAPPConstants;
import com.dombigroup.eap.app.webui.common.SAPPGlobals;
import com.dombigroup.eap.app.webui.common.SuiUtils;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.context.SecurityContext;

public class ResourceBeanFacade extends AbstractResourceBeanFacade {

	@Override
	@Deprecated
	public String getFileStoreDirectory(CallContext context, FileVisibility visibility,
			String fileName) {
		return getFileStoreDirectory(context, visibility);
	}

	@Override
	public String getFileStoreDirectory(CallContext context,
			FileVisibility visibility) {
		ServiceContext svcContext = SuiUtils.getServiceContext(context);
		SecurityContext secContext = svcContext.getSecContext();
		int userId = secContext.getUserId();
		int orgId = secContext.getOrgId();
		
		String privateFileStore = SAPPGlobals.INSTANCE.get(SAPPConstants.ATTR_FILE_SAVE_LOCATION_ROOT) + File.separator + orgId + File.separator + userId;
		String publicFileStore = SAPPGlobals.INSTANCE.get(SAPPConstants.ATTR_FILE_SAVE_LOCATION_ROOT) + File.separator + orgId + File.separator + "public";
		
		if(visibility == null) {
			return privateFileStore;
		}

		if(visibility == BasicFileVisibility.PRIVATE) {
			return privateFileStore;
		}

		if(visibility == BasicFileVisibility.PUBLIC) {
			return publicFileStore;
		}		
		return null;
	}
}