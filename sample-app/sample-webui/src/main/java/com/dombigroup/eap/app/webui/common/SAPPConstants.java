package com.dombigroup.eap.app.webui.common;

public interface SAPPConstants
{
  public static final String ATTR_SERVICE_CONTEXT         = "ATTR_SERVICE_CONTEXT";
  public static final String ATTR_CONFIG_FILE             = "ATTR_CONFIG_FILE";
  public static final String ATTR_SU_USERNAME             = "ATTR_SU_USERNAME";
  public static final String ATTR_SU_PASSWORD             = "ATTR_SU_PASSWORD";
  public static final String ATTR_SU_ORG_ID               = "ATTR_SU_ORG_ID";
  public static final String ATTR_FILE_SAVE_LOCATION_ROOT = "ATTR_FILE_SAVE_LOCATION_ROOT";
  public static final String ATTR_SAPP_HOME_DIR            = "ATTR_SAPP_HOME_DIR";
}
