package com.dombigroup.eap.app.webui.api.admin;

import java.util.ArrayList;
import java.util.List;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.CallContext;

import com.dombigroup.eap.app.webui.common.AbstractCommandHandler;
import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.CompanyMgmt;
import com.dombigroup.eap.server.platform.orgmgmt.CompanyMgmtFactory;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyInfo;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyService;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyServiceFactory;

/**
 * @author skaluska
 * 
 */
public class OrgCommandHandler extends AbstractCommandHandler
{
  private static final String GET_POLICY_LIST    = "getPolicyList";
  private static final String SET_POLICY_ENABLED = "setPolicyEnabled";
  private static final String GET_ORG_LIST       = "getOrgList";

  /*
   * (non-Javadoc)
   * 
   * @see jka.suifwk.api.CommandHandler#getNamespace()
   */
  @Override
  public String getNamespace()
  {
    return "org";
  }

  private List<AuthorizationPolicyDetail> getPolicyDetailList(
      ServiceContext ctx, List<AuthorizationPolicyInfo> list)
  {
    List<AuthorizationPolicyDetail> ret = null;

    if (list != null)
    {
      SecurityContext secCtx = ctx.getSecContext();
      ret = new ArrayList<AuthorizationPolicyDetail>();
      for (AuthorizationPolicyInfo info : list)
      {
        AuthorizationPolicyDetail detail = new AuthorizationPolicyDetail(info,
            secCtx.isPolicyEnabled(info.getPolicyImpl()));
        ret.add(detail);
      }
    }

    return ret;
  }

  private boolean decodeBooleanString(String str)
  {
    boolean ret = false;

    if ((str != null) && (str.length() > 0))
    {
      if (str.equalsIgnoreCase("true"))
        ret = true;
    }

    return ret;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.informatica.mlp.common.AbstractCommandHandler#internalCommand(jka.suifwk
   * .api.CallContext,
   * com.dombigroup.eap.server.platform.context.ServiceContext,
   * java.lang.String)
   */
  @Override
  protected APICallResponse internalCommand(CallContext context,
      ServiceContext svc, String command) throws Exception
  {
    APICallResponse ret = null;
    List<AuthorizationPolicyInfo> policyList;
    CompanyMgmt cm;

    switch (command)
    {
    case GET_POLICY_LIST:
      AuthorizationPolicyService aps = AuthorizationPolicyServiceFactory
          .getAuthorizationPolicyService();
      svc.startCall();
      policyList = aps.getAllPolicies(svc);
      List<AuthorizationPolicyDetail> policyDetailList = getPolicyDetailList(
          svc, policyList);
      svc.endCall(false);

      ret = new APICallResponse(policyDetailList);
      break;
    case SET_POLICY_ENABLED:
      cm = CompanyMgmtFactory.getCompanyMgmtService();
      int orgId = svc.getSecContext().getOrgId();
      String policyName = APIUtils.getPayloadDataAsString(context,
          "policyName", true);
      String enabledString = APIUtils.getPayloadDataAsString(context,
          "enabledValue", true);
      boolean enabledValue = decodeBooleanString(enabledString);

      svc.startTransaction();
      if (enabledValue)
        cm.enableAuthorizationPolicy(svc, orgId, policyName);
      else
        cm.disableAuthorizationPolicy(svc, orgId, policyName);
      svc.commitTransaction();
      ret = new APICallResponse(enabledValue);
      break;
    case GET_ORG_LIST:
      cm = CompanyMgmtFactory.getCompanyMgmtService();
      svc.startCall();
      List<Company> orgList = cm.getCompanyList(svc);
      svc.endCall(false);

      ret = new APICallResponse(orgList);
      break;
    default:
      break;
    }

    return ret;
  }
}