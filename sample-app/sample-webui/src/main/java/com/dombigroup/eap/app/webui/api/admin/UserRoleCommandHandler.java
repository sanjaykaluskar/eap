package com.dombigroup.eap.app.webui.api.admin;

import java.util.List;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.CallContext;

import com.dombigroup.eap.app.webui.common.AbstractCommandHandler;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.IRole;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

/**
 * @author skaluska
 * 
 */
public class UserRoleCommandHandler extends AbstractCommandHandler
{
  /*
   * (non-Javadoc)
   * 
   * @see jka.suifwk.api.CommandHandler#getNamespace()
   */
  @Override
  public String getNamespace()
  {
    return "userRoles";
  }

  /* (non-Javadoc)
   * @see com.informatica.mlp.common.AbstractCommandHandler#internalCommand(jka.suifwk.api.CallContext, com.dombigroup.eap.server.platform.context.ServiceContext, java.lang.String)
   */
  @Override
  protected APICallResponse internalCommand(CallContext context,
      ServiceContext svc, String command) throws Exception
  {
    APICallResponse ret = null;
    UserMgmt userSvc = UserMgmtServiceFactory.getUserMgmtService();

    switch (command)
    {
    case "getGrantableRoles":
      svc.startCall();
      List<IRole> grantableRoles = userSvc.getGrantableRoles(svc);
      svc.endCall(false);
      ret = new APICallResponse(grantableRoles);
      break;
    default:
      break;
    }

    return ret;
  }
}