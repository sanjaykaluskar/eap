/**
 * AuthorizationPolicyDetail.java
 */
package com.dombigroup.eap.app.webui.api.admin;

import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyInfo;

/**
 * @author skaluska
 *
 */
public class AuthorizationPolicyDetail extends AuthorizationPolicyInfo
{
  private boolean controllable;
  private boolean enabled;

  /**
   * Constructor for AuthorizationPolicyDetail
   */
  public AuthorizationPolicyDetail(AuthorizationPolicyInfo info, boolean isEnabled)
  {
    setName(info.getName());
    setDesc(info.getDesc());
    controllable = info.isOrgLevelControl();
    enabled = isEnabled;
  }

  /**
   * @return the controllable
   */
  public boolean isControllable()
  {
    return controllable;
  }

  /**
   * @return the enabled
   */
  public boolean isEnabled()
  {
    return enabled;
  }
}
