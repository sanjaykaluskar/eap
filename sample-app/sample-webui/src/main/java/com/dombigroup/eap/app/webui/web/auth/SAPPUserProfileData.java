/**
 * SAPPUserProfileData.java
 */
package com.dombigroup.eap.app.webui.web.auth;

import java.util.List;

import jka.suifwk.web.security.UserProfileData;

/**
 * @author skaluska
 *
 */
public class SAPPUserProfileData extends UserProfileData
{
  /** MLPUserProfileData.java */
  private static final long serialVersionUID = -5560254476893559455L;

  private boolean isOrgAdmin;

  private boolean isSiteAdmin;

  /**
   * Constructor for SAPPUserProfileData
   */
  public SAPPUserProfileData(String userName, String userFirstName,
      String userLastName, String userEmail, UserNameSpace userNameSpace,
      String currentRole, List<String> userPrivileges, List<String> userRoles,
      boolean orgAdmin, boolean siteAdmin)
  {
    super(userName, userFirstName, userLastName, userEmail, userNameSpace,
        currentRole, userPrivileges, userRoles, siteAdmin);
    isOrgAdmin = orgAdmin;
    isSiteAdmin = siteAdmin;
  }

  /**
   * @return the orgAdmin
   */
  public boolean isOrgAdmin()
  {
    return isOrgAdmin;
  }

  /**
   * @return the siteAdmin
   */
  public boolean isSiteAdmin()
  {
    return isSiteAdmin;
  }
}
