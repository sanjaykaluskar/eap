package com.dombigroup.eap.app.webui.api.contact;

import com.dombigroup.eap.app.backend.contact.ContactMgmtFactory;
import com.dombigroup.eap.app.common.datamodels.Contact;
import com.dombigroup.eap.app.common.exceptions.SappException;
import com.dombigroup.eap.app.webui.common.AbstractBeanFacade;
import com.dombigroup.eap.server.platform.interfaces.BOMgmt;

public class ContactFacade extends
    AbstractBeanFacade<Contact>
{
  private static final String FACADE_NAME = "contact";

  @Override
  public String getName()
  {
    return FACADE_NAME;
  }

  @Override
  protected Class<Contact> getBeanClass()
  {
    return Contact.class;
  }

  @Override
  protected BOMgmt<Contact> getBeanMgmtService() throws SappException
  {
    return ContactMgmtFactory.getContactMgmtService();
  }

  @Override
  protected void initNewBean(Contact bean)
  {
  }
}