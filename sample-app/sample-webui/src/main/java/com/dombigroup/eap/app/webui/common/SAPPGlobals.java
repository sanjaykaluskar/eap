package com.dombigroup.eap.app.webui.common;

import java.util.HashMap;
import java.util.Map;

public enum SAPPGlobals
{
  INSTANCE;

  Map<String, Object> globalObjects = new HashMap<String, Object>();

  public void put(String key, Object value)
  {
    globalObjects.put(key, value);
  }

  public Object get(String key)
  {
    return globalObjects.get(key);
  }

  public Object remove(String key)
  {
    return globalObjects.remove(key);
  }

  public boolean containsKey(String key)
  {
    return globalObjects.containsKey(key);
  }
}
