/**
 * Utils.java
 */
package com.dombigroup.eap.app.webui.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import jka.suifwk.api.CallContext;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.exception.SUIUserException;
import jka.suifwk.web.security.UserProfileData;

import com.dombigroup.eap.app.common.utils.ExceptionUtils;
import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.context.ServiceContext;

/**
 * @author skaluska
 * 
 */
public class SuiUtils
{
  public static RuntimeException mapToSuiException(Exception simException)
  {
    Throwable firstUserError = ExceptionUtils.userReportableError(simException);

    RuntimeException suiException = null;
    if (firstUserError == null)
      suiException = new SUIRuntimeException(simException);
    else
      suiException = new SUIUserException(firstUserError.getMessage(),
          simException);

    return suiException;
  }

  public static UserProfileData getUserData(CallContext context)
  {
    Object userData = context.getContextParam(SUIConstants.ATTR_USER_DATA);
    if ((userData != null && userData instanceof UserProfileData))
    {
      return (UserProfileData) userData;
    }
    return null;
  }

  public static int getOrgId(CallContext context)
  {
    UserProfileData upd = getUserData(context);
    if (upd != null)
    {
      return upd.getUserNameSpace().getNamespaceId();
    }
    return -1;
  }

  public static void cleanup(CallContext context)
  {
    try
    {
      ServiceContext ctx = getServiceContext(context);
      if (ctx != null)
        ctx.resetWorkUnit();
    }
    catch (DgException e1)
    {
      throw new SUIRuntimeException(e1);
    }
  }

  public static ServiceContext getServiceContext(CallContext context)
  {
    Object svcContext = context
        .getContextParam(SAPPConstants.ATTR_SERVICE_CONTEXT);
    if ((svcContext != null && svcContext instanceof ServiceContext))
    {
      return (ServiceContext) svcContext;
    }
    return null;
  }

  public static Map<String, Object> constructLightWeightObject(Object bean,
      List<String> params)
  {
    Map<String, Object> lwObj = new HashMap<String, Object>();
    if (params != null)
    {
      for (String propName : params)
      {
        if (propName == null || propName.trim().length() == 0)
        {
          continue;
        }
        try
        {
          lwObj.put(propName, PropertyUtils.getProperty(bean, propName));
        }
        catch (Exception e)
        {
          lwObj.put(propName, "undefined");
          // TODO:: handle later
        }
      }
    }
    return lwObj;
  }

  public static Map<String, Object> constructLightWeightObject(Object bean,
      String name)
  {
    Map<String, Object> lwObj = new HashMap<String, Object>();
    try
    {
      lwObj.put(name, bean);
    }
    catch (Exception e)
    {
      lwObj.put(name, "undefined");
      // TODO:: handle later
    }
    return lwObj;
  }
}
