package com.dombigroup.eap.app.webui.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.BOMgmt;
import com.dombigroup.eap.app.common.exceptions.SappException;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.BeanFacade;
import jka.suifwk.api.CallContext;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.exception.SUIUserException;

public abstract class AbstractBeanFacade<BeanType extends Persisted> implements
    BeanFacade
{
  private static List<String> defaultLightWeightObjectParams = null;
  private static final String BEAN_PROPERTY_CLS              = "bean";
  private static Logger       logger                         = LoggerFactory
                                                                 .getLogger(AbstractBeanFacade.class
                                                                     .getName());

  static
  {
    defaultLightWeightObjectParams = new ArrayList<String>();
    defaultLightWeightObjectParams.add("pObjInfo");
  }

  @Override
  public APICallResponse create(CallContext context)
  {
    APICallResponse r = null;
    try
    {
      logger.debug("Start create - class:{}", getBeanClass().getSimpleName());
      r = createInternal(context);
      logger.debug("End create");
    }
    catch (Exception e)
    {
      throw SuiUtils.mapToSuiException(e);
    }
    finally
    {
      SuiUtils.cleanup(context);
    }
    return r;
  }

  @Override
  public APICallResponse read(CallContext context)
  {
    APICallResponse r = null;
    try
    {
      logger.debug("Start read - class:{}", getBeanClass().getSimpleName());
      r = readInternal(context);
      logger.debug("End read");
    }
    catch (Exception e)
    {
      throw SuiUtils.mapToSuiException(e);
    }
    finally
    {
      SuiUtils.cleanup(context);
    }
    return r;
  }

  @Override
  public APICallResponse update(CallContext context)
  {
    APICallResponse r = null;
    try
    {
      logger.debug("Start update - class: {}", getBeanClass().getSimpleName());
      r = updateInternal(context);
      logger.debug("End update");
    }
    catch (Exception e)
    {
      throw SuiUtils.mapToSuiException(e);
    }
    finally
    {
      SuiUtils.cleanup(context);
    }
    return r;
  }

  @Override
  public APICallResponse delete(CallContext context)
  {
    APICallResponse r = null;
    try
    {
      logger.debug("Start delete - class: {}", getBeanClass().getSimpleName());
      r = deleteInternal(context);
      logger.debug("End delete");
    }
    catch (Exception e)
    {
      throw SuiUtils.mapToSuiException(e);
    }
    finally
    {
      SuiUtils.cleanup(context);
    }
    return r;
  }

  public Map<String, Object> getLightWeightObject(Object bean,
      List<String> params)
  {
    List<String> allParams = new ArrayList<String>();
    allParams.addAll(getDefaultLightWeightObjectAttributes());
    if (params != null)
    {
      allParams.addAll(params);
    }
    return APIUtils.getLightWeightObject(bean, allParams);
  }

  public Object convertToSerializableFormat(Object srcObject)
  {
    return srcObject;
  }

  public List<String> getDefaultLightWeightObjectAttributes()
  {
    return Collections.unmodifiableList(defaultLightWeightObjectParams);
  }

  protected APICallResponse readInternal(CallContext context) throws Exception
  {
    ServiceContext svcContext = SuiUtils.getServiceContext(context);
    int orgId = SuiUtils.getOrgId(context);
    List<String> propsToFetch = APIUtils.getPayloadDataAsList(context,
        "fields", String.class, false);
    int beanId = APIUtils.getPayloadDataAsInteger(context, "id",
        false, -1);
    int startRow = APIUtils.getPayloadDataAsInteger(context, "startRow",
        false, -1);
    int pageSize = APIUtils.getPayloadDataAsInteger(context, "pageSize",
        false, -1);

    List<Map<String, Object>> lwObjList = new ArrayList<Map<String, Object>>();
    List<BeanType> beanList = null;
    BeanType bean = null;
    if (beanId >= 0)
    {
      logger.debug("Getting bean - class:{}, id:{}", getBeanClass()
          .getSimpleName(), beanId);
      bean = getBean(svcContext, beanId);
    }
    else
    {
      logger.debug("Getting bean list - class:{}, orgId:{}", getBeanClass()
          .getSimpleName(), orgId);
      beanList = getBeanList(svcContext, orgId, startRow, pageSize);
    }

    if (beanList != null)
    {
      for (BeanType tc : beanList)
      {
        lwObjList.add(getLightWeightObject(tc, propsToFetch));
      }
    }
    else if (bean != null)
    {
      lwObjList.add(getLightWeightObject(bean, propsToFetch));
    }
    return new APICallResponse(lwObjList);
  }

  protected APICallResponse createInternal(CallContext context) throws Exception
  {
    return update(context);
  }

  protected APICallResponse updateInternal(CallContext context) throws Exception
  {
    ServiceContext svcContext = SuiUtils.getServiceContext(context);
    List<Integer> updatedBeanIdList = new ArrayList<Integer>();

    Object dataMapObject = APIUtils
        .getPayloadData(context, "dataMap", true);

    if (dataMapObject != null)
    {
      if (dataMapObject instanceof List<?>)
      {
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> propMapList = (List<Map<String, Object>>) dataMapObject;
        for (Map<String, Object> propMap : propMapList)
        {
          BeanType updatedBean = updateBeanProperties(svcContext, propMap);
          if (updatedBean != null)
          {
            updatedBeanIdList.add(updatedBean.getId());
            logger.debug("Updated bean - class:{}, id:{}", getBeanClass()
                .getSimpleName(), updatedBean.getId());
          }
        }
      }
      else if (dataMapObject instanceof Map<?, ?>)
      {
        @SuppressWarnings("unchecked")
        Map<String, Object> propMap = (Map<String, Object>) dataMapObject;
        BeanType updatedBean = updateBeanProperties(svcContext, propMap);
        if (updatedBean != null)
        {
          updatedBeanIdList.add(updatedBean.getId());
          logger.debug("Updated bean - class:{}, id:{}", getBeanClass()
              .getSimpleName(), updatedBean.getId());
        }
      }
    }
    return new APICallResponse(updatedBeanIdList);
  }

  protected APICallResponse deleteInternal(CallContext context) throws Exception
  {
    ServiceContext svcContext = SuiUtils.getServiceContext(context);
    List<Integer> deletedBeansIds = new ArrayList<Integer>();
    List<Integer> idList = APIUtils.getPayloadDataAsList(context, "id",
        Integer.class, true);

    for (int beanId : idList)
    {
      deleteBean(svcContext, beanId);

      logger.debug("Deleted bean - class:{}, id:{}", getBeanClass()
          .getSimpleName(), beanId);

      deletedBeansIds.add(beanId);
    }

    return new APICallResponse(deletedBeansIds);
  }

  private BeanType updateBeanProperties(ServiceContext svcContext,
      Map<String, Object> dataMap)
      throws DgException
  {
    int beanId = (Integer) dataMap.get("id");

    BeanType bean = null;

    if (beanId != Persisted.INVALID_ID)
    {
      bean = getBean(svcContext, beanId);
      if (bean == null)
      {
        throw new SUIUserException("Unable to find the bean to update.");
      }
    }

    Iterator<String> dataMapKeys = dataMap.keySet().iterator();

    while (dataMapKeys.hasNext())
    {
      String key = dataMapKeys.next();
      String dataValue = dataMap.get(key).toString();
      logger.trace("dataMap contents - key:{}, value:{}", key, dataValue);
      switch (key)
      {
      case BEAN_PROPERTY_CLS:
        bean = APIUtils.mergeOrCreateObjectFromSerializedData(
            dataValue, bean, getBeanClass());
        createOrUpdateBean(svcContext, bean);
        break;
      }
    }
    return bean;
  }

  protected BeanType getBean(ServiceContext svcContext,
      int beanId) throws DgException
  {
    if (beanId > Persisted.INVALID_ID)
    {
      BOMgmt<BeanType> beanSvc = getBeanMgmtService();

      svcContext.startCall();
      BeanType bean = beanSvc.getBOById(svcContext, beanId);
      svcContext.endCall(false);
      return bean;
    }
    else
    {
      logger.debug("Invalid beanId:{}, class:{}", Persisted.INVALID_ID,
          getBeanClass().getSimpleName());
      return null;
    }
  }

  protected List<BeanType> getBeanList(ServiceContext svcContext,
      int orgId, int startRow, int pageSize) throws DgException
  {
    if (orgId != Persisted.INVALID_ID)
    {
      List<BeanType> beanList = null;
      BOMgmt<BeanType> beanSvc = getBeanMgmtService();

      svcContext.startCall();
      if ((startRow != -1) || (pageSize != -1))
        beanList = beanSvc.getBOPageForOrg(svcContext, orgId, startRow,
            pageSize);
      else
        beanList = beanSvc.getBOListForOrg(svcContext, orgId);
      svcContext.endCall(false);
      return beanList;
    }
    else
    {
      logger.debug("Invalid orgId:{}, class:{}", Persisted.INVALID_ID,
          getBeanClass().getSimpleName());
      return null;
    }
  }

  protected BeanType createOrUpdateBean(ServiceContext svcContext,
      BeanType updatedBean) throws DgException
  {
    boolean isCreate = false;
    if (updatedBean != null)
    {
      if (updatedBean.getId() <= 0)
      {
        updatedBean.setId(Persisted.INVALID_ID);
        updatedBean.setOrgId(svcContext.getSecContext().getOrgId());
        isCreate = true;
      }
      logger.trace("Updated bean set up - id:{}, isCreate:{}", updatedBean
          .getId(), isCreate);
    }
    else
    {
      throw new SUIRuntimeException(
          "Unable to update user info. Null buyer info returned");
    }

    if (isCreate)
    {
      createBean(svcContext, updatedBean);
    }
    else
    {
      updateBean(svcContext, updatedBean);
    }

    return updatedBean;
  }

  protected BeanType createBean(ServiceContext svcContext, BeanType bean)
      throws DgException
  {
    BOMgmt<BeanType> beanSvc = getBeanMgmtService();

    svcContext.startTransaction();
    bean.setId(Persisted.INVALID_ID);
    bean.setOrgId(svcContext.getSecContext().getOrgId());
    initNewBean(bean);
    int objectId = beanSvc.createBO(svcContext, bean);
    svcContext.commitTransaction();
    logger.trace("Created bean - class:{}, id:{}", getBeanClass()
        .getSimpleName(), objectId);
    bean.setId(objectId);
    return bean;
  }

  protected BeanType updateBean(ServiceContext svcContext, BeanType bean)
      throws DgException
  {
    BOMgmt<BeanType> beanSvc = getBeanMgmtService();

    svcContext.startTransaction();
    beanSvc.updateBO(svcContext, bean);
    svcContext.commitTransaction();
    logger.trace("Updated bean - class:{}, id:{}", getBeanClass()
        .getSimpleName(), bean.getId());
    return bean;
  }

  protected void deleteBean(ServiceContext svcContext, int beanId)
      throws DgException
  {
    BOMgmt<BeanType> beanSvc = getBeanMgmtService();

    svcContext.startTransaction();
    beanSvc.deleteBO(svcContext, beanId);
    svcContext.commitTransaction();
    logger.trace("Deleted bean - class:{}, id:{}", getBeanClass()
        .getSimpleName(), beanId);
  }

  abstract protected Class<BeanType> getBeanClass();

  abstract protected BOMgmt<BeanType> getBeanMgmtService() throws SappException;

  abstract protected void initNewBean(BeanType bean);
}