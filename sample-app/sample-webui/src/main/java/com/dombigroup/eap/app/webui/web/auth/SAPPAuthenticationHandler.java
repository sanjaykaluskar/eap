package com.dombigroup.eap.app.webui.web.auth;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIUserException;
import jka.suifwk.web.auth.ApplicationAuthenticationHandler;
import jka.suifwk.web.rest.WebResponse;
import jka.suifwk.web.security.UserProfileData.UserNameSpace;
import jka.suifwk.web.session.SessionManager;
import jka.suifwk.web.utils.WebUtils;

import org.codehaus.jackson.JsonNode;

import com.dombigroup.eap.app.webui.common.SAPPConstants;
import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.server.mgmt.impl.ConnectionMgmt;
import com.dombigroup.eap.server.mgmt.impl.Server;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.CompanyMgmt;
import com.dombigroup.eap.server.platform.orgmgmt.CompanyMgmtFactory;
import com.dombigroup.eap.server.platform.privmgmt.PlatformRole;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtService;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

public class SAPPAuthenticationHandler implements
    ApplicationAuthenticationHandler
{

  private static Logger       logger               = Logger
                                                       .getLogger(SAPPAuthenticationHandler.class
                                                           .getName());

  private static final String USER_NAME_FIELD      = "username";
  private static final String USER_PASSWORD_FIELD  = "password";
  private static final String USER_NAMESPACE_FIELD = "namespace";

  @Override
  public void init()
  {
  }

  @Override
  public WebResponse login(HttpServletRequest request)
  {
    logger.log(Level.INFO, "Authenticating user.");

    String userName = null;
    String password = null;
    String namespace = null;

    try
    {
      JsonNode inputJsonPayload = WebUtils.getJsonPayload(request);
      JsonNode inputJsonData = null;
      if (inputJsonPayload != null)
      {
        inputJsonData = inputJsonPayload
            .get(SUIConstants.JSON_NODE_PAYLOAD_DATA);
      }
      if (inputJsonData != null)
      {
        userName = inputJsonData.get(USER_NAME_FIELD) != null ? inputJsonData
            .get(USER_NAME_FIELD).getTextValue() : null;
        password = inputJsonData.get(USER_PASSWORD_FIELD) != null ? inputJsonData
            .get(USER_PASSWORD_FIELD).getTextValue()
            : null;
        namespace = inputJsonData.get(USER_NAMESPACE_FIELD) != null ? inputJsonData
            .get(USER_NAMESPACE_FIELD).getTextValue()
            : null;
      }
    }
    catch (Exception e)
    {
      logger.log(Level.SEVERE, "Login Exception", e);
      return WebUtils.generateInternalErrorResponse(e);
    }

    HttpSession session = SessionManager.INSTANCE
        .getSession(request, false);
    if (session != null)
    {
      session.invalidate();
      logger.log(Level.INFO, "Invalidated existing session.");
    }

    SAPPUserProfileData userData = null;

    try
    {
      ConnectionMgmt cmgr = Server.getServer().getConnectionManager();
      ServiceContext svcContext = null;
      int namespaceId = -1;
      Company org = null;
      try
      {
        namespaceId = Integer.parseInt(namespace);
        svcContext = cmgr.createConnection(namespaceId, userName,
            password);
      }
      catch (NumberFormatException nfe)
      {
        svcContext = cmgr.createConnection(namespace, userName,
            password);
        if (svcContext == null)
        {
          throw new SUIUserException("SIM not available.");
        }
      }

      if (svcContext == null)
      {
        throw new SUIUserException("SIM not available.");
      }

      int userId = svcContext.getSecContext().getUserId();

      svcContext.startCall();
      /* get user and org info */
      UserMgmtService um = UserMgmtServiceFactory.getUserMgmtService();
      User u = um.getUser(svcContext, userId);
      CompanyMgmt om = CompanyMgmtFactory.getCompanyMgmtService();
      if (om != null)
      {
        org = om.getCompany(svcContext, svcContext.getSecContext().getOrgId());
      }
      svcContext.endCall(false);

      UserNameSpace uns = null;
      if (org != null)
      {
        uns = new UserNameSpace(org.getOrgId(), org.getName());
      }
      else
      {
        uns = new UserNameSpace(u.getOrgId(), namespace);
      }

      userData = new SAPPUserProfileData(userName, u.getFirstName(),
          u.getLastName(), u.getEmail(), uns, null, null, null,
          svcContext.getSecContext().hasRole(PlatformRole.ORG_ADMIN),
          svcContext.getSecContext().hasRole(PlatformRole.SITE_ADMIN));

      // Create a new session
      session = SessionManager.INSTANCE.getSession(request, true);
      if (session != null && session.isNew())
      {
        session.setAttribute(SAPPConstants.ATTR_SERVICE_CONTEXT,
            svcContext);
        return new WebResponse(
            SUIConstants.STATUS_CODE_OK, null,
            userData);
      }
      else
      {
        return new WebResponse(
            SUIConstants.STATUS_CODE_LOGIN_ERROR_INVALID_CREDENTIALS,
            "");
      }
    }
    catch (Exception e)
    {
      logger.log(Level.SEVERE, "Login Exception", e);
      return new WebResponse(
          SUIConstants.STATUS_CODE_LOGIN_ERROR_INVALID_CREDENTIALS,
          e.getMessage());
    }
  }

  @Override
  public WebResponse logout(HttpServletRequest request)
  {
    logger.log(Level.INFO, "Logging out user.");
    // return authHandler.logout(request);
    HttpSession session = SessionManager.INSTANCE
        .getSession(request, false);
    String logoutStatusMsg = "";
    int logoutStatusCode = -1;
    if (session != null)
    {
      try
      {
        ConnectionMgmt cmgr = Server.getServer().getConnectionManager();
        Object svcContext = session
            .getAttribute(SAPPConstants.ATTR_SERVICE_CONTEXT);
        if (svcContext != null && svcContext instanceof ServiceContext)
        {
          cmgr.endConnection((ServiceContext) svcContext);
        }
        logoutStatusCode = SUIConstants.STATUS_CODE_OK;
      }
      catch (Exception e)
      {
        logger.log(Level.WARNING, "Logout cleanup resource exception",
            e);
        logoutStatusMsg = e.getMessage();
        logoutStatusCode = SUIConstants.STATUS_CODE_LOGOUT_ERROR_CANNOT_CLEANUP_RESOURCES;
      }
      session.invalidate();
      logger.log(Level.INFO, "Invalidated existing session.");
      return new WebResponse(logoutStatusCode, logoutStatusMsg);
    }
    else
    {
      logger.log(Level.WARNING, "No session found during logout.");
      return new WebResponse(SUIConstants.STATUS_CODE_OK, "");
    }
  }
}