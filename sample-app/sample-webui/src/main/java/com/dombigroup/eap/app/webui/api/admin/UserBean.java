/**
 * UIUser.java
 */
package com.dombigroup.eap.app.webui.api.admin;

import java.util.List;

import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;
import com.dombigroup.eap.common.datamodels.GrantedRole;
import com.dombigroup.eap.common.datamodels.User;

/**
 * @author skaluska
 * 
 *         User information exchanged with the UI. It contains info aggregated
 *         from multiple objects.
 * 
 */
public class UserBean
{
  private User                    user;
  private List<GrantedRole>       grantedRoles;
  private List<String>            addedRoles;
  private List<String>            removedRoles;
  private List<GrantedObjectPriv> grantedPrivs;

  public UserBean()
  {
  }

  /**
   * Constructor for UIUser
   */
  public UserBean(User u)
  {
    user = u;
  }

  /**
   * @return the user
   */
  public User getUser()
  {
    return user;
  }

  /**
   * @param user
   *          the user to set
   */
  public void setUser(User user)
  {
    this.user = user;
  }

  /**
   * @return the grantedRoles
   */
  public List<GrantedRole> getGrantedRoles()
  {
    return grantedRoles;
  }

  /**
   * @param grantedRoles
   *          the grantedRoles to set
   */
  public void setGrantedRoles(List<GrantedRole> grantedRoles)
  {
    this.grantedRoles = grantedRoles;
  }

  /**
   * @return the addedRoles
   */
  public List<String> getAddedRoles()
  {
    return addedRoles;
  }

  /**
   * @param addedRoles
   *          the addedRoles to set
   */
  public void setAddedRoles(List<String> addedRoles)
  {
    this.addedRoles = addedRoles;
  }

  /**
   * @return the removedRoles
   */
  public List<String> getRemovedRoles()
  {
    return removedRoles;
  }

  /**
   * @param removedRoles
   *          the removedRoles to set
   */
  public void setRemovedRoles(List<String> removedRoles)
  {
    this.removedRoles = removedRoles;
  }

  /**
   * @return the grantedPrivs
   */
  public List<GrantedObjectPriv> getGrantedPrivs()
  {
    return grantedPrivs;
  }

  /**
   * @param grantedPrivs the grantedPrivs to set
   */
  public void setGrantedPrivs(List<GrantedObjectPriv> grantedPrivs)
  {
    this.grantedPrivs = grantedPrivs;
  }
}
