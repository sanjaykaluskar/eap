package com.acme.app.web.auth;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jka.suifwk.common.SUIConstants;
import jka.suifwk.web.auth.ApplicationAuthenticationHandler;
import jka.suifwk.web.rest.WebResponse;
import jka.suifwk.web.security.UserProfileData;
import jka.suifwk.web.session.SessionManager;
import jka.suifwk.web.utils.WebUtils;

public class AppAuthenticationHandler implements
		ApplicationAuthenticationHandler {

	private static Logger logger = Logger
			.getLogger(AppAuthenticationHandler.class.getName());

	private static final String USER_NAME_FIELD = "username";
	private static final String USER_PASSWORD_FIELD = "password";

	@Override
	public void init() {
	}

	@Override
	public WebResponse login(HttpServletRequest request) {
		logger.log(Level.INFO, "Authenticating user.");

		String userName = null;
		String password = null;

		try {
			Map<String, String> reqDataMap = WebUtils.getRequestDataMap(request); 
			userName = reqDataMap.get(USER_NAME_FIELD);
			password = reqDataMap.get(USER_PASSWORD_FIELD);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Login Exception", e);
			return WebUtils.generateInternalErrorResponse(e);
		}

		HttpSession session = SessionManager.INSTANCE
				.getSession(request, false);
		if (session != null) {
			session.invalidate();
			logger.log(Level.INFO, "Invalidated existing session.");
		}

		UserProfileData userData = null;

		try {
			if (userName != null && password != null && userName.equals("admin") && password.equals("admin")) {
				userData = new UserProfileData(userName, "Administrator", "", userName
						+ "@acme.com", null, null, null, null, true);

				// Create a new session
				session = SessionManager.INSTANCE.getSession(request, true);
				if (session != null && session.isNew()) {
					return new WebResponse(
							SUIConstants.STATUS_CODE_OK, null,
							userData);
				} else {
					return new WebResponse(
							SUIConstants.STATUS_CODE_LOGIN_ERROR_INVALID_CREDENTIALS,
							"");
				}
			} else {
				throw new RuntimeException(
						"Invalid user name and password. Login using 'admin', 'admin'.");
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Login Exception", e);
			return new WebResponse(
					SUIConstants.STATUS_CODE_LOGIN_ERROR_INVALID_CREDENTIALS,
					e.getMessage());
		}
	}

	@Override
	public WebResponse logout(HttpServletRequest request) {
		logger.log(Level.INFO, "Logging out user.");
		HttpSession session = SessionManager.INSTANCE
				.getSession(request, false);
		String logoutStatusMsg = "";
		int logoutStatusCode = -1;
		if (session != null) {
			try {
				logoutStatusCode = SUIConstants.STATUS_CODE_OK;
			} catch (Exception e) {
				logger.log(Level.WARNING, "Logout cleanup resource exception",
						e);
				logoutStatusMsg = e.getMessage();
				logoutStatusCode = SUIConstants.STATUS_CODE_LOGOUT_ERROR_CANNOT_CLEANUP_RESOURCES;
			}
			session.invalidate();
			logger.log(Level.INFO, "Invalidated existing session.");
			return new WebResponse(logoutStatusCode, logoutStatusMsg);
		} else {
			logger.log(Level.WARNING, "No session found during logout.");
			return new WebResponse(SUIConstants.STATUS_CODE_OK, "");
		}
	}
}