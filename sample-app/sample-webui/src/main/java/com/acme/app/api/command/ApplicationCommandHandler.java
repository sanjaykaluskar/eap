package com.acme.app.api.command;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.AsyncTask.State;
import jka.suifwk.api.CallContext;
import jka.suifwk.api.CommandHandler;
import jka.suifwk.api.DefaultAsyncTask;
import jka.suifwk.common.SUIConstants;

public class ApplicationCommandHandler implements CommandHandler {

	ExecutorService ex;

	@Override
	public String getNamespace() {
		return "employee";
	}

	@Override
	public APICallResponse executeCommand(String commandName,
			CallContext context) {
		switch (commandName) {
		case "longRunningCmd":
			return simulateLongRunningCommand(context);
		default:
			return null;
		}
	}

	private APICallResponse simulateLongRunningCommand(CallContext context) {
		final Integer count = APIUtils.getPayloadDataAsInteger(context,
				"count", false, 5);

		ex = Executors.newFixedThreadPool(1);
		final APICallResponse response = new APICallResponse(
				SUIConstants.STATUS_CODE_OK, "Long running operation started");
		final DefaultAsyncTask asyncTask = new DefaultAsyncTask();

		FutureTask f = new FutureTask(new Runnable() {
			public void run() {
				for (int i = 0; i < count; i++) {
					try {
						Thread.sleep(1000);
						asyncTask.setPercentCompleted((100 * (i + 1)) / count);
						asyncTask.setMessage("Completed : " + i);
						asyncTask.notifyChanges();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						asyncTask.setState(State.CANCELLED);
						return;
					}
				}
				asyncTask.setPercentCompleted(100);
				asyncTask.setState(State.COMPLETED);
				asyncTask.notifyChanges();
			}
		}, null);

		asyncTask.setMessage("Starting long running command.");
		asyncTask.setPercentCompleted(0);
		asyncTask.setState(State.CREATED);
		asyncTask.setFuture(f);
		response.attachToLongRunningTask(context, asyncTask);
		asyncTask.notifyChanges();
		ex.execute(f);

		asyncTask.setState(State.STARTED);
		asyncTask.notifyChanges();

		ex.shutdown();

		return response;
	}
}
