package com.acme.app.api.resource;

import jka.suifwk.api.CallContext;
import jka.suifwk.api.resource.AbstractResourceBeanFacade;


public class ResourceBeanFacade extends AbstractResourceBeanFacade {

	private static final String FILE_STORE_LOCATION_PRIVATE = "E:\\Test\\FileUpload\\admin";
	private static final String FILE_STORE_LOCATION_PUBLIC = "E:\\Test\\FileUpload\\public";
	
	@Override
	public String getFileStoreDirectory(CallContext context, FileVisibility visibility,
			String fileName) {		
		return getFileStoreDirectory(context, visibility);		
	}

	@Override
	public String getFileStoreDirectory(CallContext context,
			FileVisibility visibility) {
		if(visibility == null) {
			return FILE_STORE_LOCATION_PRIVATE;
		}
		
		if(visibility == BasicFileVisibility.PRIVATE) {
			return FILE_STORE_LOCATION_PRIVATE;
		}
		
		if(visibility == BasicFileVisibility.PUBLIC) {
			return FILE_STORE_LOCATION_PUBLIC;
		}
		return null;
	}	
}