package jka.suifwk.exception;

public class SUIUserException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2309259571992406929L;
	
	public SUIUserException(Throwable t) {
		super(t);
	}
	
	public SUIUserException(String message) {
		super(message);
	}
	
	public SUIUserException(String message, Throwable t) {
		super(message, t);
	}
	
}