package jka.suifwk.cache;

import java.util.ArrayList;
import java.util.List;

public class ArrayListObjectCache<E> extends AbstractGenericObjectCache<E> {

	public ArrayListObjectCache(
			String cacheId,
			int pageSize,
			CacheDataProvider<E> cacheDataProvider) {
		super(cacheId, pageSize, cacheDataProvider);
	}

	@Override
	protected List<E> getCacheList(int initCapacity) {
		return new ArrayList<E>(initCapacity);
	}

	@Override
	protected int getNumberOfPagesToCache() {
		return 7;
	}

}
