package jka.suifwk.web.messaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import jka.suifwk.api.CallContext;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.messaging.Message;
import jka.suifwk.messaging.MessageListener;
import jka.suifwk.messaging.MessageTransport;
import jka.suifwk.messaging.MessagingManager;
import jka.suifwk.web.session.SessionManager;
import jka.suifwk.web.session.WebContext;

import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.codehaus.jackson.map.ObjectMapper;

@ManagedService(path = "/webmessage")
public final class AtmosphereWebMessageHandler {

	public AtmosphereWebMessageHandler() {
		injectTransport();
	}

	private static Logger logger = Logger
			.getLogger(AtmosphereWebMessageHandler.class.getName());

	private void injectTransport() {
		AtmosphereWebMessageTransport t = new AtmosphereWebMessageTransport();
		MessagingManager.INSTANCE.addWebMessageTransport("ATMOSPHERE", t);
	}

	/**
	 * Invoked when the connection as been fully established and suspended, e.g
	 * ready for receiving messages.
	 *
	 * @param r
	 *            the atmosphere resource
	 */
	@Ready
	public final void onReady(final AtmosphereResource r) {
		HttpSession rSession = r.session();
		if (rSession != null) {
			WebContext wc = SessionManager.INSTANCE.getWebContext(rSession
					.getId());
			if (wc != null && wc.getSession() != null) {
				wc.getSession().setAttribute(
						SUIConstants.ATTR_WEB_MESSAGE_TRANSPORT, r);
				logger.info("Browser {" + r.uuid() + "} connected.");
			} else {
				logger.warning("Browser {"
						+ r.uuid()
						+ "} cannot be connected. No valid HTTP session found for this request.");
			}
		} else {
			logger.warning("Browser {"
					+ r.uuid()
					+ "} cannot be connected. No session data available in client request.");
		}
	}

	/**
	 * Invoked when the client disconnect or when an unexpected closing of the
	 * underlying connection happens.
	 *
	 * @param event
	 *            the event
	 */
	@Disconnect
	public final void onDisconnect(final AtmosphereResourceEvent event) {
		if (event.isCancelled())
			logger.warning("Browser {" + event.getResource().uuid()
					+ "} unexpectedly disconnected");
		else if (event.isClosedByClient())
			logger.warning("Browser {" + event.getResource().uuid()
					+ "} closed the connection");
		try {
			SessionManager.INSTANCE
					.getWebContext(event.getResource().session().getId())
					.getSession()
					.removeAttribute(SUIConstants.ATTR_WEB_MESSAGE_TRANSPORT);
		} catch (Exception e) {
			// Ignore. Most of the times the session will be invalidated and we
			// can safely ignore this exception
		}
	}

	private class AtmosphereWebMessageTransport implements MessageTransport {

		private Map<String, List<MessageListener>> webMessageListeners;
		MessageEncoderDecoder encDec;

		protected AtmosphereWebMessageTransport() {
			webMessageListeners = new HashMap<String, List<MessageListener>>();
			encDec = new MessageEncoderDecoder();
		}

		private void notify(Message message) {
			List<MessageListener> customTopicListeners = webMessageListeners
					.get(message.getMessageTopic());
			if (customTopicListeners != null) {
				for (MessageListener l : customTopicListeners) {
					l.onWebMessage(message);
				}
			}
		}

		@Override
		public void push(CallContext context, Message message) {
			Object r = context
					.getSessionParam(SUIConstants.ATTR_WEB_MESSAGE_TRANSPORT);
			if (r != null && r instanceof AtmosphereResource) {
				synchronized (r) {
					AtmosphereResource ar = (AtmosphereResource) r;
					ar.getResponse().write(encDec.encode(message));
					try {
						ar.getResponse().flushBuffer();
					} catch (IOException e) {
						logger.log(Level.WARNING,
								"Cannot flush data to client", e);
					}
				}
			}
			notify(message);
		}

		@Override
		public void subscribe(String topic, MessageListener subscriber) {
			List<MessageListener> listeners = webMessageListeners.get(topic);
			if (listeners == null) {
				listeners = new ArrayList<MessageListener>();
				webMessageListeners.put(topic, listeners);
			}
			listeners.add(subscriber);
		}

		@Override
		public void unsubscribe(String topic, MessageListener subscriber) {
			List<MessageListener> listeners = webMessageListeners.get(topic);
			if (listeners != null) {
				listeners.remove(subscriber);
			}
		}
	}

	private final class MessageEncoderDecoder implements
			Encoder<Message, String>, Decoder<String, Message> {
		private final ObjectMapper mapper = new ObjectMapper();

		@Override
		public Message decode(final String s) {
			try {
				return mapper.readValue(s, Message.class);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public String encode(final Message message) {
			try {
				return mapper.writeValueAsString(message);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
