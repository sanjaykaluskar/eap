package jka.suifwk.web.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public interface ApplicationSessionHandler {
	public HttpSession getSession(HttpServletRequest request,
			boolean createNewSession);
}
