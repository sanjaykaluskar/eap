package jka.suifwk.web.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DefaultHttpSessionHandler implements ApplicationSessionHandler {

	@Override
	public HttpSession getSession(HttpServletRequest request,
			boolean createNewSession) {		
		return request.getSession(createNewSession);
	}

}
