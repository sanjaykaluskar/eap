package jka.suifwk.web.command;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.CallContext;
import jka.suifwk.api.CommandHandler;
import jka.suifwk.api.CommandHandlerRegistry;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.web.api.CallContextManager;
import jka.suifwk.web.utils.WebUtils;


@Path("/")
public class SUIWebCommandHandler {
	
	private static Logger logger = Logger.getLogger(SUIWebCommandHandler.class
			.getName());

	@Context
	UriInfo uriInfo;

	@Context
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;	

	@GET
	@Path("{command}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Object handleGetCall(
			@PathParam("command") String command) {
		return handleCall(command);
	}
	
	@POST
	@Path("{command}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM })
	public Object handlePostCall(
			@PathParam("command") String command) {
		return handleCall(command);
	}
	
	protected Object handleCall(String commandName) {
		try {			
			String[] commandParts = commandName.split(Pattern.quote("."));
			String cmdNS = commandParts[0];
			String cmdName = commandParts[1];
			
			InvocationData iData = getInvocationData(cmdNS, request);
			APICallResponse apiCallResponse = iData.getCommandHandler().executeCommand(cmdName, 
					iData.getContext());
			return WebUtils.createWebResponse(apiCallResponse, request, response);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					"Cannot process Command request for : "
							+ commandName, e);
			return WebUtils.generateInternalErrorResponse(e);
		}
	}

	protected InvocationData getInvocationData(String namespace,
			HttpServletRequest request) {
		CommandHandler commandHandler = null;
		CallContext context = null;
		if (namespace != null) {
			commandHandler = CommandHandlerRegistry.INSTANCE.getCommandHandler(namespace);
			if (commandHandler != null) {
				context = CallContextManager.INSTANCE.getCallContext(request);
			} else {
				throw new SUIRuntimeException("Command handler for '"
						+ namespace + "' not found!");
			}
		} else {
			throw new SUIRuntimeException("Null Command handler request!");
		}
		return new InvocationData(commandHandler, context);
	}
	
	protected class InvocationData {
		private final CommandHandler commandHandler;
		private final CallContext context;

		public InvocationData(CommandHandler commandHandler, CallContext context) {
			this.commandHandler = commandHandler;
			this.context = context;
		}

		public CommandHandler getCommandHandler() {
			return commandHandler;
		}

		public CallContext getContext() {
			return context;
		}
	}	
}
