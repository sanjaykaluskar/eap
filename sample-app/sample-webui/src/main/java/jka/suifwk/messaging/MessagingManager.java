package jka.suifwk.messaging;

import java.util.HashMap;
import java.util.Map;

import jka.suifwk.api.CallContext;
import jka.suifwk.exception.SUIRuntimeException;

public enum MessagingManager {
	INSTANCE;
	
	private Map<String, MessageTransport> webMessageTransporters;
		
	private MessagingManager() {		
		webMessageTransporters = new HashMap<String, MessageTransport>();
	}
	
	public void addWebMessageTransport(String transportID, MessageTransport transport) {
		if(transportID == null) {
			throw new SUIRuntimeException("Transport ID cannot be null.");
		}		
		if(transport == null) {
			throw new SUIRuntimeException("Transport cannot be null.");
		}		
		this.webMessageTransporters.put(transportID, transport);	
	}
	
	public void publish(CallContext context, Message message) {
		for(MessageTransport t : webMessageTransporters.values()) {
			t.push(context, message);
		}
	}
	
	public void publish(CallContext context, String topic, Object data) {
		publish(context, MessageFactory.INSTANCE.create(topic, data));		
	}
					
	public void subscribe(String topic, MessageListener messageListener) {
		for(MessageTransport t : webMessageTransporters.values()) {
			t.subscribe(topic, messageListener);
		}			
	}
		
	public void unsubscribe(String topic, MessageListener messageListener) {
		for(MessageTransport t : webMessageTransporters.values()) {
			t.unsubscribe(topic, messageListener);
		}				
	}	
}
