package jka.suifwk.messaging;

import jka.suifwk.api.CallContext;

public interface MessageTransport {

	public void push(CallContext context, Message message);
		
	public void subscribe(String topic, MessageListener subscriber);
	
	public void unsubscribe(String topic, MessageListener subscriber);
	
}
