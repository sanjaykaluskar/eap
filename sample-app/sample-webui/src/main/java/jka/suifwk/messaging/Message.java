package jka.suifwk.messaging;

public interface Message {

	public String getMessageId();
	public Object getMessageData();
	public String getMessageTopic();
	public long getMessageTimestamp();
		
}
