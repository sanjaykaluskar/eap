/**
 * SappResourceTypeEnumerator.java
 */
package com.dombigroup.eap.app.backend.platform;

import com.dombigroup.eap.app.common.datamodels.SappResource;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.interfaces.IResourceTypeEnumerator;

/**
 * @author sanjay
 * 
 */
public class SappResourceTypeEnumerator implements IResourceTypeEnumerator
{
  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IResourceTypeEnumerator#listResourceTypes()
   */
  @Override
  public ResourceType[] listResourceTypes()
  {
    return SappResource.values();
  }
}
