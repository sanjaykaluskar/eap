/**
 * PContactMgmtService.java
 */
package com.dombigroup.eap.app.backend.contact;

import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 * 
 */
public class PContactMgmtService extends PluggableEntityMgmt<Integer, PContact>
{
  /**
   * Constructor for PContactMgmtService
   * 
   * @throws MetadataException
   */
  public PContactMgmtService() throws StorageException, MetadataException
  {
    super(Integer.class, PContact.class);
  }

  @Override
  protected void copyPo(PContact from, PContact to)
  {
    to.setPrefix(from.getPrefix());
    to.setFirstName(from.getFirstName());
    to.setMiddleName(from.getMiddleName());
    to.setLastName(from.getLastName());
    to.setAddressId(from.getAddressId());
    to.setEmail(from.getEmail());
    to.setPhoneListId(from.getPhoneListId());
    to.setNotes(from.getNotes());
  }
}
