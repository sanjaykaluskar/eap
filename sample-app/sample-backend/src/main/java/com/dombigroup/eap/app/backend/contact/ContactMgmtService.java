/**
 * ContactMgmtService.java
 */
package com.dombigroup.eap.app.backend.contact;

import com.dombigroup.eap.app.backend.interfaces.ContactMgmt;
import com.dombigroup.eap.app.backend.platform.SappEntityMgmtFactory;
import com.dombigroup.eap.app.common.datamodels.Contact;
import com.dombigroup.eap.app.common.datamodels.SappResource;
import com.dombigroup.eap.app.common.exceptions.SAPPMessages;
import com.dombigroup.eap.app.common.exceptions.SappException;
import com.dombigroup.eap.app.common.exceptions.SappRuntimeException;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.bomgmt.BOMgmtService;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.AddressMgmt;
import com.dombigroup.eap.server.platform.interfaces.PhoneMgmt;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 * 
 */
class ContactMgmtService extends BOMgmtService<Contact, PContact>implements
    ContactMgmt
{
  private PhoneMgmt           phoneSvc;
  private AddressMgmt         addressSvc;

  protected ContactMgmtService()
  {
    super(SappResource.CONTACT, PContact.class,
        SappEntityMgmtFactory.SVC_PCONTACT);
  }

  @Override
  public void boSvcStartup() throws ServiceException
  {
    try
    {
      phoneSvc = (PhoneMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PHONE);
      addressSvc = (AddressMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_ADDRESS);
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  @Override
  protected Contact populateBOFromPO(ServiceContext ctx, PContact from)
  {
    Contact to = null;
    if (from != null)
    {
      try
      {
        to = new Contact();
        to.copyPObjectInfo(from);
        to.setPrefix(from.getPrefix());
        to.setFirstName(from.getFirstName());
        to.setMiddleName(from.getMiddleName());
        to.setLastName(from.getLastName());
        to.setAddress(addressSvc.getAddress(ctx, from.getAddressId()));
        to.setEmail(from.getEmail());
        to.setPhoneNumbers(phoneSvc.getPhoneList(ctx, from.getPhoneListId()));
        to.setNotes(from.getNotes());
      }
      catch (Exception e)
      {
        throw new SappRuntimeException(SAPPMessages.SAPP_ASSET_ACCESS, e,
            String.valueOf(from.getId()));
      }
    }
    return to;
  }

  @Override
  protected PContact populatePOFromBO(ServiceContext ctx, int id,
      Contact from, PContact old)
  {
    try
    {
      boolean allocate = (old == null) ||
          ((old != null) && !old.equalAttrs(from));

      /* update address */
      int addrId = (old == null) ? Persisted.INVALID_ID : old.getAddressId();
      if (addrId != Persisted.INVALID_ID)
      {
        addressSvc.update(ctx, addrId, from.getAddress());
      }
      else if (from.getAddress() != null)
      {
        addrId = addressSvc.createAddress(ctx, from.getAddress());
        allocate = true;
      }

      /* update phone list if needed */
      int phoneListId = (old == null) ? Persisted.INVALID_ID : old
          .getPhoneListId();
      if (phoneListId != Persisted.INVALID_ID)
      {
        phoneSvc.deletePhoneList(ctx, phoneListId);
        phoneListId = Persisted.INVALID_ID;
        allocate = true;
      }
      if ((from.getPhoneNumbers() != null) && (from.getPhoneNumbers().size() > 0))
      {
        phoneListId = phoneSvc.createPhoneList(ctx, from.getPhoneNumbers());
        allocate = true;
      }

      if (allocate)
        old = new PContact(id, addrId, phoneListId, from);
    }
    catch (Exception e)
    {
      throw new SappRuntimeException(SAPPMessages.SAPP_ASSET_ACCESS, e,
          String.valueOf(from.getId()));
    }
    return old;
  }

  @Override
  protected void deleteAdditionalObjects(ServiceContext ctx, PContact po)
  {
    try
    {
      int phoneListId = po.getPhoneListId();
      if (phoneListId != Persisted.INVALID_ID)
        phoneSvc.deletePhoneList(ctx, phoneListId);
    }
    catch (Exception e)
    {
      throw new SappRuntimeException(SAPPMessages.SAPP_INTERNAL_ERROR, e);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  protected PluggableEntityMgmt<Integer, PContact> getPOMgmtService()
  {
    PluggableEntityMgmt<Integer, PContact> svc = null;

    try
    {
      svc = (PluggableEntityMgmt<Integer, PContact>) SappEntityMgmtFactory
          .getEntityMgmtService(SappEntityMgmtFactory.SVC_PCONTACT);
    }
    catch (SappException e)
    {
      throw new SappRuntimeException(SAPPMessages.SAPP_INTERNAL_ERROR, e);
    }

    return svc;
  }
}
