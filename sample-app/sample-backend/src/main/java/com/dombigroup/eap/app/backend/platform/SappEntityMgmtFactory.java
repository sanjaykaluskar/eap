/**
 * SappEntityMgmtFactory.java
 */
package com.dombigroup.eap.app.backend.platform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.dombigroup.eap.app.backend.contact.PContact;
import com.dombigroup.eap.app.backend.contact.PContactMgmtService;
import com.dombigroup.eap.app.common.exceptions.SAPPMessages;
import com.dombigroup.eap.app.common.exceptions.SappException;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.EntityMgmt;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamInfo;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 * 
 */
@SuppressWarnings({
    "rawtypes", "unchecked" })
public class SappEntityMgmtFactory implements IPluggableServiceFactory
{
  public static final String SVC_PCONTACT = "CONTACT (ENTITY) MANAGEMENT SERVICE";

  private static Map<String, EntityService> svcMap =
      new HashMap<String, EntityService>();

  static
  {
    svcMap.put(SVC_PCONTACT, new EntityService<Integer, PContact>(null, null));
  }

  private static synchronized PluggableEntityMgmt getEntityService(
      String svcName) throws SappException
  {
    EntityService es = svcMap.get(svcName);

    try
    {
      if (es.getSvc() == null)
      {
        if (svcName == SVC_PCONTACT)
        {
          PluggableEntityMgmt<Integer, PContact> svc =
              new PContactMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
      }

      return es.getSvc();
    }
    catch (Exception e)
    {
      throw new SappException(SAPPMessages.SAPP_SERVICE_INSTANTIATION, e);
    }
  }

  @Override
  public List<String> getServiceNames()
  {
    List<String> ret = new ArrayList<String>();
    for (Entry<String, EntityService> s : svcMap.entrySet())
      ret.add(s.getKey());
    return ret;
  }

  @Override
  public List<ParamInfo> getConfigParams(String svcName)
  {
    return null;
  }

  @Override
  public Config getServiceConfig(String svcName)
  {
    EntityService e = svcMap.get(svcName);
    return e.getConfig();
  }

  @Override
  public void setServiceConfig(String svcName, Config c)
  {
    EntityService e = svcMap.get(svcName);
    e.setConfig(c);
  }

  @Override
  public IPluggableService getService(String svcName) throws ServiceException
  {
    try
    {
      return getEntityService(svcName);
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  public static EntityMgmt getEntityMgmtService(String svcName)
      throws SappException
  {
    return getEntityService(svcName);
  }

  static private class EntityService<K, V extends Persisted>
  {
    private PluggableEntityMgmt<K, V> svc;
    private Config                    config;

    /**
     * Constructor for EntityService
     */
    private EntityService(PluggableEntityMgmt<K, V> svc, Config config)
    {
      this.svc = svc;
      this.config = config;
    }

    /**
     * @return the svc
     */
    private PluggableEntityMgmt<K, V> getSvc()
    {
      return svc;
    }

    /**
     * @param svc
     *          the svc to set
     */
    private void setSvc(PluggableEntityMgmt<K, V> svc)
    {
      this.svc = svc;
    }

    /**
     * @return the config
     */
    private Config getConfig()
    {
      return config;
    }

    /**
     * @param config
     *          the config to set
     */
    private void setConfig(Config config)
    {
      this.config = config;
    }
  }
}
