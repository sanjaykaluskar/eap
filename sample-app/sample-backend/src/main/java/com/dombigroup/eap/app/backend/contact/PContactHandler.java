/**
 * PContactHandler.java
 */
package com.dombigroup.eap.app.backend.contact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dombigroup.eap.common.datamodels.PrefixType;
import com.dombigroup.eap.common.utils.ExpressionUtils;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class PContactHandler extends RdbmsStorageHandler
{
  public class PContactResultSet extends StorageResultSet
  {
    ResultSet  rs;
    Statement  stmt;
    Connection cxn;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    /**
     * @param cxn
     *          the cxn to set
     */
    private void setCxn(Connection cxn)
    {
      this.cxn = cxn;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        PContact contact = null;

        if (rs.next())
        {
          contact = new PContact();
          contact.setId(rs.getInt("id"));
          contact.setOrgId(rs.getInt("org_id"));
          contact.setPrefix(PrefixType.fromValue(rs.getString("prefix")));
          contact.setFirstName(rs.getString("first_name"));
          contact.setMiddleName(rs.getString("middle_name"));
          contact.setLastName(rs.getString("last_name"));
          contact.setAddressId(rs.getInt("address_id"));
          contact.setEmail(rs.getString("email"));
          contact.setPhoneListId(rs.getInt("phone_list_id"));
          contact.setNotes(rs.getString("notes"));
        }

        return contact;
      }
      catch (Exception e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
        cxn.close();
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

  }

  private static final String STMT_CREATE_TABLE = "create table contacts ("
      + "id number primary key, "
      + "org_id number, "
      + "prefix varchar2(4000), "
      + "first_name varchar2(4000), "
      + "middle_name varchar2(4000), "
      + "last_name varchar2(4000), "
      + "address_id number, "
      + "email varchar2(4000), "
      + "phone_list_id number, "
      + "notes varchar2(4000))";

  private static final String STMT_DROP = "drop table contacts";

  private static final String STMT_SELECT = "select "
      + "id, org_id, prefix, "
      + "first_name, middle_name, last_name, "
      + "address_id, email, phone_list_id, "
      + "notes "
      + "from contacts "
      + "where id = ?";

  private static final String STMT_INSERT = "insert into contacts"
      + "(id, org_id, prefix, "
      + "first_name, middle_name, last_name, "
      + "address_id, email, phone_list_id, "
      + "notes) "
      + "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE = "update contacts set "
      + "    org_id = ?, "
      + "    prefix = ?, "
      + "    first_name = ?, "
      + "    middle_name = ?, "
      + "    last_name = ?, "
      + "    address_id = ?, "
      + "    email = ?, "
      + "    phone_list_id = ?, "
      + "    notes = ? "
      + "where (id = ?)";

  private static final String STMT_DELETE = "delete from contacts "
      + "where (id = ?)";

  private static final String STMT_SEARCH = "select "
      + "id, org_id, prefix, "
      + "first_name, middle_name, last_name, "
      + "address_id, email, phone_list_id, "
      + "notes "
      + "from contacts "
      + "where (%s)";

  public PContactHandler()
  {
    super(PContact.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE_TABLE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PContact contact = (PContact) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, contact.getId());
      stmt.setInt(2, contact.getOrgId());
      stmt.setString(3, ExpressionUtils.safeString(contact.getPrefix()));
      stmt.setString(4, contact.getFirstName());
      stmt.setString(5, contact.getMiddleName());
      stmt.setString(6, contact.getLastName());
      stmt.setInt(7, contact.getAddressId());
      stmt.setString(8, contact.getEmail());
      stmt.setInt(9, contact.getPhoneListId());
      stmt.setString(10, contact.getNotes());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer contactId = (Integer) key;
    PContact contact = (PContact) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, contactId.intValue());
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        contact.setId(rs.getInt("id"));
        contact.setOrgId(rs.getInt("org_id"));
        contact.setPrefix(PrefixType.fromValue(rs.getString("prefix")));
        contact.setFirstName(rs.getString("first_name"));
        contact.setMiddleName(rs.getString("middle_name"));
        contact.setLastName(rs.getString("last_name"));
        contact.setAddressId(rs.getInt("address_id"));
        contact.setEmail(rs.getString("email"));
        contact.setPhoneListId(rs.getInt("phone_list_id"));
        contact.setNotes(rs.getString("notes"));
        rowsExist = true;
        stmt.close();
      }
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PContact contact = (PContact) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setInt(1, contact.getOrgId());
      stmt.setString(2, ExpressionUtils.safeString(contact.getPrefix()));
      stmt.setString(3, contact.getFirstName());
      stmt.setString(4, contact.getMiddleName());
      stmt.setString(5, contact.getLastName());
      stmt.setInt(6, contact.getAddressId());
      stmt.setString(7, contact.getEmail());
      stmt.setInt(8, contact.getPhoneListId());
      stmt.setString(9, contact.getNotes());
      stmt.setInt(10, contact.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PContact contact = (PContact) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, contact.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#search
   * (java.lang.Class, java.lang.String)
   */
  @Override
  public PContactResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PContactResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new PContactResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
        rs.setCxn(c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
