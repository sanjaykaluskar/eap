/**
 * ContactMgmt.java
 */
package com.dombigroup.eap.app.backend.interfaces;

import com.dombigroup.eap.app.common.datamodels.Contact;
import com.dombigroup.eap.server.platform.interfaces.BOMgmt;

/**
 * @author sanjay
 * 
 */
public interface ContactMgmt extends BOMgmt<Contact>
{
}
