package com.dombigroup.eap.app.backend.contact;

import com.dombigroup.eap.app.common.datamodels.Contact;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.PrefixType;
import com.dombigroup.eap.common.utils.ExpressionUtils;

public class PContact extends Persisted
{
  private PrefixType prefix;

  private String firstName;

  private String middleName;

  private String lastName;

  private int addressId;

  private String email;

  private int phoneListId;

  private String notes;

  public PContact()
  {
  }

  public PContact(int contactId, int addrId, int phoneList, Contact contact)
  {
    copyPObjectInfo(contact);
    setId(contactId);
    prefix = contact.getPrefix();
    firstName = contact.getFirstName();
    middleName = contact.getMiddleName();
    lastName = contact.getLastName();
    addressId = addrId;
    email = contact.getEmail();
    phoneListId = phoneList;
    notes = contact.getNotes();
  }

  public boolean equalAttrs(Contact contact)
  {
    return equalPObject(contact) &&
        ExpressionUtils.safeEqual(prefix, contact.getPrefix())
        &&
        ExpressionUtils.safeEqual(firstName, contact.getFirstName())
        &&
        ExpressionUtils.safeEqual(middleName, contact.getMiddleName())
        &&
        ExpressionUtils.safeEqual(lastName, contact.getLastName())
        &&
        ExpressionUtils.safeEqual(email, contact.getEmail())
        &&
        ExpressionUtils.safeEqual(notes, contact.getNotes());
  }

  /**
   * @return the prefix
   */
  public PrefixType getPrefix()
  {
    return prefix;
  }

  /**
   * @param prefix the prefix to set
   */
  public void setPrefix(PrefixType prefix)
  {
    this.prefix = prefix;
  }

  /**
   * @return the firstName
   */
  public String getFirstName()
  {
    return firstName;
  }

  /**
   * @param firstName
   *          the firstName to set
   */
  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }

  /**
   * @return the middleName
   */
  public String getMiddleName()
  {
    return middleName;
  }

  /**
   * @param middleName
   *          the middleName to set
   */
  public void setMiddleName(String middleName)
  {
    this.middleName = middleName;
  }

  /**
   * @return the lastName
   */
  public String getLastName()
  {
    return lastName;
  }

  /**
   * @param lastName
   *          the lastName to set
   */
  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }

  /**
   * @return the addressId
   */
  public int getAddressId()
  {
    return addressId;
  }

  /**
   * @param addressId the addressId to set
   */
  public void setAddressId(int addressId)
  {
    this.addressId = addressId;
  }

  /**
   * @return the email
   */
  public String getEmail()
  {
    return email;
  }

  /**
   * @param email
   *          the email to set
   */
  public void setEmail(String email)
  {
    this.email = email;
  }

  /**
   * @return the phoneListId
   */
  public int getPhoneListId()
  {
    return phoneListId;
  }

  /**
   * @param phoneListId
   *          the phoneListId to set
   */
  public void setPhoneListId(int phoneListId)
  {
    this.phoneListId = phoneListId;
  }

  /**
   * @return the notes
   */
  public String getNotes()
  {
    return notes;
  }

  /**
   * @param notes
   *          the notes to set
   */
  public void setNotes(String notes)
  {
    this.notes = notes;
  }
}
