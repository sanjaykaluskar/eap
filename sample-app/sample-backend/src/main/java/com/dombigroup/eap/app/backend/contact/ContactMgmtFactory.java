/**
 * ContactMgmtFactory.java
 */
package com.dombigroup.eap.app.backend.contact;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.app.common.exceptions.SAPPMessages;
import com.dombigroup.eap.app.common.exceptions.SappException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamInfo;

/**
 * @author sanjay
 * 
 */
public class ContactMgmtFactory implements IPluggableServiceFactory
{
  private static final String       SVC_NAME = "CONTACT MANAGEMENT";
  private static Config             config;
  private static ContactMgmtService svc;

  public static synchronized ContactMgmtService getContactMgmtService()
      throws SappException
  {
    try
    {
      if (svc == null)
        svc = new ContactMgmtService();
    }
    catch (Exception e)
    {
      throw new SappException(SAPPMessages.SAPP_SERVICE_INSTANTIATION, e);
    }
    return svc;
  }

  @Override
  public List<String> getServiceNames()
  {
    List<String> l = new ArrayList<String>();
    l.add(SVC_NAME);
    return l;
  }

  @Override
  public List<ParamInfo> getConfigParams(String svcName)
  {
    return null;
  }

  @Override
  public Config getServiceConfig(String svcName)
  {
    return config;
  }

  @Override
  public void setServiceConfig(String svcName, Config c)
  {
    config = c;
  }

  @Override
  public IPluggableService getService(String svcName) throws ServiceException
  {
    try
    {
      return getContactMgmtService();
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }
}
