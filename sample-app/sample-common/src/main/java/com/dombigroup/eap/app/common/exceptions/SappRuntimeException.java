/**
 * SappRuntimeException.java
 */
package com.dombigroup.eap.app.common.exceptions;

import com.dombigroup.eap.common.exceptions.DgRuntimeException;

/**
 * @author skaluska
 *
 */
public class SappRuntimeException extends DgRuntimeException
{
  /** SappRuntimeException.java */
  private static final long serialVersionUID = 1L;

  public SappRuntimeException(SAPPMessages m)
  {
    super(m);
  }

  public SappRuntimeException(SAPPMessages m, String... arg)
  {
    super(m, arg);
  }

  public SappRuntimeException(SAPPMessages m, Exception e)
  {
    super(e, m);
  }

  public SappRuntimeException(SAPPMessages m, Exception e, String... strings)
  {
    super(e, m, strings);
  }
}
