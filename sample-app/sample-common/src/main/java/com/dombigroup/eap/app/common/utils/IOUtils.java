/**
 * IOUtils.java
 */
package com.dombigroup.eap.app.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;

import com.dombigroup.eap.app.common.utils.SerDeUtils.SerializationType;

/**
 * @author skaluska
 * 
 */
public class IOUtils
{
  /**
   * createNewFile
   * 
   * Creates a new file at the specified path. If the file already exists, it is
   * deleted and re-created. If some of the directories on the path don't exist
   * they are created.
   * 
   * @throws IOException
   * 
   * @arg path - path where the file is to be created
   * 
   */
  static public void createNewFile(String path) throws IOException
  {
    File file = new File(path);
    File dir = file.getParentFile();
    if (file.exists())
      file.delete();
    if ((dir != null) && !dir.exists())
      dir.mkdirs();
    file.createNewFile();
    file.setReadable(true, true);
    file.setWritable(true);
  }

  /**
   * Deletes a directory including its subtree (equivalent of an rm -r). It does
   * nothing if the specified path doesn't correspond to a directory.
   * 
   * @param path
   *          - path of the directory to be deleted.
   */
  static public void deleteDir(String path)
  {
    File file = new File(path);
    if (file.exists() && file.isDirectory())
    {
      for (File f : file.listFiles())
      {
        if (f.isDirectory())
          deleteDir(f.getAbsolutePath());
        else
          f.delete();
      }
      file.delete();
    }
  }

  public static String clobToString(Clob c) throws SQLException, IOException
  {
    String ret = null;
    if (c != null)
    {
      StringBuilder sb = new StringBuilder();
      Reader r = c.getCharacterStream();
      int ch;
      while ((ch = r.read()) != -1)
        sb.append((char) ch);
      ret = sb.toString();
    }
    return ret;
  }
  
  public static Object deserialize(String xml)
  {
    return SerDeUtils.unmarshallBase64(xml, true,
        SerializationType.JAXB_SERIALIZATION);
  }

  public static String serialize(Object c)
  {
    return SerDeUtils.marshallBase64(c, true,
        SerializationType.JAXB_SERIALIZATION);
  }
}
