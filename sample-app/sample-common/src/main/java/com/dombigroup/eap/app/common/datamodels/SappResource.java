package com.dombigroup.eap.app.common.datamodels;

import com.dombigroup.eap.common.datamodels.ResourceType;

/**
 * @author sanjay
 * 
 */
public enum SappResource
    implements ResourceType
{
  CONTACT("CONTACT");

  private final String value;

  private SappResource(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public static SappResource fromValue(String v)
  {
    if (v == null)
      return null;

    for (SappResource priv : SappResource.values())
    {
      if (priv.value.equals(v))
      {
        return priv;
      }
    }
    throw new IllegalArgumentException(v);
  }

  @Override
  public String getName()
  {
    return value;
  }
}
