/**
 * ExceptionUtils.java
 */
package com.dombigroup.eap.app.common.utils;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.common.exceptions.DgRuntimeException;

/**
 * @author skaluska
 *
 */
public class ExceptionUtils
{
  public static Throwable userReportableError(Exception simException)
  {
    Throwable firstUserError = null;
    for (Throwable e = simException; e != null; e = e.getCause())
    {
      if ((e instanceof DgException) ||
          (e instanceof DgRuntimeException))
      {
        firstUserError = e;
      }
    }
    return firstUserError;
  }

  public static Throwable firstExternalError(Exception simException)
  {
    Throwable firstExtErr = null;
    boolean prevErrorWasSim = false;
    for (Throwable e = simException; e != null; e = e.getCause())
    {
      if ((e instanceof DgException) ||
          (e instanceof DgRuntimeException))
      {
        prevErrorWasSim = true;
      }
      else
      {
        if (prevErrorWasSim)
          firstExtErr = e;
        prevErrorWasSim = false;
      }
    }
    return firstExtErr;
  }
}
