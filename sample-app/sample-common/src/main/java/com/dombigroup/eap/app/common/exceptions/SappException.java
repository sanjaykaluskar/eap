/**
 * SappException.java
 */
package com.dombigroup.eap.app.common.exceptions;

import com.dombigroup.eap.common.exceptions.DgException;

/**
 * @author sanjay
 * 
 */
public class SappException extends DgException
{
  /** SappException.java */
  private static final long serialVersionUID = 1L;

  public SappException(SAPPMessages m)
  {
    super(m);
  }

  public SappException(SAPPMessages m, String... arg)
  {
    super(m, arg);
  }

  public SappException(SAPPMessages m, Exception e)
  {
    super(e, m);
  }

  public SappException(SAPPMessages m, Exception e, String... strings)
  {
    super(e, m, strings);
  }
}
