package com.dombigroup.eap.app.common.datamodels;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.Phone;
import com.dombigroup.eap.common.datamodels.PrefixType;

@XmlType(name = "contact")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact extends Persisted
{
  private PrefixType prefix;

  private String firstName;

  private String middleName;

  private String lastName;

  private Address address;

  private String email;

  private List<Phone> phoneNumbers;

  private String notes;

  /**
   * @return the prefix
   */
  public PrefixType getPrefix()
  {
    return prefix;
  }

  /**
   * @param prefix the prefix to set
   */
  public void setPrefix(PrefixType prefix)
  {
    this.prefix = prefix;
  }

  /**
   * @return the firstName
   */
  public String getFirstName()
  {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }

  /**
   * @return the middleName
   */
  public String getMiddleName()
  {
    return middleName;
  }

  /**
   * @param middleName the middleName to set
   */
  public void setMiddleName(String middleName)
  {
    this.middleName = middleName;
  }

  /**
   * @return the lastName
   */
  public String getLastName()
  {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }

  /**
   * @return the address
   */
  public Address getAddress()
  {
    return address;
  }

  /**
   * @param address the address to set
   */
  public void setAddress(Address address)
  {
    this.address = address;
  }

  /**
   * @return the email
   */
  public String getEmail()
  {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email)
  {
    this.email = email;
  }

  /**
   * @return the phoneNumbers
   */
  public List<Phone> getPhoneNumbers()
  {
    return phoneNumbers;
  }

  /**
   * @param phoneNumbers the phoneNumbers to set
   */
  public void setPhoneNumbers(List<Phone> phoneNumbers)
  {
    this.phoneNumbers = phoneNumbers;
  }

  /**
   * @return the notes
   */
  public String getNotes()
  {
    return notes;
  }

  /**
   * @param notes the notes to set
   */
  public void setNotes(String notes)
  {
    this.notes = notes;
  }
}
