/**
 * ConfigMap.java
 */
package com.dombigroup.eap.server.mgmt.interfaces;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author sanjay
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="configMap")
public class ConfigMap
{
  @XmlElement(name="serviceElement", required=true)
  private final List<ServiceElement> svcConfig = new ArrayList<ServiceElement>();

  public List<ServiceElement> getSvcConfig()
  {
    return svcConfig;
  }
}
