/**
 * MgmtException.java
 */
package com.dombigroup.eap.server.mgmt.exceptions;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.mgmt.exceptions.MERRMessages;

/**
 * @author sanjay
 *
 */
public class ConnectionException extends DgException
{
  /** MgmtException.java */
  private static final long serialVersionUID = 1L;

  public ConnectionException(MERRMessages m)
  {
    super(m);
  }

  public ConnectionException(MERRMessages m, Exception e)
  {
    super(e, m);
  }

  public ConnectionException(MERRMessages m, String...strings)
  {
    super(m, strings);
  }

  public ConnectionException(MERRMessages m, Exception e, String...strings)
  {
    super(e, m, strings);
  }
}
