/**
 * ServerConfig.java
 */
package com.dombigroup.eap.server.mgmt.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.dombigroup.eap.server.platform.interfaces.Config;

/**
 * @author sanjay
 * 
 */
@XmlRootElement(name = "serverConfig")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerConfig
{
  @XmlJavaTypeAdapter(ConfigAdaptor.class)
  private Map<String, Config> svcConfigs = new HashMap<String, Config>();

  public List<String> getConfigurableServices()
  {
    List<String> retVal = new ArrayList<String>();
    if (svcConfigs.size() > 0) for (String s : svcConfigs.keySet())
      retVal.add(s);
    return retVal;
  }

  /**
   * @return the svcConfigs
   */
  public Map<String, Config> getSvcConfigs()
  {
    return svcConfigs;
  }

  /**
   * @param svcConfigs the svcConfigs to set
   */
  public void setSvcConfigs(Map<String, Config> svcConfigs)
  {
    this.svcConfigs = svcConfigs;
  }

  /**
   * @return the storage
   */
  public synchronized Config getServiceConfig(String svc)
  {
    return svcConfigs.get(svc);
  }

  /**
   * @param storage
   *          the storage to set
   */
  public void setServiceConfig(String svc, Config config)
  {
    svcConfigs.put(svc, config);
  }
}
