/**
 * MgmtException.java
 */
package com.dombigroup.eap.server.mgmt.exceptions;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.mgmt.exceptions.MERRMessages;

/**
 * @author sanjay
 *
 */
public class MgmtException extends DgException
{
  /** MgmtException.java */
  private static final long serialVersionUID = 1L;

  public MgmtException(MERRMessages m)
  {
    super(m);
  }
  
  public MgmtException(MERRMessages m, Exception e)
  {
    super(e, m);
  }
}
