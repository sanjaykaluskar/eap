package com.dombigroup.eap.server.mgmt.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.dombigroup.eap.server.platform.interfaces.Config;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="serviceElement")
public class ServiceElement
{
  private String serviceName;

  private Config serviceConfig;

  public ServiceElement(String key, Config value)
  {
    serviceName = key;
    serviceConfig = value;
  }

  public ServiceElement()
  {
    serviceName = null;
    serviceConfig = null;
  }

  /**
   * @return the serviceName
   */
  public String getServiceName()
  {
    return serviceName;
  }

  /**
   * @param serviceName the serviceName to set
   */
  public void setServiceName(String serviceName)
  {
    this.serviceName = serviceName;
  }

  /**
   * @return the serviceConfig
   */
  public Config getServiceConfig()
  {
    return serviceConfig;
  }

  /**
   * @param serviceConfig the serviceConfig to set
   */
  public void setServiceConfig(Config serviceConfig)
  {
    this.serviceConfig = serviceConfig;
  }
}
