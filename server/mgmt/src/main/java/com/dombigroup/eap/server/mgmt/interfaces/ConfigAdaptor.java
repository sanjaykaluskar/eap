package com.dombigroup.eap.server.mgmt.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.dombigroup.eap.server.platform.interfaces.Config;

public class ConfigAdaptor extends XmlAdapter<ConfigMap, Map<String,Config>>
{

  @Override
  public Map<String, Config> unmarshal(ConfigMap v) throws Exception
  {
    Map<String, Config> m = new HashMap<String, Config>();
    for (ServiceElement e : v.getSvcConfig())
    {
      m.put(e.getServiceName(), e.getServiceConfig());
    }
    return m;
  }

  @Override
  public ConfigMap marshal(Map<String, Config> v) throws Exception
  {
    ConfigMap cm = new ConfigMap();
    List<ServiceElement> svc = cm.getSvcConfig();
    for (Map.Entry<String, Config> e : v.entrySet())
    {
      svc.add(new ServiceElement(e.getKey(), e.getValue()));
    }
    return cm;
  }
}
