/**
 * ConnectionMgmt.java
 */
package com.dombigroup.eap.server.mgmt.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.common.utils.ExpressionUtils;
import com.dombigroup.eap.server.mgmt.exceptions.ConnectionException;
import com.dombigroup.eap.server.mgmt.interfaces.IConnectionMgmt;
import com.dombigroup.eap.server.mgmt.exceptions.MERRMessages;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.CompanyMgmt;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.orgmgmt.CompanyMgmtFactory;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtService;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

/**
 * @author sanjay
 * 
 */
public class ConnectionMgmt implements IConnectionMgmt
{
  private static final float hashTableLoadFactor = 0.750f;
  private static final float TARGET_CACHE_SIZE   = 20;
  private static int         lastKey;

  private LinkedHashMap<Integer, ServiceContext> connectionMap;

  public ConnectionMgmt()
  {
    int hashTableCapacity = (int) Math
        .ceil(TARGET_CACHE_SIZE / hashTableLoadFactor) + 1;

    connectionMap = new LinkedHashMap<Integer, ServiceContext>(
        hashTableCapacity, hashTableLoadFactor, true) {
      // (an anonymous inner class)
      private static final long serialVersionUID = 1;

      @Override
      protected boolean removeEldestEntry(
          Map.Entry<Integer, ServiceContext> eldest)
      {
        return (size() > TARGET_CACHE_SIZE);
      }
    };

    Random r = new Random();
    lastKey = r.nextInt();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.mgmt.interfaces.IConnectionMgmt#createConnection
   * (int, java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public ServiceContext createConnection(int orgId, String userName,
      String password) throws ConnectionException
  {
    int userId = Persisted.INVALID_ID;
    boolean authenticated;
    ServiceContext svcContext = null;

    try
    {
      svcContext = ServiceContext.bootstrapServiceContext(false);
      svcContext.startCall();

      /* bootstrapping */
      if (UserMgmtService.SU_NAME.equals(userName) &&
          UserMgmtService.SU_PASSWORD.equals(password))
      {
        userId = UserMgmtService.SU_USERID;
        authenticated = true;
      }
      else if (!Server.getServer().isRunning())
        authenticated = false;
      else
      {
        UserMgmt um = UserMgmtServiceFactory.getUserMgmtService();
        CompanyMgmt om = CompanyMgmtFactory.getCompanyMgmtService();
        User u = um.getUser(svcContext, orgId, userName);
        Company c = om.getCompany(svcContext, orgId);

        /*
         * user and org exist, password matches, and user belongs to the org
         * unless it is SU
         */
        authenticated = (u != null) && (c != null)
            && ExpressionUtils.safeEqual(u.getPassword(), password);
        if (authenticated)
          userId = u.getUserid();
      }

      if (authenticated)
      {
        svcContext
            .initSecContext(userId, orgId, Server.getServer().isRunning());
        int key = lastKey++;
        svcContext.setConnectionId(key);
        connectionMap.put(key, svcContext);
      }
      svcContext.endCall(false);

      if (!authenticated)
        svcContext = null;
    }
    catch (Exception e)
    {
      throw new ConnectionException(MERRMessages.MERR_CONNECTION_CREATE, e,
          userName, String.valueOf(orgId));
    }

    if (svcContext == null)
      throw new ConnectionException(MERRMessages.MERR_CONNECTION_CREATE,
          userName, String.valueOf(orgId));

    return svcContext;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.mgmt.interfaces.IConnectionMgmt#createConnection(
   * java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public ServiceContext createConnection(String orgName, String userName,
      String password) throws ConnectionException
  {
    Company c = null;
    int orgId;
    try
    {
      ServiceContext svcContext = ServiceContext.bootstrapServiceContext(false);
      svcContext.startCall();
      CompanyMgmt om = CompanyMgmtFactory.getCompanyMgmtService();
      c = om.getCompany(svcContext, orgName);
      orgId = c.getId();
      svcContext.endCall(false);
    }
    catch (Exception e)
    {
      throw new ConnectionException(MERRMessages.MERR_CONNECTION_CREATE, e,
          userName, orgName);
    }

    return (c == null) ? null : createConnection(orgId, userName, password);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.mgmt.interfaces.IConnectionMgmt#returnConnection
   * (int, int, int)
   */
  @Override
  public ServiceContext returnConnection(int cxnId)
  {
    return connectionMap.get(cxnId);
  }

  @Override
  public void endConnection(ServiceContext ctx) throws DgException
  {
    int cxnId = ctx.getConnectionId();
    ctx.logoff(true);
    ctx.resetWorkUnit();
    if (ctx == connectionMap.get(cxnId))
    {
      connectionMap.remove(cxnId);
    }
  }

  @Override
  public void endAllConnections() throws DgException
  {
     List<ServiceContext> activeSessions = new ArrayList<ServiceContext>();
     
     /* making a separate list of active sessions */
     Collection<ServiceContext> sessionsInMap = connectionMap.values();
     if (sessionsInMap != null)
     {
       for (ServiceContext sc : sessionsInMap)
         activeSessions.add(sc);
     }

     if (activeSessions != null)
     {
       for (ServiceContext s : activeSessions)
         endConnection(s);
     }
  }
}
