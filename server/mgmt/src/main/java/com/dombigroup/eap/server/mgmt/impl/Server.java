/**
 * Server.java
 */
package com.dombigroup.eap.server.mgmt.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ServiceLoader;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.server.mgmt.exceptions.MgmtException;
import com.dombigroup.eap.server.mgmt.interfaces.ConfigMap;
import com.dombigroup.eap.server.mgmt.interfaces.ServerConfig;
import com.dombigroup.eap.server.mgmt.interfaces.ServerMgmt;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamValue;
import com.dombigroup.eap.server.platform.mgmt.PlatformState;
import com.dombigroup.eap.server.platform.mgmt.PlatformStatus;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationService;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationServiceFactory;
import com.dombigroup.eap.server.mgmt.exceptions.MERRMessages;

/**
 * @author sanjay
 * 
 */
public class Server implements ServerMgmt
{
  private static Server                                  serverInst;
  private static ServiceLoader<IPluggableServiceFactory> svcLoader = ServiceLoader.load(IPluggableServiceFactory.class);
  private static Logger                                  logger    = Logger.getLogger(Server.class.getName());
  private List<IPluggableService>                        svcList   = new ArrayList<IPluggableService>();

  private ServerConfig   config;
  private boolean        running = false;
  private ConnectionMgmt connectionManager;

  private Server() throws ServiceException
  {
    /* initialize config so that it can be set through UI */
    config = new ServerConfig();
    connectionManager = new ConnectionMgmt();
    for (IPluggableServiceFactory f : svcLoader)
    {
      for (String svcName : f.getServiceNames())
      {
        Config svcConfig = new Config();
        svcConfig.setParamInfo(f.getConfigParams(svcName));
        config.setServiceConfig(svcName, svcConfig);
      }
    }
  }

  public synchronized static Server getServer() throws MgmtException
  {
    if (serverInst == null)
      try
      {
        serverInst = new Server();
      }
      catch (ServiceException e)
      {
        throw new MgmtException(MERRMessages.MERR_STARTUP, e);
      }
    return serverInst;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.mgmt.interfaces.ServerMgmt#setConfig(com.
   * dombigroup.eap.server.platform.context.ServiceContext,
   * com.dombigroup.eap.server.mgmt.interfaces.ServerConfig)
   */
  @Override
  public void setConfig(ServiceContext ctx, ServerConfig c)
  {
    config = c;
  }

  /**
   * @return the connectionManager
   */
  public ConnectionMgmt getConnectionManager()
  {
    return connectionManager;
  }

  /**
   * @return the running
   */
  public boolean isRunning()
  {
    return running;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.mgmt.interfaces.ServerMgmt#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws MgmtException
  {
    try
    {
      ctx.startCall();
      PlatformStatus.getInstance().setState(PlatformState.STARTUP_IN_PROGRESS);

      /*
       * We do this in multiple passes due to dependencies First pass in the
       * configs Then instantiate the services Then start them up
       */
      for (IPluggableServiceFactory f : svcLoader)
      {
        for (String svcName : f.getServiceNames())
        {
          Config svcConfig = config.getServiceConfig(svcName);
          f.setServiceConfig(svcName, svcConfig);
        }
      }

      for (IPluggableServiceFactory f : svcLoader)
      {
        for (String svcName : f.getServiceNames())
        {
          svcList.add(f.getService(svcName));
        }
      }

      for (IPluggableService s : svcList)
      {
        s.startup(ctx);
      }

      PlatformStatus.getInstance().setState(PlatformState.READY);
      ctx.endCall(false);
    }
    catch (Exception e)
    {
      throw new MgmtException(MERRMessages.MERR_STARTUP, e);
    }
    running = true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.mgmt.interfaces.ServerMgmt#install()
   */
  public void install(ServiceContext ctx) throws MgmtException
  {
    try
    {
      ctx.startCall();
      PlatformStatus.getInstance().setState(PlatformState.INSTALL_IN_PROGRESS);
      for (IPluggableService s : svcList)
      {
        s.install(ctx);
      }

      for (IPluggableService s : svcList)
      {
        s.postInstall1(ctx);
      }
      PlatformStatus.getInstance().setState(PlatformState.READY);
      ctx.endCall(false);
    }
    catch (Exception e)
    {
      throw new MgmtException(MERRMessages.MERR_INSTALL, e);
    }
  }

  public void uninstall(ServiceContext ctx) throws MgmtException
  {
    try
    {
      ctx.startCall();
      privCheck(ctx, OperationType.DELETE);
      PlatformStatus.getInstance()
          .setState(PlatformState.UNINSTALL_IN_PROGRESS);

      for (IPluggableService s : svcList)
      {
        s.uninstall(ctx);
      }
      PlatformStatus.getInstance().setState(PlatformState.NOTREADY);
      ctx.endCall(false);
    }
    catch (Exception e)
    {
      throw new MgmtException(MERRMessages.MERR_UNINSTALL, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.mgmt.interfaces.ServerMgmt#shutdown()
   */
  @Override
  public void shutdown(ServiceContext ctx) throws MgmtException
  {
    try
    {
      ctx.startCall();
      privCheck(ctx, OperationType.UPDATE);
      PlatformStatus.getInstance().setState(PlatformState.SHUTDOWN_IN_PROGRESS);

      /* end all sessions */
      logger.info("Shutting down all sessions");
      connectionManager.endAllConnections();

      for (IPluggableService s : svcList)
      {
        s.shutdown(ctx);
      }
      PlatformStatus.getInstance().setState(PlatformState.NOTREADY);
      ctx.endCall(false);
    }
    catch (Exception e)
    {
      throw new MgmtException(MERRMessages.MERR_SHUTDOWN, e);
    }
    running = false;
  }

  @Override
  public ServerConfig getConfig()
  {
    return config;
  }

  @Override
  public void saveConfig(ServiceContext ctx, String fileName)
      throws MgmtException
  {
    try
    {
      privCheck(ctx, OperationType.READ);

      File f = new File(fileName);
      if (!f.exists())
        f.createNewFile();
      BufferedOutputStream o = new BufferedOutputStream(new FileOutputStream(
          f));
      JAXBContext jaxbCtx = JAXBContext.newInstance(ServerConfig.class,
          ConfigMap.class, Config.class);
      Marshaller m = jaxbCtx.createMarshaller();
      m.marshal(config, o);
      o.flush();
      o.close();
    }
    catch (Exception e)
    {
      throw new MgmtException(MERRMessages.MERR_CONFIG_SAVE, e);
    }
  }

  @Override
  public void readConfig(ServiceContext ctx, String fileName)
      throws MgmtException
  {
    try
    {
      privCheck(ctx, OperationType.UPDATE);

      File f = new File(fileName);
      BufferedInputStream o = new BufferedInputStream(new FileInputStream(f));
      JAXBContext jaxbCtx = JAXBContext.newInstance(ServerConfig.class,
          ConfigMap.class, Config.class);
      Unmarshaller um = jaxbCtx.createUnmarshaller();
      ServerConfig c = (ServerConfig) um.unmarshal(o);
      Iterator<Entry<String, Config>> it = c.getSvcConfigs().entrySet()
          .iterator();
      while (it.hasNext())
      {
        Entry<String, Config> conf = it.next();
        Config svcConfig = config.getServiceConfig(conf.getKey());
        for (ParamValue pv : conf.getValue().getParamValues())
        {
          svcConfig.setParamValue(pv.getParamName(), pv.getParamValue());
        }
      }
      o.close();
    }
    catch (Exception e)
    {
      throw new MgmtException(MERRMessages.MERR_CONFIG_READ, e);
    }
  }

  private void privCheck(ServiceContext ctx, Operation o)
      throws ServiceException
  {
    if (running)
    {
      AuthorizationService as = AuthorizationServiceFactory
          .getAuthorizationService();
      as.assertAccess(ctx.getSecContext(), PlatformResource.SERVER,
          Persisted.INVALID_ID, this, o);
    }
  }
}
