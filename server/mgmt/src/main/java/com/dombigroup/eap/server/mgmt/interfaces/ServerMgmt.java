/**
 * ServerMgmt.java
 */
package com.dombigroup.eap.server.mgmt.interfaces;

import com.dombigroup.eap.server.mgmt.exceptions.MgmtException;
import com.dombigroup.eap.server.platform.context.ServiceContext;

/**
 * @author sanjay
 * 
 */
public interface ServerMgmt
{
  public ServerConfig getConfig();

  public void setConfig(ServiceContext ctx, ServerConfig c);
  
  public void saveConfig(ServiceContext ctx, String fileName) throws MgmtException;
  
  public void readConfig(ServiceContext ctx, String fileName) throws MgmtException;

  public void startup(ServiceContext ctx) throws MgmtException;

  public void install(ServiceContext ctx) throws MgmtException;

  public void uninstall(ServiceContext ctx) throws MgmtException;

  public void shutdown(ServiceContext ctx) throws MgmtException;
}
