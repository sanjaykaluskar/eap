/**
 * IConnectionMgmt.java
 */
package com.dombigroup.eap.server.mgmt.interfaces;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.mgmt.exceptions.ConnectionException;
import com.dombigroup.eap.server.platform.context.ServiceContext;

/**
 * @author sanjay
 * 
 */
public interface IConnectionMgmt
{
  /**
   * createConnection creates a new connection for the specified user
   * 
   * @param orgId
   *          - org id of the user
   * @param userName
   *          - user name
   * @param password
   *          - password
   */
  public ServiceContext createConnection(int orgId, String userName,
      String password) throws ConnectionException;

  /**
   * createConnection creates a new connection for the specified user
   * 
   * @param orgName
   *          - org name
   * @param userName
   *          - user name
   * @param password
   *          - password
   */
  public ServiceContext createConnection(String orgName, String userName,
      String password) throws ConnectionException;

  /**
   * returnConnection returns a connection if it exists (was previously created)
   * else null
   */
  public ServiceContext returnConnection(int cxnId);

  /**
   * endConnection removes existing connection
   * @throws DgException 
   */
  public void endConnection(ServiceContext ctx) throws DgException;

  /**
   * endAllConnections removes all existing connections
   * @throws DgException 
   */
  public void endAllConnections() throws DgException;
}
