/**
 * AddressMgmtFactory.java
 */
package com.dombigroup.eap.server.platform.pomgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.Phone;
import com.dombigroup.eap.server.platform.common.AggregateMgmtService;
import com.dombigroup.eap.server.platform.common.PClient;
import com.dombigroup.eap.server.platform.common.PClientMgmtService;
import com.dombigroup.eap.server.platform.contactmgmt.AddressMgmtService;
import com.dombigroup.eap.server.platform.contactmgmt.PhoneMgmtService;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.EntityMgmt;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamInfo;
import com.dombigroup.eap.server.platform.usermgmt.PUserMgmtService;
import com.dombigroup.eap.server.platform.orgmgmt.POrg;
import com.dombigroup.eap.server.platform.orgmgmt.POrgMgmtService;
import com.dombigroup.eap.server.platform.usermgmt.PUser;
import com.dombigroup.eap.server.platform.privmgmt.PDisabledPolicyMgmtService;
import com.dombigroup.eap.server.platform.privmgmt.PGrantedObjectPrivMgmtService;
import com.dombigroup.eap.server.platform.privmgmt.PGrantedRole;
import com.dombigroup.eap.server.platform.privmgmt.PGrantedObjectPriv;
import com.dombigroup.eap.server.platform.privmgmt.PDisabledPolicy;
import com.dombigroup.eap.server.platform.privmgmt.PGrantedRoleMgmtService;

/**
 * @author sanjay
 * 
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class EntityMgmtFactory implements IPluggableServiceFactory
{
  public static final String                SVC_AGGREGATE          = "AGGREGATE MANAGEMENT SERVICE";
  public static final String                SVC_ADDRESS            = "ADDRESS MANAGEMENT SERVICE";
  public static final String                SVC_PHONE              = "PHONE MANAGEMENT SERVICE";
  public static final String                SVC_PUSER              = "USER (ENTITY) MANAGEMENT SERVICE";
  public static final String                SVC_CLIENT             = "CLIENT MANAGEMENT SERVICE";
  public static final String                SVC_POBJECT            = "POBJECT MANAGEMENT SERVICE";
  public static final String                SVC_PORG               = "ORG (ENTITY) MANAGEMENT SERVICE";
  public static final String                SVC_PGRANTEDROLE       = "GRANTED ROLE (ENTITY) MANAGEMENT SERVICE";
  public static final String                SVC_PGRANTEDOBJECTPRIV = "GRANTED OBJECT PRIVILEGES (ENTITY) MANAGEMENT SERVICE";
  public static final String                SVC_PDISABLEDPOLICY    = "DISABLED POLICY (ENTITY) MANAGEMENT SERVICE";

  private static Map<String, EntityService> svcMap                 = new HashMap<String, EntityService>();

  static
  {
    svcMap.put(SVC_POBJECT, new EntityService<Integer, PObject>(null, null));
    svcMap
        .put(SVC_AGGREGATE, new EntityService<Integer, Aggregate>(null, null));
    svcMap.put(SVC_ADDRESS, new EntityService<Integer, Address>(null, null));
    svcMap.put(SVC_PHONE, new EntityService<Integer, Phone>(null, null));
    svcMap.put(SVC_CLIENT, new EntityService<Integer, PClient>(null, null));
    svcMap.put(SVC_PORG, new EntityService<Integer, POrg>(null, null));
    svcMap.put(SVC_PGRANTEDROLE, new EntityService<Integer, PGrantedRole>(null,
        null));
    svcMap.put(SVC_PGRANTEDOBJECTPRIV,
        new EntityService<Integer, PGrantedObjectPriv>(null,
            null));
    svcMap.put(SVC_PUSER, new EntityService<Integer, PUser>(null, null));
    svcMap.put(SVC_PDISABLEDPOLICY,
        new EntityService<Integer, PDisabledPolicy>(null, null));
  }

  private static synchronized PluggableEntityMgmt getEntityService(
      String svcName) throws MetadataException
  {
    EntityService es = svcMap.get(svcName);

    try
    {
      if (es.getSvc() == null)
      {
        if (svcName == SVC_AGGREGATE)
        {
          PluggableEntityMgmt<Integer, Aggregate> svc = new AggregateMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_ADDRESS)
        {
          PluggableEntityMgmt<Integer, Address> svc = new AddressMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_PHONE)
        {
          PluggableEntityMgmt<Integer, Phone> svc = new PhoneMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_PUSER)
        {
          PluggableEntityMgmt<Integer, PUser> svc = new PUserMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_CLIENT)
        {
          PluggableEntityMgmt<Integer, PClient> svc = new PClientMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_POBJECT)
        {
          PluggableEntityMgmt<Integer, PObject> svc = new PObjectMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_PORG)
        {
          PluggableEntityMgmt<Integer, POrg> svc = new POrgMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_PGRANTEDROLE)
        {
          PluggableEntityMgmt<Integer, PGrantedRole> svc = new PGrantedRoleMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_PGRANTEDOBJECTPRIV)
        {
          PluggableEntityMgmt<Integer, PGrantedObjectPriv> svc = new PGrantedObjectPrivMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
        else if (svcName == SVC_PDISABLEDPOLICY)
        {
          PluggableEntityMgmt<Integer, PDisabledPolicy> svc = new PDisabledPolicyMgmtService();
          svc.setServiceConfig(es.getConfig());
          es.setSvc(svc);
        }
      }

      return es.getSvc();
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  @Override
  public List<String> getServiceNames()
  {
    List<String> ret = new ArrayList<String>();
    for (String s : svcMap.keySet())
      ret.add(s);
    return ret;
  }

  @Override
  public List<ParamInfo> getConfigParams(String svcName)
  {
    return null;
  }

  @Override
  public Config getServiceConfig(String svcName)
  {
    EntityService e = svcMap.get(svcName);
    return e.getConfig();
  }

  @Override
  public void setServiceConfig(String svcName, Config c)
  {
    EntityService e = svcMap.get(svcName);
    e.setConfig(c);
  }

  @Override
  public IPluggableService getService(String svcName) throws ServiceException
  {
    try
    {
      return getEntityService(svcName);
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  public static EntityMgmt getEntityMgmtService(String svcName)
      throws MetadataException
  {
    return getEntityService(svcName);
  }

  static private class EntityService<K, V extends Persisted>
  {
    private PluggableEntityMgmt<K, V> svc;
    private Config                    config;

    /**
     * Constructor for EntityService
     */
    private EntityService(PluggableEntityMgmt<K, V> svc, Config config)
    {
      this.svc = svc;
      this.config = config;
    }

    /**
     * @return the svc
     */
    private PluggableEntityMgmt<K, V> getSvc()
    {
      return svc;
    }

    /**
     * @param svc
     *          the svc to set
     */
    private void setSvc(PluggableEntityMgmt<K, V> svc)
    {
      this.svc = svc;
    }

    /**
     * @return the config
     */
    private Config getConfig()
    {
      return config;
    }

    /**
     * @param config
     *          the config to set
     */
    private void setConfig(Config config)
    {
      this.config = config;
    }
  }
}
