/**
 * IEventFilterAttr.java
 */
package com.dombigroup.eap.server.platform.notifications;


/**
 * @author sanjay
 *
 */
public interface IEventFilterAttr<EventType extends Event>
{
  public String getName();

  public Class<?> getValueClass();
}
