/**
 * PDisabledPolicyMgmtService.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.EntityMgmt;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 *
 */
public class PDisabledPolicyMgmtService extends PluggableEntityMgmt<Integer, PDisabledPolicy>
    implements EntityMgmt<Integer, PDisabledPolicy>
{
  public PDisabledPolicyMgmtService()
      throws StorageException, MetadataException
  {
    super(Integer.class, PDisabledPolicy.class);
  }

  @Override
  protected void copyPo(PDisabledPolicy from, PDisabledPolicy to)
  {
    to.setPolicyName(from.getPolicyName());
    to.setDisabledBy(from.getDisabledBy());
    to.setDisabledTime(from.getDisabledTime());
  }
}
