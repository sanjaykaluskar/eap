/**
 * Sequence.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;

/**
 * @author sanjay
 *
 */
public interface SequenceInterface
{
  public int nextValue(ServiceContext ctx) throws MetadataException;
  public int currentValue();
}
