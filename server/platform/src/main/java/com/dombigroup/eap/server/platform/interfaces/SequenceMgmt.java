/**
 * Sequence.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;

/**
 * @author sanjay
 * 
 */
public interface SequenceMgmt
{
  public SequenceInterface create(ServiceContext ctx, String name, int start, int cacheSize) throws MetadataException;

  public void delete(ServiceContext ctx, String name) throws MetadataException;
  
  public SequenceInterface getSequence(ServiceContext ctx, String name) throws MetadataException;
}
