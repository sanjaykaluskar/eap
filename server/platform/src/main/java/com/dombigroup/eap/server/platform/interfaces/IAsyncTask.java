/**
 * IAsyncTask.java
 */
package com.dombigroup.eap.server.platform.interfaces;

/**
 * @author skaluska
 *
 */
public interface IAsyncTask
{
  public void addTask(AsyncTaskSpec spec);
}
