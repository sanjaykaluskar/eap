/**
 * SiteAdmin.java
 */
package com.dombigroup.eap.server.platform.privmgmt.policies;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;
import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy;

/**
 * @author sanjay
 * 
 */
public class SiteAdmin implements IAuthorizationPolicy
{
  private static final String NAME = "SITE ADMINISTRATOR";
  private static final String DESC = "Site administrator is allowed access to everything";

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#getName
   * ()
   */
  @Override
  public String getName()
  {
    return NAME;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#
   * getDescription()
   */
  @Override
  public String getDescription()
  {
    return DESC;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#orgLevel
   * ()
   */
  @Override
  public boolean orgLevel()
  {
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#startup
   * ()
   */
  @Override
  public void startup()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#enable()
   */
  @Override
  public void enable()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#disable
   * ()
   */
  @Override
  public void disable()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#shutdown
   * ()
   */
  @Override
  public void shutdown()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#
   * getImplementation()
   */
  @Override
  public IAuthorization getImplementation()
  {
    return new SiteAdminImpl();
  }

  @Override
  public boolean resourceTypeInScope(ResourceType rt)
  {
    return true;
  }

  @Override
  public boolean operationInScope(Operation op)
  {
    return true;
  }
}
