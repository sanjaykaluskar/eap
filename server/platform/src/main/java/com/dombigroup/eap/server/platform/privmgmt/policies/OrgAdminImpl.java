/**
 * SiteAdminImpl.java
 */
package com.dombigroup.eap.server.platform.privmgmt.policies;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;
import com.dombigroup.eap.server.platform.privmgmt.PlatformRole;

/**
 * @author sanjay
 *
 */
public class OrgAdminImpl implements IAuthorization
{
  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorization#check(com.dombigroup.eap.server.platform.context.SecurityContext, com.dombigroup.eap.common.datamodels.ResourceType, int, java.lang.Object, com.dombigroup.eap.common.datamodels.Operation)
   */
  @Override
  public boolean check(SecurityContext ctx, ResourceType resourceType,
      int resourceId, Object resource, Operation o)
  {
    boolean ret = false;
    if (ctx.hasRole(PlatformRole.ORG_ADMIN))
    {
      if (o.equals(OperationType.CREATE))
      {
        ret = !resourceType.equals(PlatformResource.COMPANY);
      }
      else if ((resource != null) && (resource instanceof Persisted))
      {
        Persisted p = (Persisted)resource;
        ret = (p.getOrgId() == ctx.getOrgId());
      }
    }
    
    return ret;
  }
}
