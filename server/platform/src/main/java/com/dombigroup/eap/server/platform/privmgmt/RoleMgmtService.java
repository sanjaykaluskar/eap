/**
 * RoleMgmtService.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.GrantedRole;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.AggregateMgmt;
import com.dombigroup.eap.server.platform.interfaces.IRole;
import com.dombigroup.eap.server.platform.interfaces.IRoleEnumerator;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.interfaces.RoleMgmt;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

/**
 * @author sanjay
 * 
 */
public class RoleMgmtService extends PluggableService implements RoleMgmt
{
  private static ServiceLoader<IRoleEnumerator> roleLoader = ServiceLoader
                                                               .load(IRoleEnumerator.class);
  private static Map<String, IRole>             lookupTable;
  private PGrantedRoleMgmtService               pGrantedRoleSvc;
  private AggregateMgmt                         aggrSvc;
  private UserMgmt                              userSvc;

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#install()
   */
  @Override
  public void install(ServiceContext ctx)
  {

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    try
    {
      // discover all roles
      lookupTable = new HashMap<String, IRole>();
      for (IRoleEnumerator re : roleLoader)
      {
        for (IRole r : re.listRoles())
          lookupTable.put(r.getName(), r);
      }

      // init references to other services
      pGrantedRoleSvc = (PGrantedRoleMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PGRANTEDROLE);

      aggrSvc = (AggregateMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_AGGREGATE);

      userSvc = UserMgmtServiceFactory.getUserMgmtService();
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_ROLE_SERVICE_STARTUP, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.RoleMgmt#getAvailableRoles
   * (com.dombigroup.eap.server.platform.context.ServiceContext)
   */
  public List<IRole> getAvailableRoles(ServiceContext ctx)
      throws MetadataException
  {
    List<IRole> ret = new ArrayList<IRole>();
    for (IRole r : lookupTable.values())
    {
      ret.add(r);
    }
    return ret;
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.RoleMgmt#getRoleList(com.dombigroup.eap.server.platform.context.ServiceContext, int)
   */
  @Override
  public List<GrantedRole> getRoleList(ServiceContext ctx, int listId)
      throws MetadataException
  {
    List<GrantedRole> ret = null;
    if (listId != Persisted.INVALID_ID)
    {
      Aggregate aggr = aggrSvc.get(ctx, listId, LockMode.SHARED, ctx
          .getWorkUnit());
      if (aggr != null)
      {
        ret = new ArrayList<GrantedRole>();
        for (Persisted grObj : aggr.getObjList())
        {
          PGrantedRole pgr = pGrantedRoleSvc.get(ctx, grObj.getId(),
              LockMode.SHARED, ctx.getWorkUnit());
          String grantorName = userSvc.getUser(ctx, pgr.getGrantor())
              .getUserName();
          String granteeName = userSvc.getUser(ctx, pgr.getUserId())
              .getUserName();
          String desc = lookupTable.get(pgr.getRoleName()).getDescription();
          GrantedRole gr = pgr.toGrantedRole(grantorName, granteeName, desc);
          ret.add(gr);
        }
      }
    }

    return ret;
  }

  private PGrantedRole createGrantedRole(ServiceContext ctx, int listId,
      int userId,
      String roleName) throws MetadataException
  {
    int pgrId = pGrantedRoleSvc.nextId(ctx);
    PGrantedRole pgr = new PGrantedRole();
    pgr.setId(pgrId);
    pgr.setUserId(userId);
    pgr.setRoleName(roleName);
    pgr.setGrantor(ctx.getSecContext().getUserId());
    pgr.setGrantDate(new DayTime());
    pgr.setEnabled(true);
    pGrantedRoleSvc.create(ctx, pgrId, pgr);
    return pgr;
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.RoleMgmt#addRole(com.dombigroup.eap.server.platform.context.ServiceContext, int, int, java.lang.String)
   */
  @Override
  public void addRole(ServiceContext ctx, int listId, int userId,
      String roleName)
      throws MetadataException
  {
    if (lookupTable.get(roleName) == null)
      throw new MetadataException(PERRMessages.PERR_INVALID_ROLE);

    /* create a grant desc */
    PGrantedRole pgr = createGrantedRole(ctx, listId, userId, roleName);

    Aggregate roleList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    roleList.add(pgr);
    aggrSvc.update(ctx, listId, roleList);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.RoleMgmt#removeRole(com.dombigroup.eap.server.platform.context.ServiceContext, int, java.lang.String)
   */
  @Override
  public void removeRole(ServiceContext ctx, int listId, String roleName)
      throws MetadataException
  {
    if (lookupTable.get(roleName) == null)
      throw new MetadataException(PERRMessages.PERR_INVALID_ROLE);

    Aggregate roleList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    Persisted found = null;
    if (roleList != null)
    {
      for (Persisted grObj : roleList.getObjList())
      {
        PGrantedRole pgr = pGrantedRoleSvc.get(ctx, grObj.getId(),
            LockMode.EXCLUSIVE, ctx.getTransaction());
        if (pgr.getRoleName().equals(roleName))
        {
          found = grObj;
          break;
        }
      }

      if (found != null)
      {
        pGrantedRoleSvc.delete(ctx, found.getId());
        roleList.remove(found);
        aggrSvc.update(ctx, listId, roleList);
      }
    }
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.RoleMgmt#createRoleList(com.dombigroup.eap.server.platform.context.ServiceContext, int, java.lang.String)
   */
  @Override
  public int createRoleList(ServiceContext ctx, int userId,
      String roleName) throws MetadataException
  {
    int listId = aggrSvc.nextId(ctx);
    PGrantedRole pgr = createGrantedRole(ctx, listId, userId, roleName);
    Aggregate roleList = new Aggregate();
    roleList.setId(listId);
    roleList.add(pgr);
    aggrSvc.create(ctx, listId, roleList);

    return listId;
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.RoleMgmt#deleteRoleList(com.dombigroup.eap.server.platform.context.ServiceContext, int)
   */
  @Override
  public void deleteRoleList(ServiceContext ctx, int listId)
      throws MetadataException
  {
    Aggregate roleList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    if (roleList != null)
    {
      for (Persisted grObj : roleList.getObjList())
      {
        pGrantedRoleSvc.delete(ctx, grObj.getId());
      }
      aggrSvc.delete(ctx, listId);
    }
  }
}
