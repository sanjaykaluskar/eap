/**
 * StorageResultSet.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.server.platform.exceptions.StorageException;

/**
 * @author sanjay
 *
 */
public abstract class StorageResultSet
{
  abstract public Object next() throws StorageException;
  
  abstract public void close() throws StorageException;
}
