/**
 * IEventService.java
 */
package com.dombigroup.eap.server.platform.notifications;

/**
 * @author sanjay
 *
 */
public interface IEventService<EventType extends Event>
{
  public Class<?> getEventType();

  public boolean evaluateFilter(EventFilter<EventType> filter, EventType event);
}
