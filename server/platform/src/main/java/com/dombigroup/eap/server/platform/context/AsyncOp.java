/**
 * 
 */
package com.dombigroup.eap.server.platform.context;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.dombigroup.eap.common.exceptions.DgException;

/**
 * @author skaluska
 * 
 *         AsyncOp represents an operation that is invoked asynchronously. The
 *         implementor of the operation defines it as a subclass of AsyncOp. For
 *         example, an asynchronous operation Foo would be defined as:
 * 
 *         class Foo extends AsyncOp {...}
 * 
 *         The implementor captures any necessary inputs, runtime state and
 *         output in the subclass. The actual work that needs to be done in the
 *         operation is implemented as the method called operation(), which is
 *         invoked in a separate thread (different from the calling thread). The
 *         operation needs to start its own work unit, and should check for
 *         InterruptedException from time to time in order to support cancelling
 *         it.
 * 
 *         The caller of the asynchronous operation invokes it by calling
 *         start(). For example,
 * 
 *         AsyncOp foo = new Foo(...); foo.start();
 * 
 *         There is an observer interface for components interested in following
 *         the progress of the operation. This can be used, for example, to
 *         monitor the operation.
 */
public abstract class AsyncOp
{
  public enum State
  {
    CREATED,
    STARTED,
    INTERRUPTED,
    CANCELLED,
    FAILED,
    COMPLETED;
  }

  private static int             NEXT_ID     = 0;

  private static ExecutorService execService = Executors.newCachedThreadPool();

  /** unique id for this operation */
  private int                    id;

  /** description */
  private String                 desc;

  /** service context to be used for the operation */
  private ServiceContext         ctx;

  /** future - used to cancel or wait for completion */
  private Future<?>              future;

  /** last known state */
  private State                  state;

  /** status message - updated by the operation */
  private String                 statusMessage;

  /** fraction of work completed- updated by the operation */
  private float                  completedFraction;

  /** error message if any */
  private String                 error;

  /** observers */
  private List<IOpNotification>  observers;

  protected AsyncOp(String d, ServiceContext sc)
  {
    id = NEXT_ID++;
    desc = d;
    ctx = sc;
    state = State.CREATED;
    observers = new ArrayList<IOpNotification>();
  }

  /**
   * Operation that is invoked. Note that this method is expected to do the
   * actual work synchronously. Return value, if any, can be saved as private
   * member, and exposed through a getter.
   */
  public abstract void operation() throws DgException;

  /**
   * @return the id
   */
  public int getId()
  {
    return id;
  }

  /**
   * @return the desc
   */
  public String getDesc()
  {
    return desc;
  }

  /**
   * @return the ctx
   */
  public ServiceContext getCtx()
  {
    return ctx;
  }

  /**
   * @return the future
   */
  public Future<?> getFuture()
  {
    return future;
  }

  /**
   * @return the state
   */
  public State getState()
  {
    return state;
  }

  /**
   * @param state
   *          the state to set
   */
  private synchronized void setState(State state)
  {
    this.state = state;
    this.notifyObservers();
  }

  /**
   * @return the statusMessage
   */
  public String getStatusMessage()
  {
    return statusMessage;
  }

  /**
   * @param statusMessage
   *          the statusMessage to set
   */
  public void setStatusMessage(String statusMessage)
  {
    this.statusMessage = statusMessage;
    this.notifyObservers();
  }

  /**
   * @return the completedFraction
   */
  public float getCompletedFraction()
  {
    return completedFraction;
  }

  /**
   * @param completedFraction
   *          the completedFraction to set
   */
  public void setCompletedFraction(float completedFraction)
  {
    this.completedFraction = completedFraction;
    this.notifyObservers();
  }

  /**
   * @return the error
   */
  public String getError()
  {
    return error;
  }

  /**
   * @param error
   *          the error to set
   */
  private void setError(String error)
  {
    this.error = error;
  }

  /**
   * Adds the specified observer to this asynchronous operation. The observer's
   * methods are invoked by the thread executing the operation when there are
   * state changes.
   */
  public synchronized void addObserver(IOpNotification o)
  {
    observers.add(o);
  }

  /**
   * Removes the specified observer.
   */
  public synchronized void removeObserver(IOpNotification o)
  {
    observers.remove(o);
  }
  
  /**
   * Notify all the observers about a change.
   */
  private void notifyObservers()
  {
    for (IOpNotification n : observers)
	  n.onAsyncOpChange(this);
  }

  /**
   * Starts the asynchronous operation in a new thread. This method sets the
   * future, which can be used by another thread to manipulate the operation,
   * e.g., wait for completion or cancel the operation.
   */
  public void start()
  {
    ctx.addAsyncOp(this);
    AsyncOpTask task = new AsyncOpTask(this);
    future = execService.submit(task);
  }

  /**
   * Interrupts the asynchronous operation. Note that interruption does not
   * guarantee cancellation.
   */
  public void interrupt()
  {
    future.cancel(true);
    setState(State.INTERRUPTED);
  }

  /**
   * Blocks until the asynchronous operation completes.
   */
  public void waitForCompletion() throws InterruptedException
  {
    try
    {
      if ((state != State.FAILED) &&
          (state != State.CANCELLED) &&
          (state != State.COMPLETED))
        future.get();
    }
    catch (ExecutionException e)
    {
      /* do nothing since the task will set it's status accordingly */
    }
  }

  /**
   * A convenience method to update status message for an operation. It sets the
   * message in an async operation being performed by the thread, if any. If
   * there is no async operation, it is a no-op.
   */
  public static void updateStatus(ServiceContext ctx, String msg)
  {
    AsyncOp op = ctx.getAsyncOp();
    if (op != null)
      op.setStatusMessage(msg);
  }

  /**
   * A convenience method to update completed fraction for an operation. It sets
   * the fraction in an async operation being performed by the thread, if any.
   * If there is no async operation, it is a no-op.
   */
  public static void updateCompletedFraction(ServiceContext ctx, float fraction)
  {
    AsyncOp op = ctx.getAsyncOp();
    if (op != null)
      op.setCompletedFraction(fraction);
  }

  /**
   * Increments the completed fraction along with an optional status message in
   * the specified async op.
   * 
   * @param asyncOp
   *          - the async op whose completed fraction and message are to be
   *          updated
   * @param incr
   *          - amount of increment for the work completed
   * @param status
   *          - optional status message
   */
  public static synchronized void incrCompletedFraction(AsyncOp asyncOp, float incr,
      String status)
  {
    if (asyncOp != null)
    {
      float workCompletedSoFar = asyncOp.getCompletedFraction() + incr;
      asyncOp.setCompletedFraction(workCompletedSoFar);
      if (status != null)
        asyncOp.setStatusMessage(status);
    }
  }

  /**
   * Cancels specified async ops
   * 
   * @param ops
   *          - async op array
   */
  public static void cancelAsyncOps(AsyncOp ops[])
  {
    if (ops != null)
    {
      for (int i = 0; i < ops.length; i++)
      {
        if (ops[i] != null)
          ops[i].interrupt();
      }
    }
  }

  private static class AsyncOpTask implements Runnable
  {
    private AsyncOp op;

    private AsyncOpTask(AsyncOp o)
    {
      op = o;
    }

    @Override
    public void run()
    {
      /* set up op in server session */
      op.getCtx().setAsyncOp(op);

      /* initialize op state */
      boolean failure = false;
      op.setState(State.STARTED);
      try
      {
        op.operation();
      }
      catch (Exception e)
      {
        /* set the state */
        failure = true;
        e.printStackTrace();
        if (e instanceof InterruptedException)
          op.setState(State.CANCELLED);
        else {          
          /* record error */
          op.setError(e.getMessage());
          op.setState(State.FAILED);
        }
        
        /* abort any ongoing work & free resources */
        try
        {
          op.getCtx().resetWorkUnit();
        }
        catch (DgException e1)
        {
        }
      }
      if (!failure)
      {
    	op.setCompletedFraction(1);
        op.setState(State.COMPLETED);        
      }
    }
  }
}
