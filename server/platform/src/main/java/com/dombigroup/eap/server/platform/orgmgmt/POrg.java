/**
 * Org.java
 */
package com.dombigroup.eap.server.platform.orgmgmt;

import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.utils.ExpressionUtils;

/**
 * @author sanjay
 * 
 */
public class POrg extends Persisted
{
  private String name;
  private int    addrId;
  private int    phoneList;
  private int    disabledPolicyListId;

  public POrg()
  {
  }

  public POrg(Company c, int addr, int ph, int dpl)
  {
    copyPObjectInfo(c);
    name = c.getName();
    addrId = addr;
    phoneList = ph;
    disabledPolicyListId = dpl;
  }

  public boolean equalAttrs(Company c)
  {
    return equalPObject(c)
        && ExpressionUtils.safeEqual(c.getName(), name);
  }

  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name
   *          the name to set
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return the addrId
   */
  public int getAddrId()
  {
    return addrId;
  }

  /**
   * @param addrId
   *          the addrId to set
   */
  public void setAddrId(int addrId)
  {
    this.addrId = addrId;
  }

  /**
   * @return the phoneList
   */
  public int getPhoneList()
  {
    return phoneList;
  }

  /**
   * @param phoneList
   *          the phoneList to set
   */
  public void setPhoneList(int phoneList)
  {
    this.phoneList = phoneList;
  }

  /**
   * @return the disabledPolicyListId
   */
  public int getDisabledPolicyListId()
  {
    return disabledPolicyListId;
  }

  /**
   * @param disabledPolicyList
   *          the disabledPolicyListId to set
   */
  public void setDisabledPolicyListId(int disabledPolicyList)
  {
    this.disabledPolicyListId = disabledPolicyList;
  }
}
