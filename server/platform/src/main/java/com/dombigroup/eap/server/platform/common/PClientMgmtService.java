/**
 * PClientMgmtService.java
 */
package com.dombigroup.eap.server.platform.common;

import com.dombigroup.eap.common.datamodels.Client;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.AddressMgmt;
import com.dombigroup.eap.server.platform.interfaces.PhoneMgmt;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public class PClientMgmtService extends PluggableEntityMgmt<Integer, PClient>
{
  private PhoneMgmt   phoneSvc;
  private AddressMgmt addressSvc;

  public PClientMgmtService() throws StorageException, MetadataException
  {
    super(Integer.class, PClient.class);
  }

  @Override
  protected void copyPo(PClient from, PClient to)
  {
    to.setPrefix(from.getPrefix());
    to.setFirstName(from.getFirstName());
    to.setMiddleName(from.getMiddleName());
    to.setLastName(from.getLastName());
    to.setEmail(from.getEmail());
    to.setAddressId(from.getAddressId());
    to.setPhoneListId(from.getPhoneListId());
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    try
    {
      phoneSvc = (PhoneMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PHONE);
      addressSvc = (AddressMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_ADDRESS);
    }
    catch (MetadataException e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  public void populateClient(ServiceContext ctx, int cid, Client client)
      throws MetadataException
  {
    try
    {
      PClient pclient = get(ctx, cid, LockMode.SHARED, ctx.getWorkUnit());
      if (pclient != null)
      {
        client.copyPObjectInfo(pclient);
        client.setPrefix(pclient.getPrefix());
        client.setFirstName(pclient.getFirstName());
        client.setMiddleName(pclient.getMiddleName());
        client.setLastName(pclient.getLastName());
        client.setEmail(pclient.getEmail());
        client.setAddress(addressSvc.getAddress(ctx, pclient.getAddressId()));
        client.setPhoneNumbers(phoneSvc.getPhoneList(ctx,
            pclient.getPhoneListId()));
      }
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_CLIENT_ACCESS, e,
          String.valueOf(cid));
    }
  }

  private PClient populatePClient(ServiceContext ctx, int pcId, Client c,
      PClient pc) throws MetadataException
  {
    boolean allocate = ((pc == null) || ((pc != null) && !pc.equalAttrs(c)));

    /* update address */
    int addrId = (pc == null) ? Persisted.INVALID_ID : pc.getAddressId();
    if (addrId != Persisted.INVALID_ID)
    {
      addressSvc.update(ctx, addrId, c.getAddress());
    }
    else if (c.getAddress() != null)
    {
      addrId = addressSvc.createAddress(ctx, c.getAddress());
      allocate = true;
    }

    /* update phone list */
    int phoneListId = (pc == null) ? Persisted.INVALID_ID : pc.getPhoneListId();
    if (phoneListId != Persisted.INVALID_ID)
    {
      phoneSvc.deletePhoneList(ctx, phoneListId);
      phoneListId = 0;
      allocate = true;
    }
    if ((c.getPhoneNumbers() != null) && (c.getPhoneNumbers().size() > 0))
    {
      phoneListId = phoneSvc.createPhoneList(ctx, c.getPhoneNumbers());
      allocate = true;
    }

    if (allocate)
      pc = new PClient(pcId, c, addrId, phoneListId);

    return pc;
  }

  public int createClient(ServiceContext ctx, Client c)
      throws MetadataException
  {
    try
    {
      int pcId = nextId(ctx);
      PClient pc = populatePClient(ctx, pcId, c, null);
      return create(ctx, pcId, pc);
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_CLIENT_CREATION, e,
          c.getFirstName(), c.getLastName());
    }
  }

  public void updateClient(ServiceContext ctx, int cid, Client c)
      throws MetadataException
  {
    try
    {
      PClient existingPClient = get(ctx, cid, LockMode.EXCLUSIVE, ctx
          .getTransaction());
      PClient newPclient = populatePClient(ctx, cid, c, existingPClient);
      if (existingPClient != newPclient)
        update(ctx, cid, newPclient);
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_CLIENT_UPDATE, e,
          c.getFirstName(), c.getLastName());
    }
  }
}
