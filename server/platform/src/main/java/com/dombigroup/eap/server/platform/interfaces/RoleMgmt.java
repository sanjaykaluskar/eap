/**
 * RoleMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.common.datamodels.GrantedRole;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;

/**
 * @author sanjay
 * 
 */
public interface RoleMgmt
{
  /**
   * getAvailableRoles - returns list of available roles
   */
  public List<IRole> getAvailableRoles(ServiceContext ctx)
      throws MetadataException;

  /**
   * getRoles - returns the list of roles currently assigned to the user
   * 
   * @throws MetadataException
   */
  public List<GrantedRole> getRoleList(ServiceContext ctx, int listId)
      throws MetadataException;

  /**
   * createRoleList
   */
  public int createRoleList(ServiceContext ctx, int userId,
      String roleName) throws MetadataException;

  /**
   * addRole - adds the specified role to the user
   */
  public void addRole(ServiceContext ctx, int listId, int userId,
      String roleName)
      throws MetadataException;

  /**
   * removeRole - removes the specified role from the user
   */
  public void removeRole(ServiceContext ctx, int listId, String roleName)
      throws MetadataException;

  /**
   * deleteRoleList
   */
  public void deleteRoleList(ServiceContext ctx, int listId)
      throws MetadataException;

}
