/**
 * ObjectLock.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

/**
 * @author sanjay
 * 
 */
@SuppressWarnings("rawtypes")
public class ObjectLock
{
  private LockMode     mode;
  private ObjectCache  cache;
  private CachedObject object;

  /**
   * Constructor for ObjectLock
   */
  ObjectLock(LockMode mode, ObjectCache cache, CachedObject object)
  {
    this.mode = mode;
    this.cache = cache;
    this.object = object;
  }

  /**
   * @return the mode
   */
  public LockMode getMode()
  {
    return mode;
  }
  
  /**
   * @return the cache
   */
  public ObjectCache getCache()
  {
    return cache;
  }
  
  /**
   * @return the object
   */
  public CachedObject getObject()
  {
    return object;
  }
}
