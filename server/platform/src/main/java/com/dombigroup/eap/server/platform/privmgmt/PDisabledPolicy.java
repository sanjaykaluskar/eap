/**
 * PDisabledPolicy.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.Persisted;

/**
 * @author sanjay
 * 
 */
public class PDisabledPolicy extends Persisted
{
  private String  policyName;
  private int     disabledBy;
  private DayTime disabledTime;
  /**
   * @return the policyName
   */
  public String getPolicyName()
  {
    return policyName;
  }
  /**
   * @param policyName the policyName to set
   */
  public void setPolicyName(String policyName)
  {
    this.policyName = policyName;
  }
  /**
   * @return the disabledBy
   */
  public int getDisabledBy()
  {
    return disabledBy;
  }
  /**
   * @param disabledBy the disabledBy to set
   */
  public void setDisabledBy(int disabledBy)
  {
    this.disabledBy = disabledBy;
  }
  /**
   * @return the disabledTime
   */
  public DayTime getDisabledTime()
  {
    return disabledTime;
  }
  /**
   * @param disabledTime the disabledTime to set
   */
  public void setDisabledTime(DayTime disabledTime)
  {
    this.disabledTime = disabledTime;
  }
}
