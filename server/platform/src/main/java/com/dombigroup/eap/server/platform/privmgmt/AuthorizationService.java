/**
 * AuthorizationService.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.util.List;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.SecurityException;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;
import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.mgmt.PlatformState;
import com.dombigroup.eap.server.platform.mgmt.PlatformStatus;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtService;

/**
 * @author sanjay
 * 
 */
public class AuthorizationService extends PluggableService implements
    IAuthorization
{
  private AuthorizationPolicyService policySvc;

  public AuthorizationService() throws ServiceException
  {
    policySvc = AuthorizationPolicyServiceFactory
        .getAuthorizationPolicyService();
  }

  @Override
  public void install(ServiceContext ctx)
  {

  }

  @Override
  public void startup(ServiceContext ctx)
  {

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationService#check
   * (com.dombigroup.eap.server.platform.context.SecurityContext,
   * com.dombigroup.eap.server.platform.interfaces.Resource,
   * com.dombigroup.eap.server.platform.interfaces.Operation)
   */
  @Override
  public boolean check(SecurityContext ctx, ResourceType resourceType,
      int resourceId, Object resource, Operation o)
  {
    /*
     * This needs to be done better. Skip priv checking for SU during
     * startup/shutdown/install/etc.
     */
    if (!PlatformStatus.getInstance().getState().equals(PlatformState.READY) &&
        (ctx.getUserId() == UserMgmtService.SU_USERID))
      return true;

    boolean result = false;
    List<IAuthorizationPolicy> policyList = policySvc.getRelevantPolicies(ctx,
        resourceType, o);

    if (policyList != null)
    {
      for (IAuthorizationPolicy p : policyList)
      {
        if (ctx.isPolicyEnabled(p))
        {
          if (p.getImplementation().check(ctx, resourceType, resourceId,
              resource, o))
          {
            result = true;
            break;
          }
        }
      }
    }
    return result;
  }

  public void assertAccess(SecurityContext ctx, ResourceType resourceType,
      int resourceId, Object resource, Operation o) throws SecurityException
  {
    if (!check(ctx, resourceType, resourceId, resource, o))
      throw new SecurityException(PERRMessages.PERR_ACCESS_DENIED,
          o.getName(), (resource == null) ? "NULL" : resource.toString(),
          String.valueOf(resourceId));
  }
}
