/**
 * LockMode.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

/**
 * @author sanjay
 *
 */
public enum LockMode
{
  NULL(0),
  SHARED(1),
  EXCLUSIVE(2);

  /*
   * (compatibilityMatrix[x][y] == true)
   * iff
   * lock held in mode x is compatible with another lock in mode y
   * on the same object
   */
  private static final boolean compatibilityMatrix[][] =
  { {true, true,  true},
    {true, true,  false},
    {true, false, false}};
  
  private int modeIndex;

  private LockMode(int i)
  {
    modeIndex = i; 
  }

  /**
   * compatible returns whether the lock mode is compatible with
   * another lock 
   */
  public boolean compatible(LockMode y)
  {
    return compatibilityMatrix[modeIndex][y.modeIndex];
  }
}
