/**
 * Sequence.java
 */
package com.dombigroup.eap.server.platform.common;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.interfaces.SequenceInterface;

/**
 * @author sanjay
 * 
 */
public class Sequence extends Persisted implements SequenceInterface
{
  private String  name;
  private int     start;
  private int     cache;
  private int     currVal;
  private int     upperBound;
  private boolean needsReplenish;

  /**
   * @param name
   *          the name to set
   */
  void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @return the start
   */
  int getStart()
  {
    return start;
  }

  /**
   * @param start
   *          the start to set
   */
  void setStart(int start)
  {
    this.start = start;
  }

  /**
   * @return the cache
   */
  int getCache()
  {
    return cache;
  }

  /**
   * @param cache
   *          the cache to set
   */
  void setCache(int cache)
  {
    this.cache = cache;
  }

  /**
   * @return the needsReplenish
   */
  boolean isNeedsReplenish()
  {
    return needsReplenish;
  }

  /**
   * @param needsReplenish
   *          the needsReplenish to set
   */
  void setNeedsReplenish(boolean needsReplenish)
  {
    this.needsReplenish = needsReplenish;
  }

  /**
   * @return the currVal
   */
  int getCurrVal()
  {
    return currVal;
  }

  /**
   * @param currVal the currVal to set
   */
  void setCurrVal(int currVal)
  {
    this.currVal = currVal;
  }

  /**
   * @return the upperBound
   */
  int getUpperBound()
  {
    return upperBound;
  }

  /**
   * @param upperBound the upperBound to set
   */
  void setUpperBound(int upperBound)
  {
    this.upperBound = upperBound;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.SequenceInterface#nextVal
   * ()
   */
  @Override
  public synchronized int nextValue(ServiceContext ctx) throws MetadataException
  {
    /* delayed replenish */
    if (needsReplenish)
    {
      try
      {
        SequenceService svc = (SequenceService) SequenceMgmtFactory
            .getSequenceMgmtService();
        svc.replenish(ctx, name);
      }
      catch (Exception e)
      {
        throw new MetadataException(PERRMessages.PERR_SEQUENCE_ACCESS, e);
      }
    }

    int nextVal = ++currVal;
    if (nextVal == upperBound)
      needsReplenish = true;

    return nextVal;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.SequenceInterface#currVal
   * ()
   */
  @Override
  public int currentValue()
  {
    return currVal;
  }
}
