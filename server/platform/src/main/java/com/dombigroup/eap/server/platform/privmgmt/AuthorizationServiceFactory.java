/**
 * StorageMgmtFactory.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamInfo;

/**
 * @author sanjay
 * 
 */
public class AuthorizationServiceFactory implements IPluggableServiceFactory
{
  private static final String         SVC_NAME = "AUTHORIZATION SERVICE";
  private static Config               config;
  private static AuthorizationService svc;

  public static synchronized AuthorizationService getAuthorizationService()
      throws ServiceException
  {
    try
    {
      if (svc == null)
      {
        svc = new AuthorizationService();
        svc.setServiceConfig(config);
      }
    }
    catch (DgException e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }

    return svc;
  }

  @Override
  public List<String> getServiceNames()
  {
    List<String> ret = new ArrayList<String>();
    ret.add(SVC_NAME);
    return ret;
  }

  @Override
  public Config getServiceConfig(String svcName)
  {
    return AuthorizationServiceFactory.config;
  }

  @Override
  public void setServiceConfig(String svcName, Config c)
  {
    AuthorizationServiceFactory.config = c;
  }

  @Override
  public IPluggableService getService(String svcName) throws ServiceException
  {
    return AuthorizationServiceFactory.getAuthorizationService();
  }

  @Override
  public List<ParamInfo> getConfigParams(String svcName)
  {
    AuthorizationConfigParams[] l = AuthorizationConfigParams.values();
    List<ParamInfo> piList = new ArrayList<ParamInfo>();

    for (AuthorizationConfigParams p : l)
    {
      ParamInfo pi = new ParamInfo();
      pi.setName(p.name());
      pi.setDescription(p.getDesc());
      pi.setRequired(p.isRequired());
      pi.setParamLov(p.getLov());
      piList.add(pi);
    }
    return piList;
  }
}
