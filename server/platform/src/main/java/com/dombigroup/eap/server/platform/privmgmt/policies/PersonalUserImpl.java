/**
 * PersonalUserImpl.java
 */
package com.dombigroup.eap.server.platform.privmgmt.policies;

import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;

/**
 * @author sanjay
 * 
 */
public class PersonalUserImpl implements IAuthorization
{
  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorization#check(com.dombigroup.eap.server.platform.context.SecurityContext, com.dombigroup.eap.common.datamodels.ResourceType, int, java.lang.Object, com.dombigroup.eap.common.datamodels.Operation)
   */
  @Override
  public boolean check(SecurityContext ctx, ResourceType resourceType,
      int resourceId, Object resource, Operation o)
  {
    boolean ret = false;

    if (resourceType.equals(PlatformResource.USER))
    {
      if (resourceId == Persisted.INVALID_ID)
      {
        if ((resource != null) && (resource instanceof User))
        {
          User u = (User) resource;
          resourceId = u.getUserid();
        }
      }

      ret = (resourceId != Persisted.INVALID_ID) &&
          (resourceId == ctx.getUserId());
    }
    else if (resourceType.equals(PlatformResource.COMPANY))
    {
      if (resourceId == Persisted.INVALID_ID)
      {
        if ((resource != null) && (resource instanceof Company))
        {
          Company c = (Company) resource;
          resourceId = c.getId();
        }
      }

      ret = (resourceId != Persisted.INVALID_ID) &&
          (resourceId == ctx.getOrgId()) &&
              o.equals(OperationType.READ);
    }

    return ret;
  }
}
