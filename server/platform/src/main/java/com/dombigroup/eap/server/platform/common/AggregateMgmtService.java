/**
 * AggregateMgmtService.java
 */
package com.dombigroup.eap.server.platform.common;

import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.AggregateMgmt;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 * 
 */
public class AggregateMgmtService extends
    PluggableEntityMgmt<Integer, Aggregate> implements AggregateMgmt
{
  public AggregateMgmtService() throws StorageException, MetadataException
  {
    super(Integer.class, Aggregate.class);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt#copyPo(com.dombigroup.eap.common.datamodels.Persisted, com.dombigroup.eap.common.datamodels.Persisted)
   */
  public void copyPo(Aggregate from, Aggregate to)
  {
    to.setObjList(from.getObjList());
  }
}
