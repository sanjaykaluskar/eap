/**
 * ObjectPrivMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.privmgmt.PGrantedObjectPriv;

/**
 * @author sanjay
 * 
 */
public interface ObjectPrivMgmt
{
  /**
   * getObjectPrivs
   */
  public List<GrantedObjectPriv> getObjectPrivs(ServiceContext ctx,
      int objectId) throws MetadataException;

  /**
   * getObjectPriv
   */
  public PGrantedObjectPriv getObjectPriv(ServiceContext ctx, int objectId,
      int granteeId, String type, String op) throws MetadataException;

  /**
   * getObjectPrivListByGrantee
   * @throws MetadataException 
   */
  public List<GrantedObjectPriv> getObjectPrivListByGrantee(ServiceContext ctx,
      int userId) throws MetadataException;

  /**
   * createObjectPrivList
   */
  public int createObjectPrivList(ServiceContext ctx, int granteeId, int id,
      String type, String op) throws MetadataException;

  /**
   * grantObjectPriv
   */
  public void grantObjectPriv(ServiceContext ctx, int granteeId, int objectId,
      String type, String op) throws MetadataException;

  /**
   * revokeObjectPriv
   */
  public void revokeObjectPriv(ServiceContext ctx, int granteeId, int objectId,
      String type, String op) throws MetadataException;

  /**
   * deleteObjectPrivList
   */
  public void deleteObjectPrivList(ServiceContext ctx, int listId)
      throws MetadataException;
}
