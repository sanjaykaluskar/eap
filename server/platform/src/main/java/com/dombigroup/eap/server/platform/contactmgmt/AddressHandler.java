/**
 * OrgHandler.java
 */
package com.dombigroup.eap.server.platform.contactmgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class AddressHandler extends RdbmsStorageHandler
{
  public class AddressResultSet extends StorageResultSet
  {
    ResultSet rs;
    Statement stmt;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        Address addr = null;

        if (rs.next())
        {
          addr = new Address();
          addr.setId(rs.getInt("id"));
          addr.setLine1(rs.getString("line1"));
          addr.setLine2(rs.getString("line2"));
          addr.setLine3(rs.getString("line3"));
          addr.setCity(rs.getString("city"));
          addr.setState(rs.getString("state"));
          addr.setCountry(rs.getString("country"));
          addr.setZipcode(rs.getLong("zipcode"));
        }

        return addr;
      }
      catch (SQLException e)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
      }
      catch (SQLException e)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

  }

  private static final String STMT_CREATE = "create table addresses ("
                                              + "id number primary key, "
                                              + "line1 varchar2(4000), "
                                              + "line2 varchar2(4000), "
                                              + "line3 varchar2(4000), "
                                              + "city varchar2(4000), "
                                              + "state varchar2(4000), "
                                              + "country varchar2(4000), "
                                              + "zipcode number)";

  private static final String STMT_DROP   = "drop table addresses";

  private static final String STMT_SELECT = "select "
                                              + "id, line1, line2, line3, city, state, country, zipcode "
                                              + "from addresses "
                                              + "where id = ?";

  private static final String STMT_INSERT = "insert into "
                                              + "addresses(id, line1, line2, line3, city, state, country, zipcode) "
                                              + "values(?, ?, ?, ?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE = "update addresses "
                                              + "set line1 = ?,"
                                              + "line2 = ?, " + "line3 = ?, "
                                              + "city = ?, " + "state = ?, "
                                              + "country = ?, "
                                              + "zipcode = ? "
                                              + "where (id = ?)";

  private static final String STMT_DELETE = "delete from addresses "
                                              + "where (id = ?)";

  private static final String STMT_SEARCH = "select "
                                              + "id, line1, line2, line3, city, state, country, zipcode "
                                              + "from addresses "
                                              + "where (%s)";

  public AddressHandler()
  {
    super(Address.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Address addr = (Address) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, addr.getId());
      stmt.setString(2, addr.getLine1());
      stmt.setString(3, addr.getLine2());
      stmt.setString(4, addr.getLine3());
      stmt.setString(5, addr.getCity());
      stmt.setString(6, addr.getState());
      stmt.setString(7, addr.getCountry());
      stmt.setLong(8, addr.getZipcode());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer userId = (Integer) key;
    Address addr = (Address) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, userId.intValue());
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        addr.setId(rs.getInt("id"));
        addr.setLine1(rs.getString("line1"));
        addr.setLine2(rs.getString("line2"));
        addr.setLine3(rs.getString("line3"));
        addr.setCity(rs.getString("city"));
        addr.setState(rs.getString("state"));
        addr.setCountry(rs.getString("country"));
        addr.setZipcode(rs.getLong("zipcode"));
        rowsExist = true;
        stmt.close();
      }
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Address addr = (Address) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setString(1, addr.getLine1());
      stmt.setString(2, addr.getLine2());
      stmt.setString(3, addr.getLine3());
      stmt.setString(4, addr.getCity());
      stmt.setString(5, addr.getState());
      stmt.setString(6, addr.getCountry());
      stmt.setLong(7, addr.getZipcode());
      stmt.setInt(8, addr.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Address addr = (Address) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, addr.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#search
   * (java.lang.Class, java.lang.String)
   */
  @Override
  public AddressResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    AddressResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new AddressResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
