/**
 * IAuthorizationPolicyMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyInfo;

/**
 * @author sanjay
 * 
 */
public interface IAuthorizationPolicyMgmt extends IPluggableService
{
  /**
   * getRelevantPolicies - returns a list of relevant policies given the
   * context, resource type and operation
   */
  public List<IAuthorizationPolicy> getRelevantPolicies(SecurityContext ctx,
      ResourceType resourceType, Operation op);

  /**
   * getAllPolicies - returns a list of all known policies
   */
  public List<AuthorizationPolicyInfo> getAllPolicies(ServiceContext ctx);

  /**
   * getPolicyInfo
   */
  public AuthorizationPolicyInfo getPolicyInfo(ServiceContext ctx,
      IAuthorizationPolicy policy);

  /**
   * getPolicyInfoByName
   */
  public AuthorizationPolicyInfo getPolicyInfoByName(ServiceContext ctx,
      String policyName);

  /**
   * getDisabledPolicyList
   * 
   * @throws MetadataException
   */
  public List<AuthorizationPolicyInfo> getDisabledPolicyList(
      ServiceContext ctx, int listId) throws MetadataException;

  /**
   * createDisabledPolicyList
   */
  public int createDisabledPolicyList(ServiceContext ctx, String policyName)
      throws MetadataException;

  /**
   * addDisabledPolicy
   */
  public void addDisabledPolicy(ServiceContext ctx, int listId,
      String policyName) throws MetadataException;

  /**
   * removeDisabledPolicy
   */
  public void removeDisabledPolicy(ServiceContext ctx, int listId,
      String policyName) throws MetadataException;

  /**
   * deleteDisabledPolicyList
   */
  public void deleteDisabledPolicyList(ServiceContext ctx, int listId)
      throws MetadataException;
}
