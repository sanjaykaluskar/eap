/**
 * RdbmsStorageMgmtService.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction.ModifiedObject;

/**
 * @author sanjay
 * 
 */
public class RdbmsStorageMgmtService extends StorageMgmtService
{
  private static final String                       ORACLE_JDBC_URL_FORMAT    = "jdbc:oracle:thin:@//%s:%s/%s";

  private static Map<Class<?>, RdbmsStorageHandler> handlers;

  private static ServiceLoader<RdbmsStorageHandler> orclHandlerLoader         = ServiceLoader
                                                                                  .load(RdbmsStorageHandler.class);
  /** URL to access the database */
  private String                                    dbUrl;
  private String                                    user;
  private String                                    password;
  private Config                                    config;

  RdbmsStorageMgmtService()
  {
    // should only be instantiated through the factory
  }

  /**
   * @return the dbUrl
   */
  public String getDbUrl()
  {
    return dbUrl;
  }

  /**
   * @return the user
   */
  public String getUser()
  {
    return user;
  }

  /**
   * @return the password
   */
  public String getPassword()
  {
    return password;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtService#
   * setServiceConfig(com.dombigroup.eap.server.platform.interfaces.Config)
   */
  public void setServiceConfig(Config c) throws ServiceException
  {
    config = c;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtService#
   * getServiceConfig()
   */
  public Config getServiceConfig() throws ServiceException
  {
    return config;
  }

  private void registerDriver(StorageType sType)
      throws ClassNotFoundException
  {
    switch (sType)
    {
    case ORACLE:
      Class.forName("oracle.jdbc.driver.OracleDriver");
      break;
    default:
      break;
    }
  }

  private void initUrl(StorageType sType, String host, String port, String sid)
      throws StorageException
  {
    try
    {
      registerDriver(sType);
      switch (sType)
      {
      case ORACLE:
        dbUrl = String.format(ORACLE_JDBC_URL_FORMAT, host, port, sid);
        break;
      default:
        break;
      }
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  private Connection getConnection() throws StorageException
  {
    Connection cxn = null;

    try
    {
      cxn = DriverManager.getConnection(dbUrl, user, password);
    }
    catch (SQLException e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }

    return cxn;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    try
    {
      user = config.getParamValue(StorageConfigParams.USER.value());
      password = config.getParamValue(StorageConfigParams.PASSWORD.value());
      StorageType sType = StorageType.fromValue(config
          .getParamValue(StorageConfigParams.STORAGE_TYPE.value()));
      String host = config.getParamValue(StorageConfigParams.HOST.value());
      String port = config.getParamValue(StorageConfigParams.PORT.value());
      String sid = config.getParamValue(StorageConfigParams.SID.value());
      initUrl(sType, host, port, sid);
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_DB_OPERATION, e);
    }

    /* load handlers */
    handlers = new HashMap<Class<?>, RdbmsStorageHandler>();
    for (RdbmsStorageHandler h : orclHandlerLoader)
    {
      handlers.put(h.supportedType, h);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#install()
   */
  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
    RdbmsTransaction tx = new RdbmsTransaction();
    try
    {
      tx.setCxn(getConnection());

      /* setup handlers */
      for (RdbmsStorageHandler h : handlers.values())
      {
        h.createContainer(this, tx);
      }

      tx.getCxn().commit();
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_DB_OPERATION, e);
    }
    finally
    {
      try
      {
        if ((tx.getCxn() != null) && !tx.getCxn().isClosed())
          tx.getCxn().close();
      }
      catch (SQLException e)
      {
        throw new ServiceException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#uninstall()
   */
  @Override
  public void uninstall(ServiceContext ctx) throws ServiceException
  {
    RdbmsTransaction tx = new RdbmsTransaction();
    try
    {
      tx.setCxn(getConnection());

      /* setup handlers */
      for (RdbmsStorageHandler h : handlers.values())
      {
        h.deleteContainer(this, tx);
      }

      tx.getCxn().commit();
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_DB_OPERATION, e);
    }
    finally
    {
      try
      {
        if ((tx.getCxn() != null) && !tx.getCxn().isClosed())
          tx.getCxn().close();
      }
      catch (SQLException e)
      {
        throw new ServiceException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#shutdown()
   */
  @Override
  public void shutdown(ServiceContext ctx) throws ServiceException
  {
    try
    {
      DriverManager.deregisterDriver(DriverManager.getDriver(dbUrl));
    }
    catch (SQLException e)
    {
      throw new ServiceException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageMgmt#beginTxn()
   */
  @Override
  public StorageTransaction beginTxn() throws StorageException
  {
    RdbmsTransaction tx = new RdbmsTransaction();
    tx.setCxn(getConnection());

    return tx;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageMgmt#commitTxn
   * (com.dombigroup.eap.server.platform.interfaces.StorageTransaction)
   */
  @Override
  public void commitTxn(StorageTransaction tx) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    try
    {
      for (ModifiedObject modObj : otx.getModifiedObjects())
      {
        Object obj = modObj.getObj();
        RdbmsStorageHandler h = handlers.get(obj.getClass());
        if (h == null)
          throw new StorageException(PERRMessages.PERR_DB_HANDLER_MISSING, obj
              .getClass().getName());
        switch (modObj.getChg())
        {
        case CREATE:
          h.create(this, otx, obj);
          break;
        case UPDATE:
          h.update(this, otx, obj);
          break;
        case DELETE:
          h.delete(this, otx, obj);
          break;
        default:
          break;
        }
      }
      otx.getCxn().commit();
    }
    catch (SQLException e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
    finally
    {
      try
      {
        if ((otx.getCxn() != null) && !otx.getCxn().isClosed())
          otx.getCxn().close();
      }
      catch (SQLException e)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageMgmt#abortTxn(
   * com.dombigroup.eap.server.platform.interfaces.StorageTransaction)
   */
  @Override
  public void abortTxn(StorageTransaction tx) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    try
    {
      if ((otx != null) && (otx.getCxn() != null) && (!otx.getCxn().isClosed()))
        otx.getCxn().rollback();
    }
    catch (SQLException e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
    finally
    {
      try
      {
        if ((otx.getCxn() != null) && !otx.getCxn().isClosed())
          otx.getCxn().close();
      }
      catch (SQLException e)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtService#add(com
   * .dombigroup.eap.server.platform.interfaces.StorageTransaction,
   * java.lang.Object,
   * com.dombigroup.eap.server.platform.interfaces.StorageMgmt.ChangeType)
   */
  @Override
  public void add(StorageTransaction tx, Object o, ChangeType c)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    otx.addModifiedObject(o, c);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtService#remove
   * (com.dombigroup.eap.server.platform.interfaces.StorageTransaction,
   * java.lang.Object)
   */
  @Override
  public void remove(StorageTransaction tx, Object o) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    otx.removeModifiedObject(o);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtService#load(
   * java.lang.Object, java.lang.Object)
   */
  @Override
  public boolean load(Object key, Object value) throws StorageException
  {
    RdbmsStorageHandler h = handlers.get(value.getClass());
    if (h == null)
      return false;
    RdbmsTransaction otx = new RdbmsTransaction();
    try
    {
      otx.setCxn(getConnection());
      boolean ret = h.read(this, otx, key, value);
      otx.getCxn().close();
      return ret;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
    finally
    {
      try
      {
        if ((otx.getCxn() != null) && !otx.getCxn().isClosed())
          otx.getCxn().close();
      }
      catch (SQLException e)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }
  }

  public StorageResultSet search(Class<?> c, String condition)
      throws StorageException
  {
    RdbmsStorageHandler h = handlers.get(c);
    if (h == null)
      return null;
    RdbmsTransaction otx = new RdbmsTransaction();
    try
    {
      otx.setCxn(getConnection());
      StorageResultSet rs = h.search(this, otx, condition);
      return rs;
    }
    catch (Exception e)
    {
      try
      {
        if ((otx.getCxn() != null) && !otx.getCxn().isClosed())
          otx.getCxn().close();
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
      catch (SQLException e2)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e2);
      }
    }
  }
  
  @Override
  public StorageResultSet searchWithPagination(Class<?> c, String condition,
      int startRow, int pageSize)
      throws StorageException
  {
    RdbmsStorageHandler h = handlers.get(c);
    if (h == null)
      return null;
    RdbmsTransaction otx = new RdbmsTransaction();
    try
    {
      otx.setCxn(getConnection());
      StorageResultSet rs = h.searchWithPagination(this, otx, condition, startRow, pageSize);
      return rs;
    }
    catch (Exception e)
    {
      try
      {
        if ((otx.getCxn() != null) && !otx.getCxn().isClosed())
          otx.getCxn().close();
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
      catch (SQLException e2)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e2);
      }
    }
  }
}
