/**
 * PRoleHandler.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class PGrantedRoleHandler extends RdbmsStorageHandler
{
  public class PGrantedRoleResultSet extends StorageResultSet
  {
    ResultSet  rs;
    Statement  stmt;
    Connection cxn;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    /**
     * @param cxn
     *          the cxn to set
     */
    private void setCxn(Connection cxn)
    {
      this.cxn = cxn;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        PGrantedRole role = null;

        if (rs.next())
        {
          role = new PGrantedRole();
          role.setId(rs.getInt("id"));
          role.setUserId(rs.getInt("userid"));
          role.setRoleName(rs.getString("rolename"));
          role.setGrantor(rs.getInt("grantor"));
          role.setGrantDate(DayTime.fromGmtDate(rs.getDate("grant_date")));
          role.setEnabled(rs.getInt("enabled") == 1);
        }

        return role;
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
        cxn.close();
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }
  }

  private static final String STMT_CREATE = "create table roles ( "
                                                 + "id number, "
                                                 + "userid number, "
                                                 + "rolename varchar2(4000), "
                                                 + "grantor number, "
                                                 + "grant_date date, "
                                                 + "enabled number)";

  private static final String STMT_DROP   = "drop table roles";

  private static final String STMT_SELECT = "select id, userid, rolename, grantor, grant_date, enabled "
                                                 + "from roles "
                                                 + "where id = ?";

  private static final String STMT_INSERT = "insert into "
                                                 + "roles(id, userid, rolename, grantor, grant_date, enabled) "
                                                 + "values(?, ?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE = "update roles set "
                                              + "userid = ?, rolename = ?, grantor = ?, "
                                              + "grant_date = ?, enabled = ? "
                                              + "where (id = ?)";

  private static final String STMT_DELETE = "delete from roles "
                                                 + "where (id = ?)";

  private static final String STMT_SEARCH = "select id, userid, rolename, grantor, grant_date, enabled "
                                              + "from roles "
                                              + "where (%s)";

  public PGrantedRoleHandler()
  {
    super(PGrantedRole.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedRole role = (PGrantedRole) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, role.getId());
      stmt.setInt(2, role.getUserId());
      stmt.setString(3, role.getRoleName());
      stmt.setInt(4, role.getGrantor());
      stmt.setDate(5, DayTime.safeDate(role.getGrantDate()));
      stmt.setInt(6, role.isEnabled() ? 1 : 0);
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer id = (Integer) key;
    PGrantedRole role = (PGrantedRole) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, id.intValue());
      ResultSet rs = stmt.executeQuery();
      while (rs.next())
      {
        role.setId(rs.getInt("id"));
        role.setUserId(rs.getInt("userid"));
        role.setRoleName(rs.getString("rolename"));
        role.setGrantor(rs.getInt("grantor"));
        role.setGrantDate(DayTime.fromGmtDate(rs.getDate("grant_date")));
        role.setEnabled(rs.getInt("enabled") == 1);
        rowsExist = true;
      }
      stmt.close();
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedRole role = (PGrantedRole) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setInt(1, role.getUserId());
      stmt.setString(2, role.getRoleName());
      stmt.setInt(3, role.getGrantor());
      stmt.setDate(4, DayTime.safeDate(role.getGrantDate()));
      stmt.setInt(5, role.isEnabled() ? 1 : 0);
      stmt.setInt(6, role.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedRole role = (PGrantedRole) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, role.getUserId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  public PGrantedRoleResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedRoleResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new PGrantedRoleResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
        rs.setCxn(c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

}
