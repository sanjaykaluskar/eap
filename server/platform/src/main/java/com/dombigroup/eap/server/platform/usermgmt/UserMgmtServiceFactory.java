package com.dombigroup.eap.server.platform.usermgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamInfo;

/**
 * @author sanjay
 * 
 */
public class UserMgmtServiceFactory implements IPluggableServiceFactory
{
  private static final String    SVC_NAME = "USER MANAGEMENT";
  private static Config          config;
  private static UserMgmtService svc;

  public static synchronized UserMgmtService getUserMgmtService()
      throws MetadataException
  {
    try
    {
      if (svc == null)
      {
        svc = new UserMgmtService();
        svc.setServiceConfig(config);
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
    return svc;
  }

  @Override
  public List<String> getServiceNames()
  {
    List<String> l = new ArrayList<String>();
    l.add(SVC_NAME);
    return l;
  }

  @Override
  public List<ParamInfo> getConfigParams(String svcName)
  {
    return null;
  }

  @Override
  public Config getServiceConfig(String svcName)
  {
    return config;
  }

  @Override
  public void setServiceConfig(String svcName, Config c)
  {
    config = c;
  }

  @Override
  public IPluggableService getService(String svcName) throws ServiceException
  {
    try
    {
      return getUserMgmtService();
    }
    catch (MetadataException e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }
}
