/**
 * AggregateMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.Aggregate;

/**
 * @author sanjay
 *
 */
public interface AggregateMgmt extends EntityMgmt<Integer, Aggregate>
{
}
