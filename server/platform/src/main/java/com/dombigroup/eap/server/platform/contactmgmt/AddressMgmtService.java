/**
 * AddressMgmtService.java
 */
package com.dombigroup.eap.server.platform.contactmgmt;

import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.AddressMgmt;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public class AddressMgmtService extends PluggableEntityMgmt<Integer, Address>
    implements AddressMgmt
{
  public AddressMgmtService() throws StorageException, MetadataException
  {
    super(Integer.class, Address.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableEntityMgmt#copyPo
   * (com.dombigroup.eap.common.datamodels.Persisted,
   * com.dombigroup.eap.common.datamodels.Persisted)
   */
  public void copyPo(Address from, Address to)
  {
    to.setLine1(from.getLine1());
    to.setLine2(from.getLine2());
    to.setLine3(from.getLine3());
    to.setCity(from.getCity());
    to.setState(from.getState());
    to.setCountry(from.getCountry());
    to.setZipcode(from.getZipcode());
  }

  @Override
  public Address getAddress(ServiceContext ctx, int addrId)
      throws MetadataException
  {
    return (addrId == Persisted.INVALID_ID) ? null : get(ctx, addrId,
        LockMode.SHARED, ctx.getWorkUnit());
  }

  @Override
  public int createAddress(ServiceContext ctx, Address addr)
      throws MetadataException
  {
    int addrId = nextId(ctx);
    Address newAddr = new Address(addrId, addr);
    create(ctx, addrId, newAddr);
    return addrId;
  }
}
