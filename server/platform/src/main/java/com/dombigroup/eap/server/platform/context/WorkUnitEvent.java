/**
 * WorkUnitEvent.java
 */
package com.dombigroup.eap.server.platform.context;

import com.dombigroup.eap.server.platform.notifications.Event;

/**
 * @author sanjay
 *
 */
public class WorkUnitEvent extends Event
{
  private WorkUnit workUnit;

  private WorkUnitState state;

  private boolean failed;

  /**
   * Constructor for WorkUnitEvent
   */
  public WorkUnitEvent(WorkUnit workUnit, WorkUnitState state, boolean failed)
  {
    this.workUnit = workUnit;
    this.state = state;
    this.failed = failed;
  }

  /**
   * @return the workUnit
   */
  public WorkUnit getWorkUnit()
  {
    return workUnit;
  }

  /**
   * @return the state
   */
  public WorkUnitState getState()
  {
    return state;
  }

  /**
   * @return the failed
   */
  public boolean isFailed()
  {
    return failed;
  }
}
