/**
 * IOperationEnumerator.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.Operation;

/**
 * @author sanjay
 *
 */
public interface IOperationEnumerator
{
  public Operation[] listOperations();
}
