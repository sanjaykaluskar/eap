/**
 * PUserMgmtService.java
 */
package com.dombigroup.eap.server.platform.usermgmt;

import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.EntityMgmt;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 *
 */
public class PUserMgmtService extends PluggableEntityMgmt<Integer, PUser> implements
    EntityMgmt<Integer, PUser>
{
  public PUserMgmtService()
      throws StorageException, MetadataException
  {
    super(Integer.class, PUser.class);
  }

  @Override
  protected void copyPo(PUser from, PUser to)
  {
    to.setClientId(from.getClientId());
    to.setUserName(from.getUserName());
    to.setPassword(from.getPassword());
    to.setRoleListId(from.getRoleListId());
  }
}
