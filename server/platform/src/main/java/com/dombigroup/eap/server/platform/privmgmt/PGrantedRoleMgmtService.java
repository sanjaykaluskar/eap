/**
 * RoleMgmtService.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.EntityMgmt;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 * 
 */
public class PGrantedRoleMgmtService extends PluggableEntityMgmt<Integer, PGrantedRole>
    implements EntityMgmt<Integer, PGrantedRole>
{
  public PGrantedRoleMgmtService()
      throws StorageException, MetadataException
  {
    super(Integer.class, PGrantedRole.class);
  }

  @Override
  protected void copyPo(PGrantedRole from, PGrantedRole to)
  {
    to.setUserId(from.getUserId());
    to.setRoleName(from.getRoleName());
    to.setGrantor(from.getGrantor());
    to.setGrantDate(from.getGrantDate());
    to.setEnabled(from.isEnabled());
  }
}
