/**
 * ObjectCache.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dombigroup.eap.server.platform.context.WorkUnit;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt.ChangeType;

/**
 * @author sanjay
 */
@SuppressWarnings("unchecked")
public class ObjectCache<K, V>
{
  public class CacheTxn
  {
    private List<CachedObject<K, V>> affected_objects = new ArrayList<CachedObject<K, V>>();

    /**
     * @return the affected_objects
     */
    List<CachedObject<K, V>> getAffected_objects()
    {
      return affected_objects;
    }

    private void clearAffected_objects()
    {
      affected_objects.clear();
    }

    private void addObject(CachedObject<K, V> o)
    {
      affected_objects.add(o);
    }
  }

  private static final Logger                  logger              = LoggerFactory
                                                                       .getLogger(ObjectCache.class);
  private static final float                   hashTableLoadFactor = 0.750f;
  @SuppressWarnings("unused")
  private Class<?>                             keyClass;
  private Class<?>                             valueClass;

  private LinkedHashMap<K, CachedObject<K, V>> map;
  private int                                  targetSize;
  private StorageMgmt                          storageManager;

  /**
   * Creates a new cache.
   * 
   * @throws StorageException
   * 
   */
  public ObjectCache(Class<?> kClass, Class<?> vClass, int cacheSize)
      throws StorageException
  {
    keyClass = kClass;
    valueClass = vClass;
    targetSize = cacheSize;
    int hashTableCapacity = (int) Math.ceil(cacheSize / hashTableLoadFactor) + 1;
    map = new LinkedHashMap<K, CachedObject<K, V>>(hashTableCapacity,
        hashTableLoadFactor, true) {
      // (an anonymous inner class)
      private static final long serialVersionUID = 1;

      @Override
      protected synchronized boolean removeEldestEntry(
          Map.Entry<K, CachedObject<K, V>> eldest)
      {
        CachedObject<K, V> o = eldest.getValue();
        if (o.isPinned() || o.isDirty())
        {
          map.remove(o.getKey());
          map.put(o.getKey(), o);
          return false;
        }
        return (size() > ObjectCache.this.targetSize);
      }
    };
    storageManager = StorageMgmtFactory.getStorageMgmtService();
  }

  private synchronized CachedObject<K, V> objectLookup(K key)
      throws StorageException
  {
    CachedObject<K, V> cv = map.get(key);
    /* TODO: need to move the load out of the synchronized method */
    if (cv == null)
    {
      cv = new CachedObject<K, V>();
      try
      {
        V value = (V) (valueClass.getConstructor().newInstance());
        cv.setKey(key);
        cv.setObj(value);
        loadObject(cv);
        map.put(key, cv);
      }
      catch (Exception e)
      {
        throw new StorageException(PERRMessages.PERR_CACHE_LOOKUP, e);
      }
    }

    cv.incrPinCount();
    return cv;
  }

  private synchronized void objectReload(CachedObject<K, V> o)
      throws StorageException
  {
    try
    {
      loadObject(o);
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_CACHE_LOOKUP, e);
    }
  }

  private void loadObject(CachedObject<K, V> cv)
      throws Exception
  {
    if (!storageManager.load(cv.getKey(), cv.getObj()))
    {
      cv.setEmpty();
    }
  }

  /**
   * Locks and retrieves an entry from the cache.<br>
   * The retrieved entry becomes the MRU (most recently used) entry.
   * 
   * @param key
   *          - the key whose associated value is to be returned.
   * @param m
   *          - mode in which the object should be locked
   * @param w
   *          - workunit for the operation locking this object
   * @return the cached object
   * @throws StorageException
   */
  public CachedObject<K, V> get(K key, LockMode m, WorkUnit w)
      throws StorageException
  {
    logger.debug("{} cache - Entering get() on object, key: {}, mode: {}",
        valueClass.getSimpleName(), key.toString(), m.toString());

    if (w == null)
      throw new StorageException(PERRMessages.PERR_CALL_TXN_NULL);

    CachedObject<K, V> cv = objectLookup(key);

    ObjectLock l = new ObjectLock(m, this, cv);
    switch (m)
    {
    case SHARED:
      cv.getReadLock();
      break;
    case EXCLUSIVE:
      cv.getWriteLock();
      break;
    default:
      break;
    }
    w.addLock(l);

    logger.debug("{} cache - Completed get() on object, key: {}, mode: {}",
        valueClass.getSimpleName(), key.toString(), m.toString());
    return cv;
  }

  public CacheTxn beginTxn()
  {
    return new CacheTxn();
  }

  public void flushTxn(CacheTxn tx, StorageTransaction stx)
      throws StorageException
  {
    List<CachedObject<K, V>> objList = tx.getAffected_objects();
    if ((objList != null) && (objList.size() > 0))
    {
      try
      {
        for (CachedObject<K, V> cachedObj : objList)
        {
          if (cachedObj.isInserted())
            storageManager.add(stx, cachedObj.getObj(), ChangeType.CREATE);
          else if (cachedObj.isUpdated())
            storageManager.add(stx, cachedObj.getObj(), ChangeType.UPDATE);
          else if (cachedObj.isDeleted())
            storageManager.add(stx, cachedObj.getObj(), ChangeType.DELETE);
        }
      }
      catch (StorageException e)
      {
        throw new StorageException(PERRMessages.PERR_CACHE_COMMIT, e);
      }
    }
  }

  public boolean commitTxn(CacheTxn tx) throws StorageException
  {
    List<CachedObject<K, V>> objList = tx.getAffected_objects();
    if ((objList != null) && (objList.size() > 0))
    {
      for (CachedObject<K, V> o : objList)
      {
        o.clearDirty();
      }
      tx.clearAffected_objects();
    }
    return true;
  }

  public boolean abortTxn(CacheTxn tx) throws StorageException
  {
    for (CachedObject<K, V> o : tx.getAffected_objects())
    {
      o.clearDirty();
      objectReload(o);
    }
    tx.clearAffected_objects();
    return true;
  }

  public void markInserted(CacheTxn tx, CachedObject<K, V> o)
      throws MetadataException
  {
    /*
     * We will allow the same object to be marked as inserted multiple times.
     */
    if (o.isDirty() && !o.isInserted())
      throw new MetadataException(PERRMessages.PERR_CACHE_CONFLICT, o
          .toString());

    if (!o.isInserted())
    {
      o.clearEmpty();
      o.setInserted();
      tx.addObject(o);
    }
  }

  public void markUpdated(CacheTxn tx, CachedObject<K, V> o)
      throws MetadataException
  {
    /*
     * We will allow a dirty object to be updated if the object is already
     * marked as updated or inserted. This allows the object to be updated
     * incrementally in the same transaction in different places. It also allows
     * a new object that is being created to be updated.
     */
    if ((o.isDirty() && !o.isUpdated() && !o.isInserted()) || o.isEmpty())
      throw new MetadataException(PERRMessages.PERR_CACHE_CONFLICT, o
          .toString());

    /*
     * If the object is marked as inserted, then it is newly created and the
     * update just translates to an insert for cache flush. If the object is
     * already marked as updated there is nothing else to do.
     */
    if (!o.isInserted() && !o.isUpdated())
    {
      o.setUpdated();
      tx.addObject(o);
    }
  }

  public void markDeleted(CacheTxn tx, CachedObject<K, V> o)
      throws MetadataException
  {
    if ((o.isDirty() && !o.isDeleted()) || o.isEmpty())
      throw new MetadataException(PERRMessages.PERR_CACHE_CONFLICT, o
          .toString());
    o.setDeleted();
    tx.addObject(o);
  }

  public void releaseLock(ObjectLock l, boolean failure)
  {
    CachedObject<K, V> co = l.getObject();
    switch (l.getMode())
    {
    case SHARED:
      co.releaseReadLock();
      break;
    case EXCLUSIVE:
      co.releaseWriteLock();
      break;
    default:
      break;
    }
    co.decrPinCount();

    logger.debug("{} cache - Released lock on object, key: {}, mode: {}",
        valueClass.getSimpleName(), co.getKey().toString(), l.getMode()
            .toString());
  }
}
