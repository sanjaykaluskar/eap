/**
 * PlatformResourceTypeEnumerator.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.interfaces.IResourceTypeEnumerator;

/**
 * @author sanjay
 *
 */
public class PlatformResourceTypeEnumerator implements IResourceTypeEnumerator
{
  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IResourceTypeEnumerator#listResourceTypes()
   */
  @Override
  public ResourceType[] listResourceTypes()
  {
    return PlatformResource.values();
  }
}
