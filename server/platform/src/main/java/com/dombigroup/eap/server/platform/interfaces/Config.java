/**
 * Config.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * @author sanjay
 * 
 */
@XmlType(name = "config")
@XmlAccessorType(XmlAccessType.FIELD)
public class Config
{
  @XmlTransient
  private List<ParamInfo>  paramInfo;

  @XmlElement(name = "param")
  private List<ParamValue> paramValues = new ArrayList<ParamValue>();

  /**
   * @return the paramInfo
   */
  public List<ParamInfo> getParamInfo()
  {
    return paramInfo;
  }

  public ParamInfo getInfo(String paramName)
  {
    ParamInfo found = null;
    for (ParamInfo pi : paramInfo)
    {
      if (pi.getName().equals(paramName))
      {
        found = pi;
        break;
      }
    }
    return found;
  }

  /**
   * @param paramInfo
   *          the paramInfo to set
   */
  public void setParamInfo(List<ParamInfo> paramInfo)
  {
    this.paramInfo = paramInfo;
  }

  /**
   * @return the paramValues
   */
  public List<ParamValue> getParamValues()
  {
    return paramValues;
  }

  /**
   * @param paramValues
   *          the paramValues to set
   */
  public void setParamValues(List<ParamValue> paramValues)
  {
    this.paramValues = paramValues;
  }

  public String getParamValue(String name)
  {
    String returnValue = null;

    for (ParamValue pv : paramValues)
    {
      if (pv.getParamName().equals(name))
      {
        returnValue = pv.getParamValue();
        break;
      }
    }

    return returnValue;
  }

  public void setParamValue(String name, String value)
  {
    ParamValue paramValue = null;

    for (ParamValue pv : paramValues)
    {
      if (pv.getParamName().equals(name))
      {
        paramValue = pv;
        break;
      }
    }

    if (paramValue == null)
    {
      paramValue = new ParamValue();
      paramValue.setParamName(name);
      paramValues.add(paramValue);
    }

    paramValue.setParamValue(value);
  }
}
