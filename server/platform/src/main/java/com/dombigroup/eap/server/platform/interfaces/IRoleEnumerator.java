/**
 * IRoleEnumerator.java
 */
package com.dombigroup.eap.server.platform.interfaces;

/**
 * @author sanjay
 *
 */
public interface IRoleEnumerator
{
  public IRole[] listRoles();
}
