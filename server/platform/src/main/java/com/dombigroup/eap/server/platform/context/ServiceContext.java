/**
 * ServiceContext.java
 */
package com.dombigroup.eap.server.platform.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.common.exceptions.DgRuntimeException;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.notifications.Subscription;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtService;

/**
 * @author sanjay
 * 
 */
@SuppressWarnings("rawtypes")
public class ServiceContext
{
  private int                   connectionId;
  private ThreadLocal<WorkUnit> workUnit      = new ThreadLocal<WorkUnit>();
  private List<WorkUnit>        operations    = new ArrayList<WorkUnit>();
  private ThreadLocal<AsyncOp>  asyncOp       = new ThreadLocal<AsyncOp>();
  private List<AsyncOp>         asyncOps      = new ArrayList<AsyncOp>();
  private AtomicBoolean         pendingLogoff = new AtomicBoolean(
      false);
  private SecurityContext       secContext;
  private Map<String, Object>   appContext;
  private NotificationContext   notificationContext;
  private static final Logger   logger        = LoggerFactory
      .getLogger(ServiceContext.class);

  private ServiceContext()
  {
  }

  public static ServiceContext bootstrapServiceContext(boolean initRoles)
      throws MetadataException, ServiceException
  {
    ServiceContext serviceContext = new ServiceContext();
    serviceContext.startCall();
    serviceContext.initSecContext(UserMgmtService.SU_USERID,
        UserMgmtService.SU_ORGID, initRoles);
    serviceContext.endCall(false);
    return serviceContext;
  }

  /**
   * @return the connectionId
   */
  public int getConnectionId()
  {
    return connectionId;
  }

  /**
   * @param connectionId
   *          the connectionId to set
   */
  public void setConnectionId(int connectionId)
  {
    this.connectionId = connectionId;
  }

  /**
   * Sets or updates the value for an application attribute in the service
   * context. The value can be retrieved later by calling getAttr(). This method
   * is synchronized.
   * 
   * @param attr
   *          - attribute to set
   * @param value
   *          - value for the attribute
   */
  public synchronized void setAttr(String attr, Object value)
  {
    if (appContext == null)
      appContext = new HashMap<String, Object>();

    appContext.put(attr, value);
  }

  /**
   * Retrieves the value for the specified attribute.
   * 
   * @param attr
   *          - attribute whose value is to be retrieved
   * @return Attribute value if present else null
   */
  public synchronized Object getAttr(String attr)
  {
    return (appContext == null) ? null : appContext.get(attr);
  }

  private synchronized void addOperation(WorkUnit w) throws ServiceException
  {
    if (pendingLogoff.get())
      throw new ServiceException(PERRMessages.PERR_LOGOFF_IN_PROGRESS);
    operations.add(w);
  }

  private synchronized void removeOperation(WorkUnit w)
  {
    operations.remove(w);
  }

  /**
   * @return the workUnit
   */
  public WorkUnit getWorkUnit()
  {
    return workUnit.get();
  }

  public AsyncOp getAsyncOp()
  {
    return asyncOp.get();
  }

  public void setAsyncOp(AsyncOp op)
  {
    asyncOp.set(op);
  }

  public Call getCall()
  {
    if (workUnit.get() instanceof Call)
      return (Call) workUnit.get();
    else
      return null;
  }

  public void startCall() throws ServiceException
  {
    if (workUnit.get() != null)
      throw new ServiceException(PERRMessages.PERR_CALL_NESTED);
    workUnit.set(new Call(this));
    addOperation(workUnit.get());
    logger.debug("Start call, id:{}", workUnit.get().getId());
  }

  public void endCall(boolean failure) throws ServiceException
  {
    Call call = getCall();
    if (call == null)
      throw new ServiceException(PERRMessages.PERR_CALL_NULL);
    logger.debug("End call, id:{}", workUnit.get().getId());
    call.endCall(failure);
    removeOperation(workUnit.get());
    workUnit.set(null);
  }

  public void startTransaction() throws ServiceException
  {
    if (workUnit.get() != null)
      throw new ServiceException(PERRMessages.PERR_TXN_NESTED);
    try
    {
      workUnit.set(new Transaction(this));
      addOperation(workUnit.get());
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_INTERNAL_OPERATION, e);
    }
    logger.debug("Start transaction, id:{}", workUnit.get().getId());
  }

  public Transaction getTransaction()
  {
    if (workUnit.get() instanceof Transaction)
      return (Transaction) workUnit.get();
    else
      return null;
  }

  public void commitTransaction() throws DgException
  {
    Transaction txn = getTransaction();
    if (txn == null)
      throw new ServiceException(PERRMessages.PERR_TXN_NULL);
    txn.commit();
    logger.debug("Commit transaction, id:{}", workUnit.get().getId());
    removeOperation(workUnit.get());
    workUnit.set(null);
  }

  public void abortTransaction() throws DgException
  {
    Transaction txn = getTransaction();
    if (txn != null)
      txn.abort();
    logger.debug("Abort transaction, id:{}", workUnit.get().getId());
    removeOperation(workUnit.get());
    workUnit.set(null);
  }

  synchronized void addAsyncOp(AsyncOp op)
  {
    /*
     * add to the beginning of the list so that newer operations show up first
     */
    asyncOps.add(0, op);
  }

  public List<AsyncOp> getAsyncOps()
  {
    return asyncOps;
  }

  /**
   * Initiates a shutdown of the current session, by preventing any new
   * operations from being started. Ongoing operations are optionally
   * interrupted (depending on the parameter).
   * 
   * @param interruptOperations
   *          - whether ongoing operations should be interrupted
   * @return whether there were any pending operations as of the this time
   * @throws MetadataException
   */
  public synchronized boolean logoff(boolean interruptOperations)
      throws MetadataException
  {
    /* prevent new operations */
    pendingLogoff.set(true);

    /* interrupt other ongoing operations if requested */
    if (interruptOperations)
    {
      for (WorkUnit w : operations)
      {
        if (w != workUnit.get())
        {
          w.interruptWork();
          logger.debug(String.format("Interrupted work unit [%d]", w.getId()));
        }
      }
    }

    /* remove any notifications */
    if (notificationContext != null)
      notificationContext.removeAllSubscriptions();

    return (operations.size() == 0);
  }

  public void initSecContext(int user, int org, boolean init_roles)
      throws MetadataException
  {
    secContext = new SecurityContext(this, user, org);
    if (init_roles)
    {
      secContext.init_roles();
      secContext.init_policies();
    }
  }

  /**
   * @return the secContext
   */
  public SecurityContext getSecContext()
  {
    return secContext;
  }

  public static synchronized ServiceContext createInternalContext(
      ServiceContext svc, boolean initRoles) throws MetadataException
  {
    ServiceContext svcContext = null;
    if (svc == null)
      throw new DgRuntimeException(PERRMessages.PERR_SESSION_NULL);

    WorkUnit currentUnit = svc.workUnit.get();
    if (currentUnit == null)
      throw new DgRuntimeException(PERRMessages.PERR_CALL_TXN_NULL);

    try
    {
      svcContext = ServiceContext.bootstrapServiceContext(initRoles);

      /* add the recursive session under the current work unit */
      currentUnit.addRecursiveSession(svcContext);
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_INTERNAL_OPERATION, e);
    }

    return svcContext;
  }

  public void resetWorkUnit() throws DgException
  {
    if (workUnit != null)
    {
      if (workUnit.get() instanceof Transaction)
        abortTransaction();
      else if (workUnit.get() instanceof Call)
        endCall(true);
    }
  }

  public void addSubscription(Subscription s) throws MetadataException
  {
    if (notificationContext == null)
      notificationContext = new NotificationContext();
    s.setOwningSession(this);
    notificationContext.addSubscription(s);
  }

  public void removeSubscription(Subscription s) throws MetadataException
  {
    notificationContext.removeSubscription(s);
  }

  public List<Subscription> getSubscriptionList()
  {
    return (notificationContext == null) ? null
        : notificationContext.getSubscriptions();
  }
}
