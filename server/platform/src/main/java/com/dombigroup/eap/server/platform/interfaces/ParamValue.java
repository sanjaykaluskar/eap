/**
 * ParamValues.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author sanjay
 *
 */
@XmlType(name="paramValue")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParamValue
{
  private String paramName;
  private String paramValue;
  
  /**
   * @return the paramName
   */
  public String getParamName()
  {
    return paramName;
  }
  
  /**
   * @param paramName the paramName to set
   */
  public void setParamName(String paramName)
  {
    this.paramName = paramName;
  }
  
  /**
   * @return the paramValue
   */
  public String getParamValue()
  {
    return paramValue;
  }
  
  /**
   * @param paramValue the paramValue to set
   */
  public void setParamValue(String paramValue)
  {
    this.paramValue = paramValue;
  }
}
