/**
 * WorkUnitEventService.java
 */
package com.dombigroup.eap.server.platform.context;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.notifications.EventFilter;
import com.dombigroup.eap.server.platform.notifications.IEventService;

/**
 * @author sanjay
 *
 */
public class WorkUnitEventService implements IEventService<WorkUnitEvent>
{
  @Override
  public Class<?> getEventType()
  {
    return WorkUnitEvent.class;
  }

  private int getIntAttr(EventFilter<WorkUnitEvent> filter, WorkUnitFilterAttr attr)
  {
    int val = Persisted.INVALID_ID;

    if (filter != null)
    {
      Object attrValue = filter.getAttr(attr);
      if (attrValue != null)
        val = ((Integer) attrValue).intValue();
    }
    return val;
  }

  private Object getObjectAttr(EventFilter<WorkUnitEvent> filter, WorkUnitFilterAttr attr)
  {
    Object val = null;

    if (filter != null)
    {
      val = filter.getAttr(attr);
    }
    return val;
  }

  @Override
  public boolean evaluateFilter(EventFilter<WorkUnitEvent> filter, WorkUnitEvent event)
  {
    boolean ret = true;
    int wuId = getIntAttr(filter, WorkUnitFilterAttr.ID);
    WorkUnitState state = (WorkUnitState) getObjectAttr(filter, WorkUnitFilterAttr.STATE);

    if ((wuId != Persisted.INVALID_ID) &&
        (wuId != event.getWorkUnit().getId()))
        ret &= false;

    if ((state != null) && !event.getState().equals(state))
      ret &= false;

    return ret;
  }
}
