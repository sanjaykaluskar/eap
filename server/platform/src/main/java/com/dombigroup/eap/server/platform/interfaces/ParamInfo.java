/**
 * ParamInfo.java
 */
package com.dombigroup.eap.server.platform.interfaces;

/**
 * @author sanjay
 *
 */
public class ParamInfo
{
  private String name;
  
  private String description;
  
  private boolean required;
  
  private String[] paramLov;

  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return the description
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description)
  {
    this.description = description;
  }

  /**
   * @return the required
   */
  public boolean isRequired()
  {
    return required;
  }

  /**
   * @param required the required to set
   */
  public void setRequired(boolean required)
  {
    this.required = required;
  }

  /**
   * @return the paramLov
   */
  public String[] getParamLov()
  {
    return paramLov;
  }

  /**
   * @param paramLov the paramLov to set
   */
  public void setParamLov(String[] paramLov)
  {
    this.paramLov = paramLov;
  }
}
