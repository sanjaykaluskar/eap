/**
 * OrgHandler.java
 */
package com.dombigroup.eap.server.platform.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class AggregateHandler extends RdbmsStorageHandler
{
  private static final String STMT_CREATE = "create table aggregates ("
                                              + "id number, "
                                              + "childid number)";

  private static final String STMT_DROP   = "drop table aggregates";

  private static final String STMT_SELECT = "select " + "id, childid "
                                              + "from aggregates "
                                              + "where id = ?";

  private static final String STMT_INSERT = "insert into "
                                              + "aggregates(id, childid) "
                                              + "values(?, ?)";

  private static final String STMT_DELETE = "delete from aggregates "
                                              + "where (id = ?)";

  public AggregateHandler()
  {
    super(Aggregate.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Aggregate aggregate = (Aggregate) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, aggregate.getId());
      for (Persisted child : aggregate.getObjList())
      {
        stmt.setInt(2, child.getId());
        stmt.executeUpdate();
      }
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer aggregateId = (Integer) key;
    Aggregate aggregate = (Aggregate) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, aggregateId.intValue());
      ResultSet rs = stmt.executeQuery();
      aggregate.setId(aggregateId);
      while (rs.next())
      {
        Persisted child = new Persisted();
        child.setId(rs.getInt("childid"));
        aggregate.add(child);
        rowsExist = true;
      }
      stmt.close();
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    try
    {
      delete(sm, tx, o);
      create(sm, tx, o);
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Aggregate aggregate = (Aggregate) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, aggregate.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
