/**
 * SecurityContext.java
 */
package com.dombigroup.eap.server.platform.context;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.datamodels.GrantedRole;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.interfaces.CompanyMgmt;
import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy;
import com.dombigroup.eap.server.platform.interfaces.IRole;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.orgmgmt.CompanyMgmtFactory;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyInfo;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

/**
 * @author sanjay
 * 
 */
public class SecurityContext
{
  private ServiceContext             svcCtx;
  private int                        userId;
  private int                        orgId;
  private List<GrantedRole>          roles;
  private List<IAuthorizationPolicy> disabledPolicyList;

  /**
   * Constructor for SecurityContext
   * 
   * @throws MetadataException
   */
  public SecurityContext(ServiceContext ctx, int userId, int orgId)
  {
    this.svcCtx = ctx;
    this.userId = userId;
    this.orgId = orgId;
  }

  public void init_roles() throws MetadataException
  {
    UserMgmt um = UserMgmtServiceFactory.getUserMgmtService();
    roles = um.getRoles(svcCtx, userId);
  }

  public void init_policies() throws MetadataException
  {
    CompanyMgmt cm = CompanyMgmtFactory.getCompanyMgmtService();
    List<AuthorizationPolicyInfo> list = cm.getDisabledAuthorizationPolicies(
        svcCtx, orgId);
    if (list != null)
    {
      disabledPolicyList = new ArrayList<IAuthorizationPolicy>();
      for (AuthorizationPolicyInfo i : list)
      {
        disabledPolicyList.add(i.getPolicyImpl());
      }
    }
  }

  /**
   * @return the svcCtx
   */
  public ServiceContext getSvcCtx()
  {
    return svcCtx;
  }

  /**
   * @return the userId
   */
  public int getUserId()
  {
    return userId;
  }

  /**
   * @param userId
   *          the userId to set
   */
  public void setUserId(int userId)
  {
    this.userId = userId;
  }

  /**
   * @return the orgId
   */
  public int getOrgId()
  {
    return orgId;
  }

  /**
   * @param orgId
   *          the orgId to set
   */
  public void setOrgId(int orgId)
  {
    this.orgId = orgId;
  }

  /**
   * @return the roles
   */
  public List<GrantedRole> getRoles()
  {
    return roles;
  }

  public boolean hasRole(IRole r)
  {
    boolean found = false;
    if (roles != null)
    {
      for (GrantedRole gr : roles)
      {
        if (gr.getRoleName().equals(r.getName()))
        {
          found = true;
          break;
        }
      }
    }
    return found;
  }

  public boolean isPolicyEnabled(IAuthorizationPolicy policy)
  {
    return (disabledPolicyList == null) ||
        ((disabledPolicyList != null) && !disabledPolicyList.contains(policy));
  }
}
