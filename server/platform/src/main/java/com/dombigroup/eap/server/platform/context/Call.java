/**
 * Call.java
 */
package com.dombigroup.eap.server.platform.context;

/**
 * @author sanjay
 *
 */
public class Call extends WorkUnit
{
  protected Call(ServiceContext ctx)
  {
    super(ctx);
  }
  
  public void endCall(boolean failure)
  {
    end(failure);
  }
}
