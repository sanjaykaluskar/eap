/**
 * BOEventService.java
 */
package com.dombigroup.eap.server.platform.bomgmt;

import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.notifications.EventFilter;
import com.dombigroup.eap.server.platform.notifications.IEventService;

/**
 * @author sanjay
 *
 */
public class BOEventService implements IEventService<BOEvent>
{
  @Override
  public Class<?> getEventType()
  {
    return BOEvent.class;
  }

  private int getIntAttr(EventFilter<BOEvent> filter, BOFilterAttr attr)
  {
    int val = Persisted.INVALID_ID;

    if (filter != null)
    {
      Object attrValue = filter.getAttr(attr);
      if (attrValue != null)
        val = ((Integer) attrValue).intValue();
    }
    return val;
  }

  private Object getObjectAttr(EventFilter<BOEvent> filter, BOFilterAttr attr)
  {
    Object val = null;

    if (filter != null)
    {
      val = filter.getAttr(attr);
    }
    return val;
  }

  @Override
  public boolean evaluateFilter(EventFilter<BOEvent> filter, BOEvent event)
  {
    boolean ret = true;
    int orgId = getIntAttr(filter, BOFilterAttr.ORG_ID);
    int objId = getIntAttr(filter, BOFilterAttr.OBJECT_ID);
    OperationType op = (OperationType) getObjectAttr(filter, BOFilterAttr.OPERATION);

    if ((orgId != Persisted.INVALID_ID) &&
        (orgId != event.getBo().getOrgId()))
        ret &= false;

    if ((objId != Persisted.INVALID_ID) &&
        (objId != event.getBo().getId()))
        ret &= false;

    if ((op != null) && !event.getOp().equals(op))
      ret &= false;

    return ret;
  }
}
