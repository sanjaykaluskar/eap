/**
 * PhoneMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.common.datamodels.Phone;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;

/**
 * @author sanjay
 * 
 */
public interface PhoneMgmt extends EntityMgmt<Integer, Phone>
{
  public List<Phone> getPhoneList(ServiceContext ctx, int id)
      throws MetadataException;

  public int createPhoneList(ServiceContext ctx, List<Phone> phoneList)
      throws MetadataException;

  public void deletePhoneList(ServiceContext ctx, int id)
      throws MetadataException;
}
