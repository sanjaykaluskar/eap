/**
 * PluggableService.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;

/**
 * @author sanjay
 * 
 */
public abstract class PluggableService implements IPluggableService
{
  private Config config;

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    throw new ServiceException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#install()
   */
  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
    throw new ServiceException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#postInstall1()
   */
  @Override
  public void postInstall1(ServiceContext ctx) throws ServiceException
  {
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#uninstall()
   */
  @Override
  public void uninstall(ServiceContext ctx) throws ServiceException
  {
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#shutdown()
   */
  @Override
  public void shutdown(ServiceContext ctx) throws ServiceException
  {
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#getServiceConfig()
   */
  @Override
  public Config getServiceConfig() throws DgException
  {
    return config;
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#setServiceConfig(com.dombigroup.eap.server.platform.interfaces.Config)
   */
  @Override
  public void setServiceConfig(Config c) throws DgException
  {
    config = c;
  }
}
