/**
 * RoleMgmtService.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.AggregateMgmt;
import com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt;
import com.dombigroup.eap.server.platform.interfaces.PObjectMgmt;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.pomgmt.PObject;
import com.dombigroup.eap.server.platform.pomgmt.PObjectMgmtService;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtServiceFactory;

/**
 * @author sanjay
 * 
 */
public class ObjectPrivMgmtService extends PluggableService implements
    ObjectPrivMgmt
{
  private PGrantedObjectPrivMgmtService pGrantedObjPrivSvc;
  private AggregateMgmt                 aggrSvc;
  private UserMgmt                      userSvc;
  private PObjectMgmt                   pObjectSvc;

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#install()
   */
  @Override
  public void install(ServiceContext ctx)
  {

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    try
    {
      // init references to other services
      pGrantedObjPrivSvc = (PGrantedObjectPrivMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PGRANTEDOBJECTPRIV);

      aggrSvc = (AggregateMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_AGGREGATE);

      userSvc = UserMgmtServiceFactory.getUserMgmtService();

      pObjectSvc = (PObjectMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_POBJECT);
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_OBJECT_PRIV_SERVICE_STARTUP,
          e);
    }
  }

  /**
   * createGrantedObjectPriv
   */
  private PGrantedObjectPriv createGrantedObjectPriv(ServiceContext ctx,
      int listId, int userId, int objectId, String objectType, String op)
      throws MetadataException
  {
    int oprivId = pGrantedObjPrivSvc.nextId(ctx);
    PGrantedObjectPriv opriv = new PGrantedObjectPriv();
    opriv.setId(oprivId);
    opriv.setGranteeId(userId);
    opriv.setObjectId(objectId);
    opriv.setObjectType(objectType);
    opriv.setOperation(op);
    opriv.setGrantor(ctx.getSecContext().getUserId());
    opriv.setGrantDate(new DayTime());
    pGrantedObjPrivSvc.create(ctx, oprivId, opriv);
    return opriv;
  }

  /**
   * getObjectPrivList
   */
  private List<GrantedObjectPriv> getObjectPrivList(ServiceContext svc, int listId) throws MetadataException
  {
    List<GrantedObjectPriv> ret = null;
    if (listId != Persisted.INVALID_ID)
    {
      try
      {
        ServiceContext recCtx = ServiceContext.createInternalContext(svc, true);
        recCtx.startCall();
        Aggregate aggr = aggrSvc.get(recCtx, listId, LockMode.SHARED,
            recCtx.getWorkUnit());
        if (aggr != null)
        {
          ret = new ArrayList<GrantedObjectPriv>();
          for (Persisted grObj : aggr.getObjList())
          {
            PGrantedObjectPriv opriv = pGrantedObjPrivSvc.get(recCtx, grObj.getId(),
                LockMode.SHARED, recCtx.getWorkUnit());
            String grantorName = userSvc.getUser(recCtx, opriv.getGrantor())
                .getUserName();
            String granteeName = userSvc.getUser(recCtx, opriv.getGranteeId())
                .getUserName();
            GrantedObjectPriv gr = opriv.toGrantedObjectPriv(grantorName,
                granteeName);
            ret.add(gr);
          }
        }
        recCtx.endCall(false);
      }
      catch (ServiceException e)
      {
        throw new MetadataException(PERRMessages.PERR_INTERNAL_OPERATION, e);
      }
    }

    return ret;
  }

  /**
   * getObjectPrivFromList
   * @throws ServiceException 
   */
  private PGrantedObjectPriv getObjectPrivFromList(ServiceContext svc, int listId, int granteeId, String type, String op)
      throws MetadataException, ServiceException
  {
    ServiceContext recCtx = ServiceContext.createInternalContext(svc, true);
    recCtx.startCall();
    Aggregate oprivList = aggrSvc.get(recCtx, listId, LockMode.SHARED, recCtx
        .getWorkUnit());
    PGrantedObjectPriv found = null;
    if (oprivList != null)
    {
      for (Persisted grObj : oprivList.getObjList())
      {
        PGrantedObjectPriv opriv = pGrantedObjPrivSvc.get(recCtx, grObj.getId(),
            LockMode.SHARED, recCtx.getWorkUnit());
        if ((opriv.getGranteeId() == granteeId) &&
             opriv.getObjectType().equals(type) &&
             opriv.getOperation().equals(op))
        {
          found = opriv;
          break;
        }
      }
    }
    recCtx.endCall(false);
    return found;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt#
   * getObjectPrivListByGrantee
   * (com.dombigroup.eap.server.platform.context.ServiceContext, int)
   */
  @Override
  public List<GrantedObjectPriv> getObjectPrivListByGrantee(ServiceContext ctx,
      int granteeId) throws MetadataException
  {
    List<GrantedObjectPriv> privList = null;
    List<PGrantedObjectPriv> pOprivList = pGrantedObjPrivSvc
        .getObjectPrivListByGrantee(ctx, granteeId);

    if (pOprivList != null)
    {
      privList = new ArrayList<GrantedObjectPriv>();
      for (PGrantedObjectPriv pOpriv : pOprivList)
      {
        String grantorName = userSvc.getUser(ctx, pOpriv.getGrantor())
            .getUserName();
        String granteeName = userSvc.getUser(ctx, pOpriv.getGranteeId())
            .getUserName();
        GrantedObjectPriv gr = pOpriv.toGrantedObjectPriv(grantorName,
            granteeName);
        privList.add(gr);

      }
    }
    return privList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt#getObjectPrivs
   * (com.dombigroup.eap.server.platform.context.ServiceContext, int)
   */
  @Override
  public List<GrantedObjectPriv> getObjectPrivs(ServiceContext ctx,
      int objectId) throws MetadataException
  {
    PObject po = pObjectSvc.get(ctx, objectId, LockMode.SHARED, ctx
        .getWorkUnit());

    List<GrantedObjectPriv> ret = null;
    if (po != null)
      ret = getObjectPrivList(ctx, po.getOprivListId());
    return ret;
  }

  /**
   * addObjectPriv
   */
  private void addObjectPriv(ServiceContext ctx, int listId, int userId,
      int objectId, String type, String op) throws MetadataException
  {
    /* create a grant desc */
    PGrantedObjectPriv opriv = createGrantedObjectPriv(ctx, listId, userId,
        objectId, type, op);

    Aggregate oprivList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    oprivList.add(opriv);
    aggrSvc.update(ctx, listId, oprivList);
  }

  /**
   * removeObjectPriv
   * @throws ServiceException 
   */
  private void removeObjectPriv(ServiceContext ctx, PObject po, int listId,
      int granteeId, String type, String op) throws MetadataException, ServiceException
  {
    PGrantedObjectPriv found = getObjectPrivFromList(ctx, listId, granteeId,
        type, op);

    if (found != null)
    {
      Aggregate oprivList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
          .getTransaction());
      Persisted listObj = null;
      for (Persisted p : oprivList.getObjList())
      {
        if (p.getId() == found.getId())
        {
          listObj = p;
          break;
        }
      }

      if (listObj != null)
      {
        pGrantedObjPrivSvc.delete(ctx, found.getId());
        if (oprivList.getObjList().size() == 1)
        {
          aggrSvc.delete(ctx, listId);
          po.setOprivListId(Persisted.INVALID_ID);
          pObjectSvc.update(ctx, po.getId(), po);
        }
        else
        {
          oprivList.remove(listObj);
          aggrSvc.update(ctx, listId, oprivList);
        }
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt#getObjectPriv
   * (com.dombigroup.eap.server.platform.context.ServiceContext, int, int,
   * java.lang.String, java.lang.String)
   */
  @Override
  public PGrantedObjectPriv getObjectPriv(ServiceContext ctx, int objectId,
      int granteeId, String type, String op) throws MetadataException
  {
    try
    {
      PObject po = pObjectSvc.get(ctx, objectId, LockMode.SHARED, ctx
          .getWorkUnit());
      if (po == null)
        return null;
      else
        return getObjectPrivFromList(ctx, po.getOprivListId(), granteeId, type,
            op);
    }
    catch (ServiceException e)
    {
      throw new MetadataException(PERRMessages.PERR_INTERNAL_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt#
   * createObjectPrivList
   * (com.dombigroup.eap.server.platform.context.ServiceContext, int, int,
   * java.lang.String, java.lang.String)
   */
  @Override
  public int createObjectPrivList(ServiceContext ctx, int granteeId,
      int objectId, String type, String op) throws MetadataException
  {
    int listId = aggrSvc.nextId(ctx);
    PGrantedObjectPriv opriv = createGrantedObjectPriv(ctx, listId, granteeId,
        objectId, type, op);
    Aggregate oprivList = new Aggregate();
    oprivList.setId(listId);
    oprivList.add(opriv);
    aggrSvc.create(ctx, listId, oprivList);

    return listId;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt#grantObjectPriv
   * (com.dombigroup.eap.server.platform.context.ServiceContext, int, int,
   * java.lang.String, java.lang.String)
   */
  @Override
  public void grantObjectPriv(ServiceContext ctx, int granteeId, int objectId,
      String type, String op) throws MetadataException
  {
    PObject po = pObjectSvc.get(ctx, objectId, LockMode.EXCLUSIVE, ctx
        .getTransaction());

    int listId = po.getOprivListId();
    List<GrantedObjectPriv> oprivList = getObjectPrivList(ctx, listId);
    if (oprivList == null)
    {
      // create a new list and associate it with the user
      po.setOprivListId(createObjectPrivList(ctx, granteeId, objectId, type, op));
      pObjectSvc.update(ctx, objectId, po);
    }
    else
      addObjectPriv(ctx, listId, granteeId, objectId, type, op);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt#revokeObjectPriv
   * (com.dombigroup.eap.server.platform.context.ServiceContext, int, int,
   * java.lang.String, java.lang.String)
   */
  @Override
  public void revokeObjectPriv(ServiceContext ctx, int granteeId, int objectId,
      String type, String op) throws MetadataException
  {
    try
    {
      PObject po = pObjectSvc.get(ctx, objectId, LockMode.EXCLUSIVE, ctx
          .getTransaction());
      int listId = po.getOprivListId();
      removeObjectPriv(ctx, po, listId, granteeId, type, op);
    }
    catch (ServiceException e)
    {
      throw new MetadataException(PERRMessages.PERR_INTERNAL_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt#
   * deleteObjectPrivList
   * (com.dombigroup.eap.server.platform.context.ServiceContext, int)
   */
  @Override
  public void deleteObjectPrivList(ServiceContext ctx, int listId)
      throws MetadataException
  {
    Aggregate oprivList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    if (oprivList != null)
    {
      for (Persisted grObj : oprivList.getObjList())
      {
        pGrantedObjPrivSvc.delete(ctx, grObj.getId());
      }
      aggrSvc.delete(ctx, listId);
    }
  }
}
