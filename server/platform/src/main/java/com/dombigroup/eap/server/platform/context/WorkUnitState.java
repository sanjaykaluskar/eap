package com.dombigroup.eap.server.platform.context;

public enum WorkUnitState
{
  STARTED("STARTED"),
  ENDED("ENDED");
  private final String value;

  WorkUnitState(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public static WorkUnitState fromValue(String v)
  {
    if (v == null)
      return null;

    for (WorkUnitState c : WorkUnitState.values())
    {
      if (c.value.equals(v))
      {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }
}
