/**
 * SellerHandler.java
 */
package com.dombigroup.eap.server.platform.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dombigroup.eap.common.datamodels.PrefixType;
import com.dombigroup.eap.common.utils.ExpressionUtils;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class PClientHandler extends RdbmsStorageHandler
{
  public class PClientResultSet extends StorageResultSet
  {
    ResultSet  rs;
    Statement  stmt;
    Connection cxn;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    /**
     * @param cxn
     *          the cxn to set
     */
    private void setCxn(Connection cxn)
    {
      this.cxn = cxn;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        PClient client = null;

        if (rs.next())
        {
          client = new PClient();
          client.setId(rs.getInt("id"));
          client.setOrgId(rs.getInt("orgid"));
          client.setPrefix(PrefixType.fromValue(rs.getString("prefix")));
          client.setFirstName(rs.getString("firstname"));
          client.setMiddleName(rs.getString("middlename"));
          client.setLastName(rs.getString("lastname"));
          client.setAddressId(rs.getInt("add_id"));
          client.setPhoneListId(rs.getInt("phone_list_id"));
          client.setEmail(rs.getString("email"));
        }

        return client;
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
        cxn.close();
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

  }

  private static final String STMT_CREATE_TABLE = "create table clients ("
                                                    + "id number primary key, "
                                                    + "org_id number, "
                                                    + "prefix varchar2(4000), "
                                                    + "firstname varchar2(4000), "
                                                    + "middlename varchar2(4000), "
                                                    + "lastname varchar2(4000), "
                                                    + "addr_id number, "
                                                    + "phone_list_id number, "
                                                    + "email varchar2(4000))";

  private static final String STMT_DROP         = "drop table clients";

  private static final String STMT_SELECT       = "select "
                                                    + "id, org_id, prefix, firstname, middlename, lastname, addr_id, phone_list_id, email "
                                                    + "from clients "
                                                    + "where id = ?";

  private static final String STMT_INSERT       = "insert into "
                                                    + "clients(id, org_id, prefix, firstname, middlename, lastname, addr_id, phone_list_id, email) "
                                                    + "values(?, ?, ?, ?, ?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE       = "update clients set "
                                                    + "    org_id = ?, "
                                                    + "    prefix = ?, "
                                                    + "    firstname = ?, "
                                                    + "    middlename = ?, "
                                                    + "    lastname = ?, "
                                                    + "    addr_id = ?, "
                                                    + "    phone_list_id = ?, "
                                                    + "    email = ? "
                                                    + "where (id = ?)";

  private static final String STMT_DELETE       = "delete from clients "
                                                    + "where (id = ?)";

  private static final String STMT_SEARCH       = "select "
                                                    + "id, org_id, prefix, firstname, middlename, lastname, addr_id, phone_list_id, email "
                                                    + "from clients "
                                                    + "where (%s)";

  public PClientHandler()
  {
    super(PClient.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE_TABLE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PClient client = (PClient) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, client.getId());
      stmt.setInt(2, client.getOrgId());
      stmt.setString(3, ExpressionUtils.safeString(client.getPrefix()));
      stmt.setString(4, client.getFirstName());
      stmt.setString(5, client.getMiddleName());
      stmt.setString(6, client.getLastName());
      stmt.setInt(7, client.getAddressId());
      stmt.setInt(8, client.getPhoneListId());
      stmt.setString(9, client.getEmail());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer clientId = (Integer) key;
    PClient client = (PClient) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, clientId.intValue());
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        client.setId(rs.getInt("id"));
        client.setOrgId(rs.getInt("org_id"));
        client.setPrefix(PrefixType.fromValue(rs.getString("prefix")));
        client.setFirstName(rs.getString("firstname"));
        client.setMiddleName(rs.getString("middlename"));
        client.setLastName(rs.getString("lastname"));
        client.setAddressId(rs.getInt("addr_id"));
        client.setPhoneListId(rs.getInt("phone_list_id"));
        client.setEmail(rs.getString("email"));
        rowsExist = true;
        stmt.close();
      }
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PClient client = (PClient) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setInt(1, client.getOrgId());
      stmt.setString(2, ExpressionUtils.safeString(client.getPrefix()));
      stmt.setString(3, client.getFirstName());
      stmt.setString(4, client.getMiddleName());
      stmt.setString(5, client.getLastName());
      stmt.setInt(6, client.getAddressId());
      stmt.setInt(7, client.getPhoneListId());
      stmt.setString(8, client.getEmail());
      stmt.setInt(9, client.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PClient client = (PClient) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, client.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#search
   * (java.lang.Class, java.lang.String)
   */
  @Override
  public PClientResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PClientResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new PClientResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
        rs.setCxn(c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
