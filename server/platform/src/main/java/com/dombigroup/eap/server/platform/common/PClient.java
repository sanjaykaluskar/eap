/**
 * PClient.java
 */
package com.dombigroup.eap.server.platform.common;

import com.dombigroup.eap.common.datamodels.Client;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.PrefixType;
import com.dombigroup.eap.common.utils.ExpressionUtils;

/**
 * @author sanjay
 * 
 */
public class PClient extends Persisted
{
  private PrefixType prefix;
  private String     firstName;
  private String     middleName;
  private String     lastName;
  private int        addressId;
  private int        phoneListId;
  private String     email;

  public PClient()
  {
  }

  public PClient(int id, Client c, int addrId, int phId)
  {
    copyPObjectInfo(c);
    setId(id);
    prefix = c.getPrefix();
    firstName = c.getFirstName();
    middleName = c.getMiddleName();
    lastName = c.getLastName();
    addressId = addrId;
    phoneListId = phId;
    email = c.getEmail();
  }

  /**
   * @return the prefix
   */
  public PrefixType getPrefix()
  {
    return prefix;
  }

  /**
   * @param prefix
   *          the prefix to set
   */
  public void setPrefix(PrefixType prefix)
  {
    this.prefix = prefix;
  }

  /**
   * @return the firstName
   */
  public String getFirstName()
  {
    return firstName;
  }

  /**
   * @param firstName
   *          the firstName to set
   */
  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }

  /**
   * @return the middleName
   */
  public String getMiddleName()
  {
    return middleName;
  }

  /**
   * @param middleName
   *          the middleName to set
   */
  public void setMiddleName(String middleName)
  {
    this.middleName = middleName;
  }

  /**
   * @return the lastName
   */
  public String getLastName()
  {
    return lastName;
  }

  /**
   * @param lastName
   *          the lastName to set
   */
  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }

  /**
   * @return the addressId
   */
  public int getAddressId()
  {
    return addressId;
  }

  /**
   * @param addressId
   *          the addressId to set
   */
  public void setAddressId(int addressId)
  {
    this.addressId = addressId;
  }

  /**
   * @return the phoneListId
   */
  public int getPhoneListId()
  {
    return phoneListId;
  }

  /**
   * @param phoneListId
   *          the phoneListId to set
   */
  public void setPhoneListId(int phoneListId)
  {
    this.phoneListId = phoneListId;
  }

  /**
   * @return the email
   */
  public String getEmail()
  {
    return email;
  }

  /**
   * @param email
   *          the email to set
   */
  public void setEmail(String email)
  {
    this.email = email;
  }

  public boolean equalAttrs(Client c)
  {
    return equalPObject(c)
        && ExpressionUtils.safeEqual(c.getPrefix(), prefix)
        && ExpressionUtils.safeEqual(c.getFirstName(), firstName)
        && ExpressionUtils.safeEqual(c.getMiddleName(), middleName)
        && ExpressionUtils.safeEqual(c.getLastName(), lastName)
        && ExpressionUtils.safeEqual(c.getEmail(), email);
  }
}
