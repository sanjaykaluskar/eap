/**
 * StorageHandler.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.server.platform.exceptions.StorageException;

/**
 * @author sanjay
 * 
 */
public interface StorageHandler
{
  /**
   * createContainer
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException;

  /**
   * deleteContainer
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException;

  /**
   * create specified object within the storage
   */
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException;

  /**
   * read object of specified class with the given key
   */
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException;

  /**
   * update specified object
   */
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException;

  /**
   * delete specified object
   */
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException;

  /**
   * search for objects satisfying a condition
   */
  public StorageResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException;
  
  /**
   * search for objects satisfying a condition with pagination;
   * returns a "page" of rows starting from a specified row-number.
   * 
   * The row numbers are upto the implementation as long as the
   * relative ordering is the same for 2 given rows.
   * 
   * @param sm - storage mgmt service
   * @param tx - storage transaction
   * @param condition - search condition
   * @param startRow - starting row number (numbering starts at 1)
   * @param pageSize - max number of rows to be returned
   */
  public StorageResultSet searchWithPagination(StorageMgmt sm, StorageTransaction tx,
      String condition, int startRow, int pageSize) throws StorageException;
}
