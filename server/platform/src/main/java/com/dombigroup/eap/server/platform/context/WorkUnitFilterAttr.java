package com.dombigroup.eap.server.platform.context;

import com.dombigroup.eap.server.platform.notifications.IEventFilterAttr;

/**
 * @author sanjay
 *
 */
public enum WorkUnitFilterAttr implements IEventFilterAttr<WorkUnitEvent>
{
  ID("ID", Integer.class),
  STATE("STATE", WorkUnitState.class);

  private final String value;
  
  private final Class<?> valueType;

  WorkUnitFilterAttr(String v, Class<?> vt)
  {
    value = v;
    valueType = vt;
  }

  public String value()
  {
    return value;
  }

  public static WorkUnitFilterAttr fromValue(String v)
  {
    if (v == null)
      return null;

    for (WorkUnitFilterAttr c : WorkUnitFilterAttr.values())
    {
      if (c.value.equals(v))
      {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  @Override
  public String getName()
  {
    return value;
  }

  @Override
  public Class<?> getValueClass()
  {
    return valueType;
  }
}
