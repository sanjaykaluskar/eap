/**
 * BOTypeMgmtService.java
 */
package com.dombigroup.eap.server.platform.bomgmt;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.common.exceptions.DgRuntimeException;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.context.Transaction;
import com.dombigroup.eap.server.platform.context.WorkUnitEvent;
import com.dombigroup.eap.server.platform.context.WorkUnitFilterAttr;
import com.dombigroup.eap.server.platform.context.WorkUnitState;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.AggregateMgmt;
import com.dombigroup.eap.server.platform.interfaces.BOMgmt;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.notifications.EventFilter;
import com.dombigroup.eap.server.platform.notifications.INotification;
import com.dombigroup.eap.server.platform.notifications.Subscription;
import com.dombigroup.eap.server.platform.notifications.SubscriptionService;
import com.dombigroup.eap.server.platform.notifications.SubscriptionServiceFactory;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationService;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationServiceFactory;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;
import com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtFactory;

/**
 * @author sanjay
 * 
 *         This is a simple template for a business object service. It provides
 *         a basic CRUD interface. An OR mapping is assumed that maps the BO of
 *         type BOType to a PO of type POType.
 * 
 */
/**
 * @author sanjay
 * 
 * @param <BOType>
 * @param <POType>
 */
abstract public class BOMgmtService<BOType extends Persisted, POType extends Persisted>
    extends PluggableService implements BOMgmt<BOType>
{
  private static final Logger                  logger = LoggerFactory
                                                          .getLogger(BOMgmtService.class);

  private ResourceType                         boResource;
  private Class<? extends Persisted>           poClass;
  private PluggableEntityMgmt<Integer, POType> pObjectSvc;
  protected AuthorizationService               authSvc;
  protected SubscriptionService                subSvc;
  private AggregateMgmt                        aggrMgmt;

  protected BOMgmtService(ResourceType resourceType,
      Class<? extends Persisted> persistedClass,
      String persistedSvcName)
  {
    boResource = resourceType;
    poClass = persistedClass;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.PluggableService
   * #startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    try
    {
      authSvc = AuthorizationServiceFactory.getAuthorizationService();
      subSvc = SubscriptionServiceFactory.getSubscriptionService();
      aggrMgmt = (AggregateMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_AGGREGATE);
      pObjectSvc = getPOMgmtService();
      boSvcStartup();
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.PluggableService
   * #install()
   */
  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
  }

  @Override
  public List<BOType> getBOListForOrg(ServiceContext ctx, int orgId)
      throws MetadataException
  {
    return getBOListForOrg(ctx, orgId, true);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BOType> getBOListForOrg(ServiceContext ctx, int orgId,
      boolean readOnlyCopy)
      throws MetadataException
  {
    List<BOType> boList = new ArrayList<BOType>();
    StorageResultSet rs = null;
    POType pObject;

    try
    {
      StorageMgmt storageManager = StorageMgmtFactory.getStorageMgmtService();
      rs = storageManager.search(poClass, "org_id=" + orgId);
      if (rs != null)
      {
        while ((pObject = (POType) rs.next()) != null)
        {
          BOType s = null;

          /*
           * For a read only copy we don't get a lock. Note that we also don't
           * copy the PO attrs such as creation / update time, version, etc.
           */
          if (readOnlyCopy)
            s = populateBOFromPO(ctx, pObject);
          else
            s = getBOById(ctx, pObject.getId());

          if (authSvc.check(ctx.getSecContext(), boResource,
              s.getId(), s, OperationType.READ))
          {
            if (logger.isTraceEnabled())
            {
              logger.trace("Adding object id: {}, type: {}", s.getId(),
                  boResource.getName());
            }
            boList.add(s);
          }
        }
        rs.close();
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_LIST, e,
          boResource.getName(), String.valueOf(orgId));
    }
    finally
    {
      try
      {
        if (rs != null)
          rs.close();
      }
      catch (StorageException e1)
      {
        throw new MetadataException(PERRMessages.PERR_BO_LIST, e1,
            boResource.getName(), String.valueOf(orgId));
      }
    }

    logger.debug("Returning list with {} objects of type {}", boList.size(),
        boResource.getName());
    return boList;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BOType> getBOPageForOrg(ServiceContext ctx, int orgId,
      int startRow, int pageSize)
      throws MetadataException
  {
    List<BOType> boList = new ArrayList<BOType>();
    StorageResultSet rs = null;
    POType pObject;

    try
    {
      StorageMgmt storageManager = StorageMgmtFactory.getStorageMgmtService();
      rs = storageManager.searchWithPagination(poClass, "org_id=" + orgId,
          startRow, pageSize);
      if (rs != null)
      {
        while ((pObject = (POType) rs.next()) != null)
        {
          BOType s = populateBOFromPO(ctx, pObject);
          if (authSvc.check(ctx.getSecContext(), boResource,
              s.getId(), s, OperationType.READ))
          {
            if (logger.isTraceEnabled())
            {
              logger.trace("Adding object id: {}, type: {}", s.getId(),
                  boResource.getName());
            }
            boList.add(s);
          }
        }
        rs.close();
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_LIST, e,
          boResource.getName(), String.valueOf(orgId));
    }
    finally
    {
      try
      {
        if (rs != null)
          rs.close();
      }
      catch (StorageException e1)
      {
        throw new MetadataException(PERRMessages.PERR_BO_LIST, e1,
            boResource.getName(), String.valueOf(orgId));
      }
    }

    logger.debug("Returning list with {} objects of type {}", boList.size(),
        boResource.getName());
    return boList;
  }

  @Override
  public BOType getBOById(ServiceContext ctx, int id)
      throws MetadataException
  {
    BOType bo = null;

    try
    {
      POType ps = pObjectSvc.get(ctx, id, LockMode.SHARED, ctx
          .getWorkUnit());
      if (ps != null)
        bo = populateBOFromPO(ctx, ps);
      else
      {
        if (logger.isTraceEnabled())
        {
          logger.trace("No object found with id: {}, type: {}", id, boResource
              .getName());
        }
      }
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_ACCESS, e,
          boResource.getName(), String.valueOf(id));
    }

    authSvc.assertAccess(ctx.getSecContext(), boResource, id, bo,
        OperationType.READ);

    logger.debug("Returning object id: {}, type {}", id, boResource.getName());
    return bo;
  }

  @Override
  public BOType getBOForUpdate(ServiceContext ctx, int id)
      throws MetadataException
  {
    BOType s = null;

    try
    {
      POType ps = pObjectSvc.get(ctx, id, LockMode.EXCLUSIVE,
          ctx.getWorkUnit());
      if (ps != null)
        s = populateBOFromPO(ctx, ps);
      else
      {
        if (logger.isTraceEnabled())
        {
          logger.trace("No object found with id: {}, type: {}", id, boResource
              .getName());
        }
      }
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_ACCESS, e,
          boResource.getName(), String.valueOf(id));
    }

    authSvc.assertAccess(ctx.getSecContext(), boResource, id, s,
        OperationType.UPDATE);

    logger.debug("Returning object id: {}, type {}", id, boResource.getName());
    return s;
  }

  @Override
  public int createBO(ServiceContext ctx, BOType bo)
      throws MetadataException
  {
    authSvc.assertAccess(ctx.getSecContext(), boResource,
        Persisted.INVALID_ID, bo, OperationType.CREATE);

    int cId = bo.getId();
    try
    {
      /* don't assign a new id if it is already set */
      if (cId == Persisted.INVALID_ID)
      {
        cId = pObjectSvc.nextId(ctx);
      }
      else
      {
        if (logger.isTraceEnabled())
        {
          logger.trace("Reused id: {} for new object of type: {}", cId,
              boResource.getName());
        }
      }

      POType ps = populatePOFromBO(ctx, cId, bo, null);
      pObjectSvc.create(ctx, cId, ps);

      /* set up to notify after txn commit */
      BOEventPush push = new BOEventPush(ctx, cId, OperationType.CREATE, this,
          subSvc);
      push.subscribeToTxnEnd();
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_CREATION, e,
          boResource.getName(), String.valueOf(bo.getOrgId()));
    }

    logger.debug("Created object id: {}, type {}", cId, boResource.getName());
    return cId;
  }

  @Override
  public void updateBO(ServiceContext ctx, BOType bo)
      throws MetadataException
  {
    int id = bo.getId();
    authSvc.assertAccess(ctx.getSecContext(), boResource, id,
        bo, OperationType.UPDATE);

    try
    {
      POType ps = pObjectSvc.get(ctx, id, LockMode.EXCLUSIVE,
          ctx.getTransaction());
      if (ps.getVersion() != bo.getVersion())
        throw new MetadataException(PERRMessages.PERR_STALE_UPDATE,
            String.valueOf(bo.getId()));
      POType newPs = populatePOFromBO(ctx, id, bo, ps);
      pObjectSvc.update(ctx, ps.getId(), newPs);

      /* set up to notify after txn commit */
      BOEventPush push = new BOEventPush(ctx, id, OperationType.UPDATE, this,
          subSvc);
      push.subscribeToTxnEnd();
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_UPDATE, e,
          boResource.getName(), String.valueOf(bo.getId()));
    }

    logger.debug("Updated object id: {}, type {}", id, boResource.getName());
  }

  @Override
  public void deleteBO(ServiceContext ctx, int id)
      throws MetadataException
  {
    try
    {
      POType pObject = pObjectSvc.get(ctx, id, LockMode.EXCLUSIVE,
          ctx.getTransaction());

      authSvc.assertAccess(ctx.getSecContext(), boResource, id,
          pObject, OperationType.DELETE);

      deleteAdditionalObjects(ctx, pObject);
      pObjectSvc.delete(ctx, pObject.getId());

      /* set up to notify after txn commit */
      BOEventPush push = new BOEventPush(ctx, id, OperationType.DELETE, this,
          subSvc);
      push.subscribeToTxnEnd();
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_DELETION, e,
          boResource.getName(), String.valueOf(id));
    }

    logger.debug("Deleted object id: {}, type {}", id, boResource.getName());
  }

  @Override
  public List<BOType> getBOList(ServiceContext ctx, int listId)
      throws MetadataException
  {
    List<BOType> boList = new ArrayList<BOType>();

    try
    {
      if (listId != Persisted.INVALID_ID)
      {
        Aggregate aggr = aggrMgmt.get(ctx, listId, LockMode.SHARED,
            ctx.getWorkUnit());
        if (aggr != null)
        {
          for (Persisted pObj : aggr.getObjList())
          {
            BOType bo = getBOById(ctx, pObj.getId());
            boList.add(bo);
          }
        }
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_LIST_ACCESS, e,
          boResource.getName(), String.valueOf(listId));
    }

    return boList;
  }

  @Override
  public int createBOList(ServiceContext ctx, List<BOType> boList)
      throws MetadataException
  {
    int newBOListId = Persisted.INVALID_ID;
    try
    {
      if ((boList != null) && (boList.size() > 0))
      {
        Aggregate aggrBOList = new Aggregate();
        for (BOType bo : boList)
        {
          int newBOId = bo.getId();
          if (newBOId == Persisted.INVALID_ID)
          {
            newBOId = createBO(ctx, bo);
            bo.setId(newBOId);
          }

          Persisted p = new Persisted();
          p.setId(newBOId);
          aggrBOList.add(p);
        }
        newBOListId = aggrMgmt.nextId(ctx);
        aggrBOList.setId(newBOListId);
        aggrMgmt.create(ctx, newBOListId, aggrBOList);
      }
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_LIST_CREATE, e,
          boResource.getName());
    }

    return newBOListId;
  }

  private boolean existsInAggregate(int id, List<Persisted> aggrList)
  {
    boolean found = false;

    if (aggrList != null)
    {
      for (Persisted pObj : aggrList)
      {
        if (pObj.getId() == id)
        {
          found = true;
          break;
        }
      }
    }

    return found;
  }

  private boolean existsInBOList(int id, List<BOType> boList)
  {
    boolean found = false;

    if (boList != null)
    {
      for (BOType col : boList)
      {
        if (col.getId() == id)
        {
          found = true;
          break;
        }
      }
    }

    return found;
  }

  @Override
  public void updateBOList(ServiceContext ctx, int listId,
      List<BOType> updatedList, boolean updateBOs) throws MetadataException
  {
    try
    {
      if (listId != Persisted.INVALID_ID)
      {
        Aggregate oldList = aggrMgmt.get(ctx, listId, LockMode.EXCLUSIVE,
            ctx.getTransaction());

        List<Persisted> newList = new ArrayList<Persisted>();

        if (updatedList != null)
        {
          for (BOType updObj : updatedList)
          {
            if (!existsInAggregate(updObj.getId(), oldList.getObjList()))
            {
              if (updObj.getId() == Persisted.INVALID_ID)
              {
                if (updateBOs)
                  updObj.setId(createBO(ctx, updObj));
                else
                  throw new MetadataException(PERRMessages.PERR_BO_NON_EXISTENT, boResource.getName());
              }
            }
            newList.add(updObj);
          }
        }

        for (Persisted pObj : oldList.getObjList())
        {
          if (!existsInBOList(pObj.getId(), updatedList))
          {
            if (updateBOs)
              deleteBO(ctx, pObj.getId());
          }
        }

        oldList.setObjList(newList);
        aggrMgmt.update(ctx, listId, oldList);
      }
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_LIST_UPDATE,
          boResource.getName(), String.valueOf(listId));
    }
  }

  @Override
  public void deleteBOList(ServiceContext ctx, int listId, boolean deleteBOs)
      throws MetadataException
  {
    try
    {
      if (listId != Persisted.INVALID_ID)
      {
        Aggregate boList = aggrMgmt.get(ctx, listId, LockMode.EXCLUSIVE,
            ctx.getTransaction());
        if (boList != null)
        {
          if (deleteBOs)
          {
            for (Persisted pObj : boList.getObjList())
            {
              deleteBO(ctx, pObj.getId());
            }
          }
          aggrMgmt.delete(ctx, listId);
        }
      }
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_BO_LIST_DELETION,
          boResource.getName(), String.valueOf(listId));
    }
  }

  /**
   * Invoked when the service is starting up. This is the place to add any
   * custom initializations.
   */
  abstract protected void boSvcStartup() throws ServiceException;

  /**
   * getPOMgmtService - returns the underlying PO mgmt service.
   */
  abstract protected PluggableEntityMgmt<Integer, POType> getPOMgmtService();

  /**
   * populateBOFromPO - should populate a new BO based on the PO. It is called
   * when populating a new copy of the BO typically as a part of a get or
   * search.
   * 
   * @param ctx
   *          - service context
   * @param from
   *          - PO to populate from
   */
  abstract protected BOType populateBOFromPO(ServiceContext ctx, POType from);

  /**
   * deleteAdditionalObjects - should delete any additional objects encapsulated
   * by the PO.
   * 
   * @param ctx
   *          - service context
   * @param pobj
   *          - PO
   */
  abstract protected void deleteAdditionalObjects(ServiceContext ctx,
      POType pobj);

  /**
   * populatePOFromBO - should populate a PO based on the BO. If there is an
   * existing PO that is being updated it is passed as "old".
   * 
   * @param ctx
   *          - service context
   * @param id
   *          - id of the PO
   * @param from
   *          - BO from which to populate the PO
   * @param old
   *          - existing PO if modifying an object
   */
  abstract protected POType populatePOFromBO(ServiceContext ctx, int id,
      BOType from, POType old);

  @SuppressWarnings("rawtypes")
  private static class BOEventPush implements INotification<WorkUnitEvent>
  {
    private ServiceContext      ctx;

    private int                 objId;

    private OperationType       op;

    private BOMgmtService       boSvc;

    private SubscriptionService subSvc;

    /**
     * Constructor for BOEventPush
     */
    private BOEventPush(ServiceContext c, int objId, OperationType op,
        BOMgmtService boSvc,
        SubscriptionService sub)
    {
      ctx = c;
      this.objId = objId;
      this.op = op;
      this.boSvc = boSvc;
      subSvc = sub;
    }

    private void subscribeToTxnEnd() throws MetadataException
    {
      Subscription<WorkUnitEvent> boSub = new Subscription<WorkUnitEvent>(
          WorkUnitEvent.class);
      EventFilter<WorkUnitEvent> f = new EventFilter<WorkUnitEvent>();
      f.setAttr(WorkUnitFilterAttr.ID, ctx.getTransaction().getId());
      f.setAttr(WorkUnitFilterAttr.STATE, WorkUnitState.ENDED);
      boSub.setFilter(f);
      boSub.setNotification(this);
      boSub.setOneTime(true);
      ctx.addSubscription(boSub);
    }

    @Override
    public void onEvent(WorkUnitEvent e)
    {
      try
      {
        if ((e.getWorkUnit() instanceof Transaction) && !e.isFailed())
        {
          ServiceContext svc = ServiceContext.createInternalContext(ctx, true);
          svc.startCall();
          Persisted bo = boSvc.getBOById(svc, objId);
          svc.endCall(false);

          BOEvent event = new BOEvent(bo, op);
          subSvc.pushEvent(event);

          logger.debug(
              "Pushed notifications for object id: {}, type {}, op: {}",
              objId, boSvc.boResource.getName(), op.value());
        }
      }
      catch (Exception e1)
      {
        throw new DgRuntimeException(e1, PERRMessages.PERR_INTERNAL_OPERATION);
      }
    }
  }
}
