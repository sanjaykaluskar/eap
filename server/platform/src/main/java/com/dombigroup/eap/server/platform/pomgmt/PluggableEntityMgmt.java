/**
 * PluggablePOMgmtService.java
 */
package com.dombigroup.eap.server.platform.pomgmt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.context.WorkUnit;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.EntityMgmt;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.storagemgmt.CachedObject;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;
import com.dombigroup.eap.server.platform.storagemgmt.ObjectCache;

/**
 * @author sanjay
 * 
 * @param <K>
 * @param <V>
 */
abstract public class PluggableEntityMgmt<K, V extends Persisted> extends
    PluggableService implements EntityMgmt<K, V>
{
  private static final int     PO_CACHE_SIZE = 100;
  private Logger               logger;
  private Class<?>             valueClass;
  protected ObjectCache<K, V>  poCache;
  protected PObjectMgmtService pObjMgmt      = null;

  public PluggableEntityMgmt(Class<?> Kclass, Class<?> Vclass)
      throws StorageException, MetadataException
  {
    valueClass = Vclass;
    poCache = new ObjectCache<K, V>(Kclass, Vclass, PO_CACHE_SIZE);
    logger = LoggerFactory.getLogger(this.getClass());
    if (!valueClass.equals(PObject.class))
      pObjMgmt = (PObjectMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_POBJECT);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#install()
   */
  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
  }

  @Override
  public synchronized int nextId(ServiceContext ctx) throws MetadataException
  {
    if (pObjMgmt != null)
      return pObjMgmt.nextId(ctx);
    else
      return 0;
  }

  /**
   * copyPo - copies the contents of one object (from) to another (to) This
   * method is called to populate a new instance of an object during some
   * operations such as create and update. This method needs to be defined by
   * subclasses. There is no need to copy the inherited fields of Persisted
   * since they are already copied before calling this method.
   */
  abstract protected void copyPo(V from, V to);

  private void copyObject(V from, V to)
  {
    to.copyPObjectInfo(from);
    copyPo(from, to);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.EntityMgmt#get(com.dombigroup.eap.server.platform.context.ServiceContext, java.lang.Object, com.dombigroup.eap.server.platform.storagemgmt.LockMode, com.dombigroup.eap.server.platform.context.WorkUnit)
   */
  @SuppressWarnings("unchecked")
  public V get(ServiceContext ctx, final K k, LockMode m, WorkUnit w) throws MetadataException
  {
    V ret = null;
    try
    {
      CachedObject<K, V> co = poCache.get(k, m, w);
      V value = co.getObj();
      if (!co.isEmpty())
      {
        ret = (V) (valueClass.getConstructor().newInstance());
        if (pObjMgmt != null)
          pObjMgmt.populateFromPObject(ctx, value, m);
        copyObject(value, ret);
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_PO_ACCESS, e, k.toString());
    }
    logger.debug("PluggableEntityMgmt.get: key={}, value={}", k.toString(),
        (ret == null) ? "null" : ret.toString());
    return ret;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.POMgmt#create(java.lang.Object
   * , com.dombigroup.eap.common.datamodels.Persisted)
   */
  @Override
  public int create(ServiceContext ctx, final K k, final V p)
      throws MetadataException
  {
    try
    {
      CachedObject<K, V> co = poCache.get(k, LockMode.EXCLUSIVE, ctx
          .getTransaction());
      if (!co.isEmpty())
        throw new MetadataException(PERRMessages.PERR_PO_DUPLICATE,
            k.toString());
      V value = co.getObj();
      ctx.getTransaction().markInserted(poCache, co);
      copyObject(p, value);
      if (pObjMgmt != null)
        pObjMgmt.createPObject(ctx, value);
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_PO_CREATION, e,
          p.toString());
    }
    return p.getId();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.POMgmt#update(java.lang.Object
   * , com.dombigroup.eap.common.datamodels.Persisted)
   */
  @Override
  public void update(ServiceContext ctx, final K k, final V a)
      throws MetadataException
  {
    try
    {
      CachedObject<K, V> co = poCache.get(k, LockMode.EXCLUSIVE, ctx
          .getTransaction());
      if (co.isEmpty())
        throw new MetadataException(PERRMessages.PERR_PO_ACCESS, k.toString());
      ctx.getTransaction().markUpdated(poCache, co);
      copyObject(a, co.getObj());
      if (pObjMgmt != null)
        pObjMgmt.updatePObject(ctx, a);
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_PO_UPDATE, e,
          a.toString());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.POMgmt#delete(java.lang.Object
   * )
   */
  @Override
  public void delete(ServiceContext ctx, final K k) throws MetadataException
  {
    try
    {
      CachedObject<K, V> co = poCache.get(k, LockMode.EXCLUSIVE, ctx
          .getTransaction());
      if (co.isEmpty())
        throw new MetadataException(PERRMessages.PERR_PO_ACCESS, k.toString());
      ctx.getTransaction().markDeleted(poCache, co);
      if (pObjMgmt != null)
        pObjMgmt.delete(ctx, co.getObj().getId());
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_PO_DELETION, e,
          k.toString());
    }
  }
}
