/**
 * PGrantedObjectPrivHandler.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class PGrantedObjectPrivHandler extends RdbmsStorageHandler
{
  public class PGrantedObjectPrivResultSet extends StorageResultSet
  {
    ResultSet  rs;
    Statement  stmt;
    Connection cxn;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    /**
     * @param cxn
     *          the cxn to set
     */
    private void setCxn(Connection cxn)
    {
      this.cxn = cxn;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        PGrantedObjectPriv opriv = null;

        if (rs.next())
        {
          opriv = new PGrantedObjectPriv();
          opriv.setId(rs.getInt("id"));
          opriv.setGranteeId(rs.getInt("grantee_id"));
          opriv.setObjectId(rs.getInt("object_id"));
          opriv.setObjectType(rs.getString("object_type"));
          opriv.setOperation(rs.getString("operation"));
          opriv.setGrantor(rs.getInt("grantor_id"));
          opriv.setGrantDate(DayTime.fromGmtDate(rs.getDate("grant_date")));
        }

        return opriv;
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
        cxn.close();
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }
  }

  private static final String STMT_CREATE = "create table oprivs ( "
                                                 + "id number, "
                                                 + "grantee_id number, "
                                                 + "object_id number, "
                                                 + "object_type varchar2(4000), "
                                                 + "operation varchar2(4000), "
                                                 + "grantor_id number, "
                                                 + "grant_date date)";

  private static final String STMT_DROP   = "drop table oprivs";

  private static final String STMT_SELECT = "select id, grantee_id, object_id, object_type, "
                                                 + "operation, grantor_id, grant_date "
                                                 + "from oprivs "
                                                 + "where id = ?";

  private static final String STMT_INSERT = "insert into "
                                                 + "oprivs(id, grantee_id, object_id, object_type, "
                                                 + "operation, grantor_id, grant_date) "
                                                 + "values(?, ?, ?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE = "update oprivs set "
                                              + "grantee_id = ?, object_id = ?, object_type = ?, "
                                              + "operation = ?, grantor_id = ?, grant_date = ? "
                                              + "where (id = ?)";

  private static final String STMT_DELETE = "delete from oprivs "
                                                 + "where (id = ?)";

  private static final String STMT_SEARCH = "select id, grantee_id, object_id, object_type, "
                                              + "operation, grantor_id, grant_date "
                                              + "from oprivs "
                                              + "where (%s)";

  public PGrantedObjectPrivHandler()
  {
    super(PGrantedObjectPriv.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedObjectPriv opriv = (PGrantedObjectPriv) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, opriv.getId());
      stmt.setInt(2, opriv.getGranteeId());
      stmt.setInt(3, opriv.getObjectId());
      stmt.setString(4, opriv.getObjectType());
      stmt.setString(5, opriv.getOperation());
      stmt.setInt(6, opriv.getGrantor());
      stmt.setDate(7, DayTime.safeDate(opriv.getGrantDate()));
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer id = (Integer) key;
    PGrantedObjectPriv opriv = (PGrantedObjectPriv) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, id.intValue());
      ResultSet rs = stmt.executeQuery();
      while (rs.next())
      {
        opriv.setId(rs.getInt("id"));
        opriv.setGranteeId(rs.getInt("grantee_id"));
        opriv.setObjectId(rs.getInt("object_id"));
        opriv.setObjectType(rs.getString("object_type"));
        opriv.setOperation(rs.getString("operation"));
        opriv.setGrantor(rs.getInt("grantor_id"));
        opriv.setGrantDate(DayTime.fromGmtDate(rs.getDate("grant_date")));
        rowsExist = true;
      }
      stmt.close();
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedObjectPriv opriv = (PGrantedObjectPriv) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setInt(1, opriv.getGranteeId());
      stmt.setInt(2, opriv.getObjectId());
      stmt.setString(3, opriv.getObjectType());
      stmt.setString(4, opriv.getOperation());
      stmt.setInt(5, opriv.getGrantor());
      stmt.setDate(6, DayTime.safeDate(opriv.getGrantDate()));
      stmt.setInt(7, opriv.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedObjectPriv opriv = (PGrantedObjectPriv) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, opriv.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  public PGrantedObjectPrivResultSet search(StorageMgmt sm,
      StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PGrantedObjectPrivResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new PGrantedObjectPrivResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
        rs.setCxn(c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

}
