/**
 * RdbmsStorageHandler.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageHandler;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;

/**
 * @author sanjay Base class for all RDBMS storage handlers
 * 
 */
public abstract class RdbmsStorageHandler implements StorageHandler
{
  protected Class<?> supportedType;

  protected RdbmsStorageHandler(Class<?> t)
  {
    supportedType = t;
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#search
   * (java.lang.Class, java.lang.String)
   */
  @Override
  public StorageResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }
  
  @Override
  public StorageResultSet searchWithPagination(StorageMgmt sm, StorageTransaction tx,
      String condition, int startRow, int pageSize) throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }
}
