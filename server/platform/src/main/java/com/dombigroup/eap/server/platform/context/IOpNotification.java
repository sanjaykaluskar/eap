/**
 * IOpNotification.java
 */
package com.dombigroup.eap.server.platform.context;

/**
 * @author skaluska
 *
 */
public interface IOpNotification
{
  public void onAsyncOpChange(AsyncOp op);
}
