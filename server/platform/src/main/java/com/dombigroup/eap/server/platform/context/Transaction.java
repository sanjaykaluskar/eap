/**
 * TransactionContext.java
 */
package com.dombigroup.eap.server.platform.context;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.CachedObject;
import com.dombigroup.eap.server.platform.storagemgmt.ObjectCache;
import com.dombigroup.eap.server.platform.storagemgmt.ObjectCache.CacheTxn;
import com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtFactory;
import com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtService;

/**
 * @author sanjay
 * 
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class Transaction extends WorkUnit
{
  private Map<ObjectCache, CacheTxn> cacheTxns;
  private StorageMgmtService         storageMgr;
  private StorageTransaction         storageTxn;

  public Transaction(ServiceContext ctx) throws StorageException
  {
    super(ctx);
    cacheTxns = new HashMap<ObjectCache, CacheTxn>();
    storageMgr = StorageMgmtFactory.getStorageMgmtService();
  }

  public void markInserted(ObjectCache c, CachedObject o)
      throws MetadataException
  {
    CacheTxn txn = cacheTxns.get(c);
    if (txn == null)
    {
      txn = c.beginTxn();
      cacheTxns.put(c, txn);
    }

    c.markInserted(txn, o);
  }

  public void markUpdated(ObjectCache c, CachedObject o)
      throws MetadataException
  {
    CacheTxn txn = cacheTxns.get(c);
    if (txn == null)
    {
      txn = c.beginTxn();
      cacheTxns.put(c, txn);
    }

    c.markUpdated(txn, o);
  }

  public void markDeleted(ObjectCache c, CachedObject o)
      throws MetadataException
  {
    ObjectCache.CacheTxn txn = cacheTxns.get(c);
    if (txn == null)
    {
      txn = c.beginTxn();
      cacheTxns.put(c, txn);
    }

    c.markDeleted(txn, o);
  }

  public void commit() throws DgException
  {
    /* start a storage txn */
    storageTxn = storageMgr.beginTxn();

    /* first flush changes */
    for (Entry<ObjectCache, CacheTxn> ct : cacheTxns.entrySet())
    {
      ObjectCache c = ct.getKey();
      CacheTxn txn = ct.getValue();

      c.flushTxn(txn, storageTxn);
    }

    /* now commit the storage txn */
    storageMgr.commitTxn(storageTxn);
    storageTxn = null;

    /* notify caches */
    for (Entry<ObjectCache, CacheTxn> ct : cacheTxns.entrySet())
    {
      ObjectCache c = ct.getKey();
      CacheTxn txn = ct.getValue();

      c.commitTxn(txn);
    }

    cacheTxns.clear();
    end(false);
  }

  public void abort() throws DgException
  {
    if (storageTxn != null)
    {
      storageMgr.abortTxn(storageTxn);
      storageTxn = null;
    }
    
    for (Entry<ObjectCache, CacheTxn> ct : cacheTxns.entrySet())
    {
      ObjectCache c = ct.getKey();
      CacheTxn txn = ct.getValue();

      c.abortTxn(txn);
    }

    cacheTxns.clear();
    end(true);
  }
}
