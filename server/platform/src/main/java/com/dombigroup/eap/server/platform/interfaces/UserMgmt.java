/**
 * UserMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;
import com.dombigroup.eap.common.datamodels.GrantedRole;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;

/**
 * @author sanjay
 * 
 */
public interface UserMgmt
{
  /**
   * getUser
   */
  public User getUser(ServiceContext ctx, int id) throws MetadataException;

  /**
   * getUserList
   */
  public List<User> getUserList(ServiceContext ctx, int orgId)
      throws MetadataException;

  /**
   * getUser
   * 
   * @throws StorageException
   */
  public User getUser(ServiceContext ctx, int orgId, String userName)
      throws MetadataException;

  /**
   * createUser
   */
  public int createUser(ServiceContext ctx, User u) throws MetadataException;

  /**
   * updateUser
   */
  public void updateUser(ServiceContext ctx, User user)
      throws MetadataException;

  /**
   * updateUser
   */
  public void updateUser(ServiceContext ctx, int userId, User user)
      throws MetadataException;

  /**
   * deleteUser
   */
  public void deleteUser(ServiceContext ctx, User user)
      throws MetadataException;

  /**
   * getRoles
   */
  public List<GrantedRole> getRoles(ServiceContext ctx, int userId)
      throws MetadataException;

  /**
   * getAvailableRoles - returns list of roles that can be granted to another
   * user
   */
  public List<IRole> getGrantableRoles(ServiceContext ctx)
      throws MetadataException;

  /**
   * addRole - adds the specified role to the user
   */
  public void addRole(ServiceContext ctx, int userId, String role)
      throws MetadataException;

  /**
   * removeRole - removes the specified role from the user
   */
  public void removeRole(ServiceContext ctx, int userId, String role)
      throws MetadataException;

  /**
   * getObjectPrivList
   */
  public List<GrantedObjectPriv> getObjectPrivList(ServiceContext ctx,
      int userId) throws MetadataException;
}
