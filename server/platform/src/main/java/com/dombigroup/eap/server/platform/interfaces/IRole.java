/**
 * IRole.java
 */
package com.dombigroup.eap.server.platform.interfaces;

/**
 * @author sanjay
 *
 */
public interface IRole
{
  public String getName();
  
  public String getDescription();
}
