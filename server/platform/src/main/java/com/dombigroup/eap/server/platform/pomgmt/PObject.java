/**
 * PObject.java
 */
package com.dombigroup.eap.server.platform.pomgmt;

import com.dombigroup.eap.common.datamodels.Persisted;

/**
 * @author sanjay
 *
 */
public class PObject extends Persisted
{
  public PObject()
  {
  }

  public PObject(Persisted p)
  {
    setId(p.getId());
    setOrgId(p.getOrgId());
    setCreatorId(p.getCreatorId());
    setCreationTime(p.getCreationTime());
    setUpdateTime(p.getUpdateTime());
    setOprivListId(p.getOprivListId());
  }
}
