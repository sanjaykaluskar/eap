/**
 * Storage.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.server.platform.exceptions.StorageException;

/**
 * @author sanjay
 * 
 */
public interface StorageMgmt
{
  public enum ChangeType
  {
    CREATE, UPDATE, DELETE;
  }

  /* transaction management */
  public StorageTransaction beginTxn() throws StorageException;

  public void commitTxn(StorageTransaction tx) throws StorageException;

  public void abortTxn(StorageTransaction tx) throws StorageException;

  public void add(StorageTransaction tx, Object o, ChangeType c)
      throws StorageException;

  public void remove(StorageTransaction tx, Object o) throws StorageException;

  /* load objects */
  public boolean load(Object key, Object value) throws StorageException;

  public StorageResultSet search(Class<?> c, String condition)
      throws StorageException;
  
  /**
   * search for objects satisfying a condition with pagination;
   * returns a "page" of rows starting from a specified row-number.
   * 
   * The row numbers are upto the implementation as long as the
   * relative ordering is the same for 2 given rows.
   * 
   * @param c - class of objects to search
   * @param condition - search condition
   * @param startRow - starting row number (numbering starts at 1)
   * @param pageSize - max number of rows to be returned
   */
  public StorageResultSet searchWithPagination(Class<?> c, String condition,
      int startRow, int pageSize)
      throws StorageException;

}
