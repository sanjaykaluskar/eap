/**
 * AuthorizationPolicyService.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.AggregateMgmt;
import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy;
import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicyMgmt;
import com.dombigroup.eap.server.platform.interfaces.IOperationEnumerator;
import com.dombigroup.eap.server.platform.interfaces.IResourceTypeEnumerator;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public class AuthorizationPolicyService extends
    PluggableService implements IAuthorizationPolicyMgmt
{
  private class PolicyLookupKey
  {
    private ResourceType resource;
    private Operation    op;
    private int          hashCode = 0;

    private PolicyLookupKey(ResourceType r, Operation o)
    {
      resource = r;
      op = o;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object key)
    {
      boolean ret = false;
      if ((key != null) && (key instanceof PolicyLookupKey))
      {
        PolicyLookupKey pk = (PolicyLookupKey) key;

        ret = (pk.resource.equals(resource) && pk.op.equals(op));
      }
      return ret;
    }

    @Override
    public int hashCode()
    {
      if (hashCode == 0)
      {
        hashCode = 17;
        hashCode = 37 * hashCode + resource.hashCode();
        hashCode = 37 * hashCode + op.hashCode();
      }
      return hashCode;
    }
  }

  private static ServiceLoader<IResourceTypeEnumerator>             resLoader    = ServiceLoader
                                                                                     .load(IResourceTypeEnumerator.class);
  private static ServiceLoader<IOperationEnumerator>                opLoader     = ServiceLoader
                                                                                     .load(IOperationEnumerator.class);
  private static ServiceLoader<IAuthorizationPolicy>                policyLoader = ServiceLoader
                                                                                     .load(IAuthorizationPolicy.class);
  private static List<ResourceType>                                 resourceTypes;
  private static List<Operation>                                    operations;
  private static Map<PolicyLookupKey, List<IAuthorizationPolicy>>   policyLookup;
  private static Map<IAuthorizationPolicy, AuthorizationPolicyInfo> policyInfo;
  private AggregateMgmt                                             aggrSvc;
  private PDisabledPolicyMgmtService                                disabledPolicySvc;

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#install()
   */
  @Override
  public void install(ServiceContext ctx)
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    // load all resource types
    resourceTypes = new ArrayList<ResourceType>();
    for (IResourceTypeEnumerator rtEnum : resLoader)
    {
      ResourceType[] rtList = rtEnum.listResourceTypes();
      for (ResourceType rt : rtList)
        resourceTypes.add(rt);
    }

    // load all operations
    operations = new ArrayList<Operation>();
    for (IOperationEnumerator opEnum : opLoader)
    {
      Operation[] opList = opEnum.listOperations();
      for (Operation op : opList)
        operations.add(op);
    }

    // load all policies
    policyInfo = new HashMap<IAuthorizationPolicy, AuthorizationPolicyInfo>();
    policyLookup =
        new HashMap<AuthorizationPolicyService.PolicyLookupKey, List<IAuthorizationPolicy>>();
    for (IAuthorizationPolicy ap : policyLoader)
    {
      // construct policy info
      AuthorizationPolicyInfo info = new AuthorizationPolicyInfo();
      info.setName(ap.getName());
      info.setDesc(ap.getDescription());
      info.setOrgLevelControl(ap.orgLevel());
      info.setPolicyImpl(ap);
      policyInfo.put(ap, info);
      populateIndex(ap);
    }

    // references to other services
    try
    {
      disabledPolicySvc = (PDisabledPolicyMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PDISABLEDPOLICY);
      aggrSvc = (AggregateMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_AGGREGATE);
    }
    catch (MetadataException e)
    {
      throw new ServiceException(
          PERRMessages.PERR_AUTHORIZATION_POLICY_SERVICE_STARTUP, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicyMgmt#
   * getRelevantPolicies
   * (com.dombigroup.eap.server.platform.context.SecurityContext,
   * com.dombigroup.eap.server.platform.interfaces.ResourceType,
   * com.dombigroup.eap.server.platform.interfaces.Operation)
   */
  @Override
  public List<IAuthorizationPolicy> getRelevantPolicies(
      SecurityContext ctx, ResourceType resourceType,
      Operation op)
  {
    return policyLookup.get(new PolicyLookupKey(
        resourceType, op));
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicyMgmt#
   * getAllPolicies(boolean)
   */
  @Override
  public List<AuthorizationPolicyInfo> getAllPolicies(ServiceContext ctx)
  {
    List<AuthorizationPolicyInfo> ret = new ArrayList<AuthorizationPolicyInfo>();
    for (AuthorizationPolicyInfo i : policyInfo.values())
      ret.add(i);
    return ret;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicyMgmt#
   * getPolicyInfo
   * (com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy)
   */
  public AuthorizationPolicyInfo getPolicyInfo(
      ServiceContext ctx, IAuthorizationPolicy policy)
  {
    return policyInfo.get(policy);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicyMgmt#
   * getPolicyInfoByName(java.lang.String)
   */
  public AuthorizationPolicyInfo getPolicyInfoByName(ServiceContext ctx,
      String policyName)
  {
    AuthorizationPolicyInfo ret = null;
    for (AuthorizationPolicyInfo i : policyInfo.values())
    {
      if (i.getName().equals(policyName))
      {
        ret = i;
        break;
      }
    }
    return ret;
  }

  private void populateIndex(IAuthorizationPolicy ap)
  {
    /* check what resource types and operations are in scope */
    for (ResourceType rt : resourceTypes)
      for (Operation op : operations)
      {
        if (ap.resourceTypeInScope(rt) && ap
            .operationInScope(op))
        {
          // construct key
          PolicyLookupKey key = new PolicyLookupKey(rt,
              op);

          // add to the map
          List<IAuthorizationPolicy> existingList = policyLookup
              .get(key);
          if (existingList == null)
          {
            existingList = new ArrayList<IAuthorizationPolicy>();
            policyLookup.put(key, existingList);
          }

          if (!existingList.contains(ap))
            existingList.add(ap);
        }
      }
  }

  private PDisabledPolicy createDisabledPolicy(ServiceContext ctx, int listId,
      String policyName) throws MetadataException
  {
    int id = disabledPolicySvc.nextId(ctx);
    PDisabledPolicy pDisPolicy = new PDisabledPolicy();
    pDisPolicy.setId(id);
    pDisPolicy.setPolicyName(policyName);
    pDisPolicy.setDisabledBy(ctx.getSecContext().getUserId());
    pDisPolicy.setDisabledTime(new DayTime());
    disabledPolicySvc.create(ctx, id, pDisPolicy);
    return pDisPolicy;
  }

  @Override
  public int createDisabledPolicyList(ServiceContext ctx, String policyName)
      throws MetadataException
  {
    int listId = aggrSvc.nextId(ctx);
    PDisabledPolicy pDisPolicy = createDisabledPolicy(ctx, listId, policyName);
    Aggregate roleList = new Aggregate();
    roleList.setId(listId);
    roleList.add(pDisPolicy);
    aggrSvc.create(ctx, listId, roleList);

    return listId;
  }

  @Override
  public void addDisabledPolicy(ServiceContext ctx, int listId,
      String policyName) throws MetadataException
  {
    AuthorizationPolicyInfo info = getPolicyInfoByName(ctx, policyName);
    if (info == null)
      throw new MetadataException(PERRMessages.PERR_POLICY_INVALID_NAME,
          policyName);

    if (!info.isOrgLevelControl())
      throw new MetadataException(PERRMessages.PERR_POLICY_ORG_CONTROL, info
          .getName());

    /* create a grant desc */
    PDisabledPolicy pDisPolicy = createDisabledPolicy(ctx, listId, policyName);

    Aggregate roleList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    roleList.add(pDisPolicy);
    aggrSvc.update(ctx, listId, roleList);
  }

  @Override
  public void removeDisabledPolicy(ServiceContext ctx, int listId,
      String policyName) throws MetadataException
  {
    AuthorizationPolicyInfo info = getPolicyInfoByName(ctx, policyName);
    if (info == null)
      throw new MetadataException(PERRMessages.PERR_POLICY_INVALID_NAME,
          policyName);

    if (!info.isOrgLevelControl())
      throw new MetadataException(PERRMessages.PERR_POLICY_ORG_CONTROL, info
          .getName());

    Aggregate roleList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    Persisted found = null;
    if (roleList != null)
    {
      for (Persisted policyObj : roleList.getObjList())
      {
        PDisabledPolicy pgr = disabledPolicySvc.get(ctx, policyObj.getId(),
            LockMode.EXCLUSIVE, ctx.getTransaction());
        if (pgr.getPolicyName().equals(policyName))
        {
          found = policyObj;
          break;
        }
      }

      if (found != null)
      {
        disabledPolicySvc.delete(ctx, found.getId());
        roleList.remove(found);
        aggrSvc.update(ctx, listId, roleList);
      }
    }
  }

  @Override
  public void deleteDisabledPolicyList(ServiceContext ctx, int listId)
      throws MetadataException
  {
    Aggregate roleList = aggrSvc.get(ctx, listId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    if (roleList != null)
    {
      for (Persisted policyObj : roleList.getObjList())
      {
        disabledPolicySvc.delete(ctx, policyObj.getId());
      }
      aggrSvc.delete(ctx, listId);
    }
  }

  @Override
  public List<AuthorizationPolicyInfo> getDisabledPolicyList(
      ServiceContext ctx, int listId) throws MetadataException
  {
    List<AuthorizationPolicyInfo> ret = null;
    if (listId != Persisted.INVALID_ID)
    {
      Aggregate aggr = aggrSvc.get(ctx, listId, LockMode.SHARED, ctx
          .getWorkUnit());
      if (aggr != null)
      {
        ret = new ArrayList<AuthorizationPolicyInfo>();
        for (Persisted grObj : aggr.getObjList())
        {
          PDisabledPolicy pDisPolicy = disabledPolicySvc
              .get(ctx, grObj.getId(), LockMode.SHARED, ctx.getWorkUnit());
          AuthorizationPolicyInfo policyInfo = getPolicyInfoByName(ctx,
              pDisPolicy.getPolicyName());
          ret.add(policyInfo);
        }
      }
    }

    return ret;
  }
}
