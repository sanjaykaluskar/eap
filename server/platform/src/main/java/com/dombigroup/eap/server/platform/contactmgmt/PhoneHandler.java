/**
 * OrgHandler.java
 */
package com.dombigroup.eap.server.platform.contactmgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dombigroup.eap.common.datamodels.Phone;
import com.dombigroup.eap.common.datamodels.PhoneLocation;
import com.dombigroup.eap.common.datamodels.PhoneType;
import com.dombigroup.eap.common.utils.ExpressionUtils;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class PhoneHandler extends RdbmsStorageHandler
{
  public class PhoneResultSet extends StorageResultSet
  {
    ResultSet rs;
    Statement stmt;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        Phone phone = null;

        if (rs.next())
        {
          phone = new Phone();
          phone.setId(rs.getInt("id"));
          phone.setNumberType(PhoneType.fromValue(rs.getString("numbertype")));
          phone.setLocation(PhoneLocation.fromValue(rs.getString("location")));
          phone.setCountryCode(rs.getInt("countrycode"));
          phone.setAreaCode(rs.getInt("areacode"));
          phone.setNumber(rs.getLong("phonenumber"));
          phone.setExtension(rs.getInt("extension"));
        }

        return phone;
      }
      catch (SQLException e)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
      }
      catch (SQLException e)
      {
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

  }

  private static final String STMT_CREATE = "create table phones ("
                                              + "id number primary key, "
                                              + "numbertype varchar2(4000), "
                                              + "location varchar2(4000), "
                                              + "countrycode number, "
                                              + "areacode number, "
                                              + "phonenumber number, "
                                              + "extension number)";

  private static final String STMT_DROP   = "drop table phones";

  private static final String STMT_SELECT = "select "
                                              + "id, numbertype, location, countrycode, areacode, phonenumber, extension "
                                              + "from phones "
                                              + "where id = ?";

  private static final String STMT_INSERT = "insert into "
                                              + "phones(id, numbertype, location, countrycode, areacode, phonenumber, extension) "
                                              + "values(?, ?, ?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE = "update phones "
                                              + "set numbertype = ?,"
                                              + "location = ?, "
                                              + "countrycode = ?, "
                                              + "areacode = ?, "
                                              + "phonenumber = ?, "
                                              + "extension = ? "
                                              + "where (id = ?)";

  private static final String STMT_DELETE = "delete from phones "
                                              + "where (id = ?)";

  private static final String STMT_SEARCH = "select "
                                              + "id, numbertype, location, countrycode, areacode, phonenumber, extension "
                                              + "from phones "
                                              + "where (%s)";

  public PhoneHandler()
  {
    super(Phone.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Phone phone = (Phone) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, phone.getId());
      stmt.setString(2, ExpressionUtils.safeString(phone.getNumberType()));
      stmt.setString(3, ExpressionUtils.safeString(phone.getLocation()));
      stmt.setInt(4, phone.getCountryCode());
      stmt.setInt(5, phone.getAreaCode());
      stmt.setLong(6, phone.getNumber());
      stmt.setInt(7, phone.getExtension());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer phoneId = (Integer) key;
    Phone phone = (Phone) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, phoneId.intValue());
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        phone.setId(rs.getInt("id"));
        phone.setNumberType(PhoneType.fromValue(rs.getString("numbertype")));
        phone.setLocation(PhoneLocation.fromValue(rs.getString("location")));
        phone.setCountryCode(rs.getInt("countrycode"));
        phone.setAreaCode(rs.getInt("areacode"));
        phone.setNumber(rs.getLong("phonenumber"));
        phone.setExtension(rs.getInt("extension"));
        rowsExist = true;
        stmt.close();
      }
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Phone phone = (Phone) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setString(1, ExpressionUtils.safeString(phone.getNumberType()));
      stmt.setString(2, ExpressionUtils.safeString(phone.getLocation()));
      stmt.setInt(3, phone.getCountryCode());
      stmt.setInt(4, phone.getAreaCode());
      stmt.setLong(5, phone.getNumber());
      stmt.setInt(6, phone.getExtension());
      stmt.setInt(7, phone.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Phone phone = (Phone) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, phone.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#search
   * (java.lang.Class, java.lang.String)
   */
  @Override
  public PhoneResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PhoneResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new PhoneResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
