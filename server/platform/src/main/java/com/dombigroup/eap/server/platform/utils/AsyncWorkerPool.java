/**
 * AsyncWorkerPool.java
 */
package com.dombigroup.eap.server.platform.utils;

import com.dombigroup.eap.server.platform.context.AsyncOp;
import com.dombigroup.eap.server.platform.interfaces.AsyncTaskSpec;

/**
 * @author skaluska
 * 
 *         Distributes tasks (AsyncTaskSpec) to a pool of workers (AsyncWorker).
 */
public class AsyncWorkerPool<Factory extends AsyncWorkerFactory, TaskSpec extends AsyncTaskSpec>
{
  /* specs */
  private AsyncWorkerFactory workerFactory;
  private int                maxTasks;
  private int                maxWorkers;
  private float              totalWork;

  /* derived quantities */
  private int                taskLimitPerWorker;
  private int                balanceTasks;
  private int                numWorkers;
  private float              workPerWorker;

  /* runtime state */
  private AsyncWorker[]      workers;
  private int                workerNo          = 0;
  private int                numTasksForWorker = 0;
  private int                taskLimit;

  /**
   * Constructor for AsyncWorkerPool
   * 
   * @param factory
   *          - A factory class to return a new worker
   * @param taskCount
   *          - Total number of tasks that will be added
   * @param workerLimit
   *          - Maximum number of workers allowed
   * @param estimatedWork
   *          - Estimate of the fraction of work
   */
  public AsyncWorkerPool(Factory factory, int taskCount, int workerLimit,
      float estimatedWork)
  {
    workerFactory = factory;
    maxTasks = taskCount;
    maxWorkers = workerLimit;
    totalWork = estimatedWork;

    if (maxTasks > 0)
    {
      /* compute number of workers and tasks per worker */
      if (maxTasks < maxWorkers)
      {
        /* one task per record */
        taskLimitPerWorker = 1;
        numWorkers = maxTasks;
      }
      else
      {
        /*
         * There will be maxTasks / maxWorkers tasks per worker. Any balance
         * will be re-distributed.
         */
        taskLimitPerWorker = maxTasks / maxWorkers;
        numWorkers = maxWorkers;
        balanceTasks = maxTasks % maxWorkers;
      }

      workPerWorker = totalWork / numWorkers;

      workers = new AsyncWorker[numWorkers];
    }
  }

  public void addTaskSpec(TaskSpec spec)
  {
    /* Start a new task if this is the first record in the task */
    if (numTasksForWorker == 0)
    {
      workers[workerNo] = workerFactory.newWorker(workPerWorker);

      /* redistributed any leftover tasks */
      if (balanceTasks > 0)
      {
        taskLimit = taskLimitPerWorker + 1;
        balanceTasks--;
      }
      else
        taskLimit = taskLimitPerWorker;        
    }

    /* add task to current worker */
    workers[workerNo].addTask(spec);
    numTasksForWorker++;

    /*
     * Start current worker if it has taskLimit tasks.
     */
    if (numTasksForWorker == taskLimit)
    {
      workers[workerNo].start();
      workerNo++;
      numTasksForWorker = 0;
    }
  }

  public void waitForCompletion() throws InterruptedException
  {
    try
    {
      for (int i = 0; i < workers.length; i++)
      {
        if (Thread.currentThread().isInterrupted())
          workers[i].interrupt();
        else
          workers[i].waitForCompletion();
      }
    }
    catch (Exception e)
    {
      AsyncOp.cancelAsyncOps(workers);
    }
  }
}
