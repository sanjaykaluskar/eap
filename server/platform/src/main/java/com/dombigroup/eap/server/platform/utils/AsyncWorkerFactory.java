/**
 * AsyncWorkerFactory.java
 */
package com.dombigroup.eap.server.platform.utils;

/**
 * @author skaluska
 *
 * AsyncOpWorker is a type of AsyncOp that works on an AsyncTask.
 */
public abstract class AsyncWorkerFactory
{
  public abstract AsyncWorker newWorker(float work);
}
