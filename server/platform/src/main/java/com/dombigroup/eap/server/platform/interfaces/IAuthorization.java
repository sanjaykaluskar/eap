/**
 * AuthorizationService.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.context.SecurityContext;

/**
 * @author sanjay
 * 
 */
public interface IAuthorization
{
  public boolean check(SecurityContext ctx, ResourceType resourceType,
      int resourceId, Object resource, Operation o);
}
