/**
 * NotificationContext.java
 */
package com.dombigroup.eap.server.platform.context;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.notifications.Subscription;
import com.dombigroup.eap.server.platform.notifications.SubscriptionService;
import com.dombigroup.eap.server.platform.notifications.SubscriptionServiceFactory;

/**
 * @author sanjay
 *
 */
@SuppressWarnings("rawtypes")
public class NotificationContext
{
  private List<Subscription> subscriptions;

  public NotificationContext()
  {
    subscriptions = new ArrayList<Subscription>();
  }

  synchronized public void addSubscription(Subscription s) throws MetadataException
  {
    SubscriptionService subSvc = SubscriptionServiceFactory.getSubscriptionService();
    subSvc.addSubscription(s);
    subscriptions.add(s);
  }

  synchronized public void removeSubscription(Subscription s) throws MetadataException
  {
    SubscriptionService subSvc = SubscriptionServiceFactory.getSubscriptionService();
    subSvc.removeSubscription(s);
    subscriptions.remove(s);
  }
  
  synchronized public void removeAllSubscriptions() throws MetadataException
  {
    SubscriptionService subSvc = SubscriptionServiceFactory.getSubscriptionService();
    for (Subscription s : subscriptions)
      subSvc.removeSubscription(s);
    subscriptions.clear();
  }

  /**
   * @return the subscriptions
   */
  public List<Subscription> getSubscriptions()
  {
    return subscriptions;
  }
}
