/**
 * AggregateMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.context.WorkUnit;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public interface EntityMgmt<K, V extends Persisted>
{
  public int nextId(ServiceContext ctx) throws MetadataException;

  public int create(ServiceContext ctx, final K k, final V v)
      throws MetadataException;

  public V get(ServiceContext ctx, final K key, LockMode m, WorkUnit w)
      throws MetadataException;

  public void update(ServiceContext ctx, final K k, final V v)
      throws MetadataException;

  public void delete(ServiceContext ctx, final K k) throws MetadataException;
}
