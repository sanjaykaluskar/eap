/**
 * PlatformStatus.java
 */
package com.dombigroup.eap.server.platform.mgmt;

/**
 * @author sanjay
 * 
 */
public class PlatformStatus
{
  private static final PlatformStatus inst = new PlatformStatus();
  private PlatformState               state;

  private PlatformStatus()
  {
    state = PlatformState.NOTREADY;
  }

  public static PlatformStatus getInstance()
  {
    return inst;
  }

  /**
   * @return the state
   */
  public PlatformState getState()
  {
    return state;
  }

  /**
   * @param state
   *          the state to set
   */
  public void setState(PlatformState state)
  {
    this.state = state;
  }
}
