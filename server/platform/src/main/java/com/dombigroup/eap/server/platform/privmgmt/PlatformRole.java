/**
 * PlatformRole.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.server.platform.interfaces.IRole;

/**
 * @author sanjay
 * 
 */
public enum PlatformRole implements IRole
{
  SITE_ADMIN("SITE_ADMIN", "Administrator for the entire site"),
  ORG_ADMIN("ORG_ADMIN", "Administrator for the entire company"),
  EMPLOYEE("EMPLOYEE", "Employee within a company"),
  CUSTOMER("CUSTOMER", "Customer of a company");

  private final String value;
  private final String description;

  private PlatformRole(String v, String desc)
  {
    value = v;
    description = desc;
  }

  public String value()
  {
    return value;
  }

  public static PlatformRole fromValue(String v)
  {
    if (v == null)
      return null;

    for (PlatformRole priv : PlatformRole.values())
    {
      if (priv.value.equals(v))
      {
        return priv;
      }
    }
    throw new IllegalArgumentException(v);
  }

  @Override
  public String getName()
  {
    return value;
  }

  @Override
  public String getDescription()
  {
    return description;
  }
}
