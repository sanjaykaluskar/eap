package com.dombigroup.eap.server.platform.bomgmt;

import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.server.platform.notifications.IEventFilterAttr;

/**
 * @author sanjay
 *
 */
public enum BOFilterAttr implements IEventFilterAttr<BOEvent>
{
  ORG_ID("ORG_ID", Integer.class),
  OBJECT_ID("OBJECT_ID", Integer.class),
  OPERATION("OPERATION", OperationType.class);

  private final String value;
  
  private final Class<?> valueType;

  BOFilterAttr(String v, Class<?> vt)
  {
    value = v;
    valueType = vt;
  }

  public String value()
  {
    return value;
  }

  public static BOFilterAttr fromValue(String v)
  {
    if (v == null)
      return null;

    for (BOFilterAttr c : BOFilterAttr.values())
    {
      if (c.value.equals(v))
      {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  @Override
  public String getName()
  {
    return value;
  }

  @Override
  public Class<?> getValueClass()
  {
    return valueType;
  }
}
