/**
 * PlatformState.java
 */
package com.dombigroup.eap.server.platform.mgmt;

/**
 * @author sanjay
 * 
 */
public enum PlatformState
{
  NOTREADY,
  STARTUP_IN_PROGRESS,
  INSTALL_IN_PROGRESS,
  UNINSTALL_IN_PROGRESS,
  SHUTDOWN_IN_PROGRESS,
  READY;
}
