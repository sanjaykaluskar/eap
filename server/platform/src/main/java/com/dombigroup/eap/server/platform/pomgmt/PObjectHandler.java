/**
 * PObjectHandler.java
 */
package com.dombigroup.eap.server.platform.pomgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.TimeZone;

import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class PObjectHandler extends RdbmsStorageHandler
{
  private static final Calendar GMT_CALENDAR = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

  public class PObjectResultSet extends StorageResultSet
  {
    ResultSet  rs;
    Statement  stmt;
    Connection cxn;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    /**
     * @param cxn
     *          the cxn to set
     */
    private void setCxn(Connection cxn)
    {
      this.cxn = cxn;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        PObject pobject = null;

        if (rs.next())
        {
          pobject = new PObject();
          pobject.setId(rs.getInt("id"));
          pobject.setOrgId(rs.getInt("org_id"));
          pobject.setOprivListId(rs.getInt("opriv_id"));
          pobject.setCreatorId(rs.getInt("creator_id"));
          pobject.setCreationTime(DayTime.fromGmtTimestamp(rs.getTimestamp("creation_time", GMT_CALENDAR)));
          pobject.setUpdateTime(DayTime.fromGmtTimestamp(rs.getTimestamp("update_time", GMT_CALENDAR)));
          pobject.setVersion(rs.getInt("version"));
        }

        return pobject;
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
        cxn.close();
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

  }

  private static final String STMT_CREATE_TABLE = "create table pobjects ("
                                                    + "id number primary key, "
                                                    + "org_id number, "
                                                    + "opriv_id number, "
                                                    + "creator_id number, "
                                                    + "creation_time date, "
                                                    + "update_time date, "
                                                    + "version number)";

  private static final String STMT_DROP         = "drop table pobjects";

  private static final String STMT_SELECT       = "select "
                                                    + "id, org_id, opriv_id, creator_id, "
                                                    + "creation_time, update_time, version "
                                                    + "from pobjects "
                                                    + "where id = ?";

  private static final String STMT_INSERT       = "insert into pobjects("
                                                    + "id, org_id, opriv_id, creator_id, "
                                                    + "creation_time, update_time, version) "
                                                    + "values(?, ?, ?, ?, ?, ?, ?)";

  private static final String STMT_S4UPDATE     = "select id, version from pobjects "
                                                    + "where id = ? "
                                                    + "for update";

  private static final String STMT_UPDATE       = "update pobjects set "
                                                    + "org_id = ?, opriv_id = ?, creator_id = ?, "
                                                    + "creation_time = ?, update_time = ?, version = ? "
                                                    + "where (id = ?)";

  private static final String STMT_DELETE       = "delete from pobjects "
                                                    + "where (id = ?)";

  private static final String STMT_SEARCH       = "select "
                                                    + "id, org_id, opriv_id, creator_id, "
                                                    + "creation_time, update_time, version "
                                                    + "from pobjects "
                                                    + "where (%s)";

  public PObjectHandler()
  {
    super(PObject.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE_TABLE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PObject pobject = (PObject) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, pobject.getId());
      stmt.setInt(2, pobject.getOrgId());
      stmt.setInt(3, pobject.getOprivListId());
      stmt.setInt(4, pobject.getCreatorId());
      stmt.setTimestamp(5, DayTime.safeTimestamp(pobject.getCreationTime()), GMT_CALENDAR);
      stmt.setTimestamp(6, DayTime.safeTimestamp(pobject.getUpdateTime()), GMT_CALENDAR);
      stmt.setInt(7, pobject.getVersion());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer pobjectId = (Integer) key;
    PObject pobject = (PObject) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, pobjectId.intValue());
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        pobject.setId(rs.getInt("id"));
        pobject.setOrgId(rs.getInt("org_id"));
        pobject.setOprivListId(rs.getInt("opriv_id"));
        pobject.setCreatorId(rs.getInt("creator_id"));
        pobject.setCreationTime(DayTime.fromGmtTimestamp(rs.getTimestamp("creation_time", GMT_CALENDAR)));
        pobject.setUpdateTime(DayTime.fromGmtTimestamp(rs.getTimestamp("update_time", GMT_CALENDAR)));
        pobject.setVersion(rs.getInt("version"));
        rowsExist = true;
        stmt.close();
      }
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PObject pobject = (PObject) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_S4UPDATE);
      stmt.setInt(1, pobject.getId());
      ResultSet rs = stmt.executeQuery();
      if (rs == null)
        throw new StorageException(PERRMessages.PERR_DB_MISSING_OBJECT,
            String.valueOf(pobject.getId()), Persisted.class.getSimpleName());
      if (rs.next())
      {
        int existingVersion = rs.getInt("version");
        if (existingVersion != pobject.getVersion())
          throw new MetadataException(PERRMessages.PERR_STALE_UPDATE,
              String.valueOf(pobject.getId()));
        pobject.setVersion(existingVersion + 1);
        stmt.close();
      }

      stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setInt(1, pobject.getOrgId());
      stmt.setInt(2, pobject.getOprivListId());
      stmt.setInt(3, pobject.getCreatorId());
      stmt.setTimestamp(4, DayTime.safeTimestamp(pobject.getCreationTime()), GMT_CALENDAR);
      stmt.setTimestamp(5, DayTime.safeTimestamp(pobject.getUpdateTime()), GMT_CALENDAR);
      stmt.setInt(6, pobject.getVersion());
      stmt.setInt(7, pobject.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PObject pobject = (PObject) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, pobject.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#search
   * (java.lang.Class, java.lang.String)
   */
  @Override
  public PObjectResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    PObjectResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new PObjectResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
        rs.setCxn(c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
