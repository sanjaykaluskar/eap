/**
 * StorageException.java
 */
package com.dombigroup.eap.server.platform.exceptions;

import com.dombigroup.eap.common.exceptions.DgException;

/**
 * @author sanjay
 *
 */
public class StorageException extends DgException
{
  /** StorageException.java */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor for StorageException
   */
  public StorageException(PERRMessages m)
  {
    super(m);
  }
  
  public StorageException(PERRMessages m, String...strings)
  {
    super(m, strings);
  }
  
  public StorageException(PERRMessages m, Exception e)
  {
    super(e, m);
  }
  
  public StorageException(PERRMessages m, Exception e, String...strings)
  {
    super(e, m, strings);
  }
}
