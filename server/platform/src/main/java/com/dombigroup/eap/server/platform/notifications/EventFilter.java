/**
 * EventFilter.java
 */
package com.dombigroup.eap.server.platform.notifications;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.dombigroup.eap.common.exceptions.DgRuntimeException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;

/**
 * @author sanjay
 *
 *         Event filter is a collection of attribute values that must match the
 *         event. This is fairly simple and can be extended in the future.
 */
public class EventFilter<EventType extends Event>
{
  private Map<String, Object> filterAttrs;

  public EventFilter()
  {
    filterAttrs = new HashMap<String, Object>();
  }

  /**
   * setAttr - set the value for the specified attribute
   */
  public void setAttr(IEventFilterAttr<EventType> attrInfo, Object value)
  {
    Class<?> valueClass = attrInfo.getValueClass();
    if (valueClass.isInstance(value))
      filterAttrs.put(attrInfo.getName(), value);
    else
      throw new DgRuntimeException(PERRMessages.PERR_INVALID_TYPE_OF_VALUE,
          value.getClass().getName(), attrInfo.getName(), valueClass.getName());
  }
  
  public Object getAttr(IEventFilterAttr<EventType> attrInfo)
  {
    return filterAttrs.get(attrInfo.getName());
  }

  private boolean sameValue(String attrName, Object value)
  {
    Object thisValue = filterAttrs.get(attrName);
    if ((thisValue == null) ^ (value == null))
      return false;

    if ((thisValue != null) && (value != null) && (!thisValue.equals(value)))
      return false;
    
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean equals(Object o)
  {
    if ((o == null) || !(o instanceof EventFilter))
      return false;

    EventFilter<EventType> other = (EventFilter<EventType>) o;
    for (Entry<String, Object> attr : filterAttrs.entrySet())
    {
      if (!other.sameValue(attr.getKey(), attr.getValue()))
        return false;
    }
    
    for (Entry<String, Object> attr : other.filterAttrs.entrySet())
    {
      if (!sameValue(attr.getKey(), attr.getValue()))
        return false;
    }

    return true;
  }
}
