/**
 * BOEvent.java
 */
package com.dombigroup.eap.server.platform.bomgmt;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.notifications.Event;

/**
 * @author sanjay
 *
 */
public class BOEvent extends Event
{
  private Persisted bo;

  private Operation op;

  /**
   * Constructor for BOEvent
   */
  public BOEvent(Persisted bo, Operation op)
  {
    this.bo = bo;
    this.op = op;
  }

  /**
   * @return the bo
   */
  public Persisted getBo()
  {
    return bo;
  }

  /**
   * @param bo the bo to set
   */
  void setBo(Persisted bo)
  {
    this.bo = bo;
  }

  /**
   * @return the op
   */
  public Operation getOp()
  {
    return op;
  }

  /**
   * @param op the op to set
   */
  void setOp(Operation op)
  {
    this.op = op;
  }
}
