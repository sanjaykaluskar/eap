/**
 * IPluggableServiceFactory.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.server.platform.exceptions.ServiceException;

/**
 * @author sanjay
 * 
 */
public interface IPluggableServiceFactory
{
  public List<String> getServiceNames();

  public List<ParamInfo> getConfigParams(String svcName);

  public Config getServiceConfig(String svcName);

  public void setServiceConfig(String svcName, Config c);

  public IPluggableService getService(String svcName) throws ServiceException;
}
