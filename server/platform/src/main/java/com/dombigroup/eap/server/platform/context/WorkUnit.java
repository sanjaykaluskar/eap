/**
 * WorkUnit.java
 */
package com.dombigroup.eap.server.platform.context;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.exceptions.DgRuntimeException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.notifications.SubscriptionService;
import com.dombigroup.eap.server.platform.notifications.SubscriptionServiceFactory;
import com.dombigroup.eap.server.platform.storagemgmt.ObjectLock;

/**
 * @author sanjay
 * 
 *         WorkUnit represents a unit of work, which is a single threaded
 *         operation. It's primary purpose is to track any resources acquired
 *         during the operation, so that they can be released at the end.
 */
public abstract class WorkUnit
{
  private static int           NEXT_ID = 0;
  private int                  id;
  private Thread               owningThread;
  private ServiceContext       svcContext;
  private List<ObjectLock>     objectLocks;
  private List<ServiceContext> recursiveSessions;

  protected WorkUnit(ServiceContext ctx)
  {
    id = NEXT_ID++;
    owningThread = Thread.currentThread();
    svcContext = ctx;
    objectLocks = new ArrayList<ObjectLock>();
    recursiveSessions = new ArrayList<ServiceContext>();

    /* notify any subscribers of the work unit event */
    pushEvent(WorkUnitState.STARTED, false);
  }

  /**
   * @return the id
   */
  public int getId()
  {
    return id;
  }

  /**
   * @return the svcContext
   */
  public ServiceContext getSvcContext()
  {
    return svcContext;
  }

  /**
   * @return the objectLocks
   */
  public List<ObjectLock> getObjectLocks()
  {
    return objectLocks;
  }

  /**
   * @return the recursiveSessions
   */
  public List<ServiceContext> getRecursiveSessions()
  {
    return recursiveSessions;
  }

  protected void end(boolean failure)
  {
    try
    {
      /* release locks */
      for (ObjectLock l : objectLocks)
        l.getCache().releaseLock(l, failure);

      /* end any recursive sessions */
      for (ServiceContext sc : recursiveSessions)
        sc.logoff(true);

      /* notify any subscribers of the work unit event */
      pushEvent(WorkUnitState.ENDED, failure);
    }
    catch (Exception e)
    {
      throw new DgRuntimeException(e, PERRMessages.PERR_INTERNAL_OPERATION);
    }
  }

  public void addLock(ObjectLock l)
  {
    if (!objectLocks.contains(l))
      objectLocks.add(l);
  }

  public void addRecursiveSession(ServiceContext sc)
  {
    if (!recursiveSessions.contains(sc))
      recursiveSessions.add(sc);
  }

  public void interruptWork()
  {
    owningThread.interrupt();
  }

  private void pushEvent(WorkUnitState st, boolean failure)
  {
    try
    {
      SubscriptionService subSvc = SubscriptionServiceFactory
          .getSubscriptionService();
      WorkUnitEvent we = new WorkUnitEvent(this, st, failure);
      subSvc.pushEvent(we);
    }
    catch (Exception e)
    {
      throw new DgRuntimeException(e, PERRMessages.PERR_INTERNAL_OPERATION);
    }
  }
}
