/**
 * IAuthorizationPolicy.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.ResourceType;

/**
 * @author sanjay
 * A policy allows certain operations on certain resources.
 */
public interface IAuthorizationPolicy
{
  /**
   * getName - returns a short name meaningful to an end user
   */
  public String getName();

  /**
   * getDescription - returns a short description meaningful to an end user
   */
  public String getDescription();

  /**
   * orgLevel - whether the policy can be enabled or disabled at the org level
   */
  public boolean orgLevel();

  /**
   * startup - called when the authorization policy mgmt service starts
   */
  public void startup();

  /**
   * enable - called when the policy is enabled
   */
  public void enable();

  /**
   * disable - called when the policy is disabled
   */
  public void disable();

  /**
   * shutdown - called when the authorization policy mgmt service is shutdown
   */
  public void shutdown();

  /**
   * resourceTypeInScope - called to check if the policy applies to the
   * specified resource type
   */
  public boolean resourceTypeInScope(ResourceType rt);

  /**
   * operationInScope - called to check if the policy applies to the specified
   * operation
   */
  public boolean operationInScope(Operation op);

  /**
   * getImplementation - returns an implementation of the policy
   */
  public IAuthorization getImplementation();
}
