/**
 * PhoneMgmtService.java
 */
package com.dombigroup.eap.server.platform.contactmgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.datamodels.Aggregate;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.Phone;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.AggregateMgmt;
import com.dombigroup.eap.server.platform.interfaces.PhoneMgmt;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public class PhoneMgmtService extends PluggableEntityMgmt<Integer, Phone>
    implements PhoneMgmt
{
  private AggregateMgmt aggrMgmt;

  public PhoneMgmtService() throws StorageException, MetadataException
  {
    super(Integer.class, Phone.class);
  }

  public void copyPo(Phone from, Phone to)
  {
    to.setNumberType(from.getNumberType());
    to.setLocation(from.getLocation());
    to.setCountryCode(from.getCountryCode());
    to.setAreaCode(from.getAreaCode());
    to.setNumber(from.getNumber());
    to.setExtension(from.getExtension());
  }

  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    super.startup(ctx);
    try
    {
      aggrMgmt = (AggregateMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_AGGREGATE);
    }
    catch (MetadataException e)
    {
      throw new ServiceException(PERRMessages.PERR_PHONE_SERVICE_STARTUP, e);
    }
  }

  public List<Phone> getPhoneList(ServiceContext ctx, int id)
      throws MetadataException
  {
    List<Phone> output = null;
    if (id != Persisted.INVALID_ID)
    {
      output = new ArrayList<Phone>();
      Aggregate aggr = aggrMgmt
          .get(ctx, id, LockMode.SHARED, ctx.getWorkUnit());
      if (aggr != null)
      {
        for (Persisted phId : aggr.getObjList())
        {
          Phone ph;
          ph = get(ctx, phId.getId(), LockMode.SHARED, ctx.getWorkUnit());
          output.add(ph);
        }
      }
    }
    return output;
  }

  public int createPhoneList(ServiceContext ctx, List<Phone> phoneList)
      throws MetadataException
  {
    int newPhoneListId = Persisted.INVALID_ID;
    if ((phoneList != null) && (phoneList.size() > 0))
    {
      Aggregate aggrPhoneList = new Aggregate();
      for (Phone ph : phoneList)
      {
        int newPhoneId = nextId(ctx);
        ph.setId(newPhoneId);
        create(ctx, newPhoneId, ph);
        Persisted p = new Persisted();
        p.setId(newPhoneId);
        aggrPhoneList.add(p);
      }
      newPhoneListId = aggrMgmt.nextId(ctx);
      aggrPhoneList.setId(newPhoneListId);
      aggrMgmt.create(ctx, newPhoneListId, aggrPhoneList);
    }

    return newPhoneListId;
  }

  public void deletePhoneList(ServiceContext ctx, int id)
      throws MetadataException
  {
    if (id != Persisted.INVALID_ID)
    {
      Aggregate phoneList = aggrMgmt.get(ctx, id, LockMode.EXCLUSIVE, ctx
          .getTransaction());
      if (phoneList != null)
      {
        for (Persisted phId : phoneList.getObjList())
          delete(ctx, phId.getId());
        aggrMgmt.delete(ctx, id);
      }
    }
  }
}
