/**
 * PGrantedRole.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;
import com.dombigroup.eap.common.datamodels.Persisted;

/**
 * @author sanjay
 * 
 */
public class PGrantedObjectPriv extends Persisted
{
  private int     granteeId;
  private int     objectId;
  private String  objectType;
  private String  operation;
  private int     grantor;
  private DayTime grantDate;

  public GrantedObjectPriv toGrantedObjectPriv(String grantorName, String granteeName)
  {
    GrantedObjectPriv gr = new GrantedObjectPriv();
    gr.copyPObjectInfo(this);
    gr.setGranteeId(granteeId);
    gr.setGrantorId(grantor);
    gr.setGrantorName(grantorName);
    gr.setGranteeName(granteeName);
    gr.setGrantDate(grantDate);
    gr.setObjectType(objectType);
    gr.setObjectId(objectId);
    gr.setOperation(operation);

    return gr;
  }

  /**
   * @return the userId
   */
  public int getGranteeId()
  {
    return granteeId;
  }

  /**
   * @param userId the userId to set
   */
  public void setGranteeId(int userId)
  {
    this.granteeId = userId;
  }

  /**
   * @return the objectId
   */
  public int getObjectId()
  {
    return objectId;
  }

  /**
   * @param objectId the objectId to set
   */
  public void setObjectId(int objectId)
  {
    this.objectId = objectId;
  }

  /**
   * @return the objectType
   */
  public String getObjectType()
  {
    return objectType;
  }

  /**
   * @param objectType the objectType to set
   */
  public void setObjectType(String objectType)
  {
    this.objectType = objectType;
  }

  /**
   * @return the operation
   */
  public String getOperation()
  {
    return operation;
  }

  /**
   * @param operationName the operation to set
   */
  public void setOperation(String operationName)
  {
    this.operation = operationName;
  }

  /**
   * @return the grantor
   */
  public int getGrantor()
  {
    return grantor;
  }

  /**
   * @param grantor the grantor to set
   */
  public void setGrantor(int grantor)
  {
    this.grantor = grantor;
  }

  /**
   * @return the grantDate
   */
  public DayTime getGrantDate()
  {
    return grantDate;
  }

  /**
   * @param grantDate the grantDate to set
   */
  public void setGrantDate(DayTime grantDate)
  {
    this.grantDate = grantDate;
  }
}
