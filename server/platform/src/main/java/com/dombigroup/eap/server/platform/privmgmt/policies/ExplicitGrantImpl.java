/**
 * SiteAdminImpl.java
 */
package com.dombigroup.eap.server.platform.privmgmt.policies;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.SecurityException;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;
import com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt;
import com.dombigroup.eap.server.platform.privmgmt.ObjectPrivMgmtServiceFactory;

/**
 * @author sanjay
 * 
 */
public class ExplicitGrantImpl implements IAuthorization
{
  private ObjectPrivMgmt objectPrivSvc;

  public ExplicitGrantImpl()
  {
    try
    {
      objectPrivSvc = ObjectPrivMgmtServiceFactory.getObjectPrivMgmtService();
    }
    catch (MetadataException e)
    {
      throw new SecurityException(PERRMessages.PERR_AUTHORIZATION_USER_SERVICE,
          e);
    }
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorization#check(com.dombigroup.eap.server.platform.context.SecurityContext, com.dombigroup.eap.common.datamodels.ResourceType, int, java.lang.Object, com.dombigroup.eap.common.datamodels.Operation)
   */
  @Override
  public boolean check(SecurityContext ctx, ResourceType resourceType,
      int resourceId, Object resource, Operation o)
  {
    boolean ret = false;
    if (resourceId == Persisted.INVALID_ID)
    {
      if ((resource != null) && (resource instanceof Persisted))
      {
        Persisted p = (Persisted) resource;
        resourceId = p.getId();
      }
    }

    if (resourceId != Persisted.INVALID_ID)
    {
      try
      {
        ret = (objectPrivSvc.getObjectPriv(ctx.getSvcCtx(), resourceId, ctx
            .getUserId(), resourceType.getName(), o.getName()) != null);
      }
      catch (MetadataException e)
      {
        throw new SecurityException(PERRMessages.PERR_AUTHORIZATION_CHECK, e,
            (resource == null) ? "NULL" : resource.toString(),
            String.valueOf(resourceId), o.getName());
      }
    }

    return ret;
  }
}
