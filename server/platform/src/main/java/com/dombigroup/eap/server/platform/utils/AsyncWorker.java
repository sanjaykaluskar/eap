/**
 * AsyncWoker.java
 */
package com.dombigroup.eap.server.platform.utils;

import com.dombigroup.eap.server.platform.context.AsyncOp;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.interfaces.IAsyncTask;

/**
 * @author skaluska
 *
 */
public abstract class AsyncWorker extends AsyncOp implements IAsyncTask
{
  protected AsyncWorker(String d, ServiceContext sc)
  {
    super(d, sc);
  }
}
