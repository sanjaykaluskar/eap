/**
 * RdbmsTransaction.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt.ChangeType;

/**
 * @author sanjay
 * 
 */
public class RdbmsTransaction extends StorageTransaction
{
  class ModifiedObject
  {
    Object     obj;
    ChangeType chg;

    /**
     * Constructor for ModifiedObject
     */
    private ModifiedObject(Object obj, ChangeType chg)
    {
      super();
      this.obj = obj;
      this.chg = chg;
    }

    /**
     * @return the obj
     */
    Object getObj()
    {
      return obj;
    }

    /**
     * @return the chg
     */
    ChangeType getChg()
    {
      return chg;
    }
  }

  private Connection           cxn;
  private List<ModifiedObject> modifiedObjects = new ArrayList<ModifiedObject>();

  /**
   * @return the cxn
   */
  public Connection getCxn()
  {
    return cxn;
  }

  /**
   * @param cxn
   *          the cxn to set
   */
  public void setCxn(Connection cxn)
  {
    this.cxn = cxn;
  }

  /**
   * @return the modifiedObjects
   */
  public List<ModifiedObject> getModifiedObjects()
  {
    return modifiedObjects;
  }

  /**
   * @param modifiedObjects
   *          the modifiedObjects to set
   */
  public void addModifiedObject(Object o, ChangeType c)
  {
    modifiedObjects.add(new ModifiedObject(o, c));
  }
  
  public void removeModifiedObject(Object o)
  {
    Object found = null;
    for (ModifiedObject m : modifiedObjects)
    {
      if (m.getObj() == o)
      {
        found = m;
        break;
      }
    }

    if (found != null)
      modifiedObjects.remove(found);
  }
}
