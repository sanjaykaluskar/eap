/**
 * SiteAdminImpl.java
 */
package com.dombigroup.eap.server.platform.privmgmt.policies;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.context.SecurityContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.SecurityException;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.pomgmt.PObjectMgmtService;
import com.dombigroup.eap.server.platform.privmgmt.PlatformRole;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public class CreatorAccessImpl implements IAuthorization
{
  private PObjectMgmtService pObjMgmt;

  public CreatorAccessImpl()
  {
    try
    {
      pObjMgmt = (PObjectMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_POBJECT);
    }
    catch (MetadataException e)
    {
      throw new SecurityException(PERRMessages.PERR_PO_SERVICE_INSTALL, e);
    }
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorization#check(com.dombigroup.eap.server.platform.context.SecurityContext, com.dombigroup.eap.common.datamodels.ResourceType, int, java.lang.Object, com.dombigroup.eap.common.datamodels.Operation)
   */
  @Override
  public boolean check(SecurityContext ctx, ResourceType resourceType,
      int resourceId, Object resource, Operation o)
  {
    boolean ret = false;
    if (ctx.hasRole(PlatformRole.EMPLOYEE))
    {
      if (o.equals(OperationType.CREATE))
      {
        ret = !resourceType.equals(PlatformResource.COMPANY) &&
            !resourceType.equals(PlatformResource.USER);
      }
      else if ((resource != null) && (resource instanceof Persisted))
      {
        Persisted p = (Persisted) resource;
        ret = ((p.getOrgId() == ctx.getOrgId()) &&
            (p.getCreatorId() == ctx.getUserId()));
      }
      else if (resourceId != Persisted.INVALID_ID)
      {
        try
        {
          Persisted p = pObjMgmt.get(ctx.getSvcCtx(), resourceId,
              LockMode.SHARED, ctx.getSvcCtx().getWorkUnit());
          ret = (p != null) &&
              (p.getOrgId() == ctx.getOrgId()) &&
              (p.getCreatorId() == ctx.getUserId());
        }
        catch (MetadataException e)
        {
          throw new SecurityException(PERRMessages.PERR_PO_ACCESS, String
              .valueOf(resourceId));
        }
      }
    }

    return ret;
  }
}
