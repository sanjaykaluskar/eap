/**
 * Subscription.java
 */
package com.dombigroup.eap.server.platform.notifications;

import com.dombigroup.eap.server.platform.context.ServiceContext;

/**
 * @author sanjay
 *
 *         Expected usage is as follows: ServiceContext svc; Subscription
 *         <BOEvent> boSub = new Subscription<BOEvent>(BOEvent.class);
 *         EventFilter<BOEvent> f = new EventFilter<BOEvent>();
 *         f.setAttr(BOFilterAttr.ORG_ID, 0); boSub.setFilter(f);
 *         boSub.setNotification(new MyNotification());
 *         svc.addSubscription(boSub);
 */
public class Subscription<EventType extends Event>
{
  private Class<? extends EventType> eventClass;

  private EventFilter<EventType> filter;

  private INotification<EventType> notification;

  private ServiceContext owningSession;

  private boolean oneTime;

  /**
   * Constructor for Subscription
   */
  public Subscription(Class<? extends EventType> eventClass)
  {
    this.eventClass = eventClass;
  }

  /**
   * @return the eventClass
   */
  public Class<? extends EventType> getEventClass()
  {
    return eventClass;
  }

  /**
   * @param eventClass
   *          the eventClass to set
   */
  public void setEventClass(Class<? extends EventType> eventClass)
  {
    this.eventClass = eventClass;
  }

  /**
   * @return the filter
   */
  public EventFilter<EventType> getFilter()
  {
    return filter;
  }

  /**
   * @param filter
   *          the filter to set
   */
  public void setFilter(EventFilter<EventType> filter)
  {
    this.filter = filter;
  }

  /**
   * @return the notification
   */
  public INotification<EventType> getNotification()
  {
    return notification;
  }

  /**
   * @param notification
   *          the notification to set
   */
  public void setNotification(INotification<EventType> notification)
  {
    this.notification = notification;
  }

  /**
   * @return the owningSession
   */
  public ServiceContext getOwningSession()
  {
    return owningSession;
  }

  /**
   * @param owningSession the owningSession to set
   */
  public void setOwningSession(ServiceContext owningSession)
  {
    this.owningSession = owningSession;
  }

  /**
   * @return the oneTime
   */
  public boolean isOneTime()
  {
    return oneTime;
  }

  /**
   * @param oneTime the oneTime to set
   */
  public void setOneTime(boolean oneTime)
  {
    this.oneTime = oneTime;
  }
}
