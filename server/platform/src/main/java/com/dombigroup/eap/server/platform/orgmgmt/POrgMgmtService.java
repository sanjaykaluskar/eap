/**
 * POrgMgmtService.java
 */
package com.dombigroup.eap.server.platform.orgmgmt;

import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;

/**
 * @author sanjay
 *
 */
public class POrgMgmtService extends PluggableEntityMgmt<Integer, POrg>
{
  public POrgMgmtService()
      throws StorageException, MetadataException
  {
    super(Integer.class, POrg.class);
  }

  @Override
  protected void copyPo(POrg from, POrg to)
  {
    to.setName(from.getName());
    to.setAddrId(from.getAddrId());
    to.setPhoneList(from.getPhoneList());
    to.setDisabledPolicyListId(from.getDisabledPolicyListId());
  }
}
