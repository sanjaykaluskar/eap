/**
 * AuthorizationPolicyInfo.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy;

/**
 * @author sanjay
 *
 */
public class AuthorizationPolicyInfo
{
  private IAuthorizationPolicy policyImpl;
  private String name;
  private String desc;
  private boolean orgLevelControl;
  
  /**
   * @return the policyImpl
   */
  public IAuthorizationPolicy getPolicyImpl()
  {
    return policyImpl;
  }
  /**
   * @param policyImpl the policyImpl to set
   */
  public void setPolicyImpl(IAuthorizationPolicy policyImpl)
  {
    this.policyImpl = policyImpl;
  }
  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }
  /**
   * @param name the name to set
   */
  public void setName(String name)
  {
    this.name = name;
  }
  /**
   * @return the desc
   */
  public String getDesc()
  {
    return desc;
  }
  /**
   * @param desc the desc to set
   */
  public void setDesc(String desc)
  {
    this.desc = desc;
  }
  /**
   * @return the orgLevelControl
   */
  public boolean isOrgLevelControl()
  {
    return orgLevelControl;
  }
  /**
   * @param orgLevelControl the orgLevelControl to set
   */
  void setOrgLevelControl(boolean orgLevelControl)
  {
    this.orgLevelControl = orgLevelControl;
  }
}
