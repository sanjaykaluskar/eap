/**
 * PObjectMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.pomgmt.PObject;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public interface PObjectMgmt extends EntityMgmt<Integer, PObject>
{
  public void populateFromPObject(ServiceContext ctx, Persisted p, LockMode m)
      throws MetadataException;

  public void createPObject(ServiceContext ctx, Persisted p)
      throws MetadataException;

  public void updatePObject(ServiceContext ctx, Persisted p)
      throws MetadataException;
}
