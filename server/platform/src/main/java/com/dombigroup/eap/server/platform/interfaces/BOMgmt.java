/**
 * BOMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;

/**
 * @author sanjay
 * 
 */
public interface BOMgmt<BOType extends Persisted>
{
  /**
   * Retrieves a list of BOs for the org. It optionally returns a read only
   * copy, which is much faster. Otherwise, it gets a read lock on each
   * object in the returned list.
   * 
   * @param ctx - service context
   * @param orgId - org id for the org
   * @param readOnlyCopy - whether a read only copy should be returned
   * @return a list of the BOs for the specified org.
   */
  public List<BOType> getBOListForOrg(ServiceContext ctx, int orgId, boolean readOnlyCopy)
      throws MetadataException;

  public List<BOType> getBOListForOrg(ServiceContext ctx, int orgId)
      throws MetadataException;

  public List<BOType> getBOPageForOrg(ServiceContext ctx, int orgId,
      int startRow, int pageSize)
          throws MetadataException;

  public BOType getBOById(ServiceContext ctx, int id)
      throws MetadataException;

  public BOType getBOForUpdate(ServiceContext ctx, int id)
      throws MetadataException;

  public int createBO(ServiceContext ctx, BOType s)
      throws MetadataException;

  public void updateBO(ServiceContext ctx, BOType s)
      throws MetadataException;

  public void deleteBO(ServiceContext ctx, int id)
      throws MetadataException;
  
  /* lists of BOs */
  public List<BOType> getBOList(ServiceContext ctx, int listId)
      throws MetadataException;

  public int createBOList(ServiceContext ctx, List<BOType> boList)
      throws MetadataException;

  public void updateBOList(ServiceContext ctx, int listId, List<BOType> columnList, boolean updateBOs)
      throws MetadataException;

  public void deleteBOList(ServiceContext ctx, int listId, boolean deleteBOs)
      throws MetadataException;
}
