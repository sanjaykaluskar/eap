/**
 * 
 */
package com.dombigroup.eap.server.platform.orgmgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.Phone;
import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.AddressMgmt;
import com.dombigroup.eap.server.platform.interfaces.CompanyMgmt;
import com.dombigroup.eap.server.platform.interfaces.PhoneMgmt;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyInfo;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyService;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyServiceFactory;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationService;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationServiceFactory;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;
import com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtFactory;
import com.dombigroup.eap.server.platform.usermgmt.UserMgmtService;

/**
 * @author sanjay
 * 
 */
class CompanyMgmtService extends PluggableService implements CompanyMgmt
{
  public static final String         SU_ORG_NAME = "SITE";
  private POrgMgmtService            porgSvc;
  private PhoneMgmt                  phoneSvc;
  private AddressMgmt                addressSvc;
  private AuthorizationService       authSvc;
  private AuthorizationPolicyService policySvc;

  public CompanyMgmtService() throws StorageException, MetadataException
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService
   * #startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    try
    {
      porgSvc = (POrgMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PORG);
      phoneSvc = (PhoneMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PHONE);
      addressSvc = (AddressMgmt) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_ADDRESS);
      authSvc = AuthorizationServiceFactory.getAuthorizationService();
      policySvc = AuthorizationPolicyServiceFactory
          .getAuthorizationPolicyService();
    }
    catch (MetadataException e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService
   * #install()
   */
  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
  }

  private Company copyFromPOrg(ServiceContext ctx, POrg from, Company to)
      throws MetadataException
  {
    if (to == null)
      to = new Company();

    to.copyPObjectInfo(from);
    to.setName(from.getName());
    to.setAddress(addressSvc.getAddress(ctx, from.getAddrId()));
    to.setPhoneNumbers(phoneSvc.getPhoneList(ctx, from.getPhoneList()));

    return to;
  }

  private POrg copyToPOrg(ServiceContext ctx, Company from, POrg to)
      throws MetadataException
  {
    int addrId = (to == null) ? Persisted.INVALID_ID : to.getAddrId();
    int phoneListId = (to == null) ? Persisted.INVALID_ID : to.getPhoneList();
    int dpl = (to == null) ? Persisted.INVALID_ID : to
        .getDisabledPolicyListId();
    boolean allocate = (to == null) || ((to != null) && !to.equalAttrs(from));

    // set new address
    Address addr = from.getAddress();
    if (addrId == Persisted.INVALID_ID)
    {
      addrId = addressSvc.createAddress(ctx, addr);
      allocate = true;
    }
    else
      addressSvc.update(ctx, addrId, addr);

    // set new phone list
    List<Phone> phList = from.getPhoneNumbers();
    if (phoneListId != Persisted.INVALID_ID)
    {
      phoneSvc.deletePhoneList(ctx, phoneListId);
    }
    int newPhoneListId;
    newPhoneListId = phoneSvc.createPhoneList(ctx, phList);
    if (phoneListId != newPhoneListId)
      allocate = true;

    if (allocate)
    {
      to = new POrg(from, addrId, newPhoneListId, dpl);
      to.copyPObjectInfo(from);
    }

    return to;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.OrgMgmt#getOrg
   * (int)
   */
  @Override
  public Company getCompany(ServiceContext ctx, int id)
      throws MetadataException
  {
    Company company = null;
    try
    {
      POrg org = porgSvc.get(ctx, id, LockMode.SHARED, ctx.getWorkUnit());
      if (org != null)
      {
        company = copyFromPOrg(ctx, org, company);
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_ORG_ACCESS, e,
          String.valueOf(id));
    }

    /*
     * There is no auth check because this method is called at connection
     * creation time before security context is initialized.
     */
    return company;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.CompanyMgmt
   * #getCompany
   * (com.dombigroup.eap.server.platform.context.ServiceContext,
   * java.lang.String)
   */
  @Override
  public Company getCompany(ServiceContext ctx, String name)
      throws MetadataException
  {
    POrg org;
    Company company = null;

    /*
     * handle SU_ORG_NAME specially due to bootstrapping
     */
    if (name.equals(SU_ORG_NAME))
    {
      company = new Company();
      company.setId(UserMgmtService.SU_ORGID);
      company.setOrgId(UserMgmtService.SU_ORGID);
      company.setName(SU_ORG_NAME);
    }
    else
    {
      StorageResultSet companyRs = null;
      try
      {
        StorageMgmt storageManager = StorageMgmtFactory.getStorageMgmtService();
        companyRs = storageManager.search(POrg.class, "(name = '" + name + "')");
        if (companyRs != null)
        {
          while ((org = (POrg) companyRs.next()) != null)
          {
            company = copyFromPOrg(ctx, org, null);
            break;
          }
          companyRs.close();
        }
      }
      catch (Exception e)
      {
        throw new MetadataException(PERRMessages.PERR_ORG_ACCESS, e,
            String.valueOf(name));
      }
      finally
      {
        try
        {
          if (companyRs != null)
            companyRs.close();
        }
        catch (StorageException e1)
        {
          throw new MetadataException(PERRMessages.PERR_ORG_ACCESS, e1,
              String.valueOf(name));
        }
      }
    }

    return company;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.CompanyMgmt
   * #getCompanyList
   * (com.dombigroup.eap.server.platform.context.ServiceContext)
   */
  @Override
  public List<Company> getCompanyList(ServiceContext ctx)
      throws MetadataException
  {
    List<Company> companyList = new ArrayList<Company>();
    StorageResultSet companyRs = null;
    POrg org;

    try
    {
      StorageMgmt storageManager = StorageMgmtFactory.getStorageMgmtService();
      companyRs = storageManager.search(POrg.class, "id >= 0");
      if (companyRs != null)
      {
        while ((org = (POrg) companyRs.next()) != null)
        {
          Company c = copyFromPOrg(ctx, org, null);
          companyList.add(c);
        }
        companyRs.close();
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_ORG_SEARCH, e);
    }
    finally
    {
      try
      {
        if (companyRs != null)
          companyRs.close();
      }
      catch (StorageException e1)
      {
        throw new MetadataException(PERRMessages.PERR_ORG_SEARCH, e1);
      }
    }

    return companyList;
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.CompanyMgmt#createCompany(com.dombigroup.eap.server.platform.context.ServiceContext, com.dombigroup.eap.common.datamodels.Company)
   */
  @Override
  public int createCompany(ServiceContext ctx, Company c)
      throws MetadataException
  {
    int id;

    if (getCompany(ctx, c.getName()) != null)
      throw new MetadataException(PERRMessages.PERR_ORG_DUPLICATE,
          c.getName());

    try
    {
      id = porgSvc.nextId(ctx);
      POrg porg = copyToPOrg(ctx, c, null);
      porg.setId(id);
      /*
       * This is a bit non-intuitive Saying that the company belongs to itself
       * means that it is accessible via the SELF & ORG-ADMIN policies.
       */
      porg.setOrgId(id);
      porgSvc.create(ctx, id, porg);
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_ORG_CREATION, e,
          c.getName());
    }
    return id;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.CompanyMgmt
   * #updateCompany
   * (com.dombigroup.eap.server.platform.context.ServiceContext,
   * com.dombigroup.eap.common.datamodels.Company)
   */
  public void updateCompany(ServiceContext ctx, Company c)
      throws MetadataException
  {
    int orgId = c.getId();
    POrg porg = porgSvc.get(ctx, orgId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    if (porg == null)
      throw new MetadataException(PERRMessages.PERR_ORG_ACCESS, c.getName());

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.COMPANY, orgId,
        c, OperationType.UPDATE);

    try
    {
      POrg newPorg = copyToPOrg(ctx, c, porg);
      if (newPorg != porg)
        porgSvc.update(ctx, orgId, newPorg);
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_ORG_DELETION, e,
          c.getName());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.OrgMgmt#deleteOrg
   * (int)
   */
  @Override
  public void deleteCompany(ServiceContext ctx, int id)
      throws MetadataException
  {
    POrg porg = porgSvc.get(ctx, id, LockMode.EXCLUSIVE, ctx.getTransaction());
    Company c = copyFromPOrg(ctx, porg, null);

    if (porg == null)
      throw new MetadataException(PERRMessages.PERR_ORG_ACCESS,
          String.valueOf(id));

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.COMPANY, id,
        c, OperationType.DELETE);

    try
    {
      addressSvc.delete(ctx, porg.getAddrId());
      phoneSvc.deletePhoneList(ctx, porg.getPhoneList());
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_ORG_DELETION, e,
          String.valueOf(id));
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.CompanyMgmt#
   * getDisabledAuthorizationPolicies
   * (com.dombigroup.eap.server.platform.context.ServiceContext,
   * int)
   */
  @Override
  public List<AuthorizationPolicyInfo> getDisabledAuthorizationPolicies(
      ServiceContext ctx, int orgId) throws MetadataException
  {
    POrg porg = porgSvc.get(ctx, orgId, LockMode.SHARED, ctx.getWorkUnit());
    Company c = copyFromPOrg(ctx, porg, null);

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.COMPANY, orgId,
        c, OperationType.READ);

    int listId = porg.getDisabledPolicyListId();
    return policySvc.getDisabledPolicyList(ctx, listId);
  }

  @Override
  public void enableAuthorizationPolicy(ServiceContext ctx, int orgId,
      String policyName) throws MetadataException
  {
    POrg porg = porgSvc.get(ctx, orgId, LockMode.SHARED, ctx.getWorkUnit());
    Company c = copyFromPOrg(ctx, porg, null);

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.COMPANY, orgId,
        c, OperationType.UPDATE);

    int listId = porg.getDisabledPolicyListId();
    policySvc.removeDisabledPolicy(ctx, listId, policyName);
  }

  @Override
  public void disableAuthorizationPolicy(ServiceContext ctx, int orgId,
      String policyName) throws MetadataException
  {
    POrg porg = porgSvc.get(ctx, orgId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    Company c = copyFromPOrg(ctx, porg, null);

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.COMPANY, orgId,
        c, OperationType.UPDATE);

    int listId = porg.getDisabledPolicyListId();
    if (listId == Persisted.INVALID_ID)
    {
      // create a new list and associate it with the user
      porg.setDisabledPolicyListId(policySvc
          .createDisabledPolicyList(ctx, policyName));
      porgSvc.update(ctx, orgId, porg);
    }
    else
      policySvc.addDisabledPolicy(ctx, listId, policyName);
  }
}
