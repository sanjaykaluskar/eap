package com.dombigroup.eap.server.platform.storagemgmt;

public enum StorageConfigParams
{
  STORAGE_TYPE("Type of storage management service", true, StorageType.stringvalues()),
  HOST("Hostname", false, null),
  PORT("Port number", false, null),
  SID("SID", false, null),
  USER("Username", false, null),
  PASSWORD("Password", false, null);
  
  private String desc;
  
  private boolean required;
  
  private String[] lov;
  
  private StorageConfigParams(String d, boolean b, String[] l)
  {
    desc = d;
    required = b;
    lov = l;
  }

  /**
   * @return the desc
   */
  public String getDesc()
  {
    return desc;
  }

  /**
   * @param desc the desc to set
   */
  public void setDesc(String desc)
  {
    this.desc = desc;
  }

  /**
   * @return the required
   */
  public boolean isRequired()
  {
    return required;
  }

  /**
   * @param required the required to set
   */
  public void setRequired(boolean required)
  {
    this.required = required;
  }

  /**
   * @return the lov
   */
  public String[] getLov()
  {
    return lov;
  }

  /**
   * @param lov the lov to set
   */
  public void setLov(String[] lov)
  {
    this.lov = lov;
  }

  public String value()
  {
    return name();
  }

  public static StorageConfigParams fromValue(String v)
  {
    return valueOf(v);
  }
  
  public static String[] valueList()
  {
    String[] returnValue = new String[StorageConfigParams.values().length];
    int i = 0;
    for (StorageConfigParams p : values())
    {
      returnValue[i++] = p.value();
    }
    return returnValue;
  }
}
