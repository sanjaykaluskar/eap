/**
 * IResourceTypeEnumerator.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.ResourceType;

/**
 * @author sanjay
 *
 */
public interface IResourceTypeEnumerator
{
  public ResourceType[] listResourceTypes();
}
