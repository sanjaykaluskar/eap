/**
 * SequenceService.java
 */
package com.dombigroup.eap.server.platform.common;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.interfaces.SequenceInterface;
import com.dombigroup.eap.server.platform.interfaces.SequenceMgmt;
import com.dombigroup.eap.server.platform.storagemgmt.CachedObject;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;
import com.dombigroup.eap.server.platform.storagemgmt.ObjectCache;

/**
 * @author sanjay
 * 
 */
public class SequenceService extends PluggableService implements SequenceMgmt
{
  private static final int              SEQUENCE_CACHE_SIZE = 100;
  private ObjectCache<String, Sequence> sequenceCache;
  private Config                        config;

  /**
   * Constructor for SequenceService
   * 
   * @throws StorageException
   */
  SequenceService() throws StorageException
  {
    sequenceCache = new ObjectCache<String, Sequence>(String.class,
        Sequence.class, SEQUENCE_CACHE_SIZE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.SequenceMgmt#create(java.
   * lang.String, int, int)
   */
  @Override
  public SequenceInterface create(ServiceContext ctx, String name, int start, int cacheSize)
      throws MetadataException
  {
    Sequence s;
    try
    {
      CachedObject<String, Sequence> co = sequenceCache.get(name,
          LockMode.EXCLUSIVE, ctx.getTransaction());
      s = co.getObj();
      if (co.isEmpty())
      {
        ctx.getTransaction().markInserted(sequenceCache, co);
        s.setName(name);
        s.setStart(start);
        s.setCache(cacheSize);
        s.setCurrVal(start - 1);
        s.setUpperBound(start + cacheSize - 1);
        s.setNeedsReplenish(false);
      }
      else
      {
        throw new MetadataException(PERRMessages.PERR_SEQUENCE_CREATION, name);
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_SEQUENCE_CREATION, e, name);
    }
    return s;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.SequenceMgmt#delete(java.
   * lang.String)
   */
  @Override
  public void delete(ServiceContext ctx, String name) throws MetadataException
  {
    try
    {
      CachedObject<String, Sequence> co = sequenceCache.get(name,
          LockMode.EXCLUSIVE, ctx.getTransaction());
      if (co.isEmpty())
      {
        throw new MetadataException(PERRMessages.PERR_SEQUENCE_DELETION, name);
      }
      else
      {
        ctx.getTransaction().markDeleted(sequenceCache, co);
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_SEQUENCE_DELETION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.SequenceMgmt#getSequence(
   * java.lang.String)
   */
  @Override
  public SequenceInterface getSequence(ServiceContext ctx, String name) throws MetadataException
  {
    try
    {
      CachedObject<String, Sequence> co = sequenceCache.get(name,
          LockMode.EXCLUSIVE, ctx.getWorkUnit());
      Sequence s = co.getObj();
      if (co.isEmpty())
      {
        s = null;
      }
      return s;
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_SEQUENCE_ACCESS, e, name);
    }
  }

  void replenish(ServiceContext svc, String name) throws MetadataException
  {
    try
    {
      ServiceContext ctx = ServiceContext.createInternalContext(svc, false);
      ctx.startTransaction();
      CachedObject<String, Sequence> co = sequenceCache.get(name,
          LockMode.EXCLUSIVE, ctx.getTransaction());
      if (co.isEmpty())
      {
        ctx.abortTransaction();
        throw new MetadataException(PERRMessages.PERR_SEQUENCE_ACCESS, name);
      }
      else
      {
        ctx.getTransaction().markUpdated(sequenceCache, co);
        co.getObj().setNeedsReplenish(true);
        ctx.commitTransaction();
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_SEQUENCE_REPLENISH, e, name);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.PluggableService#install()
   */
  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
  }

  @Override
  public Config getServiceConfig() throws DgException
  {
    return config;
  }

  @Override
  public void setServiceConfig(Config c) throws DgException
  {
    config = c;
  }
}
