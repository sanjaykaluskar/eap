/**
 * OrgHandler.java
 */
package com.dombigroup.eap.server.platform.orgmgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class POrgHandler extends RdbmsStorageHandler
{
  public class OrgResultSet extends StorageResultSet
  {
    ResultSet  rs;
    Statement  stmt;
    Connection cxn;

    /**
     * @param rs
     *          the rs to set
     */
    private void setRs(ResultSet rs)
    {
      this.rs = rs;
    }

    /**
     * @param stmt
     *          the stmt to set
     */
    private void setStmt(Statement stmt)
    {
      this.stmt = stmt;
    }

    /**
     * @param cxn
     *          the cxn to set
     */
    private void setCxn(Connection cxn)
    {
      this.cxn = cxn;
    }

    @Override
    public Object next() throws StorageException
    {
      try
      {
        POrg org = null;

        if (rs.next())
        {
          org = new POrg();
          org.setId(rs.getInt("id"));
          org.setName(rs.getString("name"));
          org.setAddrId(rs.getInt("addr_id"));
          org.setPhoneList(rs.getInt("phone_list_id"));
          org.setDisabledPolicyListId(rs.getInt("disabled_policy_list_id"));
        }

        return org;
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

    @Override
    public void close() throws StorageException
    {
      try
      {
        rs.close();
        stmt.close();
        cxn.close();
      }
      catch (SQLException e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new StorageException(PERRMessages.PERR_DB_OPERATION, e1);
        }
        throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
      }
    }

  }

  private static final String STMT_CREATE      = "create table orgs ("
                                                   + "id number primary key, "
                                                   + "name varchar2(4000), "
                                                   + "addr_id number, "
                                                   + "phone_list_id number, "
                                                   + "disabled_policy_list_id number)";

  private static final String STMT_INSERT_MGMT = "insert into "
                                                   + "orgs(id, name, addr_id, phone_list_id, disabled_policy_list_id) "
                                                   + "values(0, '" + CompanyMgmtService.SU_ORG_NAME + "', -1, -1, -1)";

  private static final String STMT_DROP        = "drop table orgs";

  private static final String STMT_SELECT      = "select "
                                                   + "id, name, addr_id, phone_list_id, disabled_policy_list_id "
                                                   + "from orgs "
                                                   + "where id = ?";

  private static final String STMT_INSERT      = "insert into "
                                                   + "orgs(id, name, addr_id, phone_list_id, disabled_policy_list_id) "
                                                   + "values(?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE      = "update orgs "
                                                   + "set name = ?, "
                                                   + "    addr_id = ?, "
                                                   + "    phone_list_id = ?, "
                                                   + "    disabled_policy_list_id = ? "
                                                   + "where (id = ?)";

  private static final String STMT_DELETE      = "delete from orgs "
                                                   + "where (id = ?)";

  private static final String STMT_SEARCH      = "select "
                                                   + "id, name, addr_id, phone_list_id, disabled_policy_list_id "
                                                   + "from orgs "
                                                   + "where (%s)";

  public POrgHandler()
  {
    super(POrg.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE);
      stmt.execute(STMT_INSERT_MGMT);
      c.commit();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    POrg org = (POrg) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setInt(1, org.getId());
      stmt.setString(2, org.getName());
      stmt.setInt(3, org.getAddrId());
      stmt.setInt(4, org.getPhoneList());
      stmt.setInt(5, org.getDisabledPolicyListId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Integer orgId = (Integer) key;
    POrg org = (POrg) value;
    boolean rowsExist = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setInt(1, orgId.intValue());
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        org.setId(rs.getInt("id"));
        org.setName(rs.getString("name"));
        org.setAddrId(rs.getInt("addr_id"));
        org.setPhoneList(rs.getInt("phone_list_id"));
        org.setDisabledPolicyListId(rs.getInt("disabled_policy_list_id"));
        rowsExist = true;
        stmt.close();
      }
    }
    catch (Exception e)
    {
      rowsExist = false;
    }
    return rowsExist;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    POrg org = (POrg) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setString(1, org.getName());
      stmt.setInt(2, org.getAddrId());
      stmt.setInt(3, org.getPhoneList());
      stmt.setInt(4, org.getDisabledPolicyListId());
      stmt.setInt(5, org.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    POrg org = (POrg) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setInt(1, org.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#search
   * (java.lang.Class, java.lang.String)
   */
  @Override
  public OrgResultSet search(StorageMgmt sm, StorageTransaction tx,
      String condition) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    OrgResultSet rs = null;

    try
    {
      Connection c = otx.getCxn();
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new OrgResultSet();
        rs.setRs(resultSet);
        rs.setStmt(stmt);
        rs.setCxn(c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
