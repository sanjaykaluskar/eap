/**
 * PGrantedObjectPrivMgmtService.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.EntityMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt;
import com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtFactory;

/**
 * @author sanjay
 * 
 */
public class PGrantedObjectPrivMgmtService extends
    PluggableEntityMgmt<Integer, PGrantedObjectPriv>
    implements EntityMgmt<Integer, PGrantedObjectPriv>
{
  public PGrantedObjectPrivMgmtService()
      throws StorageException, MetadataException
  {
    super(Integer.class, PGrantedObjectPriv.class);
  }

  @Override
  protected void copyPo(PGrantedObjectPriv from, PGrantedObjectPriv to)
  {
    to.setGranteeId(from.getGranteeId());
    to.setObjectId(from.getObjectId());
    to.setObjectType(from.getObjectType());
    to.setOperation(from.getOperation());
    to.setGrantor(from.getGrantor());
    to.setGrantDate(from.getGrantDate());
  }

  /**
   * getObjectPrivListByGrantee
   */
  public List<PGrantedObjectPriv> getObjectPrivListByGrantee(
      ServiceContext ctx, int granteeId) throws MetadataException
  {
    List<PGrantedObjectPriv> retList = null;
    StorageResultSet oprivRs = null;

    try
    {
      StorageMgmt storageManager = StorageMgmtFactory.getStorageMgmtService();
      oprivRs = storageManager.search(PGrantedObjectPriv.class, "grantee_id=" + granteeId);
      PGrantedObjectPriv opriv;

      if (oprivRs != null)
      {
        retList = new ArrayList<PGrantedObjectPriv>();
        while ((opriv = (PGrantedObjectPriv) oprivRs.next()) != null)
        {
          PGrantedObjectPriv ogp = new PGrantedObjectPriv();
          ogp.copyPObjectInfo(opriv);
          copyPo(opriv, ogp);
          retList.add(ogp);
        }
        oprivRs.close();
      }
    }
    catch (StorageException e)
    {
      throw new MetadataException(
          PERRMessages.PERR_PGRANTED_OBJECT_PRIV_SEARCH, e);
    }
    finally
    {
      try
      {
        if (oprivRs != null)
          oprivRs.close();
      }
      catch (StorageException e1)
      {
        throw new MetadataException(
            PERRMessages.PERR_PGRANTED_OBJECT_PRIV_SEARCH, e1);
      }      
    }

    return retList;
  }
}
