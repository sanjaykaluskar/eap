/**
 * CachedObject.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sanjay
 * 
 */
public class CachedObject<K, T>
{
  private static final Logger    logger      = LoggerFactory
                                                 .getLogger(CachedObject.class);
  private K                      key;
  private T                      obj;
  private ReentrantReadWriteLock lock;
  private AtomicInteger          pinCount;
  private int                    flags;
  /* flag values */
  private static final int       CV_EMPTY    = 0x0001;
  private static final int       CV_INSERTED = 0x0002;
  private static final int       CV_UPDATED  = 0x0004;
  private static final int       CV_DELETED  = 0x0008;

  CachedObject()
  {
    lock = new ReentrantReadWriteLock();
    pinCount = new AtomicInteger();
  }

  void getReadLock()
  {
    lock.readLock().lock();
    logger.debug("Acquired read lock on object, key: {}", key.toString());
  }

  void releaseReadLock()
  {
    lock.readLock().unlock();
    logger.debug("Released read lock on object, key: {}", key.toString());
  }

  void getWriteLock()
  {
    lock.writeLock().lock();
    logger.debug("Acquired write lock on object, key: {}", key.toString());
  }

  void releaseWriteLock()
  {
    lock.writeLock().unlock();
    logger.debug("Released write lock on object, key: {}", key.toString());
  }

  private boolean anyBit(int bits)
  {
    return (flags & bits) != 0;
  }

  private boolean bit(int bits)
  {
    return (flags & bits) == bits;
  }

  private void bis(int bits)
  {
    flags |= bits;
  }

  private void bic(int bits)
  {
    flags &= ~bits;
  }

  public boolean isEmpty()
  {
    return bit(CV_EMPTY);
  }

  void setEmpty()
  {
    bis(CV_EMPTY);
  }

  void clearEmpty()
  {
    bic(CV_EMPTY);
  }

  boolean isInserted()
  {
    return bit(CV_INSERTED);
  }

  void setInserted()
  {
    bis(CV_INSERTED);
  }

  void clearInserted()
  {
    bic(CV_INSERTED);
  }

  boolean isUpdated()
  {
    return bit(CV_UPDATED);
  }

  void setUpdated()
  {
    bis(CV_UPDATED);
  }

  void clearUpdated()
  {
    bic(CV_UPDATED);
  }

  boolean isDeleted()
  {
    return bit(CV_DELETED);
  }

  void setDeleted()
  {
    bis(CV_DELETED);
  }

  void clearDeleted()
  {
    bic(CV_DELETED);
  }

  boolean isPinned()
  {
    return pinCount.get() > 0;
  }

  void incrPinCount()
  {
    pinCount.incrementAndGet();
  }

  void decrPinCount()
  {
    pinCount.decrementAndGet();
  }

  boolean isDirty()
  {
    return anyBit(CV_INSERTED | CV_UPDATED | CV_DELETED);
  }

  void clearDirty()
  {
    bic(CV_INSERTED | CV_UPDATED | CV_DELETED);
  }

  /**
   * @return the key
   */
  K getKey()
  {
    return key;
  }

  /**
   * @param key
   *          the key to set
   */
  void setKey(K key)
  {
    this.key = key;
  }

  /**
   * @return the value
   */
  public T getObj()
  {
    return obj;
  }

  /**
   * @param value
   *          the value to set
   */
  void setObj(T value)
  {
    this.obj = value;
  }
}
