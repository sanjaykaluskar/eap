package com.dombigroup.eap.server.platform.interfaces;

import java.util.List;

import com.dombigroup.eap.common.datamodels.Company;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationPolicyInfo;

/**
 * @author sanjay
 * 
 */
public interface CompanyMgmt
{
  /**
   * getCompanyList
   */
  public List<Company> getCompanyList(ServiceContext ctx)
      throws MetadataException;

  /**
   * getCompany
   */
  public Company getCompany(ServiceContext ctx, int id)
      throws MetadataException;

  /**
   * getCompany
   */
  public Company getCompany(ServiceContext ctx, String name)
      throws MetadataException;

  /**
   * createCompany
   */
  public int createCompany(ServiceContext ctx, Company c)
      throws MetadataException;

  /**
   * updateCompany
   */
  public void updateCompany(ServiceContext ctx, Company c)
      throws MetadataException;

  /**
   * deleteCompany
   */
  public void deleteCompany(ServiceContext ctx, int id)
      throws MetadataException;

  /**
   * getDisabledAuthorizationPolicies
   */
  public List<AuthorizationPolicyInfo> getDisabledAuthorizationPolicies(
      ServiceContext ctx, int orgId) throws MetadataException;

  /**
   * enableAuthorizationPolicy
   */
  public void enableAuthorizationPolicy(ServiceContext ctx, int orgId,
      String policyName) throws MetadataException;

  /**
   * disableAuthorizationPolicy
   */
  public void disableAuthorizationPolicy(ServiceContext ctx, int orgId,
      String policyName) throws MetadataException;
}
