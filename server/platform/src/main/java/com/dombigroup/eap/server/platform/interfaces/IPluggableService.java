/**
 * IPluggableService.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.context.ServiceContext;

/**
 * @author sanjay
 * 
 */
public interface IPluggableService
{
  public Config getServiceConfig() throws DgException;

  public void setServiceConfig(Config c) throws DgException;

  public void startup(ServiceContext ctx) throws DgException;

  public void install(ServiceContext ctx) throws DgException;

  public void postInstall1(ServiceContext ctx) throws DgException;

  public void uninstall(ServiceContext ctx) throws DgException;

  public void shutdown(ServiceContext ctx) throws DgException;

}
