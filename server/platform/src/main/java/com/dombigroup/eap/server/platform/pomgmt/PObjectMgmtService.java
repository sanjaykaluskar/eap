/**
 * PObjectMgmt.java
 */
package com.dombigroup.eap.server.platform.pomgmt;

import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.server.platform.common.SequenceMgmtFactory;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.PObjectMgmt;
import com.dombigroup.eap.server.platform.interfaces.SequenceInterface;
import com.dombigroup.eap.server.platform.interfaces.SequenceMgmt;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;

/**
 * @author sanjay
 * 
 */
public class PObjectMgmtService extends PluggableEntityMgmt<Integer, PObject>
    implements PObjectMgmt
{
  private static final String NEXT_POID_SEQUENCE = "NEXT_POID_SEQ";
  private static final int    POID_START         = 1;
  private static final int    POID_CACHE         = 10;

  public PObjectMgmtService() throws StorageException, MetadataException
  {
    super(Integer.class, PObject.class);
  }

  @Override
  protected void copyPo(PObject from, PObject to)
  {
    to.setId(from.getId());
    to.setOrgId(from.getOrgId());
    to.setCreatorId(from.getCreatorId());
    to.setCreationTime(from.getCreationTime());
    to.setUpdateTime(from.getUpdateTime());
    to.setOprivListId(from.getOprivListId());
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.pomgmt.PluggableEntityMgmt#install()
   */
  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
    try
    {
      ServiceContext recCtx = ServiceContext.createInternalContext(ctx, true);
      SequenceMgmt seqSvc = SequenceMgmtFactory.getSequenceMgmtService();
      recCtx.startTransaction();
      seqSvc.create(recCtx, NEXT_POID_SEQUENCE, POID_START, POID_CACHE);
      recCtx.commitTransaction();
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_PO_SERVICE_INSTALL, e,
          PObjectMgmtService.class.getSimpleName());
    }
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.pomgmt.PableEntityMgmt#nextId()
   */
  @Override
  public int nextId(ServiceContext svc) throws MetadataException
  {
    try
    {
      int ret;
      SequenceMgmt seqSvc = SequenceMgmtFactory.getSequenceMgmtService();
      ServiceContext ctx = ServiceContext.createInternalContext(svc, false);
      ctx.startCall();
      SequenceInterface poSeq = seqSvc.getSequence(ctx, NEXT_POID_SEQUENCE);
      ret = poSeq.nextValue(svc);
      ctx.endCall(false);
      return ret;
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_SEQUENCE_ACCESS, e,
          NEXT_POID_SEQUENCE);
    }
  }

  @Override
  public void populateFromPObject(ServiceContext ctx, Persisted p, LockMode m)
      throws MetadataException
  {
    PObject po = get(ctx, p.getId(), m, ctx.getWorkUnit());
    if (po != null)
      p.copyPObjectInfo(po);
  }

  @Override
  public void createPObject(ServiceContext ctx, Persisted p)
      throws MetadataException
  {
    PObject po = new PObject(p);
    po.setCreatorId(ctx.getSecContext().getUserId());
    DayTime currentTime = new DayTime();
    po.setCreationTime(currentTime);
    po.setUpdateTime(currentTime);
    po.setVersion(0);
    create(ctx, po.getId(), po);
  }

  @Override
  public void updatePObject(ServiceContext ctx, Persisted p)
      throws MetadataException
  {
    PObject po = get(ctx, p.getId(), LockMode.EXCLUSIVE, ctx.getTransaction());
    po.setUpdateTime(new DayTime());
    update(ctx, p.getId(), po);
  }
}
