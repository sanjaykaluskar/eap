/**
 * INotification.java
 */
package com.dombigroup.eap.server.platform.notifications;

/**
 * @author sanjay
 *
 */
public interface INotification<EventType extends Event>
{
  public void onEvent(EventType e);
}
