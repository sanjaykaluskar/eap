/**
 * MetadataException.java
 */
package com.dombigroup.eap.server.platform.exceptions;

import com.dombigroup.eap.common.exceptions.DgRuntimeException;

/**
 * @author sanjay
 * 
 */
public class SecurityException extends DgRuntimeException
{
  /** ServiceException.java */
  private static final long serialVersionUID = 1L;

  public SecurityException(PERRMessages m)
  {
    super(m);
  }

  public SecurityException(PERRMessages m, String... arg)
  {
    super(m, arg);
  }

  public SecurityException(PERRMessages m, Exception e)
  {
    super(e, m);
  }
  
  public SecurityException(PERRMessages m, Exception e, String...strings)
  {
    super(e, m, strings);
  }
}
