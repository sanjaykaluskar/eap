/**
 * StorageMgmtService.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;

/**
 * @author sanjay
 * 
 */
abstract public class StorageMgmtService extends PluggableService implements StorageMgmt
{
  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageMgmt#beginTxn()
   */
  @Override
  public StorageTransaction beginTxn() throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageMgmt#commitTxn
   * (com.dombigroup.eap.server.platform.interfaces.StorageTransaction)
   */
  @Override
  public void commitTxn(StorageTransaction tx) throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageMgmt#abortTxn(
   * com.dombigroup.eap.server.platform.interfaces.StorageTransaction)
   */
  @Override
  public void abortTxn(StorageTransaction tx) throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.StorageMgmt#add(com.dombigroup.eap.server.platform.interfaces.StorageTransaction, java.lang.Object, com.dombigroup.eap.server.platform.interfaces.StorageMgmt.ChangeType)
   */
  @Override
  public void add(StorageTransaction tx, Object o, ChangeType c)
      throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.StorageMgmt#remove(com.dombigroup.eap.server.platform.interfaces.StorageTransaction, java.lang.Object)
   */
  @Override
  public void remove(StorageTransaction tx, Object o) throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageMgmt#load(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean load(Object key, Object value) throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.StorageMgmt#search(java.lang.Class, java.lang.String)
   */
  public StorageResultSet search(Class<?> c, String condition)
      throws StorageException
  {
    throw new StorageException(PERRMessages.PERR_UNIMPLEMENTED_FEATURE);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#getConfig()
   */
  @Override
  public Config getServiceConfig() throws DgException
  {
    return null;
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IPluggableService#setSerializedConfig(java.io.Serializable)
   */
  @Override
  public void setServiceConfig(Config c) throws DgException
  {
  }
}
