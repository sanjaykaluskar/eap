/**
 * AsyncTaskSpec.java
 */
package com.dombigroup.eap.server.platform.interfaces;

/**
 * @author skaluska
 *
 */
public class AsyncTaskSpec
{
  private int       id;

  private String    name;

  /**
   * Constructor for AsyncTaskSpec
   */
  public AsyncTaskSpec(int id, String name)
  {
    super();
    this.id = id;
    this.name = name;
  }

  /**
   * @return the id
   */
  public int getId()
  {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(int id)
  {
    this.id = id;
  }

  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name)
  {
    this.name = name;
  }
}
