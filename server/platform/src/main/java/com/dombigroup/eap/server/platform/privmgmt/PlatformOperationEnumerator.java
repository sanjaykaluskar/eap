/**
 * PlatformOperationEnumerator.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.server.platform.interfaces.IOperationEnumerator;

/**
 * @author sanjay
 *
 */
public class PlatformOperationEnumerator implements IOperationEnumerator
{
  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IOperationEnumerator#listOperations()
   */
  @Override
  public Operation[] listOperations()
  {
    return OperationType.values();
  }

}
