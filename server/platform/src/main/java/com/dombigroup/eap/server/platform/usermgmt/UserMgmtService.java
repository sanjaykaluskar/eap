/**
 * 
 */
package com.dombigroup.eap.server.platform.usermgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.datamodels.GrantedObjectPriv;
import com.dombigroup.eap.common.datamodels.GrantedRole;
import com.dombigroup.eap.common.datamodels.OperationType;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.server.platform.common.PClientMgmtService;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.IRole;
import com.dombigroup.eap.server.platform.interfaces.ObjectPrivMgmt;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.interfaces.RoleMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageResultSet;
import com.dombigroup.eap.server.platform.interfaces.UserMgmt;
import com.dombigroup.eap.server.platform.pomgmt.EntityMgmtFactory;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationService;
import com.dombigroup.eap.server.platform.privmgmt.AuthorizationServiceFactory;
import com.dombigroup.eap.server.platform.privmgmt.ObjectPrivMgmtServiceFactory;
import com.dombigroup.eap.server.platform.privmgmt.PlatformRole;
import com.dombigroup.eap.server.platform.privmgmt.RoleMgmtServiceFactory;
import com.dombigroup.eap.server.platform.storagemgmt.LockMode;
import com.dombigroup.eap.server.platform.storagemgmt.StorageMgmtFactory;

/**
 * @author sanjay
 * 
 */
public class UserMgmtService extends PluggableService
    implements UserMgmt
{
  public static final String   SU_NAME     = "su";
  public static final String   SU_PASSWORD = "su";
  public static final int      SU_USERID   = 0;
  public static final int      SU_ORGID    = 0;

  private PClientMgmtService   pclientSvc;
  private PUserMgmtService     puserSvc;
  private RoleMgmt             roleSvc;
  private ObjectPrivMgmt       objectPrivSvc;
  private AuthorizationService authSvc;

  @Override
  public void install(ServiceContext ctx)
  {
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.PluggableService#postInstall1()
   */
  @Override
  public void postInstall1(ServiceContext ctx) throws ServiceException
  {
    try
    {
      ServiceContext recCtx = ServiceContext.createInternalContext(ctx, true);

      // create SU
      recCtx.startTransaction();
      User suUser = new User();
      suUser.setUserid(SU_USERID);
      suUser.setOrgId(SU_ORGID);
      suUser.setUserName(SU_NAME);
      suUser.setPassword(SU_PASSWORD);
      int suCid = pclientSvc.createClient(recCtx, suUser);
      PUser suPuser = new PUser(SU_USERID, suCid, Persisted.INVALID_ID, suUser);
      puserSvc.create(recCtx, SU_USERID, suPuser);
      recCtx.commitTransaction();

      // grant SITE_ADMIN to SU
      recCtx.startTransaction();
      addRole(recCtx, SU_USERID, PlatformRole.SITE_ADMIN.getName());
      recCtx.commitTransaction();

      // grant ORG_ADMIN to SU
      recCtx.startTransaction();
      addRole(recCtx, SU_USERID, PlatformRole.ORG_ADMIN.getName());
      recCtx.commitTransaction();
    }
    catch (Exception e)
    {
      throw new ServiceException(PERRMessages.PERR_USER_SERVICE_INSTALL);
    }
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.PluggableService#startup()
   */
  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    try
    {
      pclientSvc = (PClientMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_CLIENT);
      puserSvc = (PUserMgmtService) EntityMgmtFactory
          .getEntityMgmtService(EntityMgmtFactory.SVC_PUSER);
      roleSvc = RoleMgmtServiceFactory.getRoleMgmtService();
      objectPrivSvc = ObjectPrivMgmtServiceFactory.getObjectPrivMgmtService();
      authSvc = AuthorizationServiceFactory.getAuthorizationService();
    }
    catch (MetadataException e)
    {
      throw new ServiceException(PERRMessages.PERR_USER_SERVICE_STARTUP, e);
    }
  }

  private User populateUser(ServiceContext ctx, PUser from)
      throws MetadataException
  {
    User to = null;
    if (from != null)
    {
      try
      {
        to = new User();
        pclientSvc.populateClient(ctx, from.getClientId(), to);
        from.populateUser(to);
      }
      catch (MetadataException e)
      {
        throw new MetadataException(PERRMessages.PERR_USER_ACCESS, e,
            String.valueOf(from.getId()));
      }
    }
    return to;
  }

  private PUser populatePUser(ServiceContext ctx, int userId, User user,
      PUser puser) throws MetadataException
  {
    try
    {
      int clientId = (puser == null) ? Persisted.INVALID_ID : puser
          .getClientId();
      if (clientId == Persisted.INVALID_ID)
        clientId = pclientSvc.createClient(ctx, user);
      else
        pclientSvc.updateClient(ctx, clientId, user);

      if ((puser == null) || ((puser != null) && !puser.equalAttrs(user)))
        puser = new PUser(userId, clientId, Persisted.INVALID_ID, user);
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_USER_ACCESS, e, String
          .valueOf(user
              .getId()));
    }

    return puser;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.UserMgmt#
   * getUserList
   * (com.dombigroup.eap.server.platform.context.ServiceContext,
   * int)
   */
  public List<User> getUserList(ServiceContext ctx, int orgId)
      throws MetadataException
  {
    List<User> userList = new ArrayList<User>();
    StorageResultSet userRs = null;
    PUser bu;

    try
    {
      StorageMgmt storageManager = StorageMgmtFactory.getStorageMgmtService();
      userRs = storageManager.search(PUser.class,
          "clientid in (select id from clients where org_id="
              + orgId
              + ")");
      if (userRs != null)
      {
        while ((bu = (PUser) userRs.next()) != null)
        {
          User u = getUser(ctx, bu.getId());
          if (authSvc.check(ctx.getSecContext(), PlatformResource.USER, u
              .getUserid(), u, OperationType.READ))
          {
            userList.add(u);
          }
        }
        userRs.close();
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_USER_LIST, e,
          String.valueOf(orgId));
    }
    finally
    {
      try
      {
        if (userRs != null)
          userRs.close();
      }
      catch (StorageException e1)
      {
        throw new MetadataException(
            PERRMessages.PERR_USER_LIST, e1, String.valueOf(orgId));
      }
    }

    return userList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.UserMgmt#
   * getUser
   * (com.dombigroup.eap.server.platform.context.ServiceContext,
   * int, java.lang.String)
   */
  public User getUser(ServiceContext ctx, int orgId, String userName)
      throws MetadataException
  {
    User found = null;
    StorageResultSet userList = null;

    /*
     * There is no auth check here right now because it is needed to check user
     * at login time.
     */
    try
    {
      StorageMgmt storageManager = StorageMgmtFactory.getStorageMgmtService();
      userList = storageManager.search(PUser.class,
          "(username='" + userName
              + "') and clientid in (select id from clients where org_id="
              + orgId
              + ")");
      if (userList != null)
      {
        PUser bu = (PUser) userList.next();
        if (bu != null)
        {
          User b = populateUser(ctx, bu);
          found = b;
        }
        userList.close();
      }
    }
    catch (Exception e)
    {
      throw new MetadataException(PERRMessages.PERR_USER_ACCESS, e, userName);
    }
    finally
    {
      try
      {
        if (userList != null)
          userList.close();
      }
      catch (StorageException e1)
      {
        throw new MetadataException(PERRMessages.PERR_USER_ACCESS, e1, userName);
      }
    }

    return found;
  }

  @Override
  public User getUser(ServiceContext ctx, int id) throws MetadataException
  {
    /*
     * There is no auth check here right now because it is needed to initialize
     * roles at login time (when there are roles granted by another user, e.g.,
     * SU).
     */
    User user = null;
    try
    {
      PUser pb = puserSvc.get(ctx, id, LockMode.SHARED, ctx.getWorkUnit());
      if (pb != null)
        user = populateUser(ctx, pb);
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_USER_ACCESS, e,
          String.valueOf(id));
    }

    return user;
  }

  public int createUser(ServiceContext ctx, User u) throws MetadataException
  {
    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, 0, u,
        OperationType.CREATE);
    if (getUser(ctx, u.getOrgId(), u.getUserName()) != null)
      throw new MetadataException(PERRMessages.PERR_USER_DUPLICATE,
          u.getUserName());

    int userId = puserSvc.nextId(ctx);
    PUser puserObj = populatePUser(ctx, userId, u, null);
    puserSvc.create(ctx, userId, puserObj);
    return userId;
  }

  @Override
  public void updateUser(ServiceContext ctx, User user)
      throws MetadataException
  {
    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, user
        .getId(), user, OperationType.UPDATE);

    try
    {
      PUser pb = puserSvc.get(ctx, user.getId(), LockMode.EXCLUSIVE, ctx
          .getTransaction());
      if (pb.getVersion() != user.getVersion())
        throw new MetadataException(PERRMessages.PERR_STALE_UPDATE,
            String.valueOf(user.getId()));
      PUser newPb = populatePUser(ctx, user.getId(), user, pb);
      puserSvc.update(ctx, pb.getId(), newPb);
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_USER_UPDATE, e, user
          .getFirstName(),
          user.getLastName());
    }
  }

  @Override
  public void updateUser(ServiceContext ctx, int userId, User user)
      throws MetadataException
  {
    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, user
        .getId(), user, OperationType.UPDATE);

    try
    {
      PUser pb = puserSvc.get(ctx, userId, LockMode.EXCLUSIVE,
          ctx.getTransaction());
      PUser newPb = populatePUser(ctx, userId, user, pb);
      puserSvc.update(ctx, pb.getId(), newPb);
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_USER_UPDATE, e, user
          .getFirstName(),
          user.getLastName());
    }
  }

  @Override
  public void deleteUser(ServiceContext ctx, User user)
      throws MetadataException
  {
    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, user
        .getId(), user, OperationType.DELETE);

    try
    {
      PUser pb = puserSvc.get(ctx, user.getId(), LockMode.EXCLUSIVE, ctx
          .getTransaction());
      pclientSvc.delete(ctx, pb.getClientId());
      puserSvc.delete(ctx, pb.getId());
    }
    catch (MetadataException e)
    {
      throw new MetadataException(PERRMessages.PERR_USER_DELETION, e,
          user.getFirstName(), user.getLastName());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.UserMgmt#
   * getRoles
   * (com.dombigroup.eap.server.platform.context.ServiceContext,
   * int)
   */
  @Override
  public List<GrantedRole> getRoles(ServiceContext ctx, int userId)
      throws MetadataException
  {
    PUser puser = puserSvc.get(ctx, userId, LockMode.SHARED, ctx.getWorkUnit());
    User user = (puser == null) ? null : populateUser(ctx, puser);

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, userId,
        user, OperationType.READ);

    List<GrantedRole> ret = null;
    if (puser != null)
      ret = roleSvc.getRoleList(ctx, puser.getRoleListId());
    return ret;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.UserMgmt#
   * addRole
   * (com.dombigroup.eap.server.platform.context.ServiceContext,
   * int, java.lang.String)
   */
  @Override
  public void addRole(ServiceContext ctx, int userId, String roleName)
      throws MetadataException
  {
    PUser puser = puserSvc.get(ctx, userId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    User user = (puser == null) ? null : populateUser(ctx, puser);

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, userId,
        user, OperationType.UPDATE);

    int listId = puser.getRoleListId();
    if (listId == Persisted.INVALID_ID)
    {
      // create a new list and associate it with the user
      puser.setRoleListId(roleSvc.createRoleList(ctx, userId, roleName));
      puserSvc.update(ctx, userId, puser);
    }
    else
      roleSvc.addRole(ctx, puser.getRoleListId(), userId, roleName);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.UserMgmt#removeRole(com.dombigroup.eap.server.platform.context.ServiceContext, int, java.lang.String)
   */
  @Override
  public void removeRole(ServiceContext ctx, int userId, String roleName)
      throws MetadataException
  {
    PUser puser = puserSvc.get(ctx, userId, LockMode.EXCLUSIVE, ctx
        .getTransaction());
    User user = (puser == null) ? null : populateUser(ctx, puser);

    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, userId,
        user, OperationType.UPDATE);

    int listId = puser.getRoleListId();
    roleSvc.removeRole(ctx, listId, roleName);
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.UserMgmt#getGrantableRoles(com.dombigroup.eap.server.platform.context.ServiceContext)
   */
  public List<IRole> getGrantableRoles(ServiceContext ctx)
      throws MetadataException
  {
    List<IRole> ret = new ArrayList<IRole>();

    if (ctx.getSecContext().hasRole(PlatformRole.SITE_ADMIN) ||
        ctx.getSecContext().hasRole(PlatformRole.ORG_ADMIN))
    {
      for (IRole r : roleSvc.getAvailableRoles(ctx))
      {
        if (r.getName().contains("ADMIN"))
        {
          if (ctx.getSecContext().hasRole(r))
            ret.add(r);
        }
        else
          ret.add(r);
      }
    }
    return ret;
  }

  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.UserMgmt#getObjectPrivList(com.dombigroup.eap.server.platform.context.ServiceContext, int)
   */
  @Override
  public List<GrantedObjectPriv> getObjectPrivList(ServiceContext ctx,
      int userId) throws MetadataException
  {
    PUser puser = puserSvc.get(ctx, userId, LockMode.SHARED, ctx.getWorkUnit());
    User user = (puser == null) ? null : populateUser(ctx, puser);
    authSvc.assertAccess(ctx.getSecContext(), PlatformResource.USER, userId,
        user, OperationType.READ);

    List<GrantedObjectPriv> ret = null;
    if (puser != null)
      ret = objectPrivSvc.getObjectPrivListByGrantee(ctx, userId);
    return ret;
  }
}
