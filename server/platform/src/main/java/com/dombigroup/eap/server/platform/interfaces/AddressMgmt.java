/**
 * AddressMgmt.java
 */
package com.dombigroup.eap.server.platform.interfaces;

import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.MetadataException;

/**
 * @author sanjay
 * 
 */
public interface AddressMgmt extends EntityMgmt<Integer, Address>
{
  public Address getAddress(ServiceContext ctx, int addrId)
      throws MetadataException;

  public int createAddress(ServiceContext ctx, Address addr)
      throws MetadataException;
}
