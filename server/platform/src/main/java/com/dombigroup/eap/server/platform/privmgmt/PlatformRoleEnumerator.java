/**
 * PlatformRoleEnumerator.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.server.platform.interfaces.IRole;
import com.dombigroup.eap.server.platform.interfaces.IRoleEnumerator;

/**
 * @author sanjay
 *
 */
public class PlatformRoleEnumerator implements IRoleEnumerator
{
  /* (non-Javadoc)
   * @see com.dombigroup.eap.server.platform.interfaces.IRoleEnumerator#listRoles()
   */
  @Override
  public IRole[] listRoles()
  {
    return PlatformRole.values();
  }
}
