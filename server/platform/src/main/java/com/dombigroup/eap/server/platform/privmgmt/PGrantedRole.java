/**
 * PGrantedRole.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import com.dombigroup.eap.common.datamodels.DayTime;
import com.dombigroup.eap.common.datamodels.GrantedRole;
import com.dombigroup.eap.common.datamodels.Persisted;

/**
 * @author sanjay
 * 
 */
public class PGrantedRole extends Persisted
{
  private int     userId;
  private String  roleName;
  private int     grantor;
  private DayTime grantDate;
  private boolean enabled;

  public GrantedRole toGrantedRole(String grantorName, String granteeName,
      String desc)
  {
    GrantedRole gr = new GrantedRole();
    gr.copyPObjectInfo(this);
    gr.setGranteeId(userId);
    gr.setGrantorId(grantor);
    gr.setRoleName(roleName);
    gr.setRoleDesc(desc);
    gr.setGrantDate(grantDate);
    gr.setEnabled(enabled);
    gr.setGrantorName(grantorName);
    gr.setGranteeName(granteeName);

    return gr;
  }

  /**
   * @return the userId
   */
  public int getUserId()
  {
    return userId;
  }

  /**
   * @param userId
   *          the userId to set
   */
  public void setUserId(int userId)
  {
    this.userId = userId;
  }

  /**
   * @return the name
   */
  public String getRoleName()
  {
    return roleName;
  }

  /**
   * @param name
   *          the name to set
   */
  public void setRoleName(String name)
  {
    this.roleName = name;
  }

  /**
   * @return the grantor
   */
  public int getGrantor()
  {
    return grantor;
  }

  /**
   * @param grantor
   *          the grantor to set
   */
  public void setGrantor(int grantor)
  {
    this.grantor = grantor;
  }

  /**
   * @return the grantDate
   */
  public DayTime getGrantDate()
  {
    return grantDate;
  }

  /**
   * @param grantDate
   *          the grantDate to set
   */
  public void setGrantDate(DayTime grantDate)
  {
    this.grantDate = grantDate;
  }

  /**
   * @return the enabled
   */
  public boolean isEnabled()
  {
    return enabled;
  }

  /**
   * @param enabled
   *          the enabled to set
   */
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }
}
