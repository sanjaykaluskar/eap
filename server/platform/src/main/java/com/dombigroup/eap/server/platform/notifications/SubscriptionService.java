/**
 * SubscriptionService.java
 */
package com.dombigroup.eap.server.platform.notifications;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import com.dombigroup.eap.common.exceptions.DgRuntimeException;
import com.dombigroup.eap.server.platform.context.ServiceContext;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.PluggableService;
import com.dombigroup.eap.server.platform.mgmt.PlatformState;
import com.dombigroup.eap.server.platform.mgmt.PlatformStatus;

/**
 * @author sanjay
 * 
 */
@SuppressWarnings("rawtypes")
public class SubscriptionService extends PluggableService
{
  private static Map<Class<?>, IEventService>      eventServices;

  private static Map<Class<?>, List<Subscription>> subscriptions;

  private static ServiceLoader<IEventService>      eventServiceLoader = ServiceLoader
                                                                          .load(IEventService.class);

  @Override
  public void startup(ServiceContext ctx) throws ServiceException
  {
    /* load event services */
    eventServices = new HashMap<Class<?>, IEventService>();
    for (IEventService evs : eventServiceLoader)
    {
      eventServices.put(evs.getEventType(), evs);
    }

    /* initialize subscriptions */
    subscriptions = new HashMap<Class<?>, List<Subscription>>();
  }

  @Override
  public void install(ServiceContext ctx) throws ServiceException
  {
  }

  synchronized public void addSubscription(Subscription s)
  {
    Class<?> eventClass = s.getEventClass();
    IEventService eventSvc = eventServices.get(eventClass);
    if (eventSvc == null)
    {
      throw new DgRuntimeException(PERRMessages.PERR_BAD_EVENT_TYPE,
          eventClass.getName());
    }

    List<Subscription> subscriptionList = subscriptions.get(eventClass);
    if (subscriptionList == null)
    {
      subscriptionList = new ArrayList<Subscription>();
      subscriptions.put(eventClass, subscriptionList);
    }
    subscriptionList.add(s);
  }

  synchronized public void removeSubscription(Subscription s)
  {
    Class<?> eventClass = s.getEventClass();

    List<Subscription> subscriptionList = subscriptions.get(eventClass);
    if (subscriptionList == null)
    {
      throw new DgRuntimeException(PERRMessages.PERR_NON_EXISTENT_SUBSCRIPTION,
          eventClass.getName());
    }
    subscriptionList.remove(s);
  }

  @SuppressWarnings("unchecked")
  public void pushEvent(Event e)
  {
    /* no notifications until the server is up */
    if (!PlatformStatus.getInstance().getState().equals(PlatformState.READY))
      return;

    Class<?> eventClass = e.getClass();
    IEventService eventSvc = eventServices.get(eventClass);
    List<Subscription> subscriptionList = subscriptions.get(eventClass);
    List<Subscription> oneTimeList = new ArrayList<Subscription>();

    /*
     * The problem here is that the subscription list can mutate as we process
     * the subscriptions. It will be simpler if this method could be
     * synchronized but that makes it very hard for the notifications to do
     * arbitrary things (including calling this method). Hence, we undertake the
     * perilous journey of walking the mutating list until we reach the end.
     * Since new subscriptions get added to the end of the list, we might notify
     * them. On the other hand, we may miss some subscriptions if some one-time
     * subscriptions get removed from the list (since our pointer will point
     * ahead of the next subscription).
     */
    if (subscriptionList != null)
    {
      int subNo = 0;
      while (subNo < subscriptionList.size())
      {
        Subscription s = subscriptionList.get(subNo);
        EventFilter filter = s.getFilter();

        if ((filter == null) ||
            ((filter != null) && eventSvc.evaluateFilter(filter, e)))
        {
          s.getNotification().onEvent(e);
          if (s.isOneTime())
          {
            oneTimeList.add(s);
          }
        }

        subNo++;
      }

      /* remove any one-time subscriptions that fired */
      if (oneTimeList.size() > 0)
      {
        for (Subscription s : oneTimeList)
        {
          try
          {
            s.getOwningSession().removeSubscription(s);
          }
          catch (Exception e1)
          {
            throw new DgRuntimeException(e1,
                PERRMessages.PERR_INTERNAL_OPERATION);
          }
        }
      }
    }
  }
}
