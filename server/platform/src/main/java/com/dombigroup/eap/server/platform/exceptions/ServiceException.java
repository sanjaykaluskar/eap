/**
 * MetadataException.java
 */
package com.dombigroup.eap.server.platform.exceptions;

import com.dombigroup.eap.common.exceptions.DgException;

/**
 * @author sanjay
 * 
 */
public class ServiceException extends DgException
{
  /** ServiceException.java */
  private static final long serialVersionUID = 1L;

  public ServiceException(PERRMessages m)
  {
    super(m);
  }

  public ServiceException(PERRMessages m, String... arg)
  {
    super(m, arg);
  }

  public ServiceException(PERRMessages m, Exception e)
  {
    super(e, m);
  }
  
  public ServiceException(PERRMessages m, Exception e, String...strings)
  {
    super(e, m, strings);
  }
}
