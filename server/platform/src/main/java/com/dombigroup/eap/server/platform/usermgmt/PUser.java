/**
 * PUser.java
 */
package com.dombigroup.eap.server.platform.usermgmt;

import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.User;
import com.dombigroup.eap.common.utils.ExpressionUtils;

/**
 * @author sanjay
 * 
 */
public class PUser extends Persisted
{
  private int    clientId;

  private String userName;

  private String password;

  private int    roleListId;

  public PUser()
  {
  }

  public PUser(int userId, int client_id, int role_list_id, User u)
  {
    copyPObjectInfo(u);
    setId(userId);
    clientId = client_id;
    userName = u.getUserName();
    password = u.getPassword();
    roleListId = role_list_id;
  }

  public void populateUser(User u)
  {
    u.copyPObjectInfo(this);
    u.setUserid(getId());
    u.setUserName(userName);
    u.setPassword(password);
    u.setRoleListId(roleListId);
  }

  public boolean equalAttrs(User u)
  {
    return equalPObject(u)
        && ExpressionUtils.safeEqual(u.getUserName(), userName)
        && ExpressionUtils.safeEqual(u.getPassword(), password)
        && (u.getRoleListId() == roleListId);
  }

  /**
   * @return the clientId
   */
  public int getClientId()
  {
    return clientId;
  }

  /**
   * @param clientId
   *          the clientId to set
   */
  public void setClientId(int clientId)
  {
    this.clientId = clientId;
  }

  /**
   * @return the userName
   */
  public String getUserName()
  {
    return userName;
  }

  /**
   * @param userName
   *          the userName to set
   */
  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  /**
   * @return the password
   */
  public String getPassword()
  {
    return password;
  }

  /**
   * @param password
   *          the password to set
   */
  public void setPassword(String password)
  {
    this.password = password;
  }

  /**
   * @return the roleListId
   */
  public int getRoleListId()
  {
    return roleListId;
  }

  /**
   * @param roleListId
   *          the roleListId to set
   */
  public void setRoleListId(int roleListId)
  {
    this.roleListId = roleListId;
  }
}
