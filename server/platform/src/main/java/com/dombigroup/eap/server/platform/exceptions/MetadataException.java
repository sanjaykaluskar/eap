/**
 * MetadataException.java
 */
package com.dombigroup.eap.server.platform.exceptions;

import com.dombigroup.eap.common.exceptions.DgException;

/**
 * @author sanjay
 * 
 */
public class MetadataException extends DgException
{
  /** MetadataException.java */
  private static final long serialVersionUID = 1L;

  public MetadataException(PERRMessages m)
  {
    super(m);
  }

  public MetadataException(PERRMessages m, String... arg)
  {
    super(m, arg);
  }

  public MetadataException(PERRMessages m, Exception e)
  {
    super(e, m);
  }
  
  public MetadataException(PERRMessages m, Exception e, String...strings)
  {
    super(e, m, strings);
  }
}
