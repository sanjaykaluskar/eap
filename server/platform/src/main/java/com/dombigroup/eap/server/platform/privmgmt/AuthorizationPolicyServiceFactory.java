/**
 * AuthorizationPolicyServiceFactory.java
 */
package com.dombigroup.eap.server.platform.privmgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamInfo;

/**
 * @author sanjay
 * 
 */
public class AuthorizationPolicyServiceFactory implements
    IPluggableServiceFactory
{
  private static final String               SVC_NAME = "AUTHORIZATION POLICY SERVICE";
  private static Config                     config;
  private static AuthorizationPolicyService svc;

  public static synchronized AuthorizationPolicyService getAuthorizationPolicyService()
      throws ServiceException
  {
    try
    {
      if (svc == null)
      {
        svc = new AuthorizationPolicyService();
        svc.setServiceConfig(config);
      }
    }
    catch (DgException e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }

    return svc;
  }

  @Override
  public List<String> getServiceNames()
  {
    List<String> ret = new ArrayList<String>();
    ret.add(SVC_NAME);
    return ret;
  }

  @Override
  public Config getServiceConfig(String svcName)
  {
    return AuthorizationPolicyServiceFactory.config;
  }

  @Override
  public void setServiceConfig(String svcName, Config c)
  {
    AuthorizationPolicyServiceFactory.config = c;
  }

  @Override
  public IPluggableService getService(String svcName) throws ServiceException
  {
    return AuthorizationPolicyServiceFactory.getAuthorizationPolicyService();
  }

  @Override
  public List<ParamInfo> getConfigParams(String svcName)
  {
    AuthorizationPolicyConfigParams[] l = AuthorizationPolicyConfigParams
        .values();
    List<ParamInfo> piList = new ArrayList<ParamInfo>();

    for (AuthorizationPolicyConfigParams p : l)
    {
      ParamInfo pi = new ParamInfo();
      pi.setName(p.name());
      pi.setDescription(p.getDesc());
      pi.setRequired(p.isRequired());
      pi.setParamLov(p.getLov());
      piList.add(pi);
    }
    return piList;
  }
}
