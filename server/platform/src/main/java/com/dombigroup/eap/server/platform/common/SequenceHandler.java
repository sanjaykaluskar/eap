/**
 * SequenceHandler.java
 */
package com.dombigroup.eap.server.platform.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.StorageMgmt;
import com.dombigroup.eap.server.platform.interfaces.StorageTransaction;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsStorageHandler;
import com.dombigroup.eap.server.platform.storagemgmt.RdbmsTransaction;

/**
 * @author sanjay
 * 
 */
public class SequenceHandler extends RdbmsStorageHandler
{
  private static final String STMT_CREATE_TABLE      = "create table sequences ("
                                                         + "name varchar2(4000) primary key, "
                                                         + "startValue number, "
                                                         + "cache number, "
                                                         + "value number)";

  private static final String STMT_DROP              = "drop table sequences";

  private static final String STMT_SELECT            = "select "
                                                         + "name, startValue, cache, value "
                                                         + "from sequences "
                                                         + "where name = ?";

  private static final String STMT_INSERT            = "insert into "
                                                         + "sequences(name, startValue, cache, value) "
                                                         + "values(?, ?, ?, ?)";

  private static final String STMT_SELECT_FOR_UPDATE = "select "
                                                         + "name, startValue, cache, value "
                                                         + "from sequences "
                                                         + "where (name = ?) "
                                                         + "for update";
  private static final String STMT_UPDATE            = "update sequences "
                                                         + "set value = value + cache "
                                                         + "where (name = ?)";

  private static final String STMT_DELETE            = "delete from sequences "
                                                         + "where (name = ?)";

  public SequenceHandler()
  {
    super(Sequence.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * createContainer()
   */
  public void createContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE_TABLE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.StorageHandler#
   * deleteContainer()
   */
  public void deleteContainer(StorageMgmt sm, StorageTransaction tx)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;

    try
    {
      Connection c = otx.getCxn();
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#create
   * (java.lang.Object)
   */
  @Override
  public void create(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Sequence seq = (Sequence) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setString(1, seq.getName());
      stmt.setInt(2, seq.getStart());
      stmt.setInt(3, seq.getCache());
      stmt.setInt(4, seq.getUpperBound());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#read(java
   * .lang.Class, java.lang.Object)
   */
  @Override
  public boolean read(StorageMgmt sm, StorageTransaction tx, Object key,
      Object value) throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    String name = (String) key;
    Sequence seq = (Sequence) value;
    boolean rowExists = false;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setString(1, name);
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        seq.setName(rs.getString("name"));
        seq.setStart(rs.getInt("startValue"));
        seq.setCache(rs.getInt("cache"));
        seq.setCurrVal(rs.getInt("value"));
        seq.setUpperBound(seq.getCurrVal());
        seq.setNeedsReplenish(true);
        rowExists = true;
        stmt.close();
      }
    }
    catch (Exception e)
    {
      rowExists = false;
    }
    return rowExists;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#update
   * (java.lang.Object)
   */
  @Override
  public void update(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Sequence seq = (Sequence) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT_FOR_UPDATE);
      stmt.setString(1, seq.getName());
      ResultSet rs = stmt.executeQuery();
      if (rs == null)
        throw new StorageException(PERRMessages.PERR_DB_MISSING_OBJECT,
            seq.getName(), Sequence.class.getSimpleName());
      if (rs.next())
      {
        seq.setCurrVal(rs.getInt("value"));
        seq.setUpperBound(seq.getCurrVal() + seq.getCache());
        seq.setNeedsReplenish(false);
        stmt.close();
        stmt = c.prepareStatement(STMT_UPDATE);
        stmt.setString(1, seq.getName());
        stmt.executeUpdate();
        stmt.close();
      }
      else
        throw new StorageException(PERRMessages.PERR_DB_MISSING_OBJECT,
            seq.getName(), Sequence.class.getSimpleName());

    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.StorageHandler#delete
   * (java.lang.Object)
   */
  @Override
  public void delete(StorageMgmt sm, StorageTransaction tx, Object o)
      throws StorageException
  {
    RdbmsTransaction otx = (RdbmsTransaction) tx;
    Sequence seq = (Sequence) o;

    try
    {
      Connection c = otx.getCxn();
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setString(1, seq.getName());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new StorageException(PERRMessages.PERR_DB_OPERATION, e);
    }
  }
}
