/**
 * SiteAdmin.java
 */
package com.dombigroup.eap.server.platform.privmgmt.policies;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;
import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy;

/**
 * @author sanjay
 * 
 */
public class CreatorAccess implements IAuthorizationPolicy
{
  private static final String NAME = "CREATOR ACCESS";
  private static final String DESC =
    "Employee can create objects within the company (other than users) "
    + "and has complete access (e.g., read, update or delete) "
    + "to objects created by him/her";

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#getName
   * ()
   */
  @Override
  public String getName()
  {
    return NAME;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#
   * getDescription()
   */
  @Override
  public String getDescription()
  {
    return DESC;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#orgLevel
   * ()
   */
  @Override
  public boolean orgLevel()
  {
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#startup
   * ()
   */
  @Override
  public void startup()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#enable()
   */
  @Override
  public void enable()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#disable
   * ()
   */
  @Override
  public void disable()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#shutdown
   * ()
   */
  @Override
  public void shutdown()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#
   * getImplementation()
   */
  @Override
  public IAuthorization getImplementation()
  {
    return new CreatorAccessImpl();
  }

  @Override
  public boolean resourceTypeInScope(ResourceType rt)
  {
    return true;
  }

  @Override
  public boolean operationInScope(Operation op)
  {
    return true;
  }
}
