/**
 * PersonalUser.java
 */
package com.dombigroup.eap.server.platform.privmgmt.policies;

import com.dombigroup.eap.common.datamodels.Operation;
import com.dombigroup.eap.common.datamodels.PlatformResource;
import com.dombigroup.eap.common.datamodels.ResourceType;
import com.dombigroup.eap.server.platform.interfaces.IAuthorization;
import com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy;

/**
 * @author sanjay
 * 
 */
public class PersonalUser implements IAuthorizationPolicy
{
  private static final String NAME = "PERSONAL USER INFO";
  private static final String DESC =
                                       "A user has complete access to his/her personal user information. "
                                           + "A user also can read basic information about his/her company.";

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#getName
   * ()
   */
  @Override
  public String getName()
  {
    return NAME;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#
   * getDescription()
   */
  @Override
  public String getDescription()
  {
    return DESC;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#orgLevel
   * ()
   */
  @Override
  public boolean orgLevel()
  {
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#startup
   * ()
   */
  @Override
  public void startup()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#enable()
   */
  @Override
  public void enable()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#disable
   * ()
   */
  @Override
  public void disable()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#shutdown
   * ()
   */
  @Override
  public void shutdown()
  {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.dombigroup.eap.server.platform.interfaces.IAuthorizationPolicy#
   * getImplementation()
   */
  @Override
  public IAuthorization getImplementation()
  {
    return new PersonalUserImpl();
  }

  @Override
  public boolean resourceTypeInScope(ResourceType rt)
  {
    return (rt.equals(PlatformResource.USER) || rt
        .equals(PlatformResource.COMPANY));
  }

  @Override
  public boolean operationInScope(Operation op)
  {
    return true;
  }
}
