/**
 * StorageMgmtFactory.java
 */
package com.dombigroup.eap.server.platform.storagemgmt;

import java.util.ArrayList;
import java.util.List;

import com.dombigroup.eap.common.exceptions.DgException;
import com.dombigroup.eap.server.platform.exceptions.PERRMessages;
import com.dombigroup.eap.server.platform.exceptions.ServiceException;
import com.dombigroup.eap.server.platform.exceptions.StorageException;
import com.dombigroup.eap.server.platform.interfaces.Config;
import com.dombigroup.eap.server.platform.interfaces.IPluggableService;
import com.dombigroup.eap.server.platform.interfaces.IPluggableServiceFactory;
import com.dombigroup.eap.server.platform.interfaces.ParamInfo;

/**
 * @author sanjay
 * 
 */
public class StorageMgmtFactory implements IPluggableServiceFactory
{
  private static final String       SVC_NAME = "STORAGE MANAGEMENT";
  private static Config             config;
  private static StorageMgmtService svc;

  public static synchronized StorageMgmtService getStorageMgmtService()
      throws StorageException
  {
    try
    {
      if (svc == null)
      {
        switch (StorageType.fromValue(config
            .getParamValue(StorageConfigParams.STORAGE_TYPE.value())))
        {
        case ORACLE:
          svc = new RdbmsStorageMgmtService();
          svc.setServiceConfig(config);
          break;
        default:
          break;
        }
      }
    }
    catch (DgException e)
    {
      throw new StorageException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }

    return svc;
  }

  @Override
  public List<String> getServiceNames()
  {
    List<String> ret = new ArrayList<String>();
    ret.add(SVC_NAME);
    return ret;
  }

  @Override
  public Config getServiceConfig(String svcName)
  {
    return StorageMgmtFactory.config;
  }

  @Override
  public void setServiceConfig(String svcName, Config c)
  {
    StorageMgmtFactory.config = c;
  }

  @Override
  public IPluggableService getService(String svcName) throws ServiceException
  {
    try
    {
      return StorageMgmtFactory.getStorageMgmtService();
    }
    catch (StorageException e)
    {
      throw new ServiceException(PERRMessages.PERR_SERVICE_INSTANTIATION, e);
    }
  }

  @Override
  public List<ParamInfo> getConfigParams(String svcName)
  {
    StorageConfigParams[] l = StorageConfigParams.values();
    List<ParamInfo> piList = new ArrayList<ParamInfo>();

    for (StorageConfigParams p : l)
    {
      ParamInfo pi = new ParamInfo();
      pi.setName(p.name());
      pi.setDescription(p.getDesc());
      pi.setRequired(p.isRequired());
      pi.setParamLov(p.getLov());
      piList.add(pi);
    }
    return piList;
  }
}
