set serveroutput on

declare
  cursor get_tables is select table_name from user_tables;
  rec get_tables%rowtype;
begin
  open get_tables;
  loop
    fetch get_tables into rec;
    exit when get_tables%NOTFOUND;
    execute immediate 'drop table ' || rec.table_name || ' cascade constraints';
    dbms_output.put_line('Dropped table ' || rec.table_name);
  end loop;
end;
/
