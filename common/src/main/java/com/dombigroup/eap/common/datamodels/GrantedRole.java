/**
 * GrantedRole.java
 */
package com.dombigroup.eap.common.datamodels;

/**
 * @author sanjay
 *
 */
public class GrantedRole extends Grant
{
  private String roleName;
  
  private String roleDesc;
  
  private boolean enabled;

  /**
   * @return the roleName
   */
  public String getRoleName()
  {
    return roleName;
  }

  /**
   * @param roleName the roleName to set
   */
  public void setRoleName(String roleName)
  {
    this.roleName = roleName;
  }

  /**
   * @return the roleDesc
   */
  public String getRoleDesc()
  {
    return roleDesc;
  }

  /**
   * @param roleDesc the roleDesc to set
   */
  public void setRoleDesc(String roleDesc)
  {
    this.roleDesc = roleDesc;
  }

  /**
   * @return the enabled
   */
  public boolean isEnabled()
  {
    return enabled;
  }

  /**
   * @param enabled the enabled to set
   */
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }
}
