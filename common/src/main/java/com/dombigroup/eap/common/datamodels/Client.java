package com.dombigroup.eap.common.datamodels;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.dombigroup.eap.common.datamodels.Address;
import com.dombigroup.eap.common.datamodels.Persisted;
import com.dombigroup.eap.common.datamodels.Phone;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "client")
public class Client extends Persisted
{
  private PrefixType        prefix;
  private String            firstName;
  private String            middleName;
  private String            lastName;
  private Address           address;
  private List<Phone>       phoneNumbers;
  private String            email;

  public Client()
  {
  }
  
  public Client(Client c)
  {
    copyPObjectInfo(c);
    prefix = c.getPrefix();
    firstName = c.getFirstName();
    middleName = c.getMiddleName();
    lastName = c.getLastName();
    address = c.getAddress();
    phoneNumbers = c.getPhoneNumbers();
    email = c.getEmail();
  }

  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Client{");
    //id
    sb.append("id:");
    sb.append(getId());
    sb.append(',');
    //firstName
    sb.append("firstName:");
    sb.append(firstName);
    sb.append(',');
    //middleName
    sb.append("middleName:");
    sb.append(middleName);
    sb.append(',');
    //lastName
    sb.append("lastName:");
    sb.append(lastName);
    sb.append('}');
    return sb.toString();
  }

  /**
   * Gets the value of the prefix property.
   * 
   * @return possible object is {@link PrefixType }
   * 
   */
  public PrefixType getPrefix()
  {
    return prefix;
  }

  /**
   * Sets the value of the prefix property.
   * 
   * @param value
   *          allowed object is {@link PrefixType }
   * 
   */
  public void setPrefix(PrefixType value)
  {
    this.prefix = value;
  }

  /**
   * Gets the value of the firstName property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getFirstName()
  {
    return firstName;
  }

  /**
   * Sets the value of the firstName property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setFirstName(String value)
  {
    this.firstName = value;
  }

  /**
   * Gets the value of the middleName property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getMiddleName()
  {
    return middleName;
  }

  /**
   * Sets the value of the middleName property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setMiddleName(String value)
  {
    this.middleName = value;
  }

  /**
   * Gets the value of the lastName property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLastName()
  {
    return lastName;
  }

  /**
   * Sets the value of the lastName property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setLastName(String value)
  {
    this.lastName = value;
  }

  /**
   * Gets the value of the address property.
   * 
   * @return possible object is {@link Address }
   * 
   */
  public Address getAddress()
  {
    return address;
  }

  /**
   * Sets the value of the address property.
   * 
   * @param value
   *          allowed object is {@link Address }
   * 
   */
  public void setAddress(Address value)
  {
    this.address = value;
  }

  /**
   * Gets the value of the phoneNumbers property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot.
   * Therefore any modification you make to the returned list will be present
   * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
   * for the phoneNumbers property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getPhoneNumbers().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Phone }
   * 
   * 
   */
  public List<Phone> getPhoneNumbers()
  {
    if (phoneNumbers == null)
    {
      phoneNumbers = new ArrayList<Phone>();
    }
    return this.phoneNumbers;
  }

  /**
   * @param phoneNumbers
   *          the phoneNumbers to set
   */
  public void setPhoneNumbers(List<Phone> phoneNumbers)
  {
    this.phoneNumbers = phoneNumbers;
  }

  /**
   * Gets the value of the email property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getEmail()
  {
    return email;
  }

  /**
   * Sets the value of the email property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setEmail(String value)
  {
    this.email = value;
  }
}
