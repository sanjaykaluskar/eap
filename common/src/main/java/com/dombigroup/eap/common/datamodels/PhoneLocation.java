package com.dombigroup.eap.common.datamodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "phoneLocation")
@XmlEnum
public enum PhoneLocation
{
  HOME("HOME"),
  WORK("WORK");
  private final String value;

  PhoneLocation(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public static PhoneLocation fromValue(String v)
  {
    if (v == null)
      return null;

    for (PhoneLocation c : PhoneLocation.values())
    {
      if (c.value.equals(v))
      {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }
}
