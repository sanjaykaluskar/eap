package com.dombigroup.eap.common.datamodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "address")
public class Address extends Persisted
{
  private String            line1;
  private String            line2;
  private String            line3;
  private String            city;
  private String            state;
  private String            country;
  private long              zipcode;

  public Address()
  {
  }
  
  public Address(int id, Address a)
  {
    copyPObjectInfo(a);
    setId(id);
    line1 = a.getLine1();
    line2 = a.getLine2();
    line3 = a.getLine3();
    city = a.getCity();
    state = a.getState();
    country = a.getCountry();
    zipcode = a.getZipcode();
  }

  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Address{");
    //id
    sb.append("id:");
    sb.append(getId());
    sb.append(',');
    //line1
    sb.append("line1:");
    sb.append(line1);
    sb.append(',');
    //city
    sb.append("city:");
    sb.append(city);
    sb.append(',');
    //state
    sb.append("state:");
    sb.append(state);
    sb.append('}');
    return sb.toString();    
  }

  /**
   * Gets the value of the country property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getCountry()
  {
    return country;
  }

  /**
   * Sets the value of the country property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setCountry(String value)
  {
    this.country = value;
  }

  /**
   * Gets the value of the state property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getState()
  {
    return state;
  }

  /**
   * Sets the value of the state property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setState(String value)
  {
    this.state = value;
  }

  /**
   * Gets the value of the city property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getCity()
  {
    return city;
  }

  /**
   * Sets the value of the city property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setCity(String value)
  {
    this.city = value;
  }

  /**
   * Gets the value of the line1 property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLine1()
  {
    return line1;
  }

  /**
   * Sets the value of the line1 property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setLine1(String value)
  {
    this.line1 = value;
  }

  /**
   * Gets the value of the line2 property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLine2()
  {
    return line2;
  }

  /**
   * Sets the value of the line2 property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setLine2(String value)
  {
    this.line2 = value;
  }

  /**
   * Gets the value of the line3 property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLine3()
  {
    return line3;
  }

  /**
   * Sets the value of the line3 property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setLine3(String value)
  {
    this.line3 = value;
  }

  /**
   * Gets the value of the zipcode property.
   * 
   */
  public long getZipcode()
  {
    return zipcode;
  }

  /**
   * Sets the value of the zipcode property.
   * 
   */
  public void setZipcode(long value)
  {
    this.zipcode = value;
  }
}
