/**
 * DgException.java
 */
package com.dombigroup.eap.common.exceptions;

/**
 * @author sanjay
 * 
 */
public class DgRuntimeException extends RuntimeException
{
  /** DgException.java */
  private static final long serialVersionUID = 1L;
  private DgMessage         message;

  public DgMessage getMessageEnum()
  {
    return message;
  }

  public DgRuntimeException(DgMessage m)
  {
    super(m.getMessage());
    message = m;
  }

  public DgRuntimeException(DgMessage m, String... args)
  {
    super(String.format(m.getMessage(), (Object[]) args));
    message = m;
  }

  public DgRuntimeException(Throwable e, DgMessage m, String... args)
  {
    super(String.format(m.getMessage(), (Object[]) args), e);
    message = m;
  }
}
