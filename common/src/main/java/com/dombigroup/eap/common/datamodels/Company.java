package com.dombigroup.eap.common.datamodels;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "company")
public class Company extends Persisted
{
  private String          name;
  private Address         address;
  private List<Phone>     phoneNumbers;

  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Company{");
    //id
    sb.append("id:");
    sb.append(getId());
    sb.append(',');
    //name
    sb.append("name:");
    sb.append(name);
    sb.append('}');
    return sb.toString();
  }

  /**
   * Gets the value of the name property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getName()
  {
    return name;
  }

  /**
   * Sets the value of the name property.
   * 
   * @param value
   *          allowed object is {@link String }
   * 
   */
  public void setName(String value)
  {
    this.name = value;
  }

  /**
   * Gets the value of the address property.
   * 
   * @return possible object is {@link Address }
   * 
   */
  public Address getAddress()
  {
    return address;
  }

  /**
   * Sets the value of the address property.
   * 
   * @param value
   *          allowed object is {@link Address }
   * 
   */
  public void setAddress(Address value)
  {
    this.address = value;
  }

  /**
   * Gets the value of the phoneNumbers property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot.
   * Therefore any modification you make to the returned list will be present
   * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
   * for the phoneNumbers property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getPhoneNumbers().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Phone }
   * 
   * 
   */
  public List<Phone> getPhoneNumbers()
  {
    if (phoneNumbers == null)
    {
      phoneNumbers = new ArrayList<Phone>();
    }
    return this.phoneNumbers;
  }

  /**
   * @param phoneNumbers the phoneNumbers to set
   */
  public void setPhoneNumbers(List<Phone> phoneNumbers)
  {
    this.phoneNumbers = phoneNumbers;
  }
}
