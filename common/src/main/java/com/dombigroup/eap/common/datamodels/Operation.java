/**
 * Operation.java
 */
package com.dombigroup.eap.common.datamodels;

/**
 * @author sanjay
 *
 */
public interface Operation
{
  public String getName();
}
