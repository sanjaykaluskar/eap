/**
 * Aggregate.java
 */
package com.dombigroup.eap.common.datamodels;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sanjay
 * 
 */
public class Aggregate extends Persisted
{
  List<Persisted> objList;

  /**
   * @return the objList
   */
  public List<Persisted> getObjList()
  {
    return objList;
  }

  /**
   * @param objList
   *          the objList to set
   */
  public void setObjList(List<Persisted> objList)
  {
    this.objList = objList;
  }

  public void add(Persisted o)
  {
    if (objList == null) objList = new ArrayList<Persisted>();
    objList.add(o);
  }

  public void remove(Persisted o)
  {
    if (objList != null) objList.remove(o);
  }
}
