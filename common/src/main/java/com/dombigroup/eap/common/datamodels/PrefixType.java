package com.dombigroup.eap.common.datamodels;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "prefixType")
@XmlEnum
public enum PrefixType
{
  MR("MR"),
  MRS("MRS"),
  MS("MS"),
  DR("DR");
  private final String value;

  PrefixType(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public static PrefixType fromValue(String v)
  {
    if (v == null)
      return null;

    for (PrefixType c : PrefixType.values())
    {
      if (c.value.equals(v))
      {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }
}
