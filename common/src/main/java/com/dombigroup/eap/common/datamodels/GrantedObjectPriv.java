/**
 * GrantedObjectPriv.java
 */
package com.dombigroup.eap.common.datamodels;

/**
 * @author sanjay
 * 
 */
public class GrantedObjectPriv extends Grant
{
  private int    objectId;
  private String objectType;
  private String operation;

  /**
   * @return the objectId
   */
  public int getObjectId()
  {
    return objectId;
  }

  /**
   * @param objectId
   *          the objectId to set
   */
  public void setObjectId(int objectId)
  {
    this.objectId = objectId;
  }

  /**
   * @return the objectType
   */
  public String getObjectType()
  {
    return objectType;
  }

  /**
   * @param objectType
   *          the objectType to set
   */
  public void setObjectType(String objectType)
  {
    this.objectType = objectType;
  }

  /**
   * @return the operation
   */
  public String getOperation()
  {
    return operation;
  }

  /**
   * @param operation
   *          the operation to set
   */
  public void setOperation(String operation)
  {
    this.operation = operation;
  }
}
