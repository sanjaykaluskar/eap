/**
 * DgMessage.java
 */
package com.dombigroup.eap.common.exceptions;

/**
 * @author sanjay
 *
 */
public interface DgMessage
{
  public String getMessage();
}
