/**
 * DgException.java
 */
package com.dombigroup.eap.common.exceptions;

/**
 * @author sanjay
 * 
 */
public class DgException extends Exception
{
  /** DgException.java */
  private static final long serialVersionUID = 1L;
  private DgMessage         message;

  public DgMessage getMessageEnum()
  {
    return message;
  }

  public DgException(DgMessage m)
  {
    super(m.getMessage());
    message = m;
  }

  public DgException(DgMessage m, String... args)
  {
    super(String.format(m.getMessage(), (Object[]) args));
    message = m;
  }

  public DgException(Throwable e, DgMessage m, String... args)
  {
    super(String.format(m.getMessage(), (Object[]) args), e);
    message = m;
  }
}
