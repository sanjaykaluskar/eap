package com.dombigroup.eap.common.utils;

/**
 * ExpressionUtils.java
 */

/**
 * @author sanjay
 *
 */
public class ExpressionUtils
{
  /**
   * nullOrEqual - a safe way to check for equality of objects
   */
  public static boolean safeEqual(Object a, Object b)
  {
    return (a == null) ? (b == null) : a.equals(b);
  }

  public static String safeString(Enum<?> e)
  {
    return (e == null) ? null : e.toString();
  }
}
