package com.dombigroup.eap.common.datamodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "phoneType")
@XmlEnum
public enum PhoneType
{
  LANDLINE("LANDLINE"),
  MOBILE("MOBILE");
  private final String value;

  PhoneType(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public static PhoneType fromValue(String v)
  {
    if (v == null)
      return null;

    for (PhoneType c : PhoneType.values())
    {
      if (c.value.equals(v))
      {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }
}
