/**
 * ResourceType.java
 */
package com.dombigroup.eap.common.datamodels;

/**
 * @author sanjay
 *
 */
public interface ResourceType
{
  public String getName();
}
