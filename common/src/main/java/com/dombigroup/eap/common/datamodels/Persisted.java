/**
 * Persisted.java
 */
package com.dombigroup.eap.common.datamodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author sanjay
 * 
 */
@XmlType(name = "persisted")
@XmlAccessorType(XmlAccessType.FIELD)
public class Persisted
{
  public static final int INVALID_ID = -1;

  @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "pObjectInfo")
  private static class PObjectInfo
  {
    private int     id;
    private int     orgId;
    private int     creatorId;
    private DayTime creationTime;
    private DayTime updateTime;
    private int     oprivListId;
    private int     version;

    /**
     * @return the id
     */
    private int getId()
    {
      return id;
    }

    /**
     * @param id
     *          the id to set
     */
    private void setId(int id)
    {
      this.id = id;
    }

    /**
     * @return the orgId
     */
    private int getOrgId()
    {
      return orgId;
    }

    /**
     * @param orgId
     *          the orgId to set
     */
    private void setOrgId(int orgId)
    {
      this.orgId = orgId;
    }

    /**
     * @return the creatorId
     */
    private int getCreatorId()
    {
      return creatorId;
    }

    /**
     * @param creatorId
     *          the creatorId to set
     */
    private void setCreatorId(int creatorId)
    {
      this.creatorId = creatorId;
    }

    /**
     * @return the creationTime
     */
    private DayTime getCreationTime()
    {
      return creationTime;
    }

    /**
     * @param creationTime
     *          the creationTime to set
     */
    private void setCreationTime(DayTime creationTime)
    {
      this.creationTime = creationTime;
    }

    /**
     * @return the updateTime
     */
    private DayTime getUpdateTime()
    {
      return updateTime;
    }

    /**
     * @param updateTime
     *          the updateTime to set
     */
    private void setUpdateTime(DayTime updateTime)
    {
      this.updateTime = updateTime;
    }

    /**
     * @return the oprivListId
     */
    private int getOprivListId()
    {
      return oprivListId;
    }

    /**
     * @param oprivListId
     *          the oprivListId to set
     */
    private void setOprivListId(int oprivListId)
    {
      this.oprivListId = oprivListId;
    }

    /**
     * @return the version
     */
    private int getVersion()
    {
      return version;
    }

    /**
     * @param version
     *          the version to set
     */
    private void setVersion(int scn)
    {
      this.version = scn;
    }
  };

  private PObjectInfo pObjInfo;

  public Persisted()
  {
    pObjInfo = new PObjectInfo();
  }

  public Persisted(Persisted from)
  {
    pObjInfo = new PObjectInfo();
    copyPObjectInfo(from);
  }

  public void copyPObjectInfo(Persisted from)
  {
    setId(from.getId());
    setOrgId(from.getOrgId());
    setCreatorId(from.getCreatorId());
    setCreationTime(from.getCreationTime());
    setUpdateTime(from.getUpdateTime());
    setOprivListId(from.getOprivListId());
    setVersion(from.getVersion());
  }

  /**
   * equalPObject whether the object is the same snapshot
   */
  public boolean equalPObject(Persisted o)
  {
    return (o != null)
        && (getId() == o.getId())
        && (getVersion() == o.getVersion());
  }

  /**
   * @return the id
   */
  public int getId()
  {
    return pObjInfo.getId();
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(int id)
  {
    pObjInfo.setId(id);
  }

  /**
   * @return the orgId
   */
  public int getOrgId()
  {
    return pObjInfo.getOrgId();
  }

  /**
   * @param orgId
   *          the orgId to set
   */
  public void setOrgId(int orgId)
  {
    pObjInfo.setOrgId(orgId);
  }

  /**
   * @return the creatorId
   */
  public int getCreatorId()
  {
    return pObjInfo.getCreatorId();
  }

  /**
   * @param creatorId
   *          the creatorId to set
   */
  public void setCreatorId(int creatorId)
  {
    pObjInfo.setCreatorId(creatorId);
  }

  /**
   * @return the creationTime
   */
  public DayTime getCreationTime()
  {
    return pObjInfo.getCreationTime();
  }

  /**
   * @param creationTime
   *          the creationTime to set
   */
  public void setCreationTime(DayTime creationTime)
  {
    pObjInfo.setCreationTime(creationTime);
  }

  /**
   * @return the updateTime
   */
  public DayTime getUpdateTime()
  {
    return pObjInfo.getUpdateTime();
  }

  /**
   * @param updateTime
   *          the updateTime to set
   */
  public void setUpdateTime(DayTime updateTime)
  {
    pObjInfo.setUpdateTime(updateTime);
  }

  /**
   * @return the pObjInfo
   */
  protected PObjectInfo getpObjInfo()
  {
    return pObjInfo;
  }

  /**
   * @param pObjInfo
   *          the pObjInfo to set
   */
  protected void setpObjInfo(PObjectInfo pObjInfo)
  {
    this.pObjInfo = pObjInfo;
  }

  /**
   * getOprivListId
   */
  public int getOprivListId()
  {
    return pObjInfo.getOprivListId();
  }

  /**
   * setOprivListId
   */
  public void setOprivListId(int id)
  {
    pObjInfo.setOprivListId(id);
  }

  /**
   * @return the version
   */
  public int getVersion()
  {
    return pObjInfo.getVersion();
  }

  /**
   * @param version
   *          the version to set
   */
  public void setVersion(int scn)
  {
    pObjInfo.setVersion(scn);
  }

}
