/**
 * OPrivilegeType.java
 */
package com.dombigroup.eap.common.datamodels;


/**
 * @author sanjay
 * 
 */
public enum OperationType implements Operation
{
  CREATE("CREATE"),
  READ("READ"),
  UPDATE("UPDATE"),
  DELETE("DELETE");

  private final String value;

  private OperationType(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public String getName()
  {
    return value;
  }

  public static OperationType fromValue(String v)
  {
    if (v == null)
      return null;

    for (OperationType priv : OperationType.values())
    {
      if (priv.value.equals(v))
      {
        return priv;
      }
    }
    throw new IllegalArgumentException(v);
  }
}
