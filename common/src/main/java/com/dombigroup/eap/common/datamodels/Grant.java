/**
 * Grant.java
 */
package com.dombigroup.eap.common.datamodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author sanjay
 * 
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class Grant extends Persisted
{
  private String  grantorName;
  private int     grantorId;
  private String  granteeName;
  private int     granteeId;
  private DayTime grantDate;

  /**
   * @return the grantorName
   */
  public String getGrantorName()
  {
    return grantorName;
  }

  /**
   * @param grantorName
   *          the grantorName to set
   */
  public void setGrantorName(String grantorName)
  {
    this.grantorName = grantorName;
  }

  /**
   * @return the grantorId
   */
  public int getGrantorId()
  {
    return grantorId;
  }

  /**
   * @param grantorId
   *          the grantorId to set
   */
  public void setGrantorId(int grantorId)
  {
    this.grantorId = grantorId;
  }

  /**
   * @return the granteeName
   */
  public String getGranteeName()
  {
    return granteeName;
  }

  /**
   * @param granteeName
   *          the granteeName to set
   */
  public void setGranteeName(String granteeName)
  {
    this.granteeName = granteeName;
  }

  /**
   * @return the granteeId
   */
  public int getGranteeId()
  {
    return granteeId;
  }

  /**
   * @param granteeId
   *          the granteeId to set
   */
  public void setGranteeId(int granteeId)
  {
    this.granteeId = granteeId;
  }

  /**
   * @return the grantDate
   */
  public DayTime getGrantDate()
  {
    return grantDate;
  }

  /**
   * @param grantDate the grantDate to set
   */
  public void setGrantDate(DayTime grantDate)
  {
    this.grantDate = grantDate;
  }
}
