package com.dombigroup.eap.common.datamodels;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "phone")
public class Phone extends Persisted
{
  private PhoneType         numberType;
  private PhoneLocation     location;
  private int               countryCode;
  private int               areaCode;
  private long              number;
  private int               extension;

  /**
   * Gets the value of the numberType property.
   * 
   * @return possible object is {@link PhoneType }
   * 
   */
  public PhoneType getNumberType()
  {
    return numberType;
  }

  /**
   * Sets the value of the numberType property.
   * 
   * @param value
   *          allowed object is {@link PhoneType }
   * 
   */
  public void setNumberType(PhoneType value)
  {
    this.numberType = value;
  }

  /**
   * Gets the value of the location property.
   * 
   * @return possible object is {@link PhoneLocation }
   * 
   */
  public PhoneLocation getLocation()
  {
    return location;
  }

  /**
   * Sets the value of the location property.
   * 
   * @param value
   *          allowed object is {@link PhoneLocation }
   * 
   */
  public void setLocation(PhoneLocation value)
  {
    this.location = value;
  }

  /**
   * Gets the value of the countryCode property.
   * 
   */
  public int getCountryCode()
  {
    return countryCode;
  }

  /**
   * Sets the value of the countryCode property.
   * 
   */
  public void setCountryCode(int value)
  {
    this.countryCode = value;
  }

  /**
   * Gets the value of the areaCode property.
   * 
   */
  public int getAreaCode()
  {
    return areaCode;
  }

  /**
   * Sets the value of the areaCode property.
   * 
   */
  public void setAreaCode(int value)
  {
    this.areaCode = value;
  }

  /**
   * Gets the value of the number property.
   * 
   * @return possible object is {@link BigInteger }
   * 
   */
  public long getNumber()
  {
    return number;
  }

  /**
   * Sets the value of the number property.
   * 
   * @param value
   *          allowed object is {@link BigInteger }
   * 
   */
  public void setNumber(long value)
  {
    this.number = value;
  }

  /**
   * Gets the value of the extension property.
   * 
   */
  public int getExtension()
  {
    return extension;
  }

  /**
   * Sets the value of the extension property.
   * 
   */
  public void setExtension(int value)
  {
    this.extension = value;
  }
}
