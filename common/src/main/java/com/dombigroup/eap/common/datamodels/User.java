/**
 * BUser.java
 */
package com.dombigroup.eap.common.datamodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sanjay
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="user")
public class User extends Client
{
  /** User.java */
  private String userName;
  private String password;
  private int    roleListId;

  public User()
  {
  }

  public User(User u)
  {
    super(u);
    copyPObjectInfo(u);
    userName = u.getUserName();
    password = u.getPassword();
    roleListId = u.getRoleListId();
  }

  /**
   * @return the userName
   */
  public String getUserName()
  {
    return userName;
  }
  /**
   * @param userName the userName to set
   */
  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  /**
   * @return the password
   */
  public String getPassword()
  {
    return password;
  }
  /**
   * @param password the password to set
   */
  public void setPassword(String password)
  {
    this.password = password;
  }
  /**
   * @return the userid
   */
  public int getUserid()
  {
    return getId();
  }
  /**
   * @param userid the userid to set
   */
  public void setUserid(int userid)
  {
    setId(userid);
  }
  
  /**
   * @return the roleListId
   */
  public int getRoleListId()
  {
    return roleListId;
  }

  /**
   * @param roleListId the roleListId to set
   */
  public void setRoleListId(int roleListId)
  {
    this.roleListId = roleListId;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("User{");
    //id
    sb.append("userid:");
    sb.append(getId());
    sb.append(',');
    //userName
    sb.append("userName:");
    sb.append(userName);
    sb.append('}');
 
    return sb.toString();
  }
}
