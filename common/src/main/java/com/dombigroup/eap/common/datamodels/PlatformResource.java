package com.dombigroup.eap.common.datamodels;


/**
 * @author sanjay
 * 
 */
public enum PlatformResource
    implements
    ResourceType
{
  SERVER("SERVER"),
  ADDRESS("ADDRESS"),
  PHONE("PHONE"),
  AGGREGATE("AGGREGATE"),
  COMPANY("COMPANY"),
  CLIENT("CLIENT"),
  USER("USER"),
  GRANTEDROLE("GRANTEDROLE");

  private final String value;

  private PlatformResource(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public static PlatformResource fromValue(String v)
  {
    if (v == null)
      return null;

    for (PlatformResource priv : PlatformResource.values())
    {
      if (priv.value.equals(v))
      {
        return priv;
      }
    }
    throw new IllegalArgumentException(v);
  }

  @Override
  public String getName()
  {
    return value;
  }
}
