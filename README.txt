Building EAP
------------
Prerequisites:
1. Install Oracle JDBC driver
   - download ojdbc6.jar from www.oracle.com
   - install it into your local repository
     mvn install:install-file -Dfile=ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.XE -Dpackaging=jar

Build:
1. Go to root directory (ssap)
   mvn clean install

License
-------
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
